#-----Macros---------------------------------
DEBUG ?= 1
ASAN ?= $(DEBUG)

SANITIZE ?=

TLIB = 

ifeq ($(ASAN), 1)
SANITIZE=-fsanitize=address -fno-omit-frame-pointer -O0 -g
#SANITIZE=-fsanitize=leak -fno-omit-frame-pointer -O0
#SANITIZE=-fsanitize=memory -fno-omit-frame-pointer -O0
endif

WARNINGS=-pedantic -Wall -Wno-gnu -Wno-variadic-macros
#WARNINGS=

# set up compiler and options
ifeq ($(DEBUG), 1)
CFLAGS = -O0 $(INCLUDES) -Wall -Winline -std=c11 $(SANITIZE) $(WARNINGS) -DDEBUG
#CFLAGS = -O0 -g $(INCLUDES)
else
CFLAGS = -O3 $(INCLUDES) -std=c11
endif

# GCC kludge
CFLAGS += -D_GNU_SOURCE
CXXFLAGS = -O0 $(INCLUDES) -Wall -Winline -std=c++11 -stdlib=libstdc++\
    $(SANITIZE) $(WANRINGS)

LDFLAGS = -g $(SANITIZE)
#LDFLAGS = -g-full

# XCode toolchain.
#CC              := $(shell xcodebuild -find clang)
#CXX             := $(shell xcodebuild -find clang++)

# Toolchain in search path.
CC              := clang
CXX             := clang++
#CC              := gcc
#CXX             := g++

# Brew llvm/toolchain.
#CC              := /usr/local/opt/llvm/bin/clang 
#CFLAGS          += -I/usr/local/opt/llvm/include
#LDFLAGS         += -L/usr/local/opt/llvm/lib
#CXX             := /usr/local/opt/llvm/bin/clang++ 
#CXXFLAGS        += -I/usr/local/opt/llvm/include


LD              := $(CC) $(LDFLAGS)
LDXX            := $(CXX) $(LDFLAGS) $(SANITIZE) --stdlib=libstdc++

MODULES := . mm lib
BUILD_ROOT := build
SRC_ROOT := src
APP_DIR := .
SRC_DIR   := $(addprefix $(SRC_ROOT)/,$(MODULES))
APP_MODULES :=
TEST_MODULES := test
TESTS := table tokenizer parser eval alloc 
TEST_SRC_DIR := $(addprefix $(SRC_ROOT)/,$(TEST_MODULES))
TEST_TARGETS := $(addsuffix _test,$(TESTS))
TEST_TARGETS_RUN := $(addsuffix _test_run,$(TESTS))

BUILD_DIR := $(addprefix $(BUILD_ROOT)/,$(MODULES) $(TEST_MODULES) $(APP_MODULES))

BASE_INCLUDES = -I./$(SRC_ROOT)
EXTRA_INCLUDES = -I./contrib/linenoise
EXTRA_SRCS = ../contrib/linenoise/linenoise.c

SRC	     := $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.c)) $(EXTRA_SRCS)
GEN_SRC  := $(subst $(SRC_ROOT)/$(APP_DIR)/repl_main.c,,$(SRC))
OBJ	     := $(patsubst $(SRC_ROOT)/%.c,$(BUILD_ROOT)/%.o,$(GEN_SRC))
INCLUDES := $(addprefix -I,$(SRC_DIR)) $(BASE_INCLUDES) $(EXTRA_INCLUDES)

APP_SRC_DIR := $(addprefix $(SRC_ROOT)/,$(APP_MODULES))
APP_SRC	    := $(foreach sdir,$(APP_SRC_DIR),$(wildcard $(sdir)/*.c))
APP_OBJ	    := $(patsubst $(SRC_ROOT)/%.c,$(BUILD_ROOT)/%.o,$(APP_SRC))

vpath %.c $(SRC_DIR)
vpath %.cpp $(TEST_SRC_DIR)

define make-goal
$(BUILD_ROOT)/$1/%.o: $(SRC_ROOT)/$1/%.c
	@echo $$< $$@
	$(CC) $(CFLAGS) $(INCLUDES) -c $$< -o $$@

$(BUILD_ROOT)/$1/%.o: $(SRC_ROOT)/$1/%.cpp
	@echo $$< $$@
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c $$< -o $$@
endef

define make-test
$1_test: $(BUILD_ROOT)/test/$1_test
	echo Created $1_test

$1_test_run: $1_test
	echo ./$(BUILD_ROOT)/test/$1_test >> ./$(BUILD_ROOT)/test/test_stages.txt

$(BUILD_ROOT)/test/$1_test: $(BUILD_ROOT)/test/$1_test.o $(OBJ)
	$(LDXX) $$^ -o $$@
endef

.PHONY: all checkdirs clean alltests

all: checkdirs proton

checkdirs: $(BUILD_DIR)

check:
	@echo gen_src: $(GEN_SRC)
	@echo src: $(SRC)
	@echo obj: $(OBJ)
	@echo vpath: $(VPATH)
	@echo src_dir: $(SRC_DIR)
	@echo main: $(SRC_ROOT)/repl_main.c
	@echo san: $(SANITIZE)
	@echo debug: $(DEBUG)
	@echo cflags: $(CFLAGS)
	@echo ldflags: $(LDFLAGS)

$(BUILD_DIR):
	@mkdir -p $@
	@echo $@

CLEAN_DIRS := $(foreach dir,$(BUILD_DIR),$(subst .,,$(basename $(dir)/)))
clean:
	@find . -name "*.o" -exec rm {} \;
	@echo $(BUILD_DIR)
	@rm -rf $(CLEAN_DIRS)

$(foreach bdir,$(MODULES) $(APP_MODULES) $(TEST_MODULES) ,$(eval $(call make-goal,$(bdir))))

$(BUILD_ROOT)/rtl.proton: $(SRC_ROOT)/lib/rtl.proton
	@echo "Copying Runtime Library (RTL) / Standard Libary"
	cp $^ $@


$(BUILD_ROOT)/proton: $(OBJ) $(SRC_ROOT)/repl_main.o
	@echo Linking...
	@echo src $^ target $@
	$(LD) $(LDFLAGS) $^ -o $@
	@echo Done.

proton: checkdirs $(BUILD_ROOT)/proton  $(BUILD_ROOT)/rtl.proton

# Tests.

tests: checkdirs $(TEST_TARGETS)

cleantests:
	@-rm ./$(BUILD_ROOT)/test/test_stages.txt
	@-rm ./$(BUILD_ROOT)/test/test_runner.sh

runalltests:
	@cp $(SRC_ROOT)/test_runner.sh $(BUILD_ROOT)/test/test_runner.sh
	cat $(BUILD_ROOT)/test/test_stages.txt | bash $(BUILD_ROOT)/test/test_runner.sh

runtests: cleantests checkdirs $(TEST_TARGETS_RUN) runalltests

$(BUILD_ROOT)/test/%.o: %.c
	@echo $< $@
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

$(foreach test,$(TESTS),$(eval $(call make-test,$(test))))
