# Lisp-style condition system.
# Example translated from:
#   https://www.tutorialspoint.com/lisp/lisp_error_handling.htm

defn handle-infinity() {
  recovery just-continue () { nil } "Ignore error and continue" {
    let result = division-function(10, 0)
    print "Value: {result}"
  }
}

defn division-function(a, b) {
  recoveries {
    {
      if (b != 0) {
        a / b
      } else {
        error(DivisionByZeroError, "denominator is zero")
      }
    }
    return-zero: ("Return 0", fn () 0)
    return-value: ("Return desired value", fn (val) val)
    recalc-using: ("Recalculate using desired denominator",
                      fn (denom) division-function(a, denom))
  }
}

defn high-level-code() {
  handle-errors {
    DivisionByZeroError: fn (err, data) { print data; invoke_recovery(return-zero) }

    handle-infinity()
  }
}

handle-errors {
  DivisionByZeroError: fn (err, data) { print data; invoke_recovery(return-value, 1) }

  handle-infinity()
}

handle-errors {
  DivisionByZeroError: fn (err, data) { print data; invoke_recovery(recalc-using, 2) }

  handle-infinity()
}

handle-error DivisionByZeroError 
    fn (err, data) { print data; invoke_recovery(just-continue) } 
{
  handle-infinity()
}

