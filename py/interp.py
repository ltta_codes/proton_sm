from parser import *
from env import *
from eval import *


if __name__ == '__main__':
  import sys
  e = env_new()
  with open(sys.argv[1], 'r') as fn:
    source = preparse(e, fn.read())
  attach_env(e, source)
  source = evall(e, expr_parse(e, source), evl_parse)

  print "Parsed:", source
  for eval_lvl in [evl_syntax, evl_defn, evl_macro]:
    e.runtime.eval_lvl = eval_lvl
    source = evall(e, expr_parse(e, source), eval_lvl)
    print ">>>> src/{}: <<<<".format(eval_lvl), source

  print ">>>> src/{}: <<<<".format(evl_run), source
  source = evall(e, source, evl_run)

  print ">>>", source
