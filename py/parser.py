from collections import deque

from env import *
from syms import *
from stypes import *

min_prec = -1000
default_token_prec = 0

def flatten(ls):
  out = List()
  for l in ls:
    if isinstance(l, list):
      if l:
        if out: out.append(semicolon_)
        out.extend(l)
    else:
      out.append(l)
  return out

def space_delims(s, delims):
  for delim in delims:
    s = s.replace(delim, ' %s ' % (delim))
  return s

def atomize(l):
  def tok():
    for le in l:
      yield le

  tokenize = tok()

  def atoms():
    while 1:
      token = tokenize.next()
      try:
        yield int(token)
      except ValueError:
        try:
          yield float(token)
        except ValueError:
          if token.startswith('"'):
            s = ''
            token = token[1:]
            while not token.endswith('"'):
              s = s + ' ' + token if len(s) > 0 else token
              try:
                token = tokenize.next()
              except StopIteration:
                raise SyntaxError("Unterminated String")
            t = token.replace('"', '')
            yield s + ' ' + t if len(s) > 0 else t
          else:
            yield sym(token)

  return List(*[a for a in atoms()], meta=t_appl)

def tokenize(lines):
  toks = [space_delims(line, ['(', ')', '{', '}', '[', ']', ':', ',', ';']).split() for line in lines]
  return atomize(flatten(toks))

def tokf(fn):
  with open(fn, 'r') as f:
    lines = f.readlines()
    return tokenize(lines)

def toks(src):
  return tokenize(src.splitlines())

def parse(s):
    return toks(s)

def parse_fn(fn):
  with open(fn, 'r') as f:
    return parse(e, f.read())

if __name__ == '__main__':
  e = Env(Runtime(), {
        'let': Token(0, 3, False, 0, 0),
        'fn':  Token(0, 2, False, 0, 0),
        '+':   Token(1, 1, False, 20, 0),
        '*':   Token(1, 1, False, 30, 0),
      }, {})

  print parse(e, "let id = fn(x) x; id(10)")
  print parse(e, "let x = 1 + 2 + 3")
  print parse(e, "let z = 1 + 2 * 5")
  print parse(e, '[ 0: "a", 1: "b", 2: "c", 3: "d", 4: "e" ]')

