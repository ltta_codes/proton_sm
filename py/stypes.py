from env import *

_symtab = {}

def sym(val):
  if val in _symtab: return _symtab[val]
  res = Sym(val)
  _symtab[val] = res
  return res

class Sym(str):
  def __repr__(self): return "$" + self


t_str, t_sym, t_appl, t_list = sym("t_str"), sym("t_sym"), sym("t_appl"), sym("t_list")
t_num, t_tuple = sym("t_num"), sym("t_tuple")
t_fn, t_closure, t_code, t_any = sym("t_fn"), sym("t_closure"), sym("t_code"), sym("t_any")
t_nil = sym("t_nil")

isa = isinstance

def typeof(val):
  import numbers
  if isa(val, numbers.Number):
    return t_num
  if isa(val, Sym):
    return t_sym
  elif isa(val, List):
    return val.meta if val.meta else t_list
  elif isa(val, str):
    return t_str
  elif isa(val, Fn):
    return t_fn
  elif isa(val, Closure):
    return t_closure
  elif val is None:
    return t_nil
  else:
    error("InternalError", "Unhandled type {}/{}".format(str(val), str(type(val))))

def appl(*args):
  return List(*args, meta=t_appl)

def tup(*args):
  return List(*args, meta=t_tuple)

def has_assoc(v):
  if isa(v, ValueAssoc):
    return v.has_assoc()
  return False

def assoc(v, val):
  if isa(v, ValueAssoc):
    v.assoc = val
  return v

class Value(object):
  def has_assoc(self): return False


class ValueAssoc(Value):
  def __init__(self, **kwargs):
    self.assoc = kwargs['assoc'] if 'assoc' in kwargs else None

  def has_assoc(self): return self.assoc is not None


class List(list, ValueAssoc):
  def __init__(self, *args, **kwargs):
    if len(args) > 0:
      list.__init__(self, args)
    self.meta = kwargs['meta'] if 'meta' in kwargs else None
    ValueAssoc.__init__(self, **kwargs)

  def __repr__(self):
    if self.meta is t_appl:
      return "<" + " ".join([repr(a) for a in self]) + ">"
    elif self.meta is t_tuple:
      return "(" + ", ".join([repr(a) for a in self]) + ")"
    elif self.meta is t_code:
      return "{" + "; ".join([repr(a) for a in self]) + "}"
    elif self.meta is None:
      return super(List, self).__repr__()
    return "<" + (repr(self.meta) if self.meta else "list") + " " + super(List, self).__repr__() + ">"

  def getmeta(self):
    return self.meta

  def setmeta(self, meta):
    self.meta = meta
    return self



class Fn(ValueAssoc):
  def __init__(self, *methods, **kwargs):
    self.methods = {}
    self.larg = None
    self.nargs = None
    self.tup_args = None
    self.var_args = None
    self.prec = None
    self.eval_lvl = None
    self.eval_args = True
    self.name = None
    self.ctx = {}
    for method in methods:
      self.add_method(method)
    ValueAssoc.__init__(self, **kwargs)

  def dispatch(self, arg_types):
    if tuple(arg_types) in self.methods:
      return self.methods[tuple(arg_types)]
    return self.methods[None]

  def __call__(self, env, args):
    arg_types = [typeof(arg) for arg in args]
    method = self.dispatch(arg_types)
    return method(env, args)

  def __repr__(self):
    return "<fn {} {}>".format(self.name or "", repr(self.methods))

  def set(self, prop, val):
    setattr(self, prop, val)
    for m in self.methods.iteritems():
      setattr(m[1], prop, val)
    return self

  def context(self, ctx):
    for name, fn in ctx.iteritems():
      assert not name in self.ctx
      self.ctx[name] = fn
    return self

  def add_method(self, method):
    if sum(1 for t in method.arg_types if t == t_any) == len(method.arg_types):
      arg_t = None
    else:
      arg_t = tuple(method.arg_types)
    if arg_t in self.methods:
      error("MultiMethodError",
          "Multi-method with type sig '{}' redefined.".format(str(arg_t)))
    if method.var_args and len(self.methods) > 0:
      error("MultiMethodError",
          "Variable argument count not supported for multi-methods.")

    larg = method.larg
    nargs = method.nargs
    tupargs = method.tup_args
    vargs = method.var_args
    lvl = method.eval_lvl
    eval_args = method.eval_args
    prec = method.prec

    if self.larg and \
        (self.larg != larg or self.nargs != self.nargs or self.tup_args != tupargs or \
             self.prec != prec or self.eval_lvl != lvl or eval_args != self.eval_args):
      error("MultiMethodError",
          ("Previous method definition differs in left arg, "
          "number of args or tuple currying."))
    self.larg = larg
    self.nargs = nargs
    self.tup_args = tupargs
    self.vars_args = vargs
    self.prec = prec
    self.eval_lvl = lvl
    self.eval_args = eval_args
    self.methods[arg_t] = method

class FnMethod(ValueAssoc):
  def __init__(self, larg, nargs, arg_names, arg_types, varargs, tup_args, eval_lvl,
      prec, body, **kwargs):
    self.larg = larg
    self.nargs = nargs
    self.arg_names = arg_names
    self.arg_types = arg_types
    self.var_args = varargs
    self.tup_args = tup_args
    self.eval_lvl = eval_lvl
    self.prec = prec
    self.body = body
    self.eval_args = True if not 'eval_args' in kwargs else kwargs['eval_args']
    ValueAssoc.__init__(self, **kwargs)

  def __call__(self, env, args):
    from eval import eval
    if callable(self.body):
      return self.body(env, args)
    else:
      names = self.arg_names
      if len(args) != len(names):
        error("ArgumentError", "Expected {} args, got {}".format(len(names), len(args)))
      e = env.push()
      for name, val in zip(names, args):
        e.define(name, val)
      import pdb; pdb.set_trace()
      res = eval(e, self.body)
      print " ->", res
      e.pop()
      return res

  def __repr__(s):
    return "<m {}:{}/{}/{} {} :: {}/p {}>".format(
        s.larg, s.nargs, s.var_args, s.tup_args, s.arg_types, s.eval_lvl, s.prec)


class Closure(ValueAssoc):
  def __init__(self, fn, cls_vars, **kwargs):
    self.fn = fn
    self.closed_vars = cls_vars
    ValueAssoc.__init__(self, **kwargs)

  def __call__(self, env, args):
    if callable(self.fn):
      return self.fn(env, args)
    else:
      assert(False)

  def set(self, k, v):
    self.fn.set(k, v)

  def __getattr__(self, name):
    return getattr(self.fn, name)

  def __repr__(self):
    return "<cl {}>".format(repr(self.fn))


class Partial(ValueAssoc):
  def __init__(self, fn):
    self.fn = fn
    self.applied_args = []

  def apply(self, arg):
    if typeof(arg) is t_tuple:
      if len(arg) != self.fn.nargs: error("InvalidArgError", "Invalid right arguments, expected %, got %".format(self.fn.nargs, len(arg)))
      self.applied_args.extend(arg)
    else:
      self.applied_args.append(arg)

  def args_full(self):
    return len(self.applied_args) == self.fn.larg + self.fn.nargs

if __name__ == '__main__':
  import sys
  plus = Fn({
        (int, int): FnMethod(0, 2, ('l', 'r'), (int, int), False, False, 0, 0,
            Closure(lambda e, l, r: "{} + {} => {}".format(l, r, l+r), {})),
        (str, str): FnMethod(0, 2, ('l', 'r'), (int, int), False, False, 0, 0,
            Closure(lambda e, l, r: "{} append {} => {}".format(l, r, l+r), {})),
      })
  print plus({}, [1, 2])
  print plus({}, ["One", "Two"])
