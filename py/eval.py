from collections import deque
from env import *
from error import *
from levels import *
from stypes import *
from syms import *

isa = isinstance

def is_callable(v, eval_lvl):
  return (isa(v, Closure) and v.fn.eval_lvl <= eval_lvl) or (isa(v, Fn) and v.eval_lvl <= eval_lvl)

def eval_appl(e, appl, eval_lvl):
  arg_stk = []
  cl_stk = []
  do_eval = True
  while app:
    ve = eval_level(e, app.popleft(), eval_lvl) if do_eval else app.popleft()
    if is_callable(ve, eval_lvl):
      p = Partial(ve.fn)
      if ve.larg:
        p.apply(ve.pop())
    else:
      if cl_stk:
        cl_stk[-1].apply(ve)
      else:
        arg_stk.append(ve)
    if cl_stk and cl_stk[-1].args_full():
      arg_stk.append(cl_stk[-1].fn(e, cl_stk[-1].applied_args))

  if cl_stk and cl_stk[-1].args_full():
    arg_stk.append(cl_stk[-1].fn(e, cl_stk[-1].applied_args))
  if cl_stk: error("InvalidAppError", "Invalid application, partial remains")
  return arg_stk[0] if len(arg_stk) == 1 else appl(*arg_stk)

def _eval_appl(e, appl, eval_lvl):
  args = []
  cls  = []
  nnargs = -1
  app = deque(appl)
  do_eval = True
  args_seen = False
  while app:
    ve = eval_level(e, app.popleft(), eval_lvl) if do_eval else app.popleft()
    if is_callable(ve, eval_lvl):
      cls.append(ve)
      do_eval = ve.eval_args
      nnargs = ve.nargs + ve.larg
      args_seen = False
    else:
      if not args_seen and isa(ve, List) and ve.meta is t_tuple and cls and cls[-1].tup_args:
        if cls[-1].nargs != len(ve):
          error("ApplicationError", "Insufficient tuple arguments.")
        args.extend(map(lambda el: eval_level(e, el, eval_lvl), ve) if do_eval else ve)
      else:
        args.append(ve)
      args_seen = True

    if len(args) == nnargs and cls:
      ve = cls[-1](e, args[-nnargs:])
      args = args[:-nnargs]
      cls.pop()
      if is_callable(ve, eval_lvl):
        app.appendleft(ve)
      elif ve is not undef:
        args.append(ve)
      do_eval = True if not cls else cls[-1].eval_args

  if len(args) > 1 or len(cls) > 0:
    if len(args) == 0 and len(cls) == 1:
      return cls[0]
    res = []
    i = 0
    for cl in cls:
      if cl.larg:
        if i < len(args): res.append(args[i])
        i = i + 1
      res.append(cl)
      res.extend(args[i:i+cl.nargs])
      i = i + cl.nargs
    res.extend(args[i:])
    return List(*res, meta=t_appl, assoc=appl.assoc)
  return assoc(args[0], appl.assoc)

def eval_level(e, code, eval_lvl):
  if has_assoc(code): e = code.assoc

  if isa(code, List):
    if code.meta is appl_:
      return _eval_appl(e, code, eval_lvl)
    elif code.meta is code_:
      res = map(lambda el: eval_level(e, el, eval_lvl), code)
      return res[-1] if eval_lvl == evl_run else List(*res, meta=code.meta, assoc=code.assoc)
    else:
      return List(*map(lambda el: eval_level(e, el, eval_lvl), code), meta=code.meta, assoc=code.assoc)
  elif isa(code, Sym):
    return e.lookup(code, eval_lvl)
  elif isa(code, int):
    return code
  elif isa(code, str):
    return code
  elif code is None:
    return None
  else:
    if isa(code, Fn):
      return code
    elif callable(code):
      return code
    else:
      error("EvalError", "Unhandled object type " + str(type(code)) + ": " + str(code))

def attach_env(e, code):
  if isa(code, List):
    code.assoc = e.push()
    for el in code: attach_env(code.assoc, el)
  elif isa(code, Fn):
    code.assoc = e.push()
    for el in code.methods.body: attach_env(code.assoc, el)
  elif isa(code, Closure):
    code.assoc = e.push()
    attach_env(code.assoc, code.fn)
  return code

def evall(e, code, eval_lvl):
  e.runtime.eval_lvl = eval_lvl
  return eval_level(e, code, eval_lvl)

def eval(e, code):
  return eval_level(e, code, e.runtime.eval_lvl)
