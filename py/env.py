from error import *
from syms import *
from stypes import *
from lib.stdlib import *
from levels import *

env_parse = {}
env_syntax = {}
env_defn = {}
env_macro = {}
env_compile = {}
env_run = {}

eval_levels = [ evl_parse, evl_syntax, evl_macro, evl_compile, evl_run ]

class Runtime(object):
  def __init__(self):
    self.eval_lvl = eval_levels[0]


class Token(object):
  def __init__(self, larg, nargs, varargs, prec, eval_lvl, ctx):
    self.prec = prec
    self.larg = larg
    self.nargs = nargs
    self.varargs = varargs
    self.eval_lvl = eval_lvl
    self.context = ctx

  @classmethod
  def from_fn(clz, fn):
    return Token(fn.larg, fn.nargs, fn.var_args, fn.prec, fn.eval_lvl, fn.context)


class Env(dict):
  def __init__(self, runtime, vals, **kwargs):
    super(Env, self).__init__(vals)
    self.parent = kwargs['parent'] if 'parent' in kwargs else None
    self.runtime = runtime
    self.tokens = {}

  def define(self, name, val):
    if name in self[self.runtime.eval_lvl]:
      error("EnvDefinitionError", "Cannot redefine {}.".format(name))
    self[self.runtime.eval_lvl][name] = val
    return val

  def add_token(self, name, token):
    self.tokens[name] = token

  def token_lookup(self, ident, eval_lvl):
    e = self
    while e:
      if ident in self.tokens: return self.tokens[ident]
      e = e.parent
    return None

  def add_fn(self, name, fn):
    assert not name in self[fn.eval_lvl]
    self[fn.eval_lvl][name] = fn
    fn.name = name
    self.add_token(name, Token.from_fn(fn))

  def lookup(self, k, eval_lvl):
    while self:
      if eval_lvl in self and k in self[eval_lvl]:
        return self[eval_lvl][k]
      self = self.parent
    if eval_lvl == evl_run: error("EnvLookupError", "Undefined symbol " + str(k))
    return k

  def push(self, *a):
      return Env(self.runtime, a[0] if a else {lvl: {} for lvl in self.iterkeys()}, parent=self)

  def pop(self):
    return self.parent


def env_new():
  env = Env(Runtime(), {
    evl_parse: env_parse,
    evl_syntax: env_syntax,
    evl_defn: env_defn,
    evl_macro: env_macro,
    evl_compile: env_compile,
    evl_run: env_run,
  })
  stl_init(env)
  return env

