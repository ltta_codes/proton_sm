from eval import *
from levels import *
from stypes import *
from syms import *


def arg_types(args):
  return map(lambda t:
      t_any if len(t) == 1 else \
          t[2] if chk_args(t, [t_sym, colon_, t_sym]) else syn_error(),
      args)

def arg_names(args):
  return map(lambda t:
      t if len(t) == 1 else \
          t[2] if chk_args(t, [t_sym, colon_, t_sym]) else syn_error(),
      args)

def _analyze_defn(ast_defnn):
  assert typeof(ast_defnn) is t_appl
  assert len(ast_defnn) == 3
  assert ast_defnn[0] == 'fn'
  args = ast_defnn[1]
  return 0, len(args), False, 0

def _defn(e, args):
  print "_DEFN:",args
  if typeof(args[1]) is t_appl and args[1][0] == 'fn':
    largs, nargs, vargs, prec = _analyze_defn(args[1])
    anames = arg_names(args[1][1])
    atypes = arg_types(args[1][1])
    e.define(args[0], Fn(FnMethod(0, nargs, anames, atypes, vargs, True, evl_run, 0, None)))
  return appl(sym('define'), tup(*args))

def stl_init(env):
  def fn(nargs, slambda):
    return Fn(Closure(
        FnMethod(0, nargs, [None] * nargs, [t_any] * nargs,
            False, True, evl_run, 0, slambda), {}))

  def method(nargs, typed_lambdas):
    for tl in  typed_lambdas: assert len(tl[0]) == nargs
    methods = [
      Closure(FnMethod(0, nargs, [None] * nargs,
          tl[0], False, True, evl_run, 0, tl[1]), {}) \
              for tl in typed_lambdas
    ]
    return Fn(*methods)

  def op(nargs, prec, typed_lambdas):
    for tl in  typed_lambdas: assert len(tl[0]) == nargs
    methods = [
      Closure(FnMethod(1, nargs - 1, [None] * nargs,
          tl[0], False, False, evl_run, prec, tl[1]), {}) \
              for tl in typed_lambdas
    ]
    return Fn(*methods)

  def macro(nargs, slambda):
    return Fn(Closure(
        FnMethod(0, nargs, [None] * nargs, [t_any] * nargs,
            False, True, evl_macro, 0, slambda), {}))

  def syntax(nargs, slambda):
    return Fn(Closure(
        FnMethod(0, nargs, [None] * nargs, [t_any] * nargs,
            False, True, evl_syntax, 0, slambda), {}))


  def special(nargs, slambda):
    return Fn(Closure(
        FnMethod(0, nargs, [None] * nargs, [t_any] * nargs,
            False, True, evl_run, 0, slambda, eval_args=False), {}))

  def parser_macro(nargs, slambda):
    return Fn(Closure(
        FnMethod(0, nargs, [None] * nargs, [t_any] * nargs,
            False, True, evl_macro, 0, slambda), {}))

  def chk_arg(a, tv):
    if tv is t_any: return True
    if a == tv: return True
    if isa(tv, list):
      return chk_args(a, tv)
    return typeof(a) is tv

  def chk_args(args, t_vals):
    if len(args) != len(t_vals):
      for a, tv in zip(args, t_vals):
        if not chk_arg(a, tv): return False
    return True

  def user_fn(args, body):
    import pdb; pdb.set_trace()
    return Closure(Fn(FnMethod(0, len(args), arg_names(args), arg_types(args), False, True, evl_run, 0, body)), {})

  def prn(x):
    print x
  def brk(v):
    import pdb; pdb.set_trace(); return v

  env.add_fn(";", Fn(Closure(
      FnMethod(1, 1, [None, None], [t_any, t_any], False, False, evl_parse,
          -10, lambda e, a:
              List(*(a[0] + a[1]), meta=t_code) if typeof(a[0]) is t_code else List(*a, meta=t_code)
              ), {})))

  lib = {
    # Builtin Macros.
    "let":
        syntax(1, lambda e, a:
          appl(define_, tup(*a[0][0::2])) if chk_args(a, [[t_sym, eq_, t_any]]) \
             else error("SyntaxError", " Expected 'let <name> = <value>'.")) \
          .set("prec", -10) \
          .context({"=": op(2, -10, [ ((t_any, t_any), lambda e, a: tup(*a)) ])}),
    "define": (
        special(2, lambda e, a: prn(e) or e.define(a[0], eval(e, a[1]))),
        fn(2, _defn).set("eval_lvl", evl_defn),
        ),
    "fn": (
        special(2, lambda e, a: user_fn(a[0], a[1])).set('tup_args', False),
        ),

    # Numerical Operators.
    "+":
        op(2, 30, [
          ( (t_num, t_num), lambda e, a: a[0] + a[1] ),
          ( (t_str, t_str), lambda e, a: a[0] + a[1] ),
        ]),
    "-":
        op(2, 30, [
          ( (t_num, t_num), lambda e, a: a[0] - a[1] ),
        ]),
    "*":
        op(2, 40, [
          ( (t_num, t_num), lambda e, a: a[0] * a[1] ),
          ( (t_str, t_num), lambda e, a: a[0] * a[1] ),
        ]),
    "/":
        op(2, 40, [
          ( (t_num, t_num), lambda e, a: a[0] * a[1] ),
        ]),

    # Builtin Functions.
    "print":
      fn(1, lambda e, a: prn(a[0]))
  }

  for k, v in lib.iteritems():
    if isa(v, tuple):
      for vv in v:
        env.add_fn(k, vv)
    else:
      env.add_fn(k, v)
