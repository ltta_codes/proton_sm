from stypes import *

code_ = sym('t_code')
appl_ = sym('t_appl')
undef = sym('undef')
eof = sym('eof')

comma_ = sym(",")
colon_ = sym(":")
semicolon_ = sym(";")

let_ = sym('let')
define_ = sym('define')
eq_ = sym("=")
anon_ = sym("<anonymous>")
