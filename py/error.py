class Error(Exception):
  pass

def error(sig, msg):
  import pdb; pdb.set_trace()
  raise Error(sig + ": " + msg)
