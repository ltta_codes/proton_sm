let (defn,
  syn (name, args, code) {
    let (args, map (args, fn(x, if (pair? x, fst(x), x))))
    let (argchecks, [])
    foreach (arg, args) {
      if (pair? arg, {
        let (arg_name, fst(arg))
        let (arg_t, snd(arg))
        assert(istype(snd(arg)), TypeError, "{arg_t} is not a valid type.")
        argchecks append( $( assert(isa(`arg_name, `arg_t), TypeError, \
            "TypeError", "Expected " . # `arg_t . ", got " . # typeof(`arg_name))) ))
      })
    }
    $( let(`name, fn (`args) { `(to_code(arg_checks); `code } ) 
  }
)

let (syntax,
  set-exec-level(evl_preprocess,
    fn (name, args, code) {
      
    }
  )
)

# defn fib (n: num) { if (n < 2, n, fib(n - 1) + fib(n - 2)) }
