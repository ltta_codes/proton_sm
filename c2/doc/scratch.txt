Eval grouping:

* When?
  - Just before runtime eval, same time or preceding pass?
  - Preceding pass => operator precedences need to be defined at that point. Def/Fn ... need to be pre-eval'd and envs persisted across eval levels
  - Same-time => possibly slower, might make compilation harder

===

Applications:

* Write AllSignals repl sig handler
* Write simple debugger maybe? Based on condition system as well.
* Allow proton code to push recoveries.
* Write type-annotated-proto/C99 code generator
 
===
Code to get working:
  - define a ls_get [1] 0; a                          =>  :: 1
  - define (l, [1,2]); 1 and ls_len(l) < 2            =>  :: 1
===

# Immediate:
-> Eval rewrite
-> error/errors accept AST node for reporting source file position

# Next in Line:
-> Deliminted Continuations/Fibers/Coroutines from C and Proton for error/cond system e.g. (speed?)
-> libuv integration
-> Multiple Dispatch with focus on predefined cases/speed up for those (name/hash-based dispatch?)
-> FFI

# TODO
-> Eval precedence
  * "Precomputed" pass before eval?
  * precedence computed for evey application, stored in assoc
  * resolve all symbols first, then compute precedences and groupings
  * use Assoc to store each node's precedence?
  * eval becomes straight-forward
-> Delimited continuations
  * possibly also stackless coroutines or one-shot/linear stack continuations for more efficiency
  * re-implement condition sys in delimted conts
-> Better Error messaging / assoc source lines with AST nodes.
  * assoc types, maybe mem-based structs
-> mem + multiple dispatch based structs with refs
  * mem gets a cell as tag, dispatched on that tag
  * every property/slot has accessor fn that returns a ref?
  * external "shape-hinting" allows external control of mem-layout (i.e. to support 3rd party libraries via FFI)
  * reflection
-> syn-def, quote, unquote
-> apply as just a regular multi-method
  * proper multiple dispatch
-> libuv integration
-> parser tests
-> partials:
  * partial vs closure clarification, multiple calls what happens to args?
-> c translation/backend? 
-> builtin fns
-> delimited continuations
-> fiber/thread pool
-> locking or async primitives or immutable datastructures
  * possibly John Carmacks idea of "locking" data, moving it to read-only protected pages
-> GC combined with manual mem management
-> Real closures
-> Fix nested comments
-> Inlining for, e.g., mem-based structs and their accessor fns
-> FFI
  * libffi or generate .so and load?
  * http://luajit.org/ext_ffi.html
-> Pretty good reflection on all methods, types and structs
-> Port nanogui or imgui
  * Reactive programming framework / reactive gui framework with imm-mode and diffing gui?
-> Write super-fast GPU accelerated picktorial/luminar/adobe lr clone
-> Modules

# Big Topics
-> Assoc, Mem and Refs
-> Multiple Dispatch
-> Proper closures and closed variables
-> Delimited Continuations
-> Great Error Messaging
-> Async primitives + libuv (?)
-> GUI library + immediate mode gui libs (a la om.next and react)
-> FFI
-> C99 codegen
-> Optimization

-> Write Linear Algebra DSL + BLAS bindings
-> Write an agent-based simulation

# Done
-> Comma in lists/appl
  * argument-specific environments/symbol defs
-> code-blocks:
  * '\n' operator: takes expr or code-block left, expr right
  * insert BOF, EOF tokens so that '\n' op always has left arg, BOF, EOF evaluate to void
-> Evaluation-levels
  * sorted builtin
-> proper hash-maps
-> eval:
  * var_args (-n => at least n args)
  * handle eval levels
  * make "define a 1" and "define(a, 1)" work
-> ";" operator => define(id,fn(x,x)); define(b, 1)
-> error/condition sys port
  * condsys fallback handler in proton rtl
-> Assoc
  * Dictionary with "hidden class" (hashed keys?)
  * Embedded in s_any lists
  * Used for:
    -> AST nodes, storing original node position, eval "grouping"

===

# BUGS

===

# RTL
-> Builtin RTL fns:

env:
  - define, env
math:
  - c: abs, cos, ceiling, even? = %2, exp, floor, log, max, min, odd?, random, mod, sin, sqrt, 
logic:
  - &&, ||, &, |, not, 
comparison:
  - ==, <=, <, >, >=, is
types:
  - isa, typeof
list:
  - nth, append, extend, len, find, head, tail, prepend, pop, delete, empty?, copy
  - setmeta, meta
  - foreach
string:
  - contains?
fn:
  - apply, repeated, 
functional:
  - accumulate, foldl == fold, foldr == reduce, unfold, map, filter
types:
  - assoc, getassoc, null?, sym, $, quote
  - str?, num?, list?, pair?, tuple?, code?, void?
  - typecheck 
control flow:
  - cond == if, error, foreach, 
i/o:
  - print, open, read, write, close,
language features:
  - fn, quote, syn, 
debug:
  - trace, untrace, break


let(name, val) => define
fn([args], code)
syn([args], code)
cond(c1,b1,...,bn) or if(cond,then,else)
loop(...) + break + continue or with-label(label, code) + goto
foreach(...)
$(...) and `(...)
map(...)
filter(...)
fst(...), snd()
assert($...)
# comments

list:
  append(v)


############################################################################################################################################
Scratch
############################################################################################################################################


[blubb, blaa] => ,: fn(0) -> void

if (cond) {code_then} else {code_else} => else: fn(1) -> [2::else]
foreach k, v in it { ... } => in: fn(2) -> []

* callable has arg_processors
  - arg_processor entered into env when processing callable's arguments, removed when done
  - arg_processors are executed like regular functions before callable is executed
  - serve deliniation, parsing, error messaging in syntactic fns

* Syntax error handling?
  - if (cond) {code_then} elif (cond) {code} else {code} => syntax if (cond: [::code], code_then: [::code], elif: [::elif], else: [::else]) {
    elif: op([::code], [::code], [::code]) -> [::cond], else: op([::code]) -> [::cond]


############################################################################################################################################
Syntax
############################################################################################################################################

let(defn,
  syn([name, args, code],
    $(let(`name, fn(`args, `code)))
  )
)

let (syntax,
  syn([name, args, code],
...
  )
)

syntax defn (name: sym, [args: (sym | pair[sym,sym]]), code) {
  
}

defn fib(args) => code


defn fib(n: num) = (n < 2) ? n : fib(n - 1) + fib(n - 2)


############################################################################################################################################
Euler
############################################################################################################################################


# Find the sum of all the multiples of 3 or 5 below 1000.
defn multiples_3_5(max: num) = \
  filter range(max) \x: num => x % 3 == 0 || x % 5 == 0

# By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
defn fib_gen(max: num) = foreach i: num in range(max) while res < max { yield set res: num = fib(i) }
defn fib_even(max: num) = \
  filter fib_gen(4_000_000) \x: num => x % 2 == 0

# What is the largest prime factor of the number 600851475143 ?
defn prime_factor(n: num) {

}

# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 x 99.
# Find the largest palindrome made from the product of two 3-digit numbers.

