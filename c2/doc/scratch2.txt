Appliction:

1 + 1 print 2 
- recursive forward eval
- appl left and pop+replace


1. Debugging:

  define.(.a.,.fn.(.x.,.x.+.1.).) =>
  ______________________________
         ___
           ___________________
               __ ___________

  - Evaluation => how to best debug evaluation?
    * Break points on AST node execution?
    * Break into debugger: 
      . https://github.com/scottt/debugbreak/blob/master/debugbreak.h
      . https://hg.mozilla.org/mozilla-central/file/98fa9c0cff7a/js/src/jsutil.cpp#l66
      . #include <signal.h>\n   raise(SIGABRT)
    * Stepping through AST:
      - where to stop? unit of step? so many evals per each AST node
    * Show internal state somehow
      - built-in debugger, steps through, know internal local/global/state, can display state+current ast


arg format

  <kwd (name : type)>


define (id, fn(x, x))

<define , (id, <fn (x, x)>)>


* appl fn-appl right associate lower equal prio without grouping
* empty kwd names for args


kwd examples:

if <cond> then <ifb> [elif<elifnb>*] else <elseb> end
defn <name> ( < args> ) do <code> end



TODO:

1. kwd args in grouping
2. kwd arg encoding in callable
3. add kwd args in parser list routines


* rewrite list parsing with kwd-args
  - (
* argument data to store:
  - optional
  - kwd-only => allows seq of multiple symbols as kwds
* kwd args in eval_grouping
  - kwd args storage:
    . in arg_names, but arg_names is optional for native_fns
    . in arg_kwds, but how to mark kwd-only args
* optional arguments


Method vs Fn and Callable
  => Fn callable points to Fn?
  => Method Callable contains arg names and types? No, arg names may differ from Fn to Fn.
  
STEPS:
* mm_mk_ take and parse arg opts via arg_names or args
* 
