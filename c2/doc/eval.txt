
Run-Level-Specific Environments?
  - First pass creates envs and associates with AST-nodes
  - new AST-nodes are assoc'd with current env (eval results)
  - 


Grouping algo:


1. Linear pass, lookup idents
2. group-sub:
  - expressions bind until prec reached
  - recursive bin-op parsing
    . left: group-sub w/ current prec
    . if next has args: group-sub for-each arg with default-prec, last with op's prec
    . 




defn fib(n: num) = n if n < 2 then n else fib(n - 1) + fib (n - 2) end


syntax 0 ( (
  elements, `),
  
  [ `, : op -10 (l, r) { ... } ]
) {
  ...
}

syntax 10 if (
  cond, `then, then_exp, `else, else_exp, `end
) {

}

syntax 10 `do {
  code, `end
}

macro defn (args, `=, code)
macro do (code, `end)


<defn fib> << `( <n: num> `) >> <= ...>
defn fib (( n: num )) = if n < 2 then n else fib ((n - 1)) + fib ((n - 2)) end
set(fib, fn(n: num, if ...))

======================



* operand stack
* operator stack
  - before being pushed, operators with multiple right-side arguments grab all but last right-side arg, unless last arg is tuple which is directly applied
  - when op pushed, pop all operators of higher or equal precedence
    . popped operators grab their left and right arguments (if applicable)


1 and ls_len(2) < 2

1, ls_len(2), 2         | and <

(1 and (ls_len(2) < 2))


defn fib(n: num) = n if n < 2 else fib(n - 1) + fib (n - 2)

n if n < 2 else fib(n - 1 + fib(n - 2)
<n> if <n < 2> else <fib ... >


1 2 3 ; 4 5 6
(1 2 3) | ;
(1 2 3) (4 5 6) ; |


define (l, [1,2]); 1 and ls_len(l) < 2


1 and ls_len(l) < 2
and (1, ls_len(l) < 2)

1 and ls_len(l) < 2

1    | and
1, ls_len l, 2 | <, and

1 and ls_len l + 1 < 2

1, l, 1, +, 2, <, ls_len, and

1 + 2 + 3 + 4
1 + 2 + 3 + 4

1 + 2 * 3 ^ 4
1 + 2 
1 + (2 * 3
1 + (2 * (3 ^ 4))

1 2 3 4 | ^ * +

1 + 2 * 3 ^ 4
1 2 3 4 ^ * +
+ 1 (* 2 (^ 3 4))


- curried and single tuple-arg functions

Most syntax is curried:
- curried if
  . context: { then elif, else }
  . if cond then code => closure 1-arg (end or elif- or else-block)
  . elif cond then code => closure a-arg
  . else code => closure 1-arg
  => if cond code1 (else code2 end)
- curried defn
  . context: { = }
  . defn <name> <args> code
    => defn fib (x) { if x < 2 then x else fib(x - 2) + fib(x - 1)
- varags
  . only in tuple-notation?

Implementation:
- dequeue/slice
  . pop two off front -> append to output list
  . look-ahead two appl elements for possible infix-op with higher prec 


add(2, 3 + 2 * 2) + fib(10)
add_|(2, |3 + |2 * 2||) + fib_|(10)|


STEPS:
  - deque
  - mk_fn + multimethods (dispatch, ...)
  - 
