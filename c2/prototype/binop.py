
env = {
    'and': 10,
    'or': 10,
    '<': 20,
    '<=': 20,
    '>': 20,
    '>=': 20,
    '+': 30,
    '-': 30,
    '*': 40,
    '/': 40,
    '^': 40,
}

binops = set([
    'and',
    'or',
    '<',
    '<=',
    '>',
    '>=',
    '+',
    '-',
    '*',
    '/',
    '^',
])

fns = { 'len': 1, 'pair': 2 }

def precedence(tok):
    if tok in env: return env[tok]
    return 0

def eval(src):
    tokens = src.split(' ')
    print tokens
    operands = []
    ops = []
    prev_op = None
    last_op = None
    for tok in tokens:
        if tok in binops:
            op_info = ( len(operands), 2, tok )
            ops.append(op_info)
            operands.append(tok)
        elif tok in fns:
            op_info = ( len(operands), fns[tok], tok )
            ops.append(op_info)
            operands.append(tok)
        else:
            operands.append(tok)
                
    print operands, ops

if __name__ == '__main__':
    eval("1 + 2 * 3")
