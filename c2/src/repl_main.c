#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "assoc.h"
#include "cell.h"
#include "error.h"
#include "eval.h"
#include "linenoise.h"
#include "mm.h"
#include "parser.h"
#include "rtl.h"
#include "sym.h"
#include "util.h"

#define HISTORY_FILE ".proton_history"

static char *line = NULL;

bool load_rtl(size_t argc, char **argv, size_t start) {
  for (size_t i = start; i < argc; ++i) {
    if (strcmp(argv[i], "--nortl") == 0) {
      return false;
    }
  }
  return true;
}

static cell select_recovery(env *e, cell args, cell closed_vars[],
    mmethod *self) {
  cell recoveries;
print_recos:
  recoveries = list_recoveries();
  list *recos = (celltype(recoveries) == t_list)
      ? to_list(recoveries)
      : ls_of(1, recoveries);
  printf("Available recoveries:\n\n");
  for (size_t i = 0; i < ls_len(recos); ++i) {
    cell p = ls_get(recos, i);
    char *name = cell_print(fst(p), NULL),
         *desc = cell_print(snd(p), NULL);
    printf("%zu) %s %s\n", i, name, desc);
    free(name); free(desc);
  }
  char *line = NULL;
  size_t len = 0;
  num choice;
  if (getline(&line, &len, stdin) == -1 || (line[strlen(line) - 1] = '\0')
      || !str_to_num(line, &choice) ||
      !(choice < ls_len(recos) && abs(choice) <= ls_len(recos))) {
    printf("Invalid input, try again.\n\n");
    goto print_recos;
  }
  cell reco = fst(ls_get(recos, choice)); 
  invoke_recovery(reco, nil);
  return nil;
}

void push_all_signal_handler() {
  push_signal_handler(sym("AllSignals"),
      from_method(mm_mk_native(select_recovery, false, ls_of(0), NULL, 0)));
}

int main(int argc, char **argv) {
  gc_init();
  symtab_init();
  globals *globals = env_init();
  condsys_init(globals);
  parser_init(globals);
  assoc_init();

  rtl_init(globals);

  char *cwd = get_cwd();

  // Exit interpreter recovery.
  jmp_buf recovery_exit;
  if (setjmp(recovery_exit) == 0) {
    push_recovery(&recovery_exit, sym("Exit"),
        from_cstr("Exit the interpreter."));

    size_t flags = 0;
    if (load_rtl(argc, argv, 1)) {
      eval_file(globals, "build/rtl.0.proton");
      //eval_file(globals, "build/rtl.proton");
    } else {
      ++flags;
    }

    if (argc - flags >= 2) {
      // Reload main source recovery.
      jmp_buf recovery_restart;
      if (setjmp(recovery_restart) == 0) {
        push_recovery(&recovery_restart, sym("ReloadSource"),
            from_cstr("Reload main source."));
      } else {
        set_cwd(cwd);
        printf("Reloading Source Code \"%s\"...", argv[1]);
      }

      cell res = eval_file(globals, argv[1]);
      char *sres = cell_print(res, NULL);
      printf("\nRES: %s\n", sres);
      free(sres);
    } else {
      push_all_signal_handler();

      linenoiseSetMultiLine(1);
      linenoiseHistoryLoad(HISTORY_FILE);

      jmp_buf recovery_ignore_error;
      if (setjmp(recovery_ignore_error) == 0) {
        push_recovery(&recovery_ignore_error, sym("Ignore"),
            from_cstr("Ignore last input"));
      } else {
        if (line) { free(line); }
      }

      while((line = linenoise("p+ > ")) != NULL) {
        if (line[0] != '\0' && line[0] != '/') {
            cell res = eval_src(globals, line, "<repl>");
            char *sres = cell_print(res, NULL);
            printf(":: %s\n", sres);
            free(sres);
            fflush(0);

            linenoiseHistoryAdd(line); /* Add to the history. */
            linenoiseHistorySave(HISTORY_FILE); /* Save the history on disk. */
        } 
        free(line);
        line = NULL;
      }
      if (cwd) { free(cwd); }
      return 0;
    }

  } else {
    if (line) { free(line); }
  }

  if (cwd) { free(cwd); }

  rtl_finalize();
  assoc_finalize();
  condsys_finalize();
  symtab_finalize();
  parser_finalize(globals);
  env_finalize();
  gc_collect(gc, NULL);
  gc_finalize();

  return 0;
}
