#pragma once

#include <stdint.h>

#include "cell.h"
#include "env.h"
#include "mm.h"

// TYPE SYSTEM

typedef uintptr_t type_id_word_int;
//typedef uint64_t type_id_word_int;
typedef union { type_id_word_int w; struct type_info *p; } type_id_word;
typedef uint8_t type_id_component;

#define TYPE_ID_COMP_BITS (8 * sizeof(type_id_component))
#define TYPE_ID_COMP_PER_WORD (sizeof(type_id_word_int) / sizeof(type_id_component))
#define TYPE_ID_COMP_EXT_VAL ((1 << TYPE_ID_COMP_BITS) - 1)
#define TYPE_ID_COMP_MASK ((1 << TYPE_ID_COMP_BITS) - 1)

#define TYPE_ID_COMP_UNUSED 0

#define TYPE_ID_WORDS 2

#if (TYPE_ID_WORDS < 2)
# error "Expecting type_id to contain at least two type_id_words"
#endif

typedef struct {
  type_id_word words[TYPE_ID_WORDS];
} type_id;

typedef struct {
  enum { ti_abstract, ti_prim, ti_struct, ti_mem } kind;
  union {
    struct {
      size_t bits;
    } prim;
    struct {
      size_t n_elems;
      cell *elem_names;
    } struc;
    struct {
      //
    } mem;
  };
} type_spec;

typedef struct type_info {
  cell name;
  type_spec spec;
  type_id id;

  struct type_info *super;
  list *children;

  size_t typeid_n_components;
  type_id_component typeid_comps[];
} type_info;

type_id typeid(cell type_sym);
type_id type_def(cell type_sym, cell super, type_spec *ts);
type_info  *type_resolve(cell sym);
type_info *typeid_typeinfo(type_id id);
type_id typeid_super(type_id id);

bool type_is_subtype(type_id super, type_id sub);
bool type_is_abstract(type_info *t);

int typeid_cmp(type_id a, type_id b);
bool typeid_eq(type_id a, type_id b);
int typeid_cmp_roots(type_id a, type_id b);

void types_init(env *e);
void types_gc(gc_state *gcs);
void types_finalize();

extern type_id tid_none;
extern type_id tid_any;
