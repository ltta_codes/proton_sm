#include "list.h"

#include <string.h>
#include <stdio.h>

#include "assoc.h"
#include "error.h"
#include "mm.h"
#include "sort.h"
#include "types.h"
#include "util.h"

#define INITIAL_LIST_ALLOC 10


typedef struct {
  size_t len;
  bool realloced;
  void *data[];
} array_buf;

typedef struct {
  size_t alloc_len;
} coalloc_buf;

typedef struct {
  size_t start;
  list *ls;
  cell meta;
} slice;

list *ls_mk() {
  return ls_mk_init(0, INITIAL_LIST_ALLOC, s_any, NULL, false, false, nil);
}

size_t ls_elem_sz(elem_type et) {
  switch (et) {
    case  s_char:
      return sizeof(char);
    case s_num:
      return sizeof(num);
    case s_pair:
      return 2 * sizeof(cell);
    case s_ptr:
      return sizeof(void *);
    case s_any:
      return sizeof(cell);
  }
}

static size_t ls_struct_ext_size(size_t alloc, elem_type elem_t, bool has_meta) {
  return alloc * ls_elem_sz(elem_t)
      + ((has_meta) ? sizeof(cell) : 0);
}

static bool ls_elemtype_can_unbox(elem_type et) {
  switch (et) {
    case  s_char:
    case s_num:
    case s_pair:
    case s_ptr:
      return true; 
    case s_any:
      return false;
  }
}

static elem_type elemtype(cell c) {
  if (is_assoc(c)) { c = deassociate(c); }
  switch (celltype(c)) {
    case t_num:
      return s_num;
    case t_ptr:
      return s_ptr;
    case t_list:
      if (is_pair(c.l)) { return s_pair; }
      return s_any;
    case t_sym:
    case t_mem:
    case t_assoc:
    case t_nil:
    case t_void:
    case t_partial:
    case t_fn:
    case t_method:
      return s_any;
  }
}

static inline array_buf *ls_mut_buf(list *l) {
  assert(l->alloc_type == da_mut_buf
      && "List: Attempting to access mutable buffer of immutable list");
  return (array_buf *) l->data;
}

static inline void *ls_data(list *l) {
  switch (l->alloc_type) {
    case da_slice: {
      slice *sl = (slice *) l->data;
      return ls_at(sl->ls, sl->start);
    }
    case da_coalloced:
      return ((char *) &l->data) + sizeof(coalloc_buf);
    case da_ptr:
      return l->data;
    case da_mut_buf:
      return ls_mut_buf(l)->data; 
  }
}

static inline size_t ls_alloc_leni(list *l) {
  switch (l->alloc_type) {
    case da_mut_buf: return ls_mut_buf(l)->len;
    case da_coalloced: return ((coalloc_buf *) &l->data)->alloc_len;
    case da_slice: return 0;
    case da_ptr: return 0;
  }
}

size_t ls_alloc_len(list *l) {
  return ls_alloc_leni(l);
}

bool ls_is_sorted_ex(list *l, cell_cmp_fn cmp) {
  if (l->sorted) { return true; }
  for (size_t i = 1; i < ls_len(l); ++i) {
    if (cmp(ls_get(l, i - 1), ls_get(l, i)) <= 0) {
      l->sorted = false;
      return false;
    }
  }
  l->sorted = true;
  return true;
}

size_t ls_sorted_find_ex(list *l, cell val, bool *found, cell_cmp_fn cmp) {
  size_t low = 0, high = ls_len(l);
  while (low < high) {
    size_t mid = low + (high - low) / 2;
    cell cv = ls_get(l, mid);
    int cmp_val = cmp(cv, val);
    if (cmp_val < 0) {
      high = mid;
    } else if (cmp_val > 0) {
      low = mid + 1;
    } else {
      if (found) { *found = true; }
      return mid;
    }
  }
  if (found) { *found = false; }
  return low;
}

void ls_sorted_insert_ex(list *l, cell val, cell_cmp_fn cmp) {
  eassert(l->sorted || l->len == 0, "ListInsertError",
      "Trying to insert sorted into non-sorted list");
  ls_insert(l, ls_sorted_find_ex(l, val, NULL, cmp), val);
  l->sorted = true;
}

void ls_sorted_insert(list *l, cell val) {
  ls_sorted_insert_ex(l, val, cell_cmp);
}

bool ls_is_sorted(list *l) {
  return ls_is_sorted_ex(l, cell_cmp);
}

size_t ls_sorted_find(list *l, cell val, bool *found) {
  return ls_sorted_find_ex(l, val, found, cell_cmp);
}

static inline cell *ls_meta_ofs(list *l) {
  if (!l->has_meta) {
    return NULL;
  }

  switch (l->alloc_type) {
    case da_slice:
      return &(((slice *) l->data)->meta);
    case da_ptr:
    case da_mut_buf:
    case da_coalloced: {
      if (l->alloc_type != da_coalloced && !l->data) { return NULL; }
      size_t n = ls_alloc_leni(l);
      return (cell *) ((char *) ls_data(l) + n * ls_elem_sz(l->entry_type));
    }
  }
}

void ls_mut_buf_realloc(list *l, size_t nelems, bool has_meta) {
  assert(l->alloc_type == da_mut_buf
      && "Trying to re-allocate list buffer other than mut-buf.");
  cell meta = l->has_meta ? ls_getmeta(l) : nil;
  l->data = mm_realloc(l->data, sizeof(array_buf)
      + ls_struct_ext_size(nelems, l->entry_type, has_meta));
  array_buf *a = (array_buf *) l->data;
  a->len = nelems;
  if (l->has_meta) {
    *ls_meta_ofs(l) = meta;
  }
}

void ls_init_data(list *l, size_t nelems, elem_type entry_type, void *data) {
  *l = (list) {
    .has_meta = false,
    .unboxed = ls_elemtype_can_unbox(entry_type),
    .sorted = false,
    .homogenuous = entry_type != s_any,
    .immutable = true,
    .shape_immutable = true,
    .alloc_type = da_ptr,
    .entry_type = entry_type,
    .len = nelems,
    .data = data,
  };
}

list *ls_mk_init(size_t nelems, size_t nalloc, elem_type entry_type,
    const void *data, bool immutable, bool shape_immutable, cell meta) {
  assert(nalloc >= nelems && "List: expect nalloc >= nelems.");

  if (immutable) { shape_immutable = true; }
  bool has_meta = !is_nil(meta);
  bool co_allocated = shape_immutable;

  // Immutable lists are of data_alloc_type `da_coalloced` and have
  // element buffer "baked in" at end of struct. `data` points at first list element.
  // In mutable lists `data` points to an array_buf.
  list *res = (co_allocated)
      ? mm_alloc(sizeof(list) + sizeof(coalloc_buf)
          + ls_struct_ext_size(nalloc, entry_type, has_meta))
      : new(list);
    
  *res = (list) {
    .has_meta = has_meta,
    .unboxed = ls_elemtype_can_unbox(entry_type), 
    .sorted = false,
    .immutable = immutable,
    .shape_immutable = shape_immutable,
    .homogenuous = entry_type != s_any,
    .alloc_type = (co_allocated) ? da_coalloced : da_mut_buf,
    .entry_type = entry_type,
    .len = nelems,
  };

  // Immutable data is adjunct to list structure, mutable data is in its own
  // memory because the array location may change during reallocs.
  if (!co_allocated) {
    res->data = NULL;
    ls_mut_buf_realloc(res, nalloc, has_meta);
    ls_mut_buf(res)->len = nalloc;
  } else {
    coalloc_buf *cdata = (coalloc_buf *) &res->data;
    cdata->alloc_len = nalloc;
  }
  void *dest_data = ls_data(res);
  size_t elem_sz = ls_elem_sz(entry_type);
  memcpy(dest_data, data, nelems * elem_sz);
  memset(((char *)dest_data) + nelems * elem_sz, 0, (nalloc - nelems) * elem_sz);
  if (has_meta) {
    *ls_meta_ofs(res) = meta;
  }
  return res;
}

list *vls_of_m(cell meta, bool imm, size_t n, va_list args) {
  list *res = ls_mk_init(0, n, s_any, NULL, false, imm, meta);
  while (n--) {
    cell c = va_arg(args, cell);
    ls_append(res, c);
  }
  res->immutable = imm;
  va_end(args);
  return res;
}

list *ls_of(size_t n, ...) {
  va_list args;
  va_start(args, n);
  return vls_of_m(nil, true, n, args);
}

list *ls_of_mut(size_t n, ...) {
  va_list args;
  va_start(args, n);
  return vls_of_m(nil, false, n, args);
}

list *ls_of_m(cell meta, size_t n, ...) {
  va_list args;
  va_start(args, n);
  return vls_of_m(meta, true, n, args);
}

list *ls_of_m_mut(cell meta, size_t n, ...) {
  va_list args;
  va_start(args, n);
  return vls_of_m(meta, false, n, args);
}

list *ls_of_nils(size_t n) {
  list *res = ls_mk_init(0, n, s_any, NULL, false, false, nil);
  for (size_t i = 0; i < n; ++i) { ls_append(res, nil); }
  return res;
}

static inline void ls_check_shapemutable(list *l) {
  if (l->shape_immutable) {
    errors("MutationError",
        "Cannot modify shape immutable list, cannot set meta.");
  }
}

static inline void ls_check_mutable(list *l) {
  if (l->immutable) {
    errors("MutationError", "Cannot mutate immutable list, cannot set meta.");
  }
}

list *ls_setmeta(list *l, cell meta) {
  if (l->has_meta) {
    *ls_meta_ofs(l) = meta;
    return l;
  }
  if (!l->has_meta && is_nil(meta)) {
    return l;
  }

  ls_check_shapemutable(l);
  // Reallocate mut-buf including appended slot for meta.
  ls_mut_buf_realloc(l, ls_mut_buf(l)->len, true);
  l->has_meta = true; 
  *ls_meta_ofs(l) = meta;

  return l;
}

cell ls_getmeta(list *l) {
  cell *meta = ls_meta_ofs(l);
  return (!meta) ? nil : *meta;
}

bool ls_hasmeta(list *l) {
  return l->has_meta && !is_nil(*ls_meta_ofs(l));
}

bool meta_sym_eq(cell c, cell meta) {
  assert(celltype(meta) == t_sym);
  if (celltype(c) != t_list) { return false; }
  return sym_eq(ls_getmeta(to_list(c)), meta);
}

size_t ls_len(list *l) {
  return l->len;
}

static inline size_t ls_absidx(list *l, int index) {
  return (index < 0) ? (l->len + index) : index;
}

static inline void *ls_ati(list *l, size_t i) {
  if (l->unboxed) {
    switch (l->entry_type) {
      case s_char:
        return &(((char *) ls_data(l))[i]);
      case s_num:
        return &(((num *) ls_data(l))[i]);
      case s_pair:
        return &((cell *) ls_data(l))[2 * i];
      case s_ptr:
        return &((void **) ls_data(l))[i];
      default: assert(false && "List: invalid unboxed list entry type");
    }
  }
  return &((cell *) ls_data(l))[i];
}

void *ls_at_unchecked(list *l, size_t index) {
  return ls_ati(l, index);
}

void *ls_at(list *l, int index) {
  size_t i = ls_absidx(l, index);
  assert(i < l->len && "List: index out of bounds");
  eassert(i < l->len, "OutOfBoundsError", "List index out of bounds.");
  return ls_ati(l, i);
}

cell ls_get(list *l, int index) {
  void *p = ls_at(l, index);
  if (l->unboxed) {
    switch (l->entry_type) {
      case s_char:
        return from_num(*((char *) p)); 
      case s_num:
        return from_num(*((num *) p));
      case s_pair: {
        cell *cp = (cell *) p; 
        return pair_mk(*cp, *(cp + 1));
      }
      case s_ptr:
        return (cell) { .ty = t_ptr, .ptr = *((void **) p) };
      default: assert(false && "List: invalid unboxed list entry type");
    }
  }
  return *((cell *) p);
}

static void ls_resize(list *l, size_t nalloc) {
  ls_check_shapemutable(l);
  assert(l->alloc_type == da_mut_buf
      && "List: Trying to reallocate buffer other than array_buf.");
  cell meta = ls_getmeta(l);

  if (nalloc) {
    ls_mut_buf_realloc(l, nalloc, l->has_meta); 
    if (l->has_meta) {
      *ls_meta_ofs(l) = meta;
    }
    ls_mut_buf(l)->realloced = true;
  }
}

static void ls_checkresize(list *l, size_t newlen) {
  if (l->shape_immutable) { return; }
  size_t nalloced = ls_alloc_leni(l);
  if (newlen > nalloced) {
    ls_resize(l, 2 * newlen + 1);
  } else if (newlen < nalloced / 2 && ls_mut_buf(l)->realloced) {
    ls_resize(l, nalloced / 2 + 1);
  }
}

static inline void ls_try_unbox(list *l, cell val) {
  if (l->len == 0 && l->entry_type == s_any
      && ls_elemtype_can_unbox(elemtype(val))
      && l->alloc_type == da_mut_buf) {
    cell meta = ls_getmeta(l);
    l->unboxed = true;
    l->entry_type = elemtype(val);
    ls_mut_buf(l)->len =
        ls_mut_buf(l)->len * sizeof(cell) / ls_elem_sz(elemtype(val));
    if (l->has_meta) {
      ls_setmeta(l, meta);
    }
  }
}

list *ls_append(list *l, cell val) {
  ls_check_mutable(l);
  if (l->len == 0) { ls_try_unbox(l, val); }
  ls_checkresize(l, l->len + 1);
  eassertr(l, l->len < ls_alloc_leni(l), "ListAppendError",
      "Cannot append past allocation on shape immutable list.");
  ls_set(l, l->len++, val);
  return l;
}

static void ls_unbox_all(list *l) {
  ls_check_shapemutable(l);
  ls_check_mutable(l);
  assert(!l->immutable && "List: Unboxing and thus mutating immutable list");

  list nl = *l;
  array_buf *data = ls_mut_buf(l);
  nl.data = NULL;
  nl.entry_type = s_any;
  nl.unboxed = false;
  ls_mut_buf_realloc(&nl, data->len, l->has_meta);
  array_buf *ndata = ls_mut_buf(&nl);
  for (size_t i = 0; i < l->len; ++i) {
    cell c = ls_get(l, i);
    ((cell *) ndata->data)[i] = c;
  }
  if (l->has_meta) { *ls_meta_ofs(&nl) = ls_getmeta(l); }
  l->data = ndata;
  l->unboxed = false;
  l->entry_type = s_any;
}

static bool ls_can_store_unboxed(cell val, elem_type et) {
  cell_type ct = celltype(val);
  switch (et) {
    case  s_char:
      return ct == t_num;
    case s_num:
      return ct == t_num;
    case s_ptr:
      return ct == t_ptr;
    case s_pair:
      return ct == t_list && to_list(val)->len == 2
          && to_list(val)->immutable;
    case s_any:
      return false;
  }
}

bool ls_is_immutable(list *l) {
  return l->immutable;
}

list *ls_set_immutable(list *l, bool imm) {
  l->immutable = imm;
  return l;
}

void ls_set(list *l, int index, cell val) {
  ls_check_mutable(l);
  size_t i = ls_absidx(l, index);
  assert(i < l->len && "List: set, index out of bounds");
  if (i >= l->len) {
    errors("BoundsError", "List acces out of bounds");
  }

  if (l->len == 0) { ls_try_unbox(l, val); }
  l->sorted = false;

  if (l->unboxed && !ls_can_store_unboxed(val, l->entry_type)) {
    ls_unbox_all(l);
    l->homogenuous = false;
  }

  switch (l->entry_type) {
    case s_char:
      //((char *) ls_data(l))[i] = (char) to_num(val);
      *((char *) ls_at(l, i)) = (char) to_num(val);
      break;
    case s_num:
      //((num *) ls_data(l))[i] = to_num(val);
      *((num *) ls_at(l, i)) = to_num(val);
      break;
    case s_pair:
      *((cell *) ls_at(l, i)) = ls_get(to_list(val), 0); 
      *((cell *) ls_at(l, i) + 1) = ls_get(to_list(val), 1); 
      break;
    case s_ptr:
      *((void **) ls_at(l, i)) = (void *) val.ptr; 
      break;
    default:
      *((cell *) ls_at(l, i)) = val; 
      l->homogenuous = l->homogenuous
          && celltype(*((cell *) ls_at(l, 0))) == celltype(val);
  }
}

void ls_insert(list *l, int index, cell val) {
  size_t i = ls_absidx(l, index);
  eassert(i <= ls_len(l), "InsertError", "Invalid insert index");
  ls_checkresize(l, l->len + 1);
  size_t nbytes = ls_elem_sz(l->entry_type),
         count = l->len - i;
  if (count > 0) {
    memmove(
        ls_at_unchecked(l, i + 1),
        ls_at_unchecked(l, i),
        nbytes * count); 
  }
  ++l->len;
  ls_set(l, i, val);
}

cell ls_pop(list *l) {
  ls_check_mutable(l);
  cell res = ls_get(l, -1);
  ls_checkresize(l, --l->len); // check compaction.
  return res;
}

void ls_droplast(list *l) {
  ls_checkresize(l, --l->len); // check compaction.
}

list *ls_slice(list *l, int start, int end) {
  size_t s = ls_absidx(l, start),
         e = ls_absidx(l, end) + 1;
  if (s >= l->len) { s = 0; e = 0; }
  assert((s < l->len || l->len == 0) && "List: slice, start index out of bounds");
  assert((e <= l->len || l->len == 0) && "List: slice, end index out of bounds");
  assert(s <= e && "List: slice, start <= end, index out of bounds");
  list *ls = l;
  size_t offs = 0;
  while (ls->alloc_type == da_slice) {
    slice *sl = (slice *) ls->data;
    ls = sl->ls;
    offs += sl->start;
  }
  slice *sl = new(slice);
  *sl = (slice) {
    .start = offs + s,
    .ls = ls,
    .meta = ls_getmeta(l),
  };
  list *res = new(list);
  *res = (list) {
    .has_meta = l->has_meta,
    .unboxed = l->unboxed,
    .sorted = l->sorted,
    .immutable = l->immutable,
    .homogenuous = false,
    .alloc_type = da_slice,
    .entry_type = l->entry_type,
    .len = e - s,
    .data = sl,
  };
  return res;
}

void slice_reslice(list *l, int start_adj, int len_adj) {
  assert(l->alloc_type == da_slice && "List: Expected slice for reslice.");
  slice *sl = (slice *) l->data;
  sl->start += start_adj;
  long dlen = (long) len_adj - (long) start_adj;
  if (sl->start > sl->ls->len || (long) l->len + dlen <= 0) {
    l->len = 0;
  } else {
    l->len += dlen;
  }
  assert((l->len == 0 || sl->start < sl->ls->len)
      && "List: slice start out of bounds");
}

cell slice_popfront(list *l) {
  cell res = ls_get(l, 0);
  slice_reslice(l, 1, 0);
  return res;
}

static inline size_t max(size_t a, size_t b) {
  return (a < b) ? b : a;
}

static inline size_t ls_alloc_needed(list *l) {
  return max(l->len, ls_alloc_leni(l));
}

list *ls_copy(list *l) {
  return ls_mk_init(l->len, ls_alloc_needed(l), l->entry_type, ls_data(l),
      l->immutable, l->shape_immutable, ls_getmeta(l));
}

list *ls_copy_ex(list *l, bool imm, bool shape_imm, cell meta) {
  return ls_mk_init(l->len, ls_alloc_needed(l), l->entry_type, ls_data(l),
      imm, shape_imm, meta);
}

void ls_extend(list *l, list *to_add) {
  size_t lenl = ls_len(l),
         lenr = ls_len(to_add);
  ls_checkresize(l, lenl + lenr);
  l->len += lenr;
  for (size_t i = 0; i < lenr; ++i) {
    ls_set(l, lenl + i, ls_get(to_add, i));
  }
}

void ls_clear(list *l) {
  l->len = 0;
}

void ls_compact(list *l) {
  ls_resize(l, ls_len(l));
}

char *ls_join(list *l, const char *sep) {
  char *res = NULL;
  size_t ll = ls_len(l);
  for (size_t i = 0; i < ll; ++i) {
    res = cell_print(ls_get(l, i), res);
    res = esprintf(res, "_");
  }
  if (ll) { res[strlen(res) - 1] = '\0'; }
  return res;
}

bool ls_is_homogenuous(list *l) {
  if (l->alloc_type == da_slice) {
    return ls_is_homogenuous(((slice *) l->data)->ls);
  }
  return l->homogenuous;
}

bool ls_contains_str_data(list *l) {
  return l->immutable && l->entry_type == s_char 
      && ((char *)ls_data(l))[ls_len(l) - 1] == '\0'; 
}

bool ls_is_str(list *l) {
  return ls_contains_str_data(l) && sym_eq(ls_getmeta(l), ct_str);
}

bool ls_is_sym(list *l) {
  return ls_contains_str_data(l) && is_nil(ls_getmeta(l));
}

void ls_sort(list *l) {
  sort(l, cell_cmp);
}

int ls_cmp(list *a, list *b) {
  size_t la = ls_len(a), lb = ls_len(b);
  if (la != lb) { return lb - la; }
  /*if (a->immutable != b->immutable) { 
    return b->immutable ? -1 : 1;
  }*/

  if (ls_is_str(a) && ls_is_str(b)) {
    return -strcmp((char *) ls_data(a), (char *) ls_data(b));
  }

  for (size_t i = 0; i < la; ++i) {
    int cmp = cell_cmp(ls_get(a, i), ls_get(b, i));
    if (cmp != 0) { return cmp; }
  }
  return 0;
}

static char *ls_printi(list *l, char *ex, char *open, char *close, char *sep) {
  ex = esprintf(ex, "%s", open);
  for (long i = 0; i < (long) ls_len(l) - 1; ++i) {
    ex = cell_print(ls_get(l, i), ex);
    ex = esprintf(ex, sep);
  }
  if (l->len) { ex = cell_print(ls_get(l, ls_len(l) - 1), ex); }
  ex = esprintf(ex, "%s", close);

  return ex;
}

void ls_printd(list *l) {
  char *s = ls_printi(l, NULL, "\u2039", "\u203A", " ");
  printf("%s\n", s);
  free(s);
}

char *ls_print(list *l, char *ex) {
  if (ls_hasmeta(l)) {
    cell meta = ls_getmeta(l);
    if (sym_eq(meta, ct_appl)) {
      //return ls_printi(l, ex, "(", ")", " ");
      return ls_printi(l, ex, "\u2039", "\u203A", " ");
    } if (sym_eq(meta, ct_appl_grouped)) {
      //return ls_printi(l, ex, "(", ")", " ");
      return ls_printi(l, ex, "\u2039", "\u203A", " ");
    } else if (sym_eq(meta, ct_code)) {
      return ls_printi(l, ex, "{", "}", "; ");
    } else if (sym_eq(meta, ct_tuple)) {
      return ls_printi(l, ex, "(", ")", ",");
    } else if (sym_eq(meta, ct_comment)) {
      return ls_printi(l, ex, "/* ", " */", ";");
    }
  }
  // String.
  if (ls_is_str(l)) {
    return esprintf(ex, "\"%s\"", (char *)ls_data(l));
  }
  // Pair.
  if (ls_len(l) == 2 && l->immutable) {
    ex = cell_print(ls_get(l, 0), ex);
    ex = esprintf(ex, ": ");
    ex = cell_print(ls_get(l, 1), ex);
    return ex;
  }
  return ls_printi(l, ex, "[", "]", ", ");
}



// TESTS //

#ifdef TESTING

// Test Suite init/fin need to be set before including test.h. Hacky :/
#define TEST_SUITE_INIT gc_init(); symtab_init();
#define TEST_SUITE_FIN gc_finalize(); symtab_finalize();

#include "test.h"

static int test_unbox_char() {
  list *l = ls_mk_init(0, 5, s_char, NULL, false, false, nil);
  ls_append(l, from_num('a'));
  ls_append(l, from_num('b'));
  ls_append(l, from_num('c'));
  test_assert(l->unboxed, true);

  // An implementation independent way to test unboxing would be better.
  test_assert(*(char *) ls_at(l, 0), 'a');
  test_assert(*(char *) ls_at(l, 1), 'b');
  test_assert(*(char *) ls_at(l, 2), 'c');

  test_assert(to_num(ls_get(l, 0)), 'a');
  test_assert(to_num(ls_get(l, 1)), 'b');
  test_assert(to_num(ls_get(l, 2)), 'c');

  return OK;
}

static int test_unbox_num() {
  list *l = ls_mk(); 
  ls_append(l, from_num(1000000));
  ls_append(l, from_num(1000001));
  ls_append(l, from_num(1000002));
  test_assert(l->unboxed, true);

  // An implementation independent way to test unboxing would be better.
  test_assert(*(num *) ls_at(l, 0), 1000000);
  test_assert(*(num *) ls_at(l, 1), 1000001);
  test_assert(*(num *) ls_at(l, 2), 1000002);

  test_assert(to_num(ls_get(l, 0)), 1000000);
  test_assert(to_num(ls_get(l, 1)), 1000001);
  test_assert(to_num(ls_get(l, 2)), 1000002);

  return OK;
}

static int test_unbox_pair() {
  list *l = ls_mk();
  ls_append(l, pair_mk(from_num(0), from_num(1)));
  ls_append(l, pair_mk(from_num(1), from_num(1)));
  ls_append(l, pair_mk(from_num(2), from_num(1)));
  test_assert(l->unboxed, true);

  test_assert(to_num(*(cell *) ls_at(l, 0)), 0);
  test_assert(to_num(*((cell *) ls_at(l, 0) + 1)), 1);
  test_assert(to_num(*(cell *) ls_at(l, 1)), 1);
  test_assert(to_num(*((cell *) ls_at(l, 1) + 1)), 1);
  test_assert(to_num(*(cell *) ls_at(l, 2)), 2);
  test_assert(to_num(*((cell *) ls_at(l, 2) + 1)), 1);

  test_assert(to_num(ls_get(to_list(ls_get(l, 0)), 0)), 0);
  test_assert(to_num(ls_get(to_list(ls_get(l, 1)), 0)), 1);
  test_assert(to_num(ls_get(to_list(ls_get(l, 2)), 0)), 2);
  test_assert(to_num(ls_get(to_list(ls_get(l, 0)), 1)), 1);

  return OK;
}

static int test_box_heterogenous() {
  list *l = ls_mk_init(0, 5, s_char, NULL, false, false, nil);
  ls_append(l, from_num('a'));
  ls_append(l, from_num('b'));
  ls_append(l, from_num('c'));
  test_assert(l->unboxed, true);
  ls_append(l, nil);
  test_assert(l->unboxed, false);

  test_assert(to_num(*(cell *) ls_at(l, 0)), 'a');
  test_assert(to_num(*(cell *) ls_at(l, 1)), 'b');
  test_assert(to_num(*(cell *) ls_at(l, 2)), 'c');

  test_assert(to_num(ls_get(l, 0)), 'a');
  test_assert(to_num(ls_get(l, 1)), 'b');
  test_assert(to_num(ls_get(l, 2)), 'c');

  return OK;
}

static int test_append() {
  list *l = ls_mk();
  test_assert(ls_len(l), 0);
  ls_append(l, from_num(0));
  test_assert(ls_len(l), 1);
  ls_append(l, nil);
  test_assert(ls_len(l), 2);
  ls_append(l, from_num(1));
  test_assert(ls_len(l), 3);

  test_assert(to_num(ls_get(l, 0)), 0);
  test_assert(is_nil(ls_get(l, 1)), true);
  test_assert(to_num(ls_get(l, 2)), 1);

  return OK;
}

static int test_meta() {
#define check_list_props(list, len, hasmeta, meta_t, meta_val)   \
  do {                                                           \
    test_assert(ls_len((list)), (len));                          \
    test_assert(ls_hasmeta(list), (hasmeta));                    \
    test_assert(meta_t(ls_getmeta((list))), (meta_val));         \
  } while (0)

  // Unboxed list entries.
  list *l = ls_mk();
  ls_append(l, from_num(0));
  ls_append(l, from_num(1));
  ls_append(l, from_num(2));
  check_list_props(l, 3, false, is_nil, true);

  ls_setmeta(l, nil);
  check_list_props(l, 3, false, is_nil, true);

  ls_setmeta(l, from_num(1001));
  check_list_props(l, 3, true, to_num, 1001);

  // Force unbox list.
  ls_append(l, nil);
  check_list_props(l, 4, true, to_num, 1001);

  // Boxed list entries.
  l = ls_mk();
  ls_append(l, nil);
  ls_append(l, from_num(0));
  check_list_props(l, 2, false, is_nil, true);

  ls_setmeta(l, nil);
  check_list_props(l, 2, false, is_nil, true);

  ls_setmeta(l, from_num(1001));
  check_list_props(l, 2, true, to_num, 1001);

  ls_setmeta(l, nil);
  check_list_props(l, 2, false, is_nil, true);

  // Start with meta, boxed, then unbox all.
  l = ls_setmeta(ls_mk(), ct_appl);
  ls_append(l, from_num(0));
  ls_append(l, nil);
  ls_append(l, sym("s"));
  test_assert(ls_len(l), 3);
  test_assert(sym_eq(ls_getmeta(l), ct_appl), true);

  return OK;

#undef check_list_props
}

static int test_len() {
  list *l = ls_mk();
  test_assert(ls_len(l), 0);
  ls_append(l, from_num(0));
  test_assert(ls_len(l), 1);
  ls_append(l, from_num(1));
  test_assert(ls_len(l), 2);
  ls_append(l, from_num(2));
  test_assert(ls_len(l), 3);
  ls_set(l, 0, from_num(4));
  test_assert(ls_len(l), 3);
  ls_set(l, 2, from_num(5));
  test_assert(ls_len(l), 3);
  ls_pop(l);
  test_assert(ls_len(l), 2);
  ls_setmeta(l, nil);
  test_assert(ls_len(l), 2);
  ls_setmeta(l, from_num(1001));
  test_assert(ls_len(l), 2);

  return OK;
}

static int test_mk_init() {
  const char *data = "ab";
  size_t len = strlen(data) + 1;
  list *l = ls_mk_init(len, len, s_char, data, true, true, nil);
  test_assert(ls_len(l), len);
  for (size_t i = 0; i < len; ++i) {
    test_assert(*(char *) ls_at(l, i), data[i]);
  }

  num datan[] = { 2, 3, 4 };
  l = ls_mk_init(3, 10, s_num, datan, true, true, nil);
  for (size_t i = 0; i < 3; ++i) {
    test_assert(*(num *) ls_at(l, i), datan[i]);
  }

  cell datac[] = { nil, nil, _void };
  l = ls_mk_init(3, 3, s_any, datac, true, true, nil);
  for (size_t i = 0; i < 3; ++i) {
    test_assert((*(cell *) ls_at(l, i)).ty, datac[i].ty);
  }

  return OK;
}

static int test_get_immutable() {
  cell data[] = { from_num(100), from_num(200), from_num(300) };
  list *l = ls_mk_init(3, 3, s_any, data, true, true, nil);
  test_assert(to_num(ls_get(l, 0)), 100);
  test_assert(to_num(ls_get(l, 1)), 200);
  test_assert(to_num(ls_get(l, 2)), 300);
  test_assert(ls_len(l), 3);

  return OK;
}

static int test_set_get() {
#define chki(l, idx, val) \
    test_assert(to_num(ls_get((l), (idx))), (val));
#define chkn(l, start, end, ...)                                      \
    do {                                                                \
      num __data[] = { __VA_ARGS__ };                                   \
      for (size_t i = start; i < end; ++i) { chki(l, i, __data[i]); }   \
    } while (0)


  list *l = ls_mk();
  ls_append(l, from_num(1));
  ls_append(l, from_num(2));
  ls_append(l, from_num(3));
  ls_append(l, from_num(4));
  ls_append(l, from_num(5));
  chkn(l, 0, 5, 1, 2, 3, 4, 5);
  test_assert(ls_len(l), 5);

  ls_set(l, 0, from_num(0));
  chkn(l, 0, 5, 0, 2, 3, 4, 5);
  test_assert(ls_len(l), 5);

  ls_set(l, 0, from_num(0));
  chkn(l, 0, 5, 0, 2, 3, 4, 5);
  test_assert(ls_len(l), 5);

  ls_set(l, 3, from_num(100001));
  chkn(l, 0, 5, 0, 2, 3, 100001, 5);
  test_assert(ls_len(l), 5);

  return OK;

#undef chki
#undef chkn
}

static int test_pop() {
  list *l = ls_mk();
  ls_append(l, nil);
  ls_append(l, from_num(1));
  ls_append(l, from_num(2));
  ls_append(l, from_num(3));
  test_assert(ls_len(l), 4);

  test_assert(to_num(ls_pop(l)), 3);
  test_assert(ls_len(l), 3);

  test_assert(to_num(ls_pop(l)), 2);
  test_assert(ls_len(l), 2);

  ls_append(l, from_num(10001));
  test_assert(ls_len(l), 3);
  test_assert(to_num(ls_pop(l)), 10001);
  test_assert(ls_len(l), 2);

  test_assert(to_num(ls_pop(l)), 1);
  test_assert(ls_len(l), 1);

  test_assert(is_nil(ls_pop(l)), true);
  test_assert(ls_len(l), 0);

  ls_append(l, from_num(10001));
  test_assert(ls_len(l), 1);

  return OK;
}

static int test_cmp_lists(list *a, list *b) {
  test_assert(ls_cmp(b, a), 0);
  test_assert(a->immutable, b->immutable);
  test_assert(a->shape_immutable, b->shape_immutable);
  test_assert(cell_cmp(ls_getmeta(a), ls_getmeta(b)), 0);
  test_assert(ls_len(a), ls_len(b));
  return OK;
}

static int test_copy() {
  cell data[] = {
    nil, from_num(1), from_num(2), nil,
  };
  list *a1 = ls_mk_init(4, 4, s_any, data, true, true, nil);
  if (test_cmp_lists(a1, ls_copy(a1)) != OK) { return FAILED; }

  list *a2 = ls_mk_init(4, 4, s_any, data, false, false, nil);
  if (test_cmp_lists(a2, ls_copy(a2)) != OK) { return FAILED; }

  list *a3 = ls_mk_init(4, 4, s_any, data, false, false, ct_appl);
  if (test_cmp_lists(a3, ls_copy(a3)) != OK) { return FAILED; }

  char *d3 = "abcd";
  list *a4 = ls_mk_init(5, 5, s_char, d3, true, true, nil);
  if (test_cmp_lists(a4, ls_copy(a4)) != OK) { return FAILED; }

  cell d2[] = { from_num(1), from_num(10), from_num(100), from_num(1000) };
  list *a5 = ls_mk_init(4, 4, s_num, d2, true, true, nil);
  if (test_cmp_lists(a5, ls_copy(a5)) != OK) { return FAILED; }

  list *a6 = ls_mk_init(4, 4, s_num, d2, true, true, ct_tuple);
  if (test_cmp_lists(a6, ls_copy(a6)) != OK) { return FAILED; }

  return OK;
}

static int test_isimmutable() {
  list *l = ls_mk();
  ls_append(l, from_num(1));
  ls_append(l, from_num(2));
  test_assert(ls_is_immutable(l), false);
  ls_append(l, nil); // Force unbox.
  ls_set(l, 1, from_num(10001));
  test_assert(ls_is_immutable(l), false);

  const char *data = "ab";
  size_t len = strlen(data) + 1;
  l = ls_mk_init(len, len, s_char, data, true, true, nil);
  test_assert(ls_is_immutable(l), true);

  return OK;
}

static int test_shapeimmutable() {
  list *l = ls_mk_init(0, 4, s_any, NULL, false, true, nil);
  test_assert(ls_len(l), 0);
  ls_append(l, nil);
  test_assert(ls_len(l), 1);
  ls_append(l, from_num(1));
  test_assert(ls_len(l), 2);
  ls_append(l, from_num(2));
  test_assert(ls_len(l), 3);
  ls_append(l, from_num(3));
  test_assert(ls_len(l), 4);

  clear_signal();
  list *l2 = ls_mk_init(0, 2, s_any, NULL, false, true, ct_appl);
  ls_append(l2, from_num(0));
  ls_append(l2, from_num(1));
  ls_append(l2, from_num(2));
  test_assert_signal("ListAppendError");
  test_assert(ls_len(l2), 2);
  test_assert(to_num(ls_get(l2, 0)), 0);
  test_assert(to_num(ls_get(l2, 1)), 1);
  test_assert(sym_eq(ls_getmeta(l2), ct_appl), true);
  clear_signal();

  return OK;
}

static int test_at() {
  list *l = ls_mk();
  ls_append(l, from_num(1));
  ls_append(l, from_num(2));
  test_assert(*(num *)ls_at(l, 0), 1);
  test_assert(*(num *)ls_at(l, 1), 2);

  ls_append(l, nil); // Force unbox.
  test_assert(to_num(*(cell *)ls_at(l, 0)), 1);
  test_assert(to_num(*(cell *)ls_at(l, 1)), 2);
  test_assert(is_nil(*(cell *)ls_at(l, 2)), true);

  ls_set(l, 1, from_num(10001));
  test_assert(to_num(*(cell *)ls_at(l, 1)), to_num(ls_get(l, 1)));
  test_assert(to_num(*(cell *)ls_at(l, 0)), to_num(ls_get(l, 0)));

  return OK;
}

static int test_slice() {
  list *l = ls_mk();
  ls_append(l, from_num(1));
  ls_append(l, from_num(2));
  ls_append(l, from_num(3));
  ls_append(l, from_num(4));
  ls_append(l, from_num(5));

  list *sl = ls_slice(l, 0, -1);
  for (size_t i = 0; i < 5; ++i) {
    test_assert(to_num(ls_get(sl, i)), i + 1);
  }

  sl = ls_slice(l, 0, 3);
  for (size_t i = 0; i < 3; ++i) {
    test_assert(to_num(ls_get(sl, i)), i + 1);
  }

  sl = ls_slice(l, 3, -1);
  for (size_t i = 0; i < 2; ++i) {
    test_assert(to_num(ls_get(sl, i)), i + 4);
  }

  num nums[5] = { 1, 2, 3, 4, 5 };
  ls_init_data(l, 5, s_num, nums);
  sl = ls_slice(l, 3, -1);
  for (size_t i = 0; i < 2; ++i) {
    test_assert(to_num(ls_get(sl, i)), i + 4);
  }
  sl = ls_slice(sl, 0, -1);
  for (size_t i = 0; i < 2; ++i) {
    test_assert(to_num(ls_get(sl, i)), i + 4);
  }
  slice_reslice(sl, -1, 0);
  for (size_t i = 0; i < 3; ++i) {
    test_assert(to_num(ls_get(sl, i)), i + 3);
  }

  // Test empty slice.
  list *ll = ls_mk();
  test_assert(ls_len(ls_slice(ll, 0, -1)), 0);

  return OK;
}

static int test_cmp() {
  const char *data = "ab";
  const char *data2 = "ba";
  size_t len = strlen(data) + 1;
  list *l1 = ls_mk_init(len, len, s_char, data, true, true, nil),
       *l2 = ls_mk_init(len, len, s_char, data, true, true, nil),
       *l3 = ls_mk_init(len, len, s_char, data, false, true, nil),
       *l4 = ls_mk_init(len, len, s_char, data2, true, true, nil),
       *l5 = ls_mk_init(len, len, s_char, data2, false, true, nil),
       *l6 = ls_mk_init(len - 1, len - 1, s_char, data, true, true, nil);

  test_assert(ls_cmp(l1, l2), 0);
  test_assert(ls_cmp(l1, l3) == 0, false);
  test_assert(ls_cmp(l2, l3) == 0, false);
  test_assert(ls_cmp(l1, l4) == 0, false);
  test_assert(ls_cmp(l1, l5) == 0, false);
  test_assert(ls_cmp(l4, l5) == 0, false);
  test_assert(ls_cmp(l1, l6) == 0, false);

  // TODO: test immutable cmp.

  return OK;
}

static int test_extend() {
  list *l = ls_mk_init(0, 2, s_any, NULL, false, false, ct_appl);
  ls_append(l, from_num(0));
  ls_append(l, from_num(1));
  list *l2 = ls_mk_init(0, 2, s_any, NULL, false, false, ct_appl);
  ls_append(l, from_num(2));
  ls_append(l, from_num(3));

  ls_extend(l, l2);
  test_assert(ls_len(l), 4);
  test_assert(sym_eq(ls_getmeta(l), ct_appl), true);
  for (size_t i = 0; i < ls_len(l); ++i) {
    test_assert(to_num(ls_get(l, i)), i);
  }

  list *l3 = ls_mk_init(0, 2, s_any, NULL, false, false, ct_appl);
  ls_append(l3, from_num(0));
  ls_append(l3, from_num(1));

  ls_extend(l3, l3); // extend list with itself :/
  test_assert(ls_len(l3), 4);
  test_assert(to_num(ls_get(l3, 0)), 0);
  test_assert(to_num(ls_get(l3, 1)), 1);
  test_assert(to_num(ls_get(l3, 2)), 0);
  test_assert(to_num(ls_get(l3, 3)), 1);
  return OK;
}

static int test_insert() {
  list *l = ls_mk();
  ls_append(l, from_num(2));
  ls_append(l, from_num(3));
  ls_insert(l, 0, from_num(1));

  test_assert(to_num(ls_get(l, 0)), 1);
  test_assert(to_num(ls_get(l, 1)), 2);
  test_assert(to_num(ls_get(l, 2)), 3);
  test_assert(ls_len(l), 3);

  ls_insert(l, 0, from_num(1));
  test_assert(to_num(ls_get(l, 0)), 1);
  test_assert(to_num(ls_get(l, 1)), 1);
  test_assert(to_num(ls_get(l, 3)), 3);
  test_assert(ls_len(l), 4);

  ls_insert(l, 4, from_num(100));
  test_assert(to_num(ls_get(l, 4)), 100);
  test_assert(ls_len(l), 5);

  return OK;
}

static int test_insert_sorted() {
  list *l = ls_mk();
  ls_sorted_insert(l, from_num(3));
  ls_sorted_insert(l, from_num(2));
  ls_sorted_insert(l, from_num(1));

  test_assert(to_num(ls_get(l, 0)), 1);
  test_assert(to_num(ls_get(l, 1)), 2);
  test_assert(to_num(ls_get(l, 2)), 3);
  test_assert(ls_len(l), 3);

  ls_sorted_insert(l, from_num(0));
  test_assert(to_num(ls_get(l, 0)), 0);
  test_assert(to_num(ls_get(l, 1)), 1);
  test_assert(to_num(ls_get(l, 2)), 2);
  test_assert(to_num(ls_get(l, 3)), 3);
  test_assert(ls_len(l), 4);

  ls_sorted_insert(l ,from_num(100));
  test_assert(to_num(ls_get(l, 4)), 100);
  test_assert(ls_len(l), 5);

  return OK;
}

static int test_sorted_find() {
  list *l = ls_mk();
  ls_sorted_insert(l, from_num(3));
  ls_sorted_insert(l, from_num(2));
  ls_sorted_insert(l, from_num(1));
  ls_sorted_insert(l, from_num(4));
  ls_sorted_insert(l, from_num(5));
  ls_sorted_insert(l, from_num(20));
  ls_sorted_insert(l, from_num(100));

  bool found;
  test_assert(ls_sorted_find(l, from_num(1), &found), 0);
  test_assert(found, true);
  test_assert(ls_sorted_find(l, from_num(2), &found), 1);
  test_assert(found, true);
  test_assert(ls_sorted_find(l, from_num(3), &found), 2);
  test_assert(found, true);
  test_assert(ls_sorted_find(l, from_num(4), &found), 3);
  test_assert(found, true);
  test_assert(ls_sorted_find(l, from_num(5), &found), 4);
  test_assert(found, true);
  test_assert(ls_sorted_find(l, from_num(20), &found),5);
  test_assert(found, true);
  test_assert(ls_sorted_find(l, from_num(100), &found), 6);
  test_assert(found, true);

  for (size_t i = 0; i < 100; ++i) {
    bool found = false;
    ls_sorted_find(l, from_num(i + 1000), &found);
    test_assert(found, false);
  }

  return OK;
}

static test_info tests[] = {
  TEST(mk_init),
  TEST(append),
  TEST(unbox_char),
  TEST(unbox_num),
  TEST(unbox_pair),
  TEST(box_heterogenous),
  TEST(meta),
  TEST(len),
  TEST(set_get),
  TEST(get_immutable),
  TEST(pop),
  TEST(isimmutable),
  TEST(shapeimmutable),
  TEST(at),
  TEST(cmp),
  TEST(copy),
  TEST(slice),
  TEST(extend),
  TEST(insert),
  TEST(insert_sorted),
  TEST(sorted_find),
};

TEST_SUITE(tests, "list.c")

#endif
