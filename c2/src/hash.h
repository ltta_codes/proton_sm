#pragma once

#include "cell.h"

typedef size_t hash_t;

typedef hash_t (*hash_fn)(cell c);

hash_t hash(cell c);
hash_t hash_list(list *l);
hash_t hash_combine(hash_t h1, hash_t h2);
