#include "cell.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "assoc.h"
#include "error.h"
#include "list.h"
#include "mm.h"
#include "sym.h"
#include "util.h"

cell nil = (cell) { .ty = t_nil };
cell _void = (cell) { .ty = t_void };

static inline cell deassoc(cell c) {
  if (c.ty == t_assoc ) { return deassociate(c); }
  return c;
}

cell_type celltype(cell c) {
  c = deassoc(c);
  return c.ty;
}

cell typeof(cell c) {
  c = deassoc(c);
  if (c.ty == t_list && c.l->has_meta) {
    return ls_getmeta(c.l);
  }
  return ty_to_sym(c.ty);
}

bool isa(cell c, cell type) {
  assert(type.ty == t_sym);
  c = deassoc(c);
  return sym_eq(typeof(c), type);
}

bool is_assoc(cell c) {
  return c.ty == t_assoc;
}

cell from_cstr(const char *s) {
  size_t len = strlen(s);
  list *lres = ls_mk_init(len + 1, len + 1, s_char, s, true, true, ct_str);
  return from_list(lres);
}

cell from_num(num n) {
  return (cell) { .ty = t_num, .n = n };
}

cell from_list(list *l) {
  return (cell) { .ty = t_list, .l = l };
}

cell from_ptr(void *p) {
  return (cell) { .ty = t_ptr, .ptr = p };
}

cell from_fn(fn *f) {
  return (cell) { .ty = t_fn, .f = f };
}

cell from_method(mmethod *m) {
  return (cell) { .ty = t_method, .mm = m };
}

cell from_assoc(assoc *a) {
  return (cell) { .ty = t_assoc, .a = a };
}

num to_num(cell c) {
  c = deassoc(c);
  eassert(c.ty == t_num, "TypeError", "Expected numeric type.");
  return c.n;
}

const char *to_cstr(cell c) {
  c = deassoc(c);
  eassert(is_str(c), "TypeError", "Expected string (char list).");
  return (char *) ls_at(c.l, 0);
}

list *to_list(cell c) {
  c = deassoc(c);
  eassert(c.ty == t_list, "TypeError", "Expected list");
  return c.l;
}

void *to_ptr(cell c) {
  c = deassoc(c);
  eassert(c.ty == t_ptr, "TypeError", "Expected pointer");
  return c.ptr;
}

mmethod *to_method(cell c) {
  c = deassoc(c);
  eassert(c.ty == t_method, "TypeError", "Expected method");
  return c.mm;
}

fn *to_fn(cell c) {
  c = deassoc(c);
  eassert(c.ty == t_fn, "TypeError", "Expected fn");
  return c.f;
}

partial *to_partial(cell c) {
  c = deassociate(c);
  assert(celltype(c) == t_partial);
  return c.p;
}

static int ptrdiff_cmp(void *a, void *b) {
  ptrdiff_t diff = (ptrdiff_t) a - (ptrdiff_t) b;
  if (diff == 0) { return 0; }
  return (diff < 0) ? 1 : -1;
}

// Sign(return) indicates "side" of larger value.
// Returns > 0 if a less than b, 0 if a == b, < 0 if b less than a.
int cell_cmp(cell a, cell b) {
  a = deassoc(a);
  b = deassoc(b);
  cell_type ct1 = celltype(a),
            ct2 = celltype(b);

  if (ct1 != ct2) { return (int) ct1 - (int) ct2; }
  switch (ct1) {
    case t_num:
      return b.n - a.n;
    case t_sym:
      return ptrdiff_cmp(a.l, b.l);
    case t_list:
      return ls_cmp(a.l, b.l);
    case t_assoc:
      assert(false && "Cmp: Assocs should be resolved here");
      return -1;
    case t_partial:
    case t_fn:
    case t_mem:
    case t_ptr:
    case t_method:
      return ptrdiff_cmp(a.m, b.m);
    case t_nil:
      return 0;
    case t_void:
      return 0;
  }
}

#ifndef __MEM_SAVE__

static const char * celltypes[t_ptr + 1] = {
  [t_num] = "num",
  [t_sym] = "sym",
  [t_list] = "list",
  [t_mem] = "mem",
  [t_assoc] = "assoc",
  [t_fn] = "fn",
  [t_method] = "method",
  [t_partial] = "partial",
  [t_nil] = "nil",
  [t_void] = "void",
  [t_ptr] = "ptr",
};

const char *celltype_to_str(cell_type t) {
  return celltypes[t];
}

#endif

bool cell_print_assocs = false;

extern char *partial_print(partial *p, char *ex);
extern char *mm_print(mmethod *p, char *ex);
extern char *fn_print(fn *p, char *ex);
static char *cell_printi(cell c, char *ex) {
  switch (c.ty) {
    case t_num:
      return esprintf(ex, "%d", c.n);
    case t_sym:
      return esprintf(ex, "%s", sym_to_cstr(c));
    case t_nil:
      return esprintf(ex, "nil");
    case t_void:
      return esprintf(ex, "void");
    case t_list:
      return ls_print(c.l, ex);
    case t_fn:
      return fn_print(c.f, ex);
    case t_method:
      return mm_print(c.mm, ex);
    case t_mem:
    case t_ptr:
      return esprintf(ex, "<mem %p>", c.m);
    case t_assoc:
      if (cell_print_assocs) {
        ex = esprintf(ex, "<assoc ");
        ex = assoc_print(c, ex);
        ex = esprintf(ex, " -> ");
        ex = cell_printi(deassociate(c), ex);
        return esprintf(ex, ">");
      } else {
        return cell_printi(deassociate(c), ex);
      }
    case t_partial:
      return partial_print(c.p, ex);
  }
  assert(false && "Implementation error");
  return NULL;
}

char *cell_print(cell c, char *ex) {
  return cell_printi(deassoc(c), ex);
}

void cell_printd(cell c) {
  char *s = cell_printi(c, NULL);
  printf("%s\n", s);
  free(s);
}

bool is_nil(cell c) {
  c = deassoc(c);
  return c.ty == t_nil;
}

bool is_void(cell c) {
  c = deassoc(c);
  return c.ty == t_void;
}

bool is_set(cell c) {
  c = deassoc(c);
  return !(c.ty == t_void || c.ty == t_nil);
}

bool is_str(cell c) {
  c = deassoc(c);
  return c.ty == t_list && ls_is_str(to_list(c));
}

bool is_tuple(cell c) {
  c = deassoc(c);
  return c.ty == t_list && sym_eq(ls_getmeta(to_list(c)), ct_tuple);
}

void cell_gc_mark(struct gc_state *gs, cell c) {
  assert(false && "TODO");
}

void cell_gc_mark_globals(struct gc_state *gs) {
  assert(false && "TODO");
}

