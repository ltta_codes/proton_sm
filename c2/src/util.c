#include "util.h"

#include <errno.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
#define POSIX
#endif

#ifdef POSIX
  #include <libgen.h>
  #include <unistd.h>
#else
  #error TODO: Implement non-POSIX dirname.
#endif

#include "cell.h"
#include "list.h"

static inline void bitfield_index(size_t i, size_t *w, size_t *wi) {
  *w = i / BITFIELD_WORD_BITS;
  *wi = i - (*w * BITFIELD_WORD_BITS);
}

void bitfield_init(bitfield *bt, bool val) {
  memset(bt->words, (val) ? ~((bitfield_word) 0) : ((bitfield_word) 0),
      BITFIELD_WORDS(bt->len) * sizeof(bitfield_word));
}

bool bitfield_get(bitfield *bt, size_t index) {
  assert(index < bt->len);

  bitfield_word word_idx, bit_idx;
  bitfield_index(index, &word_idx, &bit_idx);
  return bt->words[word_idx] & (1 << bit_idx);
}

static inline void bitfield_set_word(bitfield *bf, size_t word_idx,
    bitfield_word mask, bool val) {
  if (val) {
    bf->words[word_idx] |= mask;
  } else {
    bf->words[word_idx] &= ~mask;
  }
}

void bitfield_set(bitfield *bt, size_t index, bool val) {
  assert(index < bt->len);
  bitfield_word word_idx, bit_idx;
  bitfield_index(index, &word_idx, &bit_idx);

  bitfield_word mask = 1 << bit_idx;
  bitfield_set_word(bt, word_idx, mask, val);
}

void bitfield_set_range(bitfield *bt, size_t start, size_t end, bool val) {
  assert(start < bt->len && end < bt->len);
  bitfield_word start_word, start_bit;
  bitfield_index(start, &start_word, &start_bit);
  bitfield_word end_word, end_bit;
  bitfield_index(end, &end_word, &end_bit);

  if (start_word == end_word) {
    bitfield_word word =
        (~((bitfield_word) 0) >> (BITFIELD_WORD_BITS - end_bit))
          & ~(((bitfield_word) 1 << start_bit) - 1);
    bitfield_set_word(bt, start_word, word, val);
  } else {
    bitfield_word word = (~((bitfield_word) 0) << start_bit);
    bitfield_set_word(bt, start_word, word, val);
    for (size_t i = start_word + 1; i < end_word; ++i) {
      bitfield_set_word(bt, i, ~(bitfield_word) 0, val);
    }
    word = (~((bitfield_word) 0) >> (BITFIELD_WORD_BITS - end_bit));
    bitfield_set_word(bt, end_word, word, val);
  }
}

void bitfield_union(bitfield *a, bitfield *b) {
  assert(a->len == b->len);
  for (size_t i = 0; i < BITFIELD_WORDS(a->len); ++i) {
    a->words[i] |= b->words[i];
  }
}

void bitfield_intersect(bitfield *a, bitfield *b) {
  assert(a->len == b->len);
  for (size_t i = 0; i < BITFIELD_WORDS(a->len); ++i) {
    a->words[i] &= b->words[i];
  }
}

char *bitfield_print(bitfield *b, char *ex) {
  ex = esprintf(ex, "[!");
  size_t words = BITFIELD_WORDS(b->len);
  const bitfield_word last_bit = 1 << (sizeof(bitfield_word) * 8 - 1);
  for (size_t i = 1; i <= words; ++i) {
    bitfield_word w = b->words[words - i];
    if (!w) { continue; }
    size_t j = 0;
    while (j < sizeof(bitfield_word) * 8) {
      if (j++ % 8 == 0) { ex = esprintf(ex, "."); }
      ex = esprintf(ex, (w & last_bit) ? "1" : "0"); 
      w <<= 1;
    }
  }
  return esprintf(ex, "!]");
}

cell pair_mk(cell fst, cell snd) {
  cell cells[] = { fst, snd };
  list *p = ls_mk_init(2, 2, s_any, cells, true, true, nil);
  return from_list(p);
}

void pair_init(list *l, cell data[2]) {
  ls_init_data(l, 2, s_any, data);
}

bool is_pair(list *l) {
  return is_nil(ls_getmeta(l)) && ls_len(l) == 2 && ls_is_immutable(l);
}

bool is_application(cell c) {
  return celltype(c) == t_list && sym_eq(ls_getmeta(to_list(c)), ct_appl);
}

const int debruijn_bit_position[32] = {
  0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
  31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
};

char *get_cwd(const char *main_file) {
#ifdef POSIX
  return getcwd(NULL, 0);
#else
# error TODO: Implement non-POSIX dirname.
#endif
}

void set_cwd(const char *path) {
#ifdef POSIX
  chdir(path);
#else
# error TODO: Implement non-POSIX dirname.
#endif
}

void set_cwd_fn(const char *main_file) {
#ifdef POSIX
  char *fn = strdup(main_file);
  const char *path = dirname(fn);
  chdir(path);
  free(fn);
#else
# error TODO: Implement non-POSIX dirname.
#endif
}

num str_to_num(const char *s, int *ret) {
  char *end;
  intmax_t i = strtoimax(s, &end, 0);
  *ret = i;
  if (end && *end == '\0') {
    if (errno == ERANGE) {
      printf("WARNING: Integer constant overflows valid range.");
      return false;
    }
    return true;
  }
  return false;
}

char *vesprintf(char *s, const char *fmt, va_list args) {
  va_list argsc;
  va_copy(argsc, args);
  int len = vsnprintf(NULL, 0, fmt, argsc);
  va_end(argsc);

  size_t slen = s == NULL ? 0 : strlen(s);
  s = realloc(s, slen + len + 1);
  vsnprintf(&s[slen], len + 1, fmt, args);
  return s;
}

char *esprintf(char *s, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  char *res = vesprintf(s, fmt, args);
  va_end(args);
  return res;
}

bool str_to_long(char *s, long *res) {
  char *e = NULL;
  *res = strtol(s, &e, 10);
  if (e && *e == '\0') {
    return true;
  }
  return false;
}

#include "cell.h"

#ifdef VERBOSE
#include <stdarg.h>

void verbose_lp(void *l) {
  char *s = ls_print(l, NULL);
  printf("%s", s);
  free(s);
}

void verbose_cp(cell c) {
  char *s = cell_print(c, NULL);
  printf("%s", s);
  free(s);
}

cell verbose_cpl(cell c) {
  char *s = cell_print(c, NULL);
  printf("%s\n", s);
  free(s);
  return c;
}

#endif

// GDB/LLDB debugging short symbols.

void _cp(cell c) {
  char *s = cell_print(c, NULL);
  printf("%s\n", s);
  fflush(0);
  free(s);
}

void _lp(list *t) {
  char *s = ls_print(t, NULL);
  printf("%s\n", s);
  fflush(0);
  free(s);
}

extern char *partial_print(partial *p, char *ex);
void _pp(partial *p) {
  char *s = partial_print(p, NULL);
  printf("%s\n", s);
  free(s);
  fflush(0);
}

#ifdef TESTING

#define TEST_SUITE_INIT gc_init(); symtab_init(NULL);
#define TEST_SUITE_FIN symtab_finalize(); gc_finalize();

#include "test.h"

static int test_pair() {
  cell p = pair_mk(from_num(1), from_num(2));
  list *lp = to_list(p);
  test_assert(to_num(ls_get(lp, 0)), 1);
  test_assert(to_num(ls_get(lp, 1)), 2);
  test_assert(ls_is_immutable(lp), true);
  test_assert(ls_len(lp), 2);
  test_assert(is_pair(lp), true);
 return OK;
}

static int test_bitfield() {
#define BF_LEN (4 * BITFIELD_WORD_BITS + 10)
  bitfield *a = bitfield_onstack(BF_LEN),
           *b = bitfield_onstack(BF_LEN);
  bitfield_set(a, 0, 1);
  bitfield_set(a, BF_LEN - 1, 1);
  bitfield_set(b, 0, 1);
  bitfield_set(b, 1, 1);
  bitfield_set(b, 2, 1);
  bitfield_set(b, BF_LEN - 1, 1);

  test_assert(bitfield_get(a, 0), 1);
  test_assert(bitfield_get(a, BF_LEN - 1), 1);
  for (size_t i = 1; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(a, i), 0);
  }
  test_assert(bitfield_get(b, 0), 1);
  test_assert(bitfield_get(b, 1), 1);
  test_assert(bitfield_get(b, 2), 1);
  test_assert(bitfield_get(b, BF_LEN - 1), 1);
  for (size_t i = 3; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(b, i), 0);
  }
  return OK;
#undef BF_LEN
}

static int test_bitfield_union() {
#define BF_LEN (4 * BITFIELD_WORD_BITS + 10)
  bitfield *a = bitfield_onstack(BF_LEN),
           *b = bitfield_onstack(BF_LEN);
  bitfield_set(a, 0, 1);
  bitfield_set(a, BF_LEN - 1, 1);
  bitfield_set(b, 0, 1);
  bitfield_set(b, 1, 1);
  bitfield_set(b, 2, 1);
  bitfield_set(b, BF_LEN - 1, 1);

  bitfield_union(a, b);

  test_assert(bitfield_get(a, 0), 1);
  test_assert(bitfield_get(a, 1), 1);
  test_assert(bitfield_get(a, 2), 1);
  test_assert(bitfield_get(a, BF_LEN - 1), 1);
  for (size_t i = 3; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(a, i), 0);
  }
  return OK;
#undef BF_LEN
}

static int test_bitfield_intersect() {
#define BF_LEN (4 * BITFIELD_WORD_BITS + 10)
  bitfield *a = bitfield_onstack(BF_LEN),
           *b = bitfield_onstack(BF_LEN);
  bitfield_set(a, 0, 1);
  bitfield_set(a, BF_LEN - 1, 1);
  bitfield_set(b, 0, 1);
  bitfield_set(b, 1, 1);
  bitfield_set(b, 2, 1);
  bitfield_set(b, BF_LEN - 1, 1);

  bitfield_intersect(a, b);

  test_assert(bitfield_get(a, 0), 1);
  test_assert(bitfield_get(a, BF_LEN - 1), 1);
  for (size_t i = 1; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(a, i), 0);
  }
  return OK;
#undef BF_LEN
}

static int test_bitfield_set_range() {
#define BF_LEN (4 * BITFIELD_WORD_BITS + 10)
  bitfield *a = bitfield_onstack(BF_LEN);
  bitfield_set_range(a, 4, BITFIELD_WORD_BITS / 2, true);
  for (size_t i = 0; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(a, i), i > 3 && i < BITFIELD_WORD_BITS / 2);
  }

  bitfield_set_range(a, 4, BF_LEN / 4, true);
  for (size_t i = 0; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(a, i), i > 3 && i < BF_LEN / 4);
  }

  bitfield_set_range(a, 0, BF_LEN / 4, true);
  for (size_t i = 0; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(a, i), i < BF_LEN / 4);
  }

  bitfield_set_range(a, BF_LEN / 4 * 3, BF_LEN - 1, true);
  for (size_t i = 0; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(a, i), i < BF_LEN / 4 || i >= BF_LEN / 4 * 3);
  }

  bitfield_set_range(a, 0, BF_LEN / 4, false);
  for (size_t i = 1; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(a, i), i >= BF_LEN / 4 * 3);
  }

  bitfield_set_range(a, BF_LEN / 4 * 3, BF_LEN - 1, false);
  for (size_t i = 0; i < BF_LEN - 1; ++i) {
    test_assert(bitfield_get(a, i), 0); 
  }
  return OK;
#undef BF_LEN
}

define_ctz_nobuiltin(uint64_t)
static int test_ctz_nobuiltin() {
  test_assert(ctz_uint64_t(0x100000000), 32);
  test_assert(ctz_uint64_t(0x200000000), 33);
  test_assert(ctz_uint64_t(0x300000000), 32);
  test_assert(ctz_uint64_t(0x400000000), 34);
  test_assert(ctz_uint64_t(0x0), 64);
  test_assert(ctz_uint64_t(0x1), 0);
  test_assert(ctz_uint64_t(0x2), 1);
  test_assert(ctz_uint64_t(0x3), 0);
  test_assert(ctz_uint64_t(0x4), 2);
  test_assert(ctz_uint64_t(0x5), 0);
  test_assert(ctz_uint64_t(0xFFFFFFFFFFFFFFFF), 0);
  return OK;
}

define_clz_nobuiltin(uint64_t)
static int test_clz_nobuiltin() {
  test_assert(clz_uint64_t(0x1), 63);
  test_assert(clz_uint64_t(0x2), 62);
  test_assert(clz_uint64_t(0x3), 62);
  test_assert(clz_uint64_t(0x4), 61);
  test_assert(clz_uint64_t(0x0), 64);
  test_assert(clz_uint64_t(0x10000), 47);
  test_assert(clz_uint64_t(0x20000), 46);
  test_assert(clz_uint64_t(0x30000), 46);
  test_assert(clz_uint64_t(0x40000), 45);
  test_assert(clz_uint64_t(0x50000), 45);
  test_assert(clz_uint64_t(0x60000), 45);
  return OK;
}

static test_info tests[] = {
  TEST(pair),
  TEST(bitfield),
  TEST(bitfield_set_range),
  TEST(bitfield_union),
  TEST(bitfield_intersect),
  TEST(ctz_nobuiltin),
  TEST(clz_nobuiltin),
};

TEST_SUITE(tests, "util.c")

#endif
