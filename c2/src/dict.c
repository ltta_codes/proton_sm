#include "dict.h"

#include <string.h>

#include "cell.h"
#include "error.h"
#include "hash.h"
#include "types.h"
#include "util.h"

#define LOAD_FACTOR 0.7

typedef enum { ef_load, ef_end } dict_extra_fields;


#define DEFAULT_DICT_SZ 8     // Needs to be a power of 2.
list *dict_mk() {
  list *res = ls_mk_init(0, DEFAULT_DICT_SZ + ef_end, s_pair,
      NULL, false, false, ct_dict);
  res->shape_immutable = true;
  res->len = DEFAULT_DICT_SZ;
  assert(res->unboxed && res->entry_type == s_pair);
  for (size_t i = 0; i < DEFAULT_DICT_SZ; ++i) {
    *((cell *) ls_at(res, i)) = _void;
  }
  return res;
}

static inline void assert_dict(list *d) {
  eassert(is_dict(d), "DictError", "Dict: not a dictionary");
}

void *ls_at_unchecked(list *l, size_t index);
void ls_mut_buf_realloc(list *l, size_t nelems, bool has_meta);

static inline size_t dict_probe(list *d, cell k, hash_t hash, bool *found) {
  size_t len = ls_len(d),
         slot = hash % len;
  if (found) { *found = false; }
  for (size_t i = 0; i < len; ++i) {
    size_t ki = (slot + i) % len;
    cell *v = ls_at_unchecked(d, ki);
    if (is_void(*v)) { return ki; }
    if (cell_cmp(*v, k) == 0) {
      if (found) { *found = true; }
      return ki;
    }
  }
  return 0;
}

static cell *dict_extrafield(list *l, dict_extra_fields f) {
  assert(f < ef_end);
  assert(l->unboxed && l->entry_type == s_pair);
  return ls_at_unchecked(l, ls_alloc_len(l) - ef_end + f);
}

static size_t *dict_load(list *d) {
  return (size_t *) &(dict_extrafield(d, ef_load)->n);
}

static inline void dict_insert(list *d, cell k, cell v, hash_t hash,
    bool *overwritten) {
  size_t slot = dict_probe(d, k, hash, overwritten);
  //char *s = cell_print(k, NULL), *t = cell_print(* (cell *)ls_at_unchecked(d, slot), NULL);
  //printf("DD: Inserting %s at %zu, prev %s\n", s, slot, t);
  //free(s); free(t);
  assert(d->entry_type == s_pair);
  *((cell *) ls_at_unchecked(d, slot)) = k;
  *((cell *) ls_at_unchecked(d, slot) + 1) = v;
}

void dict_rehash(list *d, size_t new_sz) {
  list d_old = *d;
  d_old.data = d->data;
  cell meta = ls_getmeta(d);

  d->data = NULL;
  ls_mut_buf_realloc(d, new_sz + ef_end, d->has_meta);
  assert(d->unboxed && d->entry_type == s_pair);
  d->len = new_sz;
  for (size_t i = 0; i < new_sz; ++i) {
    void *p = ls_at_unchecked(d, i);
    *(cell *) p = _void;
    *((cell *) p + 1) = _void;
  }
  for (size_t i = 0; i < d_old.len; ++i) {
    cell *ov = ls_at_unchecked(&d_old, i);
    if (ov->ty != t_void) {
      dict_insert(d, *ov, *(ov + 1), hash(*ov), NULL);
    }
  }
  if (d->has_meta) { ls_setmeta(d, meta); }
}

list *dict_set_hashed(list *d, cell k, cell v, hash_t hash) {
  assert_dict(d);
  size_t *load = dict_load(d);
  size_t len = ls_len(d);
  if ((double) *load > ((double) len) * LOAD_FACTOR) {
    dict_rehash(d, 2 * len);
  }

  bool found = false;
  dict_insert(d, k, v, hash, &found);
  if (!found) { *dict_load(d) = *load + 1; }
  return d;
}

list *dict_set(list *d, cell k, cell v) {
  return dict_set_hashed(d, k, v, hash(k));
}

cell *dict_get_hashed(list *d, cell k, hash_t h) {
  assert_dict(d);
  bool found = false;
  size_t slot = dict_probe(d, k, h, &found);
  if (!found) { return NULL; }
  assert(d->entry_type == s_pair && d->unboxed);
  return ((cell *) ls_at_unchecked(d, slot)) + 1;
}

cell *dict_get(list *d, cell k) {
  return dict_get_hashed(d, k, hash(k));
}

bool dict_contains(list *d, cell k) {
  return dict_get(d, k) != NULL;
}

bool is_dict(list *d) {
  return d->entry_type == s_pair;
}

size_t dict_len(list *d) {
  return *dict_load(d);
}

dict_it dict_iter(dict *d) {
  dict_it i = (dict_it) { .i = SIZE_MAX, .d = d };
  dict_itnext(&i);
  return i;
}

bool dict_itend(dict_it i) {
  return i.i >= ls_len(i.d);
}

void dict_itnext(dict_it *i) {
  while (++(i->i) < ls_len(i->d)) {
    list *p = to_list(ls_get(i->d, i->i));
    if (!is_void(ls_get(p, 0))) { return; } 
  }
}

cell dict_itval(dict_it i) {
  return ls_get(i.d, i.i);
}

char *dict_print(dict *d, char *ex) {
  ex = esprintf(ex, "{{");
  for (dict_it i = dict_iter(d); !dict_itend(i); dict_itnext(&i)) {
    cell v = dict_itval(i);
    ex = cell_print(v, ex);
    ex = esprintf(ex, ", ");
  }
  return esprintf(ex, "}}");
}

// Debugging shortcut.
void _dp(dict *d) {
  char *r = dict_print(d, NULL);
  printf("%s\n", r);
  free(r);
}

// TESTS

#ifdef TESTING

// Test Suite init/fin need to be set before including test.h. Hacky :/
#define TEST_SUITE_INIT gc_init();
#define TEST_SUITE_FIN gc_finalize(); 

#include "test.h"


#ifdef DEBUG
# define M 100
# define N 1000
#else
# define M 100
# define N 10000
#endif

static int test_setget() {
  for (size_t m = 0; m < M; ++m) {
    dict *d = dict_mk();
    for (size_t i = 0; i < N; ++i) {
      dict_set(d, from_num(i), from_num(i + 1));
    }

    test_assert(dict_len(d), N);

    for (size_t i = 0; i < N; ++i) {
      test_assert(to_num(*dict_get(d, from_num(i))), i + 1);
    }
  }
  return OK;
}

static int test_contains() {
  for (size_t m = 0; m < M; ++m) {
    dict *d = dict_mk();
    for (size_t i = 0; i < N; ++i) {
      dict_set(d, from_num(i), from_num(i + 1));
    }

    test_assert(dict_len(d), N);

    for (size_t i = 0; i < N; ++i) {
      test_assert(dict_contains(d, from_num(i)), true);
    }
    for (size_t i = N; i < 10 * N; ++i) {
      test_assert(dict_contains(d, from_num(i)), false);
    }
  }
  return OK;
}

#include "time.h"
static int test_rand() {
  for (size_t m = 0; m < M; ++m) {
    dict *d = dict_mk();
    list *l = ls_mk();
    srand(time(NULL));
    for (size_t i = 0; i < N; ++i) {
      cell n = from_num(rand() - (RAND_MAX >> 1));
      ls_append(l, n); 
      dict_set(d, n, from_num(i));
    }
    for (size_t i = 0; i < N; ++i) {
      cell n = ls_get(l, i);
      test_assert(to_num(*dict_get(d, n)), i);
      test_assert(dict_contains(d, n), true);
    }
  }
  return OK;
}

static test_info tests[] = {
  TEST(setget),
  TEST(contains),
  TEST(rand),
};

TEST_SUITE(tests, "dict.c")

#endif
