#pragma once

#include <stdarg.h>
#include <stdbool.h>

#include "cell.h"
#include "error.h"

// UTILITY TYPES AND FUNCTIONS

// Util Functions.

static inline cell tc(cell c, cell_type t) {
  // TODO: error messages.
  eassert(celltype(c) == t, "TypeError", "Unexpected type.");
  return c;
}

// https://stackoverflow.com/questions/3982348/implement-generic-swap-macro-in-c
#define swap(x,y) do                                                           \
   { unsigned char swap_temp[sizeof(x) == sizeof(y) ? (signed)sizeof(x) : -1]; \
     memcpy(swap_temp, &y, sizeof(x));                                         \
     memcpy(&y, &x, sizeof(x));                                                \
     memcpy(&x, swap_temp, sizeof(x));                                         \
    } while(0)

#define swap_ptr(x,y) do                              \
   {                                                  \
     unsigned char swap_temp[sizeof(*x) == sizeof(*y) \
         ? (signed)sizeof(*x) : -1];                  \
     memcpy(swap_temp, y, sizeof(*x));                \
     memcpy(y, x, sizeof(*x));                        \
     memcpy(x, swap_temp, sizeof(*x));                \
    } while(0)

// https://embeddedgurus.com/state-space/2014/09/fast-deterministic-and-portable-counting-leading-zeros/
static inline size_t clz32(uint32_t x) {
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  x -= ((x >> 1) & 0x55555555U);
  x = (x & 0x33333333U) + ((x >> 2) & 0x33333333U);
  return 32U - ( (((x + (x >> 4)) & 0x0F0F0F0FU) * 0x01010101U) >> 24 );
}

extern const int debruijn_bit_position[32];
static inline size_t ctz32(uint32_t v) {
  return debruijn_bit_position[((uint32_t)((v & -v) * 0x077CB531U)) >> 27];
}

#define define_clz_nobuiltin(type)                                            \
static inline size_t clz_##type(type t) {                                     \
  size_t r = 0;                                                               \
  for (size_t i = 1; i <= sizeof(type) / sizeof(uint32_t); ++i) {             \
    uint32_t v = t << ((sizeof(type) * 8) - i * sizeof(uint32_t) * 8);        \
    if (v) { return r + clz32(v); }                                           \
    r += sizeof(uint32_t) * 8;                                                \
  }                                                                           \
  return r;                                                                   \
}

#if  __has_builtin(__builtin_clz)
#  define define_clz(type)                                                    \
    static inline size_t clz_##type(type t) { return __builtin_clz(t); } 
#else
#  define define_clz(type) define_clz_nobuiltin(type)
#endif

// https://graphics.stanford.edu/~seander/bithacks.html#ZerosOnRightMultLookup
#define define_ctz_nobuiltin(type)                                            \
static inline size_t ctz_##type(type t) {                                     \
  size_t r = 0;                                                               \
  for (size_t i = 0; i < sizeof(type) / sizeof(uint32_t); ++i) {              \
    uint32_t v = t >> (i * sizeof(uint32_t) * 8);                             \
    if (v) { return r + ctz32(v); }                                           \
    r += sizeof(uint32_t) * 8;                                                \
  }                                                                           \
  return r;                                                                   \
}

#if  __has_builtin(__builtin_ctz)
#  define define_ctz(type)                                                    \
    static inline size_t ctz_##type(type t) { return __builtin_ctz(t); } 
#else
#  define define_ctz(type) define_ctz_nobuiltin(type)
#endif

// BIT FIELD.

typedef uintptr_t bitfield_word;
typedef size_t bitfield_length;
typedef struct {
  size_t len;
  bitfield_word words[];
} bitfield;

#define BITFIELD_WORD_BITS (sizeof(bitfield_word) * 8)
#define BITFIELD_WORDS(bits) \
  ((bits + BITFIELD_WORD_BITS) / BITFIELD_WORD_BITS)

// Hackage, similar to how GCC allows struct flexible member initialization
// by treating it effectively as two structs declared one after the other.
#define bitfield_onstack(bits)  ((bitfield *)                              \
  &((struct { bitfield_length l; bitfield_word w[BITFIELD_WORDS(bits)]; }) \
    { .l = bits, .w = { 0 }  }))


void bitfield_init(bitfield *bt, bool val);
bool bitfield_get(bitfield *bt, size_t index);
void bitfield_set(bitfield *bt, size_t index, bool val);
void bitfield_set_range(bitfield *bt, size_t start, size_t end, bool val);
void bitfield_union(bitfield *a, bitfield *b);
void bitfield_intersect(bitfield *a, bitfield *b);
char *bitfield_print(bitfield *b, char *ex);

// PAIR.

cell pair_mk(cell fst, cell snd);
void pair_init(list *l, cell data[2]);
bool is_pair(list *l);

bool is_application(cell c);

char *get_cwd();
void set_cwd_fn(const char *from_filename);
void set_cwd(const char *path);
bool str_to_long(char *s, long *res);
int str_to_num(const char *s, int *ret);

char *vesprintf(char *s, const char *fmt, va_list args);
char *esprintf(char *s, const char *fmt, ...);

#define fst(x) (celltype(x) == t_list ? ls_get(to_list(x), 0) : (x))
#define nth(l, x) (ls_get(to_list(l), x))
#define snd(x) (nth(x, 1)) 

#define VERBOSE
#ifdef VERBOSE

void verbose_lp(void *l);
struct cell;
void verbose_cp(struct cell c);
struct cell verbose_cpl(struct cell c);

#include <stdio.h>
# define verbose_sp(...) printf(__VA_ARGS__)
#else
# define verbose_lp(...)
# define verbose_cp(...)
# define verbose_sp(...)
# define verbose_cpl(c) (c)
#endif

