#include "assoc.h"

#include "list.h"
#include "error.h"
#include "hash.h"
#include "util.h"

#ifdef ENSURE
# define ensure(...) assert(__VA_ARGS__)
#else
# define ensure(...)
#endif

typedef struct {
  union {
    struct {
      bool is_multival: 1;
      hash_t hash: sizeof(hash_t) - 1;
    } hash;
    hash_t hash_val;
  };
} schema_hash;

typedef struct {
  schema_hash hash;
  list *keys;
} schema;

struct assoc {
  schema_hash sch_hash;
  cell ref;
};

typedef struct {
  assoc base;
  cell val;
} assoc_singleval;

typedef struct {
  assoc base;
  schema *schema;
  size_t len;
  cell vals[];
} assoc_multival;

static dict *schemas;

static schema_hash hash_schema(list *schema) {
  return (schema_hash) {
    .hash = {
      .hash = hash_list(schema),
      .is_multival = ls_len(schema) > 1,
    },
  };
}

static void schema_add_key(list *schema) {
  assert(false && "TODO");
}

static schema *schema_for_hash(hash_t hash) {
  cell *c = dict_get(schemas, from_num((int) hash));
  if (!c) { return NULL; }
  return (schema *) to_ptr(*c);
}

static schema *schema_add(list *schema_template) {
  schema *scm = new(schema);
  list *keys = ls_copy_ex(schema_template, true, true, sym("$schema$"));
  *scm = (schema) {
    .hash = hash_schema(keys),
    .keys = keys,
  };
  dict_set(schemas, from_num((int) scm->hash.hash_val), from_ptr(scm));
  return scm;
}

static schema *schema_find(list *schema_template) {
  schema_hash hash = hash_schema(schema_template);
  return schema_for_hash(hash.hash_val);
}

static inline size_t schema_offs(assoc *as, cell k) {
  bool found = false;
  size_t res =
      ls_sorted_find(schema_for_hash(as->sch_hash.hash_val)->keys, k, &found);
  eassert(found, "SchemaKeyError", "Key not found");
  return res;
}

cell associate(cell c, cell name, cell val) {
  assert(celltype(c) != t_assoc);
  cell ls_data[] = { name };
  list l;
  ls_init_data(&l, 1, s_any, &ls_data);

  schema * ex_schema = schema_find(&l);
  if (!ex_schema) { ex_schema = schema_add(&l); }

  assoc_singleval *av = new(assoc_singleval);
  *av = (assoc_singleval) {
    .base = (assoc) {
      .sch_hash = ex_schema->hash,
      .ref = c,
    },
    .val = val,
  };
  return from_assoc((assoc *) av);
}

cell associate_list(cell c, list *ls_schema, list *vals) {
  assert(celltype(c) != t_assoc);
  assert(ls_len(ls_schema) >= 1);
  assert(ls_len(vals) <= ls_len(ls_schema));

  schema *ex_schema = schema_find(ls_schema);
  if (!ex_schema) { ex_schema = schema_add(ls_schema); }

  if (ls_len(ls_schema) > 1) {
    size_t sch_len = ls_len(ex_schema->keys);
    assoc_multival *res = mm_alloc(
        sizeof(assoc_multival) + sizeof(cell) * sch_len);

    *res = (assoc_multival) {
      .base = (assoc) {
        .sch_hash = ex_schema->hash,
        .ref = c,
      },
      .len = sch_len,
    };
    for (size_t i = 0; i < ls_len(vals); ++i) {
      res->vals[i] = ls_get(vals, i);
    }
    for (size_t i = ls_len(vals); i < sch_len; ++i) {
      res->vals[i] = nil;
    }

    return from_assoc((assoc *) res);
  } else {
    return associate(c, ls_get(ls_schema, 0),
        (ls_len(vals) > 0) ? ls_get(vals, 0) : nil);
  }
}

assoc *to_assoc(cell c) {
  eassert(c.ty == t_assoc, "TypeError", "Expected Assoc");
  return c.a;
}

cell deassociate(cell c) {
  if(c.ty != t_assoc) { return c; }
  return to_assoc(c)->ref;
}

cell assoc_geti(assoc *as, size_t offs) {
  assert(offs == 0 || as->sch_hash.hash.is_multival);
  ensure(offs < ls_len(schema_for_hash(as->sch_hash.hash_val)));
  if (as->sch_hash.hash.is_multival) {
    return ((assoc_multival *) as)->vals[offs];
  } else {
    return ((assoc_singleval *) as)->val;
  } 
}

cell assoc_get(assoc *as, cell k) {
  size_t offs = schema_offs(as, k);
  return assoc_geti(as, offs);
}

cell assoc_set(assoc *as, cell k, cell v) {
  size_t offs = schema_offs(as, k);
  return assoc_seti(as, offs, v);
}

cell assoc_seti(assoc *as, size_t offs, cell v) {
  assert(offs == 0 || as->sch_hash.hash.is_multival);
  ensure(offs < ls_len(schema_for_hash(as->sch_hash.hash_val)));
  if (as->sch_hash.hash.is_multival) {
    ((assoc_multival *) as)->vals[offs] = v;
  } else {
    ((assoc_singleval *) as)->val = v;
  } 
  return v;
}

char *assoc_print(cell assoc, char *ex) {
  struct assoc *a = to_assoc(assoc);
  schema *s = schema_for_hash(a->sch_hash.hash_val);
  ex = esprintf(ex, "#{");
  size_t len = ls_len(s->keys);
  for (size_t i = 0; i < len; ++i) {
    cell k = ls_get(s->keys, i),
         v = assoc_geti(a, i);
    ex = cell_print(k, ex);
    ex = esprintf(ex, " -> ");
    ex = cell_print(v, ex);
    if (i < len - 1) { ex = esprintf(ex, ", "); }
  }
  return esprintf(ex, "}#");
}

// TODO: GC unused schemas.

static void gc_mark_schema(gc_state *gs, schema *scm) {
  gc_mark(gs, scm, sizeof(schema));
  gc_push_gray(gs, from_list(scm->keys));
}

void gc_mark_assoc(gc_state *gs, cell c) {
  assoc *a = to_assoc(c);
  gc_mark(gs, a, sizeof(a));
  gc_push_gray(gs, a->ref);
  if (a->sch_hash.hash.is_multival) {
    assoc_multival *mv = (assoc_multival *) a;
    for (size_t i = 0; i < mv->len; ++i) {
      gc_push_gray(gs, mv->vals[i]);
    }
  } else {
    gc_push_gray(gs, ((assoc_singleval *) a)->val);
  }
}

static void gc_mark_mod_assoc(gc_state *gs) {
  if (!schemas) { return; }

  gc_push_gray(gs, from_list(schemas));
  for (size_t i = 0; i < ls_len(schemas); ++i) {
    cell scm = ls_get(schemas, i);
    if (!is_void(scm)) {
      gc_mark_schema(gs, (schema *) to_ptr(ls_get(to_list(scm), 1)));
    }
  }
}

void assoc_init() {
  schemas = dict_mk();
  gc_add_module(gc, gc_mark_mod_assoc);
}

void assoc_finalize() {
  schemas = NULL;
  gc_add_module(gc, NULL);
}

// TESTS

#ifdef TESTING

// Test Suite init/fin need to be set before including test.h. Hacky :/
#define TEST_SUITE_INIT gc_init(); symtab_init(); assoc_init();
#define TEST_SUITE_FIN assoc_finalize(); symtab_finalize(); gc_finalize();

#include "test.h"

#include <string.h>

static int test_assoc_singleval() {
  cell c = from_num(123);
  cell ac = associate(c, from_cstr("origin"), from_cstr("test"));
  test_assert(strcmp(
        to_cstr(assoc_get(to_assoc(ac), from_cstr("origin"))), "test"), 0)
  test_assert(cell_cmp(c, ac), 0);

  return OK;
}

static int test_assoc_multival() {
  cell c = from_num(123);
  list *scm = ls_of(2, from_cstr("key1"), from_cstr("key2")),
       *vals = ls_of(2, from_num(0), from_num(1));
  cell ac = associate_list(c, scm, vals);
  test_assert(to_num(assoc_get(to_assoc(ac), from_cstr("key1"))), 0);
  test_assert(to_num(assoc_get(to_assoc(ac), from_cstr("key2"))), 1);
  test_assert(cell_cmp(c, ac), 0);

  assoc_set(to_assoc(ac), from_cstr("key2"), from_num(2));
  test_assert(to_num(assoc_get(to_assoc(ac), from_cstr("key2"))), 2);

  assoc_set(to_assoc(ac), from_cstr("key1"), from_num(3));
  test_assert(to_num(assoc_get(to_assoc(ac), from_cstr("key1"))), 3);

  assoc_set(to_assoc(ac), from_cstr("key2"), from_num(4));
  test_assert(to_num(assoc_get(to_assoc(ac), from_cstr("key2"))), 4);

  return OK;
}


static test_info tests[] = {
  TEST(assoc_singleval),
  TEST(assoc_multival),
};

TEST_SUITE(tests, "assoc.c")

#endif
