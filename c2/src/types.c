#include "types.h"

#include <assert.h>
#include <string.h>

#include "error.h"
#include "list.h"
#include "sym.h"
#include "util.h"

// TYPE SYSTEM

static dict *type_table;

type_id tid_none; 
type_id tid_any;

type_info *ti_any;

static inline size_t min(size_t a, size_t b) {
  return (a < b) ? a : b;
}

static inline bool typeid_eq_il(type_id a, type_id b) {
  for (size_t i = 0; i < TYPE_ID_WORDS; ++i) {
    if (a.words[i].w != b.words[i].w) { return false; }
  }
  return true;
}

bool typeid_eq(type_id a, type_id b) {
  return typeid_eq_il(a, b);
}

type_id typecomps_to_typeid(type_id_component *tcs, size_t ntcs,
    type_info *ti) {
  type_id id;
  type_id_component *comps = (type_id_component *) id.words;
  // If the type component paths cannot be fit into the mem alloted for type_id
  // then store as many path components as fit into TYPE_ID_WORDS - 1 memory
  // as well as a pointer to type_info into last word.
  if (ntcs * sizeof(type_id_component) > TYPE_ID_WORDS * sizeof(type_id_word)) {
    for (size_t i = 0; i < TYPE_ID_COMP_PER_WORD * (TYPE_ID_WORDS - 1); ++i) {
      comps[i] = tcs[i];
    }
    comps[0] |= 0x1;
    id.words[TYPE_ID_WORDS - 1].p = ti;
  } else {
    // Complete path descriptor fits into type_id memory, mark unused
    // path components.
    size_t max_comps = TYPE_ID_COMP_PER_WORD * TYPE_ID_WORDS;
    size_t ncomps = min(max_comps, ntcs);
    for (size_t i = 0; i < ncomps; ++i) {
      comps[i] = tcs[i];
    }
    for (size_t i = ncomps; i < max_comps; ++i) {
      comps[i] = TYPE_ID_COMP_UNUSED;
    }
  } 
  return id;
}

type_id typeid(cell type_sym) {
  cell *t = &type_sym;
  while (t && (t = dict_get(type_table, *t)) && celltype(*t) == t_sym) {}

  if (t) {
    type_info *ti = to_ptr(*t);
    return typecomps_to_typeid(ti->typeid_comps, ti->typeid_n_components, ti);
  }

  errors("TypeError", "Invalid type %s."); // TODO(errormsg)
  return tid_none;
}

static inline size_t n_comps_to_word_bytes(size_t n_comps) {
  return ((n_comps + TYPE_ID_COMP_PER_WORD - 1) / TYPE_ID_COMP_PER_WORD) 
      * sizeof(type_id_word);
}

static type_id_component *typeid_mk_child(type_info *parent,
    size_t *typeid_n_comps) {
  assert(parent && "Parent cannot be NULL");

  size_t n = ls_len(parent->children) + 1,
         n_parent_comps = parent->typeid_n_components;

  if (typeid_eq_il(parent->id, tid_any)) {
    n_parent_comps = 0;
    n = n << 1;
  }

  // New path spec is parent plus an additional component to encode child's
  // "index" in parent (len(children) + 1).

  // If len(children) value is larger than can fit into single type_id_component
  // then use extension scheme, where a component representing a max value
  // of type_id_component indicates that the next component is still part of
  // current path spec.
  size_t n_comps = n_parent_comps + 1;
  while (n >= TYPE_ID_COMP_EXT_VAL) {
    n = n >> TYPE_ID_COMP_BITS;
    n_comps++;
  }
  if (n == 0) { n = 1; }

  size_t bytes = n_comps_to_word_bytes(n_comps);
  type_id_component *comps = calloc(bytes, 1);
  memcpy(comps, parent->typeid_comps,
      n_parent_comps * sizeof(type_id_component));
  
  // Fill with EXT_VAL (type_id_component with all bits set) and remainder of n.
  for (size_t i = n_parent_comps; i < n_comps - 1; ++i) {
    comps[i] = TYPE_ID_COMP_EXT_VAL;
  }
  comps[n_comps - 1] = n;

  *typeid_n_comps = n_comps;

  return comps;
}

static inline type_info *typeinfo_mk(type_info *super, type_spec *ts,
    size_t tid_n_components, type_id_component *tid_comps) {
  size_t tid_n_bytes = n_comps_to_word_bytes(tid_n_components);
  type_info *nti = (type_info *) mm_alloc(sizeof(type_info) + tid_n_bytes);
  *nti = (type_info) {
    .spec = *ts,
    .id = typecomps_to_typeid(tid_comps, tid_n_components, nti),
    .super = super,
    .children = ls_mk_init(0, 0, s_any, NULL, false, false, nil),
    .typeid_n_components = tid_n_components,
  };
  memcpy(nti->typeid_comps, tid_comps, tid_n_bytes); 
  return nti;
}

type_id type_def(cell type_sym, cell super, type_spec *ts) {
  eassert(celltype(type_sym) == t_sym, "TypeDefError",
      "Invalid type name %s."); // TODO(errormsg)

  eassert(!dict_contains(type_table, type_sym), "TypeDefError",
      "Type %s already defined."); // TODO(errormsg)

  type_info *ti_super = type_resolve(super);
  size_t ti_n_comps;
  type_id_component *ti_comps =
      typeid_mk_child(ti_super, &ti_n_comps);

  type_info *nti = typeinfo_mk(ti_super, ts, ti_n_comps, ti_comps);
  nti->name = type_sym;

  dict_set(type_table, type_sym, from_ptr(nti));

  ls_append(ti_super->children, from_ptr(nti));
  
  free(ti_comps);

  return nti->id; 
}

static bool typeid_is_ext(type_id id) {
  type_id_word w1 = id.words[0];
  return (w1.w & 0x1);
}

static type_info *typeid_get_ext(type_id id) {
  assert(typeid_is_ext(id) && "Expected extended type_id");
  return (type_info *) id.words[TYPE_ID_WORDS - 1].p;
}

static inline type_id_component typeid_comp(type_id id, size_t n) {
  size_t w = n / TYPE_ID_COMP_PER_WORD,
         i = n - w * TYPE_ID_COMP_PER_WORD;
  type_id_component mask = (n == 0) 
      ? (TYPE_ID_COMP_MASK & ~1)
      : TYPE_ID_COMP_MASK;
  type_id_word tw;
  if (typeid_is_ext(id)) {
    if (w < TYPE_ID_WORDS - 1) {
        tw = id.words[w];
    } else {
      type_info *ti = typeid_get_ext(id);
      if (n >= ti->typeid_n_components) { return 0; }
      return ti->typeid_comps[n];
    }
  } else {
    if (w >= TYPE_ID_WORDS) { return 0; }
    tw = id.words[w];
  }
  type_id_component *comps = (type_id_component *) &tw.w;
  return comps[i] & mask;
}

define_clz(type_id_word_int)

static inline size_t typeid_ncomps(type_id id) {
  if (typeid_is_ext(id)) {
    return typeid_get_ext(id)->typeid_n_components;
  }
  size_t i = 0;
  while (i <= TYPE_ID_WORDS && id.words[TYPE_ID_WORDS - i - 1].w == 0) { ++i; }
  size_t sz = (TYPE_ID_WORDS - i) * sizeof(type_id_word_int) * 8;
  sz -= clz_type_id_word_int(id.words[TYPE_ID_WORDS - i - 1].w);
  return (sz + TYPE_ID_COMP_BITS - 1) / TYPE_ID_COMP_BITS;
}

type_id typeid_super(type_id id) {
  size_t ncomps = typeid_ncomps(id);
  if (typeid_eq(id, tid_any)) { return tid_none; }
  if (ncomps == 1) { return tid_any; }
  if (typeid_is_ext(id)) {
    return typeid_get_ext(id)->super->id;
  } else {
    assert(ncomps / TYPE_ID_COMP_PER_WORD < TYPE_ID_WORDS);

    type_id_component *comps = (type_id_component *) id.words;
    comps[--ncomps] = 0;
    while (comps[ncomps - 1] == TYPE_ID_COMP_EXT_VAL) {
      comps[--ncomps] = 0;
    }
    return id;
  }
}

bool type_is_subtype(type_id super, type_id sub) {
  if (typeid_eq_il(super, tid_any)) { return true; }
  size_t ns = typeid_ncomps(super);
  for (size_t i = 0; i < ns; ++i) {
    if (typeid_comp(super, i) != typeid_comp(sub, i)) { return false; }
  }
  return true;
}

type_info *typeid_typeinfo(type_id id) {
  type_info *r = ti_any;
  size_t ncomps = typeid_ncomps(id);
  if (ncomps == 0) { return r; }
  r = (type_info *) to_ptr(ls_get(r->children, (typeid_comp(id, 0) >> 1) - 1));
  for (size_t i = 1; i < ncomps; ++i) {
    r = (type_info *) to_ptr(ls_get(r->children, typeid_comp(id, i) - 1));
  }

  return r;
}

bool type_is_abstract(type_info *t) {
  return t->spec.kind == ti_abstract;
}

int typeid_cmp(type_id a, type_id b) {
  for (size_t i = 0; i < TYPE_ID_WORDS; ++i) {
    if (a.words[i].w < b.words[i].w) { return -1; }
    if (a.words[i].w > b.words[i].w) { return 1; }
  }
  return 0;
}

int typeid_cmp_roots(type_id a, type_id b) {
  type_id_component ca = typeid_comp(a, 0),
                    cb = typeid_comp(b, 0);
  size_t i = 1;
  while (ca == cb && ca == TYPE_ID_COMP_EXT_VAL) {
    ca = typeid_comp(a, i);
    cb = typeid_comp(b, i++);
  }
  return ca - cb;
}

type_info *type_resolve(cell name) {
  cell *res = dict_get(type_table, name);
  while (res && celltype(*res) != t_ptr) {
    res = dict_get(type_table, *res);
  }
  eassert(res, "InvalidType", "Cannot resolve type %s."); // TOOD(errormsg)
  if (res) { return (type_info *) to_ptr(*res); }
  return NULL;
}

void types_init(env *e) {
  type_table = dict_mk();

  type_spec ts_abstract = (type_spec) {
    .kind = ti_abstract,
  };
  tid_none.words[0] = (type_id_word) { .w = 0 };
  for (size_t i = 1; i < TYPE_ID_WORDS; ++i) { tid_none.words[i].w = 0; }
  type_info *ti_none = typeinfo_mk(NULL, &ts_abstract, 0,
      (type_id_component *) &tid_none);
  ti_none->super = ti_none;
  ti_none->children = ls_mk_init(0, 0, s_any, NULL, true, true, nil);
  dict_set(type_table, sym("t_none"), from_ptr(ti_none));

  tid_any.words[0] = (type_id_word) { .w = 1 << 1 };
  for (size_t i = 1; i < TYPE_ID_WORDS; ++i) { tid_any.words[i].w = 0; }

  ti_any = typeinfo_mk(ti_none, &ts_abstract, 1,
      (type_id_component *) &tid_any);
  // Any is the root of all types, even Any, type_ids 0x0 and 0x1
  // are its children. 
  ls_append(ti_any->children, from_ptr(ti_any));
  dict_set(type_table, sym("any"), from_ptr(ti_any));
  dict_set(type_table, nil, sym("any")); 
}

void types_gc(gc_state *gcs) {
  assert(false && "TODO");
  // mark type table.
}

void types_finalize() {
  type_table = NULL;
}

// TESTS

#ifdef TESTING

// Test Suite init/fin need to be set before including test.h. Hacky :/
#define TEST_SUITE_INIT gc_init(); symtab_init(NULL); types_init(NULL); setup();
#define TEST_SUITE_FIN types_finalize(); symtab_finalize(); gc_finalize();

#include "test.h"

static type_id tid_num;
static type_id tid_int;
static type_id tid_int8;
static type_id tid_int16;

#define EXT_VAL_CHILDREN  ((1 << TYPE_ID_COMP_BITS) + 10)
static const char *ext_val_child_fmt = "child%zu";
static type_id child_ids[EXT_VAL_CHILDREN];

#define EXT_TYPES (TYPE_ID_WORDS * TYPE_ID_COMP_PER_WORD + 10)
static const char *ext_type_fmt = "ext_%zu";
static type_id ext_ids[EXT_TYPES];

static void setup() {
  type_spec ts_abstract = (type_spec) {
    .kind = ti_abstract,
  }; 
  tid_num = type_def(sym("num"), nil, &ts_abstract);
  tid_int = type_def(sym("int"), sym("num"), &ts_abstract);

  type_spec ts_int8 = (type_spec) { .kind = ti_prim, .prim = { .bits = 8 } };
  tid_int8 = type_def(sym("int8"), sym("int"), &ts_int8);

  type_spec ts_int16 = (type_spec) { .kind = ti_prim, .prim = { .bits = 16 } };
  tid_int16 = type_def(sym("int16"), sym("int"), &ts_int16);

  // A type with many sub-types that should "overflow" into an ext_val in
  // 3rd component.
  type_def(sym("collection"), nil, &ts_abstract);
  type_def(sym("container"), sym("collection"), &ts_abstract);
  char sname[11];
  for (size_t i = 0; i < EXT_VAL_CHILDREN; ++i) {
    snprintf(sname, 10, ext_val_child_fmt, i);
    child_ids[i] = type_def(sym(sname), sym("container"), &ts_abstract);
  }

  // Deep hierarchy that extends past TYPE_ID_WORDS * TYPE_COMP_PER_WORD number
  // of components.
  type_def(sym("extended"), nil, &ts_abstract);
  strcpy(sname, "extended");
  for (size_t i = 0; i < EXT_TYPES; ++i) {
    cell parent = sym(sname);
    snprintf(sname, 10, ext_type_fmt, i);
    ext_ids[i] = type_def(sym(sname), parent, &ts_abstract);
  }
}

static bool typeid_eq_all(type_id a, type_id b) {
  bool ext_a = typeid_is_ext(a),
       ext_b = typeid_is_ext(b);
  if (ext_a != ext_b) { return false; }
  if (ext_a) {
    type_info *ta = typeid_get_ext(a),
              *tb = typeid_get_ext(b);
    if (ta->typeid_n_components != tb->typeid_n_components) { return false; }
    for (size_t i = 0; i < ta->typeid_n_components; ++i) {
      if (ta->typeid_comps[i] != tb->typeid_comps[i]) { return false; }
    }
    return true;
  } else {
    for (size_t i = 0; i < TYPE_ID_WORDS; ++i) {
      if (a.words[i].w != b.words[i].w) { return false; }
    }
    return true;
  }
}

static int test_typeid() {
  test_assert(typeid_eq_all(typeid(sym("int8")), tid_int8), true); 
  test_assert(typeid_eq_all(typeid(sym("int16")), tid_int16), true);
  test_assert(typeid_eq_all(typeid(sym("int")), tid_int), true);
  test_assert(typeid_eq_all(typeid(sym("num")), tid_num), true);
  test_assert(typeid_eq_all(typeid(nil), tid_any), true);
  test_assert(typeid_eq_all(typeid(sym("any")), tid_any), true);
  test_assert(typeid_eq_all(typeid(sym("t_none")), tid_none), true);

  return OK;
}

static int test_typeid_cmp() {
  test_assert(typeid_cmp(tid_none, tid_none), 0);
  test_assert(typeid_cmp(tid_any, tid_any), 0);
  test_assert(typeid_cmp(tid_num, tid_num), 0);
  test_assert(typeid_cmp(tid_int8, tid_int8), 0);
  test_assert(typeid_cmp(tid_int16, tid_int16), 0);

  test_assert_lt(typeid_cmp(tid_none, tid_any), 0);
  test_assert_gt(typeid_cmp(tid_any, tid_none), 0);
  test_assert_lt(typeid_cmp(tid_any, tid_num), 0);
  test_assert_gt(typeid_cmp(tid_num, tid_any), 0);
  test_assert_lt(typeid_cmp(tid_num, tid_int), 0);
  test_assert_gt(typeid_cmp(tid_int, tid_num), 0);
  test_assert_lt(typeid_cmp(tid_int, tid_int8), 0);
  test_assert_gt(typeid_cmp(tid_int8, tid_int), 0);
  test_assert_lt(typeid_cmp(tid_int, tid_int16), 0);
  test_assert_gt(typeid_cmp(tid_int16, tid_int), 0);
  test_assert_lt(typeid_cmp(tid_int8, tid_int16), 0);
  test_assert_gt(typeid_cmp(tid_int16, tid_int8), 0);

  return OK;
}

static int test_typeid_cmp_roots() {
  for (size_t i = 0; i < EXT_VAL_CHILDREN; ++i) {
    test_assert_neq(typeid_cmp_roots(child_ids[i], tid_num), 0);
    test_assert_neq(typeid_cmp_roots(child_ids[i], tid_int), 0);
    test_assert_neq(typeid_cmp_roots(child_ids[i], tid_int8), 0);
    test_assert_neq(typeid_cmp_roots(child_ids[i], tid_int16), 0);
    for (size_t j = 0; j < EXT_VAL_CHILDREN; ++j) {
      test_assert(typeid_cmp_roots(child_ids[i], child_ids[j]), 0);
    }
    for (size_t j = 0; j < EXT_TYPES; ++j) {
      test_assert_neq(typeid_cmp_roots(child_ids[i], ext_ids[j]), 0);
    }
  }
  return OK;
}

static int test_typeid_ext_val() {
  for (size_t i = TYPE_ID_COMP_EXT_VAL - 1; i < EXT_VAL_CHILDREN; ++i) {
    size_t comps = typeid_ncomps(child_ids[i]);
    test_assert_gt(comps, 2);
    test_assert(typeid_comp(child_ids[i], comps - 2),
        TYPE_ID_COMP_EXT_VAL);

    for (size_t j = 0; j < TYPE_ID_COMP_EXT_VAL - 1; ++j) {
      size_t ccomps = typeid_ncomps(child_ids[j]);
      test_assert_gt(comps, ccomps);
    }
    for (size_t j = TYPE_ID_COMP_EXT_VAL - 1; j < EXT_VAL_CHILDREN; ++j) {
      size_t ccomps = typeid_ncomps(child_ids[j]);
      test_assert(comps, ccomps);
    }
  }
  for (size_t i = 0; i < TYPE_ID_COMP_EXT_VAL - 1; ++i) {
    size_t comps = typeid_ncomps(child_ids[i]);
    test_assert_lt(typeid_comp(child_ids[i], comps - 1), TYPE_ID_COMP_EXT_VAL);

    for (size_t j = 0; j < TYPE_ID_COMP_EXT_VAL - 1; ++j) {
      size_t ccomps = typeid_ncomps(child_ids[j]);
      test_assert(comps, ccomps);
    }
    for (size_t j = TYPE_ID_COMP_EXT_VAL - 1; j < EXT_VAL_CHILDREN; ++j) {
      size_t ccomps = typeid_ncomps(child_ids[j]);
      test_assert_lt(comps, ccomps);
    }
  }

  return OK;
}

static int test_typeid_ext() {
  // extXX types have a parent "extended" so there's fewer components before they
  // extend.
  size_t max_comps = TYPE_ID_WORDS * TYPE_ID_COMP_PER_WORD;
  size_t ext_comps_start = TYPE_ID_WORDS * TYPE_ID_COMP_PER_WORD - 1;
  for (size_t i = 0; i < ext_comps_start; ++i) {
    test_assert(typeid_is_ext(ext_ids[i]), false);
    test_assert_lte(typeid_ncomps(ext_ids[i]), max_comps);
  }
  for (size_t i = ext_comps_start; i < EXT_TYPES; ++i) {
    test_assert(typeid_is_ext(ext_ids[i]), true);
    type_info *ext = typeid_get_ext(ext_ids[i]);
    test_assert_gt(ext->typeid_n_components, max_comps);
    test_assert_gt(typeid_ncomps(ext_ids[i]), max_comps);
  }
  for (size_t i = 0; i < EXT_VAL_CHILDREN; ++i) {
    test_assert(typeid_is_ext(child_ids[i]), false);
  }
  test_assert(typeid_is_ext(tid_num), false);
  test_assert(typeid_is_ext(tid_int), false);
  test_assert(typeid_is_ext(tid_int8), false);
  test_assert(typeid_is_ext(tid_int16), false);

  return OK;
}

static int test_typecomps_to_typeid() {
  size_t ext_comps_start = TYPE_ID_WORDS * TYPE_ID_COMP_PER_WORD + 1;
  size_t max_comps = ext_comps_start + 10;
  type_id_component comps[max_comps];
  for (size_t i = 0; i < max_comps; ++i) { comps[i] = i + 2; }

  for (size_t i = 1; i < ext_comps_start; ++i) {
    type_id id = typecomps_to_typeid(comps, i, NULL);
    test_assert(typeid_is_ext(id), false);
    test_assert(typeid_ncomps(id), i);
  }
  for (size_t i = ext_comps_start; i < max_comps; ++i) {
    type_id id = typecomps_to_typeid(comps, i, NULL);
    test_assert(typeid_is_ext(id), true);
  }

  return OK;
}

static int test_type_is_subtype() {
  test_assert(type_is_subtype(tid_any, tid_int), true);
  test_assert(type_is_subtype(tid_any, tid_num), true);
  test_assert(type_is_subtype(tid_any, tid_int8), true);
  test_assert(type_is_subtype(tid_any, tid_int16), true);
  test_assert(type_is_subtype(tid_any, tid_any), true);
  test_assert(type_is_subtype(tid_int, tid_int), true);
  test_assert(type_is_subtype(tid_num, tid_num), true);
  test_assert(type_is_subtype(tid_int8, tid_int8), true);
  test_assert(type_is_subtype(tid_int16, tid_int16), true);
  test_assert_neq(type_is_subtype(tid_int, tid_any), true);
  test_assert_neq(type_is_subtype(tid_num, tid_any), true);
  test_assert_neq(type_is_subtype(tid_int8, tid_any), true);
  test_assert_neq(type_is_subtype(tid_int16, tid_any), true);

  test_assert(type_is_subtype(tid_num, tid_int), true);
  test_assert(type_is_subtype(tid_num, tid_int8), true);
  test_assert(type_is_subtype(tid_num, tid_int16), true);
  test_assert_neq(type_is_subtype(tid_int, tid_num), true);
  test_assert_neq(type_is_subtype(tid_int8, tid_num), true);
  test_assert_neq(type_is_subtype(tid_int16, tid_num), true);

  test_assert(type_is_subtype(tid_int, tid_int8), true);
  test_assert(type_is_subtype(tid_int, tid_int16), true);
  test_assert_neq(type_is_subtype(tid_int8, tid_int), true);
  test_assert_neq(type_is_subtype(tid_int16, tid_int), true);

  test_assert_neq(type_is_subtype(tid_int16, tid_int8), true);
  test_assert_neq(type_is_subtype(tid_int8, tid_int16), true);

  type_id tid_container = typeid(sym("container"));
  type_id tid_ext = typeid(sym("extended"));
  for (size_t i = 0; i < EXT_VAL_CHILDREN; ++i) {
    test_assert(type_is_subtype(child_ids[i], child_ids[i]), true);
    test_assert(type_is_subtype(tid_container, child_ids[i]), true);
    test_assert(type_is_subtype(tid_any, child_ids[i]), true);
    test_assert(type_is_subtype(tid_num, child_ids[i]), false);
    test_assert(type_is_subtype(tid_int, child_ids[i]), false);
    test_assert(type_is_subtype(tid_int8, child_ids[i]), false);
    test_assert(type_is_subtype(tid_int16, child_ids[i]), false);
    test_assert(type_is_subtype(tid_ext, child_ids[i]), false);
  }

  for (size_t i = 0; i < EXT_TYPES; ++i) {
    for (size_t j = 0; j < i; ++j) {
      test_assert(type_is_subtype(ext_ids[i], ext_ids[j]), false);
      test_assert(type_is_subtype(ext_ids[j], ext_ids[i]), true);
    }
    test_assert(type_is_subtype(ext_ids[i], ext_ids[i]), true);
    for (size_t j = i + 1; j < EXT_TYPES; ++j) {
      test_assert(type_is_subtype(ext_ids[i], ext_ids[j]), true);
      test_assert(type_is_subtype(ext_ids[j], ext_ids[i]), false);
    }
    test_assert(type_is_subtype(tid_ext, ext_ids[i]), true);
    test_assert(type_is_subtype(tid_any, ext_ids[i]), true);
    test_assert(type_is_subtype(tid_num, ext_ids[i]), false);
    test_assert(type_is_subtype(tid_int, ext_ids[i]), false);
    test_assert(type_is_subtype(tid_int8, ext_ids[i]), false);
    test_assert(type_is_subtype(tid_int16, ext_ids[i]), false);
    test_assert(type_is_subtype(tid_container, ext_ids[i]), false);
  }
  return OK;
}

static int test_typeid_typeinfo() {
  char sname[11];
  for (size_t i = 0; i < EXT_TYPES; ++i) {
    type_info *ti = typeid_typeinfo(ext_ids[i]);
    test_assert(
        typeid_eq_il(
          typecomps_to_typeid(ti->typeid_comps, ti->typeid_n_components, ti),
          ext_ids[i]),
        true);
    snprintf(sname, 10, ext_type_fmt, i);
    type_info *tir = type_resolve(sym(sname));
    test_assert(ti, tir);
  }
  return OK;
}

static int test_typeid_super() {
  test_assert(typeid_eq_il(typeid_super(tid_int8), tid_int), true);
  test_assert(typeid_eq_il(typeid_super(tid_int16), tid_int), true);
  test_assert(typeid_eq_il(typeid_super(tid_int), tid_num), true);
  test_assert(typeid_eq_il(typeid_super(tid_num), tid_any), true);
  return OK;
}

static test_info tests[] = {
  TEST(typeid),
  TEST(typeid_cmp),
  TEST(typeid_cmp_roots),
  TEST(type_is_subtype),
  TEST(typeid_ext_val),
  TEST(typeid_ext),
  TEST(typecomps_to_typeid),
  TEST(typeid_typeinfo),
  TEST(typeid_super),
};

TEST_SUITE(tests, "types.c")

#endif
