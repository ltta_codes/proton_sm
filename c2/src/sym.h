#pragma once

#include "cell.h"
#include "mm.h"

extern cell ct_num;
extern cell ct_list;
extern cell ct_partial;
extern cell ct_str;
extern cell ct_fn;
extern cell ct_pair;
extern cell ct_appl;
extern cell ct_appl_grouped;
extern cell ct_mem;
extern cell ct_assoc;
extern cell ct_nil;
extern cell ct_void;
extern cell ct_dict;
extern cell ct_code;
extern cell ct_dequeue;
extern cell ct_tuple;
extern cell ct_grouped_args;
extern cell ct_code_block;
extern cell ct_comment;
extern cell ct_unquote_splice;

extern cell s_self;

extern cell s_close_paren;
extern cell s_open_paren;
extern cell s_close_brace;
extern cell s_open_brace;
extern cell s_close_bracket;
extern cell s_open_bracket;
extern cell s_comma;
extern cell s_colon;
extern cell s_doublecolon;
extern cell s_semicolon;
extern cell s_qm;
extern cell s_asterisk;
extern cell s_plus;
extern cell s_dollar;
extern cell s_backquote;
extern cell s_pipe;

extern cell s_comment_start;
extern cell s_comment_end;
extern cell s_line_comment;
extern cell s_source;

extern cell s_BOF;
extern cell s_EOF;

cell sym(const char *s);
const char *sym_to_cstr(cell s);
cell sym_cat(cell s1, cell s2);
cell ty_to_sym(cell_type t);

#define sym_eq(a, b) ((a).ty == t_sym && (b).ty == t_sym && (a).l == (b).l)

void symtab_init();
void symtab_finalize();

void symtab_gc_mark_globals(gc_state *gs);

