#pragma once

/**
 * Assoc is an associative data structure that maps keys to values.
 *
 * In order to allow efficient access JS-style "hidden classes" are
 * generated for each schema called a "schema". The interpreter or VM
 * can cache these offsets having only to compare the hash stored
 * within the cached op/AST node to the hash stored within the assoc.
 *
 * Changing an assoc schema by adding a key (calling assoc_set with a
 * new key, really) causes the schema of the assoc to change as well, 
 * which results in a new schema hash for the assoc.
 *
 * Schemas are stored in a global table, quickly accessible by hash.
 *
 */

#include "cell.h"
#include "env.h"
#include "list.h"

cell associate_list(cell c, list *schema, list *val);
cell associate(cell c, cell name, cell val);
assoc *to_assoc(cell c);
cell deassociate(cell c);

typedef struct assoc assoc;

cell assoc_get(assoc *as, cell k);
cell assoc_geti(assoc *as, size_t offs);
cell assoc_set(assoc *as, cell k, cell v);
cell assoc_seti(assoc *as, size_t offs, cell v);

char *assoc_print(cell assoc, char *ex);

size_t assoc_schema_offset(assoc *as, cell k);
size_t assoc_schema_hash(assoc *as);
bool assoc_schema_offs_stale(assoc *as, size_t hash);

void assoc_init();
void assoc_finalize();

