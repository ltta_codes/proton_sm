#include "eval.h"

#include <limits.h>
#include <stdio.h>

#include "assoc.h"
#include "cell.h"
#include "error.h"
#include "list.h"
#include "sym.h"
#include "types.h"

#define DEFAULT_GROUP_PREC 0

#define TRACE

list *stack;

typedef struct {
  int status;
  cell res;
} err_res;

#define res_ok(val) ((err_res) { .status = 0, .res = val })
#define res_error(stat, val) ((err_res) { .status = stat, .res = val })

static cell evali(env *e, cell ast);

static bool partial_can_execute(partial *p) {
  return ls_len(p->applied_args) == partial_expected_args(p);
}

static inline cell to_partial_if_exec(cell c) {
  cell_type ct = celltype(c);
  if (ct == t_method || ct == t_fn) {
    return partial_mk(c);
  }
  return c;
}

static inline cell partial_execute(env *e, partial *p) {
  cell fn = (is_assoc(p->fn)) ? deassociate(p->fn) : p->fn;
  callable *cl = to_callable(fn);
  list *largs = p->applied_args;
  if (cl->eval_args) {
    size_t l = ls_len(p->applied_args);
    largs = ls_mk_init(0, l, s_any, NULL, false, true, ct_grouped_args);
    for (size_t i = 0; i < l; ++i) {
      ls_append(largs, evali(e, ls_get(p->applied_args, i)));
    }
  }
  switch (celltype(fn)) {
    case t_fn:
      return to_partial_if_exec(fn_call(to_fn(fn), e, largs));
    case t_method:
      return to_partial_if_exec(method_execute(to_method(fn), e, largs));
    default:
      errors("InvalidPartialError", "Invalid Partial callable");
      return nil;
  }
}

static cell to_partial_evald(env *e, cell c) {
  cell_type ct = celltype(c);
  if (ct == t_method || ct == t_fn) {
    c = partial_mk(c);
    ct = celltype(c);;
  }
  if (ct == t_partial) {
    if (partial_can_execute(to_partial(c))) {
      return partial_execute(e, to_partial(c));
    }
    return c;
  }
  return c;
}

static cell *env_resolve(env *e, cell c, env *argp_env, bool *arg_proc) {
  if (is_assoc(c)) { c = deassociate(c); }
  if (celltype(c) != t_sym) { return NULL; }
  cell *r = env_lookup(e, c);
  if (!r && argp_env) {
    r = env_lookup(argp_env, c);
    if (r && arg_proc) { *arg_proc = true; }
  } else if (arg_proc) {
    *arg_proc = false;
  }
  return r;
}

static cell simplify_list(list *l) {
  cell meta = ls_getmeta(l);
  if (ls_len(l) == 1) {
    if (sym_eq(ct_appl, meta) || sym_eq(ct_tuple, meta)) {
      return ls_get(l, 0);
    }
  }
  return from_list(l);
}

static cell simplify(cell c) {
  if (celltype(c) == t_list) {
    return simplify_list(to_list(c));
  }
  return c;
}

static cell resolve(env *e, env *argp_env, cell c) {
  if (is_assoc(c)) { return c; }
  cell *r = env_resolve(e, c, argp_env, NULL);
  return associate((r) ? *r : c, s_source, c);
}

static size_t is_kwd(list *kwds, cell c) {
  if (is_assoc(c)) { c = deassociate(c); }
  if (celltype(c) == t_sym) { // TODO: allow other types of kwds?
    size_t l = ls_len(kwds);
    for (size_t i = l; i > 0; --i) {
      arg_opt *kwd = (arg_opt *) to_ptr(ls_get(kwds, i - 1));
      cell ckwd = (celltype(kwd->kwd) == t_list)
          ? (ls_get(to_list(kwd->kwd), 0)) : kwd->kwd;
      if (sym_eq(ckwd, c)) { return l - i + 1; }
      if (!kwd->optional) { break; } 
    }
  }
  return 0;
}

static num precedence(env *e, env *argp_env, cell c) {
  cell_type ct = celltype(c);
  if (ct == t_sym) {
    cell *r = env_resolve(e, c, argp_env, NULL);
    c = (r) ? *r : c;
  }
  ct = celltype(c);
  switch (ct) {
    case t_assoc:
      return precedence(e, argp_env, deassociate(c));
    case t_fn:
    case t_method:
    case t_partial:
      return to_callable(c)->prec;
    case t_list:
      if (meta_sym_eq(c, ct_tuple)) { return PREC_TUPLE; }
    default:
      return 0;
  }
}

static list *resolve_appl(env *e, list *appl) {
  list *res = ls_mk_init(0, ls_len(appl), s_any, NULL, false, false, ct_appl);
  for (size_t i = 0; i < ls_len(appl); ++i) {
    ls_append(res, resolve(e, NULL, ls_get(appl, i)));
  }
  return res;
}

static cell group_sub(env *e, list *appl, int prec, list *kwds,
    env *arg_proc);

static size_t last_nonkwdonly_arg(callable *cl) {
  for (size_t i = cl->n_args; i > 0; --i) {
    if (!cl->arg_opts[i].kwd_only) { return i - 1; }
  }
  return 0;
}

static inline int imin(int a, int b) { return (a < b) ? a : b; }
static cell group_expr(env *e, cell c);

/*
  
   -> multi-kwd     =>  [ kwd-n: arg-n ]
   -> optional      => nil
   -> kwd-only      => kwd
   -> repeating     => list of [ [ kwd-n: arg-n ] | arg | kwd | nil ]

   => match_kwd_arg : multi-kwd => list, optional => false, kwd => pair, arg => cell
   => group_one_arg : optional => nil, repeating => list [match_kwd_arg], kwd-only => kwd

 */

list *group_args(env *e, list *lappl, cell *oop, list *kwds, env *arg_proc);

static bool match_kwd_arg(env *e, list *appl, cell kwd, cell *arg, bool kwd_only,
    bool last_arg, size_t n_kwd, list *kwds, env *argp_env, arg_opt *argopt,
    int prec) {
  if (!ls_len(appl)) { return false; }
  *arg = nil;
  if (is_set(kwd)) {
    cell akwd = deassociate(ls_get(appl, 0));
    if (!sym_eq(kwd, akwd)) {
      return false;
    }
    if (is_set(kwd)) {
      ls_pop(kwds);
    }
    slice_popfront(appl);
    if (kwd_only) { return true; }
  }
  if (!ls_len(appl)) { return false; }

  env *argproc_env = argopt_argproc_for_arg(argp_env, e, argopt, n_kwd); 
  cell *default_argp = dict_get(argproc_env->vals, nil);
  if (default_argp) {
    list *args = group_args(e, appl, default_argp, kwds, argproc_env);
    *arg = from_list(ls_of_m(ct_appl, 2, *default_argp, from_list(args)));
  } else {
    *arg = group_sub(e, appl, 
        (last_arg) ? prec : imin(prec, DEFAULT_PREC),
        kwds, argproc_env);
  }
  return true;
}

static err_res match_argopt(env *e, list *appl, arg_opt argopt,
    bool last_arg, list *kwds, env *argp_env, int prec) {
  bool repeating = argopt.repeating,
       multi_kwd = is_set(argopt.kwd) && celltype(argopt.kwd) == t_list;
  list *res = ls_of_m_mut(ct_tuple, 0);

  cell last_kwd = argopt.kwd;

  cell old_argopt = nil;

  // Push expanded keyword sequence in reverse order if multi-kwd.
  size_t kwds_len = ls_len(kwds);
  arg_opt argopts[(multi_kwd) ? ls_len(to_list(argopt.kwd)) : 0];

  if (multi_kwd) {
    assert(cell_cmp(
          ((arg_opt *) to_ptr(ls_get(kwds, -1)))->kwd,
          argopt.kwd) == 0);
    old_argopt = ls_pop(kwds);

    list *kwd_seq = to_list(argopt.kwd);
    for (size_t i = ls_len(kwd_seq); i > 0; --i) {
      argopts[i - 1] = (arg_opt) {
        .kwd = ls_get(kwd_seq, i - 1),
        .kwd_only = false,
        .optional = false,
        .repeating = false,
      };
      ls_append(kwds, from_ptr(&argopts[i - 1]));
    }
  }

  do {
    if (multi_kwd) {
      list *kwd_seq = to_list(argopt.kwd);
      bool succ = true;
      for (size_t i = 0; i < ls_len(kwd_seq) && succ; ++i) {
        cell kwd = last_kwd = ls_get(kwd_seq, i);
        cell arg;
        succ = match_kwd_arg(e, appl, kwd, &arg, argopt.kwd_only,
            last_arg, i, kwds, argp_env, &argopt, prec);
        if (succ) {
          ls_append(res, pair_mk(kwd, arg));
        }
      }
      if (!succ) { break; }
    } else {
      cell arg;
      if (!match_kwd_arg(e, appl, argopt.kwd, &arg, argopt.kwd_only,
          last_arg, 0, kwds, argp_env, &argopt, prec)) {
        break;
      }
      if (is_set(argopt.kwd)) {
        ls_append(res, pair_mk(argopt.kwd, arg));
      } else {
        ls_append(res, arg);
      }
    }
  } while (repeating && ls_len(appl));

  // Pop remaining keywords, if any.
  while (ls_len(res) && ls_len(kwds) > kwds_len) {
    ls_pop(kwds); 
  }

  switch (ls_len(res)) {
    case 0: return (argopt.optional)
                ? ls_pop(kwds), res_ok(nil)
                : res_error(1, last_kwd); 
    case 1: return res_ok(ls_pop(res));
    default:  return res_ok(from_list(ls_set_immutable(res, true)));
  }
}

static cell group_expr_with_argprocs(env *e, cell c, env *argp_env);

static cell group_argopt(env *e, cell arg, list *kwds, arg_opt *argopt,
    env *argp_env, size_t n_kwd) {
  env *argproc_env = argopt_argproc_for_arg(argp_env, e, argopt, n_kwd); 
  cell *default_argp = dict_get(argproc_env->vals, nil);

  if (default_argp) {
    list *appl =
       ls_slice((is_application(arg)) ? to_list(arg) : ls_of(1, arg), 0, -1);
    list *args = group_args(e, appl, default_argp, kwds, argproc_env);
    return from_list(ls_of_m(ct_appl, 2, *default_argp, args));
  } else {
    return group_expr_with_argprocs(e, arg, argproc_env);
  }
}

static err_res group_one_arg(env *e, list *appl, arg_opt argopt,
    list *kwds, env *argp_env, bool last_arg, bool args_tuple, int prec) {
  if (args_tuple) {
    bool multi_kwd = is_set(argopt.kwd) && celltype(argopt.kwd) == t_list;
    if (multi_kwd) {
      cell next = slice_popfront(appl);
      if (!is_set(next) && argopt.optional) { 
        return res_ok(next);
      }
      list *lt = to_list(next),
           *la = to_list(argopt.kwd);
      size_t ll = ls_len(lt); 
      eassert(ll == ls_len(la), "SyntaxError",
          "Expected %d-tuple to match keyword argument %s");
      if (sym_eq(ls_getmeta(lt), ct_grouped_args)) {
        return res_ok(from_list(lt));
      }

      list *res =
          ls_mk_init(0, ls_len(lt), s_pair, NULL, true, false, ct_grouped_args);
      for (size_t i = 0; i < ll; ++i) {
        ls_append(res, pair_mk(ls_get(la, i),
              group_argopt(e, ls_get(lt, i), kwds, &argopt, argp_env, i)));
      }
      return res_ok(from_list(res));
    } else {
      if (argopt.repeating) {
        if (ls_len(appl) == 1
            && meta_sym_eq(ls_get(appl, 0), ct_grouped_args)) {
          return res_ok(slice_popfront(appl));
        }
        list *res = ls_mk_init(0, ls_len(appl), s_any, NULL,
            false, false, ct_grouped_args);
        while (ls_len(appl)) {
          ls_append(res,
              group_argopt(e, slice_popfront(appl), kwds, &argopt, argp_env, 0));
        }
        ls_set_immutable(res, true);
        return res_ok(from_list(res));
      }
      return res_ok(
          group_argopt(e, slice_popfront(appl), kwds, &argopt, argp_env, 0));
    }
  } else {
    return match_argopt(e, appl, argopt, last_arg, kwds, argp_env, prec);
  }
}

// TODO: Syntactic dispatch should probably done via FA or by building
//       a parser table and using appropriate algorithms.
// TODO: Resolve/notify of syntactic dispatch ambiguities,
//       currently last definition wins.
list *group_args(env *e, list *lappl, cell *oop, list *kwds, env *arg_proc) {
  list *args = ls_mk_init(0, 5, s_any, NULL, false, false, ct_grouped_args);

  bool syn_dispatch = celltype(*oop) == t_fn && to_fn(*oop)->syntactic_dispatch;
  list *alternatives = (syn_dispatch) ? to_fn(*oop)->methods : ls_of(1, *oop);

  for (size_t j = ls_len(alternatives); j > 0; --j) { 
    cell op = ls_get(alternatives, j - 1);
    callable *cl = to_callable(op);
    list *appl = ls_slice(lappl, 0, -1);
    int nargs = cl->n_args;
    assert(nargs >= 0);

    size_t last_arg = last_nonkwdonly_arg(cl);
    cell next = ls_len(appl) ? ls_get(appl, 0) : nil;
    bool args_tuple = (cl->accepts_tupleargs && is_tuple(next))
                      || meta_sym_eq(next, ct_grouped_args),
         args_curried = cl->accepts_curriedargs;

    if (!(args_tuple || args_curried || cl->n_args <= 1)) {
      continue;
    }

    ls_clear(args);

    for (size_t i = nargs; i > 0; --i) {
      cell kwd = cl->arg_opts[i - 1].kwd;
      if (!is_nil(kwd)) {
        ls_append(kwds, from_ptr(&cl->arg_opts[i - 1]));
      }
    }

    env *argp_env = cl_argprocessors_env(arg_proc, e, cl);
    bool lappl_popped = false;

    // TODO(refactor): cleaner w/ separate tuple/free arg handling?
    if (args_tuple || meta_sym_eq(next, ct_grouped_args)) {
      appl = ls_slice(to_list(slice_popfront(lappl)), 0, -1);
      lappl_popped = true;
    }

    bool matched = true;
    for (size_t i = 0; i < nargs; ++i) {
      err_res arg = group_one_arg(e, appl, cl->arg_opts[i], kwds, argp_env,
              i == last_arg, args_tuple, cl->prec);

      if (arg.status != 0) { 
        matched = false;
        verbose_sp("syn_dispatch no match: %s %d at ",
            (is_set(cl->assigned_name)) ? sym_to_cstr(cl->assigned_name) : "anon",
            cl->n_args);
        verbose_cp(arg.res);
        verbose_lp(args);
        //verbose_sp(" :: ");
        //verbose_cp(from_list(lappl));
        //verbose_sp(" => ");
        //verbose_cp(from_list(appl));
        verbose_sp("\n");
        break;
      }
      ls_append(args, arg.res);
    }

    if (matched) {
      *oop = op;
      while (!lappl_popped && ls_len(lappl) > ls_len(appl)) { slice_popfront(lappl); }
      return args;
    }
  }

  // TODO(errormsg): Describe what was provided and expected.
  errors("SyntaxError", "Invalid syntax.");
  *oop = nil;
  return ls_clear(args), args; 
}

static cell fcall_expr_mk(cell left, cell fn, list *args, size_t nargs) {
  bool has_rargs = nargs > 0;
  if (!is_void(left)) {
    if (has_rargs) {
      return from_list(ls_of_m_mut(ct_appl, 3, left, fn, from_list(args)));
    } else {
      return from_list(ls_of_m_mut(ct_appl, 2, left, fn));
    }
  } else {
    if (has_rargs) {
      return from_list(ls_of_m_mut(ct_appl, 2, fn, from_list(args)));
    } else {
      return fn;
    }
  }
}

static cell group_next(env *e, list *appl, cell left, int prec, list *kwds,
    env *arg_proc) {
  int last_prec = prec;
  while (ls_len(appl)) {
    cell next = ls_get(appl, 0);
    if (is_kwd(kwds, next)) { break; }
    bool is_argp = false;
    cell *pnext = env_resolve(e, next, arg_proc, &is_argp),
         enext = (pnext) ? *pnext : next;
    int next_prec = precedence(e, arg_proc, enext);
    if (is_callable(enext) && to_callable(enext)->has_leftarg
        && next_prec < prec) {
      break;
    }

    slice_popfront(appl);
    switch (celltype(enext)) {
      case t_method:
      case t_fn:
      case t_partial:
        if (to_callable(enext)->has_leftarg) {
          list *rargs = group_args(e, appl, &enext, kwds, arg_proc);
          list *app = ls_mk_init(0, 2 + ls_len(rargs),
              s_any, NULL, false, false, ct_appl);
          ls_append(app, left);
          ls_append(app, enext);
          ls_extend(app, rargs);
          if (is_argp) {
            left = evali(e, from_list(app));
          } else {
            left = from_list(app);
          }
          break;
        } else {
          list *rargs = group_args(e, appl, &enext, kwds, arg_proc);
          list *app = ls_mk_init(0, 1 + ls_len(rargs),
              s_any, NULL, false, false, ct_appl);
          ls_append(app, enext);
          ls_extend(app, rargs);
          if (is_argp) {
            next = evali(e, from_list(app));
          } else {
            next = from_list(app);
          }
        }
        last_prec = DEFAULT_PREC;
        goto append;
      default:
        next = group_expr(e, next);
      append:
        if (is_application(left)) {
          last_prec = next_prec;
          ls_append(to_list(left), next);
        } else if (!is_void(left)) {
          last_prec = next_prec;
          left = from_list(ls_of_m_mut(ct_appl, 2, left, next));
        } else {
          last_prec = next_prec;
          left = next;
        }
    }
  }

  return simplify(left);
}

static cell group_sub(env *e, list *appl, int prec, list *kwds,
    env *arg_proc) {
  if (ls_len(appl) == 0 || is_kwd(kwds, ls_get(appl, 0))) { return _void; }

  cell left = ls_get(appl, 0), 
       *pleft = env_resolve(e, left, arg_proc, NULL);
  if (pleft && is_callable(*pleft)) {
    left = group_next(e, appl, _void,
        precedence(e, arg_proc, *pleft) - 1, kwds, arg_proc);
  } else {
    slice_popfront(appl);
    left = group_expr(e, left);
  }

  if (ls_len(appl) > 0) {
    left = group_next(e, appl, left, prec, kwds, arg_proc);
  }
  return simplify(left);
}

static cell group_appl(env *e, list *lappl, env *argp_env) {
  list *out = ls_mk_init(0, ls_len(lappl) / 2 + 1, s_any,
      NULL, false, false, ct_appl);
  list *appl = ls_slice(resolve_appl(e, lappl), 0, -1);
  argp_env = (!argp_env) ? env_new(NULL, NULL) : argp_env;
  while (ls_len(appl)) {
    cell fst = ls_get(appl, 0);
    // TODO: argproc and kwds.
    list *kwds = ls_mk_init(0, 10, s_any, NULL, false, false, nil);
    int prec = precedence(e, argp_env, fst);
    cell *pfst = env_resolve(e, fst, argp_env, NULL),
         efst = (pfst) ? *pfst : fst;

    switch (celltype(efst)) {
      case t_fn:
      case t_method:
      case t_partial: {
        slice_popfront(appl);
        list *args = group_args(e, appl, &efst, kwds, argp_env);
        if (to_callable(efst)->has_leftarg) {
          //ls_append(out, fst);
          //ls_append(out, from_list(args));
          if (!ls_len(out)) { ls_append(out, nil); }
          ls_append(out,
              fcall_expr_mk(ls_pop(out), efst, args, to_callable(efst)->n_args));
        } else {
          //ls_append(out, from_list(ls_of_m(ct_appl, 2, fst, from_list(args))));
          ls_append(out, fcall_expr_mk(_void, efst, args, to_callable(efst)->n_args));
        }
        break;
      }
      default:
        fst = group_sub(e, appl, prec, kwds, argp_env);
        //if (is_application(fst)) {
        //  ls_extend(out, to_list(fst));
        //} else {
          ls_append(out, fst);
        //}
    }
  }
  return simplify(from_list(out));
}

static cell group_expr_with_argprocs(env *e, cell c, env *argp_env) {
  switch (celltype(c)) {
    case t_list: {
      list *l = to_list(c);
      if (ls_is_str(l)) { return c; }
      if (sym_eq(ls_getmeta(l), ct_appl)) {
        return group_appl(e, l, argp_env);
      }
      size_t len = ls_len(l);
      bool shape_imm = l->shape_immutable;
      list *res =
          ls_mk_init(0, len, s_any, NULL, false, shape_imm, ls_getmeta(l));
      for (size_t i = 0; i < len; ++i) {
        ls_append(res, group_expr(e, ls_get(l, i)));
      }
      res->immutable = l->immutable;
      return from_list(res);
    }
    case t_sym:
    default:
      return c;
  }
}

static cell group_expr(env *e, cell c) {
  return group_expr_with_argprocs(e, c, NULL);
}

static err_res apply_2(env *e, cell *l, cell *r, list *rest);
static cell apply(env *e, cell left, cell right, list *rest);

typedef err_res (*apply_fn) (env *e, cell, cell, list *rest);

static err_res apply_appl_any(env *e, cell left, cell right, list *rest) {
  list *l = to_list(left);
  assert(meta_sym_eq(left, ct_appl));
  if (ls_len(l) > 0) {
    cell last = ls_get(l, -1);
    apply_2(e, &last, &right, rest);
    ls_set(l, -1, last);
    if (!is_void(right)) { ls_append(l, right); }
  } else {
    ls_append(l, right);
  }
  return res_ok(left);
}

static err_res apply_list_any(env *e, cell l, cell r, list *rest) {
  if (meta_sym_eq(l, ct_appl)) { return apply_appl_any(e, l, r, rest); }
  return res_error(1, _void);
}

static inline cell from_partial(partial *p) {
  return (cell) { .ty = t_partial,  .p = p };
}

static err_res apply_callable_any(env *e, cell left, cell right, list *rest) {
  partial *p = to_partial(to_partial_if_exec(left));
  if (partial_can_execute(p)) {
    return res_ok(apply(e, partial_execute(e, p), right, rest));
  }
  p = partial_copy(p);
  callable *c = to_callable(p->fn);
  bool rtup = (c->accepts_tupleargs && is_tuple(right))
      || meta_sym_eq(right, ct_grouped_args);
  int n = (rtup) ? ls_len(to_list(right)) : 1,
         n_args = ls_len(p->applied_args) + n;
  if (n_args > partial_expected_args(p)) {
    errors("InvalidInvokationError",
        "Too many arguments for callable %s"); // TODO(errormsg)
    return res_error(1, _void);
  }
  //cell args = (c->eval_args) ? evali(e, right) : right;
  cell args = right;
  if (rtup) {
    if (celltype(args) == t_list) {
      ls_extend(p->applied_args, to_list(args));
    } else {
      ls_append(p->applied_args, args);
    }
  } else {
    ls_append(p->applied_args, args);
  }
  if (partial_can_execute(p)) { return res_ok(partial_execute(e, p)); }
  return res_ok(from_partial(p));
}

static err_res apply_any_any(env *e, cell left, cell right, list *rest) {
  return res_ok(from_list(ls_mk_init(2, 2, s_any,
      (cell []) { left, right }, false, false, ct_appl)));
}

static err_res apply_any_callable(env *e, cell left, cell right, list *rest) {
  //right = evali(e, right);
  //if (!is_callable(right)) { return apply(e, left, right, rest); }
  partial *p = to_partial(to_partial_if_exec(right));
  if (partial_can_execute(p)) {
    return res_ok(apply(e, left, partial_execute(e, p), rest));
  }
  p = partial_copy(p);
  callable *c = to_callable(p->fn);
  if (c->has_leftarg) {
    ls_append(p->applied_args, left);
    if (partial_can_execute(p)) { return res_ok(partial_execute(e, p)); }
    return res_ok(from_partial(p));
  }

  right = from_partial(p);
  while (is_callable(right) && !to_callable(right)->has_leftarg
      && ls_len(rest) > 0) {
    right = apply(e, right, slice_popfront(rest), rest);
  }
  if (!is_callable(right)) { return res_ok(apply(e, left, right, rest)); }
  return apply_any_any(e, left, right, rest);
}

static err_res apply_list_list(env *e, cell left, cell right, list *rest) {
  list *l = to_list(left),
       *r = to_list(right);
  if (ls_hasmeta(r)) { return apply_list_any(e, left, right, rest); }
  size_t ll = ls_len(r);
  if ((ll >= 1 || ll <= 3) && r->entry_type == s_num) {
    switch (ll) {
      case 1:
        return res_ok(ls_get(to_list(left), to_num(ls_get(r, 0))));
      case 2:
        return res_ok(from_list(
            ls_slice(to_list(left), to_num(ls_get(r, 0)), to_num(ls_get(r, 1)))));
      case 3:
        assert(false && "TODO");
    }
  }
  if (ls_hasmeta(l)) { 
    if (sym_eq(ls_getmeta(l), ct_appl)) {
      return apply_appl_any(e, left, right, rest);
    }
  }
  return apply_list_any(e, left, right, rest);
}

static err_res apply_void_any(env *e, cell left, cell right, list *rest) {
  if (is_callable(right)) { return apply_any_callable(e, left, right, rest); }
  return res_ok(right);
}

static err_res apply_any_void(env *e, cell left, cell right, list *rest) {
  if (is_callable(left)) { return apply_callable_any(e, left, right, rest); }
  return res_ok(left);
}

static err_res apply_2(env *e, cell *l, cell *r, list *rest) {
  static apply_fn lut[(t_any + 1) * (t_any + 1)] = {
    [t_list * t_any + t_any] = apply_list_any,
    [t_list * t_any + t_list] = apply_list_list,
    [t_fn * t_any + t_any] = apply_callable_any,
    [t_method * t_any + t_any] = apply_callable_any,
    [t_partial * t_any + t_any] = apply_callable_any,
    [t_any * t_any + t_fn] = apply_any_callable,
    [t_any * t_any + t_method] = apply_any_callable,
    [t_any * t_any + t_partial] = apply_any_callable,
    [t_void * t_any + t_any] = apply_void_any,
    [t_any * t_any + t_void] = apply_any_void,
    [t_any * t_any + t_any] = apply_any_any,
  };
  *r = (is_callable(*l)) ? *r : evali(e, *r);
  cell_type cl = celltype(*l),
            cr = celltype(*r);
  apply_fn f = lut[cl * t_any + cr];
  if (!f) { f = lut[t_any * t_any + cr]; }
  if (!f) { f = lut[cl * t_any + t_any]; }
  if (f) {
    return f(e, *l, *r, rest);
  }
  return res_error(1, nil);
}

static cell apply(env *e, cell left, cell right, list *rest) {
  err_res res = apply_2(e, &left, &right, rest);
  if (res.status == 0) { return res.res; }
  return apply_any_any(e, left, right, rest).res;
}

static cell eval_appl(env *e, list *l) {
  if (ls_len(l) == 0) { return from_list(l); }
  list *app = ls_slice(l, 0, -1);
  cell left = evali(e, slice_popfront(app));
  while (ls_len(app)) {
    left = apply(e, left, slice_popfront(app), app);
  }
  return left;
}

static cell eval_list(env *e, list *l) {
  if (ls_is_str(l)) {
    return from_list(l);
  }
  cell meta = ls_getmeta(l);
  if (!is_nil(meta)) {
    if (sym_eq(meta, ct_comment)) {
      if (e->is_runtime) { return nil; }
      return from_list(l);
    }
    if (sym_eq(meta, ct_appl)) {
      //regroup_appl(e, l);
      //verbose_sp("Grouped APPL: ");
      //verbose_lp(l);
      //verbose_sp("\n");
      return eval_appl(e, l);
    }
    if (sym_eq(meta, ct_appl_grouped)) {
      return eval_appl(e, l);
    }
    if ((sym_eq(meta, ct_code) || sym_eq(meta, ct_code_block))
        && e->is_runtime) {
      cell res = nil;
      env *ne = (e->is_root) ? e : env_new(e, e->globals);
      for (size_t i = 0; i < ls_len(l); ++i) {
        res = evali(ne, ls_get(l, i));
      }
      return res;
    }
    if (sym_eq(meta, ct_comment)) {
      return _void;
    }
    if (sym_eq(meta, ct_tuple)) {
      if (ls_len(l) == 1 && e->is_runtime) {
        return evali(e, ls_get(l, 0));
      }
      list *res = ls_mk_init(0, ls_len(l), s_any, NULL, false, true, ct_tuple);
      for (size_t i = 0; i < ls_len(l); ++i) {
        ls_append(res, evali(e, ls_get(l, i)));
      }
      res->immutable = true;
      return from_list(res);
    }
  }
  if (ls_is_str(l)) { return from_list(l); }
  list *res = ls_mk_init(0, ls_len(l), l->entry_type, NULL,
      false, false, ls_getmeta(l));
  for (size_t i = 0; i < ls_len(l); ++i) {
    ls_append(res, evali(e, ls_get(l, i)));
  }
  res->immutable = l->immutable;
  return from_list(res);
}

static cell eval_cell(env *e, cell c) {
  if (is_assoc(c)) { c = deassociate(c); }

  cell_type ct = celltype(c);
  switch (ct) {
    case t_num:
    case t_mem:
    case t_nil:
    case t_ptr:
      return c;
    case t_assoc:
      errors("EvaluationError", "Assocs should be resolved by now.");
    case t_any:
      errors("EvaluationError", "Any should never occur here.");
    case t_sym: {
      cell *v = env_resolve(e, c, NULL, NULL);
      if (v) { return to_partial_evald(e, *v); }
      if (e->sym_self_eval) { return c; }
      errorsf("EnvLookupError", "Symbol \"%s\" is not defined in current env.",
          sym_to_cstr(c));
      return nil;
    }
    case t_list:
      return eval_list(e, c.l);
    case t_void:
      return c;
    case t_fn:
    case t_method:
    case t_partial:
      return to_partial_evald(e, c);
  }
  return _void;
}

static cell evali(env *e, cell ast) {
  cell c = eval_cell(e, ast);
  if (celltype(c) == t_partial && partial_can_execute(c.p)) {
    return partial_execute(e, c.p);
  }
  return c;
}

// EVAL UTIL FUNCTIONS //

cell eval_for_levels(globals *glob, cell ast, int start, int end) {
  for (dict_it i = dict_iter(glob->envs); !dict_itend(i); dict_itnext(&i)) {
    list *p = to_list(dict_itval(i));
    env *e = (env *) to_ptr(ls_get(p, 1));
    if (e->eval_level >= start && e->eval_level <= end) {
      ast = group_expr(e, ast);
      verbose_sp("Eval level %d AST:\n", to_num(ls_get(p, 0)));
      verbose_cp(ast);
      verbose_sp("\n");
      cell nast = evali(e, ast);
      verbose_cp(nast);
      verbose_sp("\n=======\n");
      ast = nast;
    }
  }
  return ast;
}

cell eval(env *e, cell ast) {
  ast = group_expr(e, ast);
  return evali(e, ast);
}

cell eval_rt(globals *glob, cell ast) {
  env *rte = env_for_eval_level(glob, eval_levels[evl_runtime]);
  return eval(rte, ast);
}

cell eval_macro(globals *g, cell ast) {
  env *me = env_for_eval_level(g, eval_levels[evl_macro]);
  return eval(me, ast);
}

char *read_all(const char *filename) {
  char *res;
  long len;
  FILE *f = fopen(filename, "r");
  if (f) {
    fseek(f, 0, SEEK_END);
    len = ftell(f);
    rewind(f);
    res = malloc((len + 1) * (sizeof(char)));
    fread(res, sizeof(char), len, f);
    fclose(f);
    res[len] = '\0';
    return res;
  } else {
    errors("IOError", "Cannot open file");
    return NULL;
  }
}

cell eval_all_levels(globals *glob, cell ast) {
  return eval_for_levels(glob, ast, INT_MIN, INT_MAX);
}

cell eval_file(globals *glob, const char *filename) {
  char *cwd = get_cwd();

  // Read file, then set cwd so that relative imports work.
  char *src = read_all(filename);
  set_cwd_fn(filename);

  cell ast = parse(src);
  free(src);

  cell res = eval_all_levels(glob, ast);

  // Reset cwd.
  set_cwd(cwd);
  free(cwd);

  return res;
}

cell eval_src(globals *glob, char *src, char *display_filename) {
  cell ast = parse(src);
  return eval_all_levels(glob, ast);
}

// UTIL FUNCTIONS //

bool cell_print_partial_ext = false;

char *partial_print(partial *p, char *ex) {
  if (!cell_print_partial_ext && !is_nil(to_callable(p->fn)->assigned_name)) {
      ex = esprintf(ex, "\u1D18"); 
      return cell_print(to_callable(p->fn)->assigned_name, ex);
  } else {
    ex = esprintf(ex, "\u2039\\");
    callable *cl = to_callable(p->fn);
    size_t n = 0;
    if (cl->has_leftarg && ls_len(p->applied_args) > 0) {
      ex = cell_print(ls_get(p->applied_args, n++), ex);
    }
    ex = esprintf(ex, " ");
    if (!is_nil(cl->assigned_name)) {
      ex = cell_print(cl->assigned_name, ex);
    } else {
      ex = esprintf(ex, "anon");
    }

    for (; n < ls_len(p->applied_args); ++n) {
      ex = esprintf(ex, " ");
      ex = cell_print(ls_get(p->applied_args, n), ex);
    }

    return esprintf(ex, "\u203A");
  }
}

#ifdef TESTING

// Test Suite init/fin need to be set before including test.h. Hacky :/

extern void parser_env(env *e);
static env *env_parser;

static globals glob = { .envs = NULL };

#define TEST_SUITE_INIT \
    gc_init(); symtab_init(); assoc_init(); \
    parser_env(env_parser = env_new(NULL, &glob));
#define TEST_SUITE_FIN \
    symtab_finalize(); assoc_finalize(); gc_finalize();

#include "test.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <time.h> -- signal() conflicts error.h
extern int time(void *);

#include "fn.h"
#include "sym.h"

#define op(name, operator)                                                \
static cell name(env *e, cell args, cell closed_vars[], mmethod *self) {  \
  return from_num(to_num(nth(args, 0)) operator to_num(nth(args, 1)));    \
}

op(nadd, +)
op(nsub, -)
op(nmul, *)
op(ndiv, /)
op(nmod, %)

static cell mm(mmethod *m, num prec) {
  mm_setprec(m, prec);
  return (cell) { .ty = t_method, .mm = m };
}

static cell eval_arith(env *e, const char *src) {
  cell ast = evali(env_parser, parse(src));
  return ls_get(to_list(evali(e, ast)), 0);
}

static cell nop(env *e, cell args, cell closed_vars[], mmethod *self) {
  return nil;
}

static mmethod *mk_f(native_fn f, size_t nargs, bool left_arg) {
  list *args = ls_mk_init(0, nargs, s_any, NULL, false, true, nil);
  assert(nargs < 27);
  char s[2] = "a";
  for (size_t i = 0; i < nargs; ++i) {
    s[0] = 'a' + i;
    ls_append(args, sym(s));
  }
  return mm_mk_native(f, left_arg, args, NULL, 0);
}

static env *mk_test_env() {
  env *e = env_new(NULL, &glob);
  e->is_root = true;
  env_set(e, sym("+"), mm(mk_f(nadd, 2, true), 30));
  env_set(e, sym("-"), mm(mk_f(nsub, 2, true), 30));
  env_set(e, sym("*"), mm(mk_f(nmul, 2, true), 40));
  env_set(e, sym("/"), mm(mk_f(ndiv, 2, true), 40));
  env_set(e, sym("%"), mm(mk_f(nmod, 2, true), 40));
  env_set(e, sym("::"),  mm(mk_f(nop, 3, true), 50));
  env_set(e, sym("print"),  mm(mk_f(nop, 1, false), 0));

  env_set(e, s_BOF, _void);
  env_set(e, s_EOF, _void);

  return e;
}

int test_expr_grouping() {
  env *e = mk_test_env();
  cell add = resolve(e, NULL, sym("+")),
       sub = resolve(e, NULL, sym("-")),
       mul = resolve(e, NULL, sym("*")),
       cat = resolve(e, NULL, sym("::"));

  list *appl = ls_of_m(ct_appl, 5, from_num(10), sym("+"),
      from_num(20), sym("*"), from_num(30));
  cell res = group_expr(e, from_list(appl));
  list *expected = ls_of_m(ct_appl, 3, from_num(10), add, from_list(
          ls_of_m(ct_appl, 3, from_num(20), mul, from_num(30))));
  _cp(res);
  _lp(expected);
  test_assert(ls_cmp(to_list(res), expected), 0);

  appl = ls_of_m(ct_appl, 7, from_num(10), sym("+"), from_num(15),
      sym("*"), from_num(20), sym("-"), from_num(30));
  res = group_expr(e, from_list(appl));
  expected = ls_of_m(ct_appl, 5, from_num(10), add, from_list(
      ls_of_m(ct_appl, 3,
          from_num(15), mul, from_num(20))), sub, from_num(30));
  _cp(res);
  _lp(expected);
  test_assert(ls_cmp(to_list(res), expected), 0);

  appl = ls_of_m(ct_appl, 6, from_num(10), sym("+"), from_num(15),
      sym("::"), from_num(20), from_num(30));
  res = group_expr(e, from_list(appl));
  expected = ls_of_m(ct_appl, 3, from_num(10), add, from_list(
      ls_of_m(ct_appl, 4,
          from_num(15), cat, from_num(20), from_num(30))));
  _cp(res);
  _lp(expected);
  test_assert(ls_cmp(to_list(res), expected), 0);

  appl = ls_of_m(ct_appl, 6, sym("print"), from_num(10), sym("+"),
      from_num(20), sym("*"), from_num(30));
  res = group_expr(e, from_list(appl));
  expected = ls_of_m(ct_appl, 2, resolve(e, NULL, sym("print")),
      from_list(ls_of_m(ct_tuple, 1, from_list(
          ls_of_m(ct_appl, 3, from_num(10), add, from_list(
              ls_of_m(ct_appl, 3, from_num(20), mul, from_num(30))))))));
  _cp(res);
  _lp(expected);
  test_assert(ls_cmp(to_list(res), expected), 0);
  return OK;
}

static test_info tests[] = {
  TEST(expr_grouping),
};

TEST_SUITE(tests, "eval.c")

#endif

