#include "fn.h"

#include <stdlib.h>
#include <string.h>

#include "assoc.h"
#include "error.h"
#include "eval.h"
#include "sym.h"
#include "types.h"
#include "util.h"

#define DEFAULT_EVAL_LVL INT_MIN

static void callable_init(callable *c, num prec, bool has_leftarg,
    bool accepts_tupleargs, bool eval_args, num n_args, cell assigned_name) {
  *c = (callable) {
    .prec = prec,
    .has_leftarg = has_leftarg,
    .accepts_tupleargs = accepts_tupleargs,
    .accepts_curriedargs = false,
    .eval_args = eval_args,
    .n_args = n_args,
    .assigned_name = assigned_name,
    .arg_processors = NULL,
    .eval_level = DEFAULT_EVAL_LVL,
  };
}

// Argument foramt:
// ( <arg_opt>, <arg_opt>, ... )
// modifiers := `+` | `?`
// name      := <'name'> | <'name'> `:` <'type'>
// <name> <modifiers>? | <name> <modifiers>? `$` <'kwd name'> | `$` <'kwd name'>

static void argopts_error() {
  errors("SyntaxError", "Invalid argument specification syntax:\n"
      "Expected: \n"
      "[$ <kwd_sym>] name [+ | ? | *].");
}

static void argopts_checkls(list *l) {
  if (!ls_len(l)) { argopts_error(); }
}

static cell argopts_checksym(cell c) {
  if (celltype(c) != t_sym) { argopts_error(); }
  return c;
}

static void verify_argopts(arg_opt *ao, size_t n) {
  bool seen_nonkwd_optional = false,
       seen_nonkwd_repeat = false;
  for (size_t i = 0; i < n; ++i) {
    arg_opt a = ao[i];
    if (!is_set(a.kwd)) {
      eassert(!seen_nonkwd_optional, "InvalidArgumentDefError",
          "Optional non-keyword argument needs to be last argument.");
      eassert(!seen_nonkwd_repeat, "InvalidArgumentDefError",
          "Repeating non-keyword argument needs to be last argument.");
      if (a.optional) { seen_nonkwd_optional = true; }
      if (a.repeating) { seen_nonkwd_repeat = true; }
    }
    if (a.kwd_only) {
      eassert(is_set(a.kwd), "InvalidArgumentDefError",
          "Keyword only argument but keyword is not set.");
    }
  }
}

static bool is_kwd_ident(list *larg) {
  if (!ls_len(larg)) { return false; }

  cell next = ls_get(larg, 0);
  if (ls_len(larg) > 1 && sym_eq(next, s_dollar)) {
    return true;
  }
  if (celltype(next) == t_sym) {
    const char *s = sym_to_cstr(next);
    if (s[0] == '$') {
      return true;
    }
  }

  return false;
}

static cell get_kwd_ident(list *larg) {
  cell next = deassociate(ls_get(larg, 0)),
       kwd = nil;

  if (sym_eq(next, s_dollar)) {
    argopts_checkls(larg);
    slice_popfront(larg);
    kwd = slice_popfront(larg);
  } else if (celltype(next) == t_sym) {
    const char *s = sym_to_cstr(next);
    if (s[0] == '$') {
      kwd = sym(&s[1]);
      slice_popfront(larg);
    }
  }

  return kwd;
}

static cell get_argopt_argprocs(list *args) {
  if (ls_len(args) < 2) { return nil; }
  cell next = deassociate(ls_get(args, 0));

  if (!sym_eq(next, s_doublecolon)) { return nil; }
  cell aps = ls_get(args, 1);
  if (is_callable(aps) || celltype(aps) == t_sym) {
    aps = from_list(ls_of(1, aps));
  }
  if (celltype(aps) != t_list) { return nil; }

  slice_popfront(args); slice_popfront(args);
  dict *apd = dict_mk();
  list *apl = to_list(aps);
  for (size_t i = 0; i < ls_len(apl); ++i) {
    cell v = ls_get(apl, i);
    if (celltype(v) == t_list && is_pair(to_list(v))) {
      dict_set(apd, fst(v), snd(v));
    } else {
      eassert(!dict_contains(apd, nil), "SyntaxError",
          "Multiple default arg-processors for argument %s."); // TODO(errmsg)
      dict_set(apd, nil, v);
    }
  }
  return from_list(apd);
}

static arg_opt *get_argopts(list *args, list *names, list *types) {
  ls_clear(names);
  ls_clear(types);
  arg_opt *res = mm_alloc(sizeof(arg_opt) * ls_len(args));
  for (size_t i = 0; i < ls_len(args); ++i) {
    cell arg = ls_get(args, i);
    cell kwd = nil;
    bool kwd_only = false;
    bool optional = false;
    bool repeating = false;
    bool multi_kwd = false;
    cell arg_procs = nil;
    if (meta_sym_eq(arg, ct_appl)) {
      list *larg = ls_slice(to_list(arg), 0, -1);

      argopts_checkls(larg);
      kwd = get_kwd_ident(larg);
      cell next = nil;

      if (ls_len(larg) == 0) {
        if (!is_set(kwd)) { argopts_error(); }
        kwd_only = true;
      } else {
        next = deassociate(slice_popfront(larg));
        arg_procs = get_argopt_argprocs(larg);

        if (is_kwd_ident(larg)) {
          list *kwds = ls_of_m_mut(ct_tuple, 1, kwd);
          list *kwd_argnames = ls_of_m_mut(ct_tuple, 1, next);
          arg_procs = from_list(ls_of_mut(1, arg_procs));
          do {
            ls_append(kwds, get_kwd_ident(larg));
            if (!is_kwd_ident(larg)) {
              ls_append(kwd_argnames, tc(slice_popfront(larg), t_sym));
            } else {
              ls_append(kwd_argnames, kwd);
            }
            ls_append(to_list(arg_procs), get_argopt_argprocs(larg));
          } while (is_kwd_ident(larg));
          ls_append(names, sym(ls_join(kwd_argnames, "_")));
          ls_append(types,
              from_list(ls_setmeta(ls_of_nils(ls_len(kwds)), ct_tuple)));
          kwd = from_list(kwds);
          multi_kwd = true;
          if (ls_len(larg)) {
            next = deassociate(slice_popfront(larg));
          } else {
            next = nil;
          }
        }

        if (!is_set(arg_procs)) {
          arg_procs = get_argopt_argprocs(larg);
        }

        if (sym_eq(next, s_plus) || sym_eq(next, s_qm)
            || sym_eq(next, s_asterisk)) {
          if (!is_set(kwd) || !is_set(arg)) { argopts_error(); }
          kwd_only = celltype(kwd) == t_sym;
        } else {
          arg = next;
          next = (ls_len(larg)) ? deassociate(slice_popfront(larg)) : nil;
        }

        if (is_set(next)) {
          if (sym_eq(next, s_plus)) {
            repeating = true;
          } else if (sym_eq(next, s_qm)) {
            optional = true;
          } else if (sym_eq(next, s_asterisk)) {
            optional = true;
            repeating = true;
          } else {
            argopts_error();
          }
        }
      }
      if (ls_len(larg)) { argopts_error(); }
    }

    if (kwd_only) {
      ls_append(names, sym_cat(s_dollar, kwd));
      ls_append(types, nil);
    } else if (!multi_kwd) {
      if (celltype(arg) == t_list) {
        list *larg = to_list(arg);
        if (is_pair(larg)) {
          list *tup = to_list(arg);
          cell name = argopts_checksym(ls_get(tup, 0)),
               type = argopts_checksym(ls_get(tup, 1));

          if (names) { ls_append(names, name); }
          if (types) { ls_append(types, type); }
        } else {
          argopts_error();
        }
      } else {
        if (celltype(arg) == t_sym && sym_to_cstr(arg)[0] == '$') {
          kwd = sym(&sym_to_cstr(arg)[1]);
          kwd_only = true;
          if (names) { ls_append(names, arg); }
        } else {
          if (names) { ls_append(names, argopts_checksym(arg)); }
        }
        if (types) { ls_append(types, nil); }
      }
    }

    res[i] = (arg_opt) {
      .kwd = kwd,
      .kwd_only = kwd_only,
      .optional = optional,
      .repeating = repeating,
      .arg_proc = arg_procs, 
    };
  }
  verify_argopts(res, ls_len(args));
  return res;
}

mmethod *mm_mk_native_n(native_fn f, bool left_arg, int nargs,
    cell closed_vars[], size_t n_closed_vars) {
  return
      mm_mk_native(f, left_arg, mk_arg_ls(nargs), closed_vars, n_closed_vars);
}

mmethod *mm_mk_native(native_fn f, bool left_arg, list *args,
    cell closed_vars[], size_t n_closed_vars) {
  assert(args);
  mmethod *m = mm_alloc(sizeof(mmethod) + sizeof(cell) * n_closed_vars);
  *m = (mmethod) {
    .type = clt_ffi,
    .fn = f,
    .arg_names = ls_mk(),
    .arg_types = ls_mk(),
    .closed_var_names = NULL,
    .n_closed_vars = n_closed_vars,
  };
  callable_init(&m->callable, DEFAULT_PREC, left_arg, false, true,
      ls_len(args) - ((left_arg) ? 1 : 0), nil);
  m->callable.arg_opts = get_argopts(args, m->arg_names, m->arg_types);

  if (closed_vars) {
    memcpy(m->closed_vars, closed_vars, sizeof(cell) * n_closed_vars);
  } else {
    for (size_t i = 0; i < n_closed_vars; ++i) { m->closed_vars[i] = nil; }
  }

  return m;
}

mmethod *mm_mk_src(cell args, cell body, cell closed_vars[],
    size_t n_closed_vars) {
  size_t na_args = (celltype(args) == t_list) ? ls_len(to_list(args)) : 1;
  list *largs = ls_mk_init(0, na_args, s_any, NULL, false, true, nil);
  if (celltype(args) == t_list) {
    ls_extend(largs, to_list(args));
  } else {
    ls_append(largs, args);
  }

  mmethod *m = mm_alloc(sizeof(mmethod) + sizeof(cell) * n_closed_vars);
  list *arg_types = ls_of_nils(na_args);
  for (size_t i = 0; i < na_args; ++i) {
    cell arg = ls_get(largs, i);
    if (celltype(arg) == t_list && is_pair(to_list(arg))) {
      list *p = to_list(arg);
      cell name = ls_get(p, 0),
           type = ls_get(p, 1);
      ls_set(arg_types, i, type);
      ls_set(largs, i, name);
    }

  }
  *m = (mmethod) {
    .type = clt_proton,
    .body = body,
    .arg_names = ls_mk(),
    .arg_types = ls_mk(),
    .closed_var_names = NULL,
    .n_closed_vars = 0,
  };
  callable_init(&m->callable, DEFAULT_PREC, false, false, true, na_args, nil);
  m->callable.arg_opts = get_argopts(largs, m->arg_names, m->arg_types);

  if (closed_vars) {
    memcpy(m->closed_vars, closed_vars, sizeof(cell) * n_closed_vars);
  } else {
    for (size_t i = 0; i < n_closed_vars; ++i) { m->closed_vars[i] = nil; }
  }

  assert(arg_types ? na_args == ls_len(arg_types) : true
      && "Expected nargs == ls_len(arg_types)");

  return m;
}

fn *fn_mk(size_t n_methods) {
  fn *rfn = new(fn);
  *rfn = (fn) {
    .methods = ls_mk_init(0, n_methods, s_ptr, NULL, false, false, nil),
  };
  callable_init(&rfn->callable, DEFAULT_PREC, false, false, false, 0, nil);

  return rfn;
}

fn *fn_mk_native(native_fn f, bool left_arg, list *args,
    cell closed_vars[], size_t n_closed_vars) {
  mmethod *m = mm_mk_native(f, left_arg, args, closed_vars, n_closed_vars);
  fn *rfn = new(fn);
  *rfn = (fn) {
    .methods = ls_mk_init(0, 1, s_ptr, NULL, false, false, nil),
  };
  callable_init(&rfn->callable, DEFAULT_PREC, left_arg,
      false, true, ls_len(args), nil);
  m->callable.arg_opts = get_argopts(args, m->arg_names, m->arg_types);
  ls_append(rfn->methods, from_ptr(m));
  return rfn;
}

fn *fn_mk_src(cell src, num nargs, bool left_arg, list *arg_names, list *arg_types) {
  assert(false && "TODO");
  return NULL;
}

void fn_method_add(fn *f, mmethod *m) {
  assert(false && "TODO");
}

fn *fn_syntax_mk(cell cl) {
  //eassert(cl_has_syntax(to_callable(cl)),
  //    "SyntaxDefinitionError", "Callable does not support syntactic dispatch");
  fn *ff = fn_mk(2);
  ff->callable.n_args = to_callable(cl)->n_args;
  ls_append(ff->methods, cl);
  ff->syntactic_dispatch = true; 
  return ff;
}

fn *syntax_add(cell ex, cell cl) {
  switch (celltype(ex)) {
    case t_fn: {
      fn *f = to_fn(ex);
      if (f->syntactic_dispatch) {
        // TODO: find if method maps to an existing entry.
        ls_append(f->methods, cl);
      } else if (cl_has_syntax(&f->callable) || cl_has_syntax(to_callable(cl))) {
        fn *ff = fn_mk(2);
        ls_append(ff->methods, from_fn(f));
        ls_append(ff->methods, cl);
        ff->syntactic_dispatch = true;
        return ff;
      } else {
        fn_method_add(f, to_method(cl));
      }
      return f;
    }
    case t_method: {
      fn *ff = fn_mk(2);
      if (cl_has_syntax(to_callable(ex)) || cl_has_syntax(to_callable(cl))) {
        ls_append(ff->methods, ex);
        ls_append(ff->methods, cl);
        ff->syntactic_dispatch = true;
      } else {
        fn_method_add(ff, to_method(ex));
        fn_method_add(ff, to_method(cl));
      }
      return ff;
    }
    default:
      errors("TypeError", "Expected callable.");
      return NULL;
  }
  errors("TypeError", "Expected callable.");
  return NULL;
}

fn *fn_setprec(fn *f, num prec) {
  ((callable*) f)->prec = prec;
  return f;
}

mmethod *mm_setprec(mmethod *m, num prec) {
  ((callable*) m)->prec = prec;
  return m;
}

fn *fn_setname(fn *f, cell name) {
  ((callable *) f)->assigned_name = name;
  return f;
}

callable *to_callable(cell c) {
  if (is_assoc(c)) { c = deassociate(c); }
  switch (celltype(c)) {
    case t_fn: return (callable *) c.f;
    case t_method: return (callable *) c.mm;
    case t_partial: return to_callable(c.p->fn);
    default:
      errors("InvalidPartialError", "Invalid partial callable");
      return NULL;
  }
}

void cl_add_argprocessor(callable *c, cell name, cell processor) {
  if (!c->arg_processors) {
    c->arg_processors = dict_mk();
  }
  to_callable(processor)->eval_args = false;
  dict_set(c->arg_processors, name, processor);
}

// DISPATCH TABLES.
//
// TODO(opt): Benchmark against standard class/table based approach.

typedef uint8_t dispatch_index;

#define DISPATCH_INDEX_MAX ((1 << (sizeof(dispatch_index) * 8)) - 1)
//#define DISPATCH_INDEX_MAX 32

// Dispatch reimpl.
//
// 0. Store method indices for each row
// 1. Sort each col + associated method index
// 2. Dispatch:
//    - Binary search for type + all parents (easy by masking out type_id components)
//    - Build bitmask of matched types
//    - Unify (&) bit masks for all arguments, == 1 => dispatch, > 1 ambiguous
//
// Optimizations:
//    - Keep shorter bitmasks for < 256 methods
//
// Limitations:
//    - Max 256 (configurable) methods per multi-method
//    - 256 max methods => 64 * 4 bit (4 words 64) bitmasks (shorter possible though)
//
// Complexity:
//    - Dispatch: k * log_2(n) * h
//        (n < N::methods, k::args, h::depth of type hierarchy, N = 2^8|2^16


typedef struct {
  dispatch_index len;
  dispatch_index n_args;

  mmethod **methods;

  dispatch_index *method_index;
  type_id table[];
} dispatch_table;

static type_id *dt_entry(dispatch_table *t, dispatch_index method,
    dispatch_index arg) {
  return &t->table[arg * t->len + method];
}

static dispatch_index *dt_method_index_entry(dispatch_table *t,
    dispatch_index method, dispatch_index arg) {
  return &t->method_index[arg * t->len + method];
}

static dispatch_table *dt_alloc(dispatch_index len, dispatch_index args) {
  const size_t sz_tid_table = len * args * sizeof(type_id),
               sz_mth_table = len * sizeof(mmethod *),
               sz_index = len * args * sizeof(dispatch_index);
  dispatch_table *res = mm_alloc(sizeof(dispatch_table)
      + sz_tid_table
      + sz_mth_table
      + sz_index);
  *res = (dispatch_table) {
    .n_args = args,
    .len = len,
  };
  res->methods = (mmethod **) (
      ((char *) res) + sizeof(dispatch_table) + sz_tid_table);
  res->method_index = (dispatch_index *) (
      ((char *) res) + sizeof(dispatch_table) + sz_tid_table + sz_mth_table);
  return res;
}

static dispatch_index dispatch_qsort_partition(type_id *ids,
    dispatch_index from, dispatch_index to, dispatch_index index[]) {
  type_id pivot_id = ids[index[to]]; // *dt_entry(dt, index[to], arg);
  dispatch_index l = from;
  for (size_t i = from; i < to; ++i) {
    type_id id = ids[index[i]]; // *dt_entry(dt, index[i], arg);
    int cmp = typeid_cmp(id, pivot_id);
    if (cmp < 0) { swap(index[i], index[l]); l++; }
  }
  swap(index[to], index[l]);
  return l;
}

static void dispatch_sort_rowr(type_id *ids,
    dispatch_index from, dispatch_index to, dispatch_index index[]) {
  if (to <= from) { return; }
  dispatch_index split = dispatch_qsort_partition(ids, from, to, index);
  if (split > 0) {
    dispatch_sort_rowr(ids, from, split - 1, index);
  }
  if (split < DISPATCH_INDEX_MAX) {
    dispatch_sort_rowr(ids, split + 1, to, index);
  }
}

static void dispatch_sort_row(dispatch_table *dt, dispatch_index arg) {
  // 1. build sorted list of indexes.
  dispatch_index index[dt->len];
  for (size_t i = 0; i < dt->len; ++i) {
    index[i] = i;
  }
  // 2. shuffle/sort index in qsort list.
  dispatch_sort_rowr(dt_entry(dt, 0, arg), 0, dt->len - 1, index);
  // 3. reorder table entries according to index list.
  type_id cpt[dt->len];
  for (dispatch_index i = 0; i < dt->len; ++i) {
    cpt[i] = *dt_entry(dt, index[i], arg);
    *dt_method_index_entry(dt, i, arg) = index[i];
  }
  memcpy(dt_entry(dt, 0, arg), cpt, sizeof(type_id) * dt->len);
}

// TODO: dispatch only on certain desired arguments.
static dispatch_table *dispatch_table_mk(list *methods) {
  const size_t len = ls_len(methods);
  eassert(len > 0, "MethodDefinitionError",
      "Invalid multimethod, require at least one method to dispatch to.");
  eassert(len < DISPATCH_INDEX_MAX, "MethodDefinitionError",
      "Multimethod has too many dispatched methods (%d max).");// TODO(errormsg)

  const size_t nargs = to_method(ls_get(methods, 0))->callable.n_args;
  eassert(nargs > 0, "MethodDefinitionError",
      "Invalid multimethod, require at least one dispatched arguments.");

  dispatch_table *t = dt_alloc(len, nargs);
  // 1. Build the basic method dispatch table from passed method arguments.
  for (size_t i = 0; i < len; ++i) {
    mmethod *m = to_method(ls_get(methods, i));
    eassert(nargs == m->callable.n_args, "MethodDefinitionError",
        "Invalid multi-method definition, fn arguments do not match.");
    eassert(ls_len(m->arg_types) == nargs, "MethodDefinitionError",
        "Method definition has insufficient argument types.");
    t->methods[i] = m;
    for (size_t j = 0; j < nargs; ++j) {
      *dt_entry(t, i, j) = typeid(ls_get(m->arg_types, j));
    }
  }

  // 2. Sort each row and its method index.
  for (size_t j = 0; j < nargs; ++j) {
    dispatch_sort_row(t, j);
  }

  return t;
}


typedef enum { bs_first, bs_last } search_direction;
typedef struct { dispatch_index start; dispatch_index end; } dispatch_range;
static inline size_t min(size_t a, size_t b) { return (a < b) ? a : b; }

static dispatch_index dispatch_bin_search(dispatch_table *dt, type_id tid,
    dispatch_index arg, dispatch_range search_range,
    search_direction dir) {
  dispatch_index l = search_range.start, h = search_range.end;
  dispatch_index rounds_up = (dir == bs_first) ? 0 : 1;
  while (l < h) {
    dispatch_index m = l + (h - l + rounds_up) / 2;
    int cmp = typeid_cmp(tid, *dt_entry(dt, m, arg));
    if (cmp < 0) {
      h = m - 1;
    } else if (cmp == 0) {
      if (dir == bs_first) { h = m; } else { l = m; }
    } else {
      l = m + 1;
    }
  }
  return h;
}

static dispatch_index dispatch_exp_search_last(dispatch_table *dt, type_id tid,
    dispatch_index arg, dispatch_index start) {
  dispatch_index i = 1;
  while (start + i < dt->len) {
    if (typeid_cmp(tid, *dt_entry(dt, start + i, arg)) != 0) { break; }
    i *= 2;
  }
  return dispatch_bin_search(dt, tid, arg,
      (dispatch_range) {
        .start = start + i / 2,
        .end = min(start + i, dt->len - 1),
      }, bs_last);
}

static void dispatch_arg_matches(dispatch_table *dt, type_id tid,
    dispatch_index arg, bitfield *prev_matches,
    uint16_t method_penalties[]) {
  bitfield *curr_matches = bitfield_onstack(DISPATCH_INDEX_MAX);
  dispatch_index last = dt->len - 1;
  size_t depth = 0;

  // Try to find tid and it's parents.
  while (!typeid_eq(tid, tid_none)) {
    dispatch_index fst = dispatch_bin_search(dt, tid, arg,
        (dispatch_range) { .start = 0, .end = last }, bs_first),
                  lst = dispatch_exp_search_last(dt, tid, arg, fst);
    //printf("arg %d: [", arg);
    if (lst >= fst) {
      for (size_t i = fst; i <= lst; ++i) {
        dispatch_index k = *dt_method_index_entry(dt, i, arg);
        if (!bitfield_get(curr_matches, k)) {
          method_penalties[k] += depth;
        }
        bitfield_set(curr_matches, k, true);
        //printf("%d, ", k);
      }
    }
    //printf("] <- tid %x, %x\n", tid.words[0].w, tid.words[1].w);
    tid = typeid_super(tid);
    last = fst - 1;
    depth++;
  }

  //char *e = bitfield_print(curr_matches, NULL);
  //printf("Bitfield %s\n", e);
  //free(e);
  bitfield_intersect(prev_matches, curr_matches);
}

define_clz(bitfield_word)

static size_t dispatch(dispatch_table *dt, type_id *args) {
  // TODO: Benchmark dynamic allocation with dt->len length.

  // Dispatch all arguments, mark matches in bitfield.
  // The matches for each argument need to be intersected so that only
  // methods matched for all arguments get recorded.
  // Additionally, each match that is a super type of a passed type incurrs
  // a penalty proportional to the difference in depth of the type hierarchy.
  // The match with the lowest penalty is chosen, multiple matches with similar
  // penalty indicate that the method match is ambiguous.
  bitfield *curr_matches = bitfield_onstack(DISPATCH_INDEX_MAX);
  uint16_t method_penalties[DISPATCH_INDEX_MAX];
  memset(method_penalties, 0, sizeof(uint16_t) * dt->len);
  bitfield_init(curr_matches, 1);
  size_t nargs = dt->n_args;
  for (size_t i = 0; i < nargs; ++i) {
    dispatch_arg_matches(dt, args[i], i, curr_matches, method_penalties);
  }

  const size_t words = BITFIELD_WORDS(curr_matches->len);
  dispatch_index candidate = dt->len;
  uint16_t best_penalty = UINT16_MAX;
  for (size_t i = 1; i <= words; ++i) {
    while (curr_matches->words[words - i]) {
      dispatch_index r = (words - i) * sizeof(bitfield_word) * 8
          + sizeof(bitfield_word) * 8
              - clz_bitfield_word(curr_matches->words[words - i]) - 1;
      bitfield_set(curr_matches, r, false);
      if (method_penalties[r] == best_penalty) {
        errors("MultiMethodError",
            "Ambiguous multi-method dispatch for following arguments (%s)");
        // TODO(errormsg)
      }
      if (method_penalties[r] < best_penalty) {
        candidate = r;
        best_penalty = method_penalties[r];
      }
    }
  }
  if (candidate < dt->len) {
    return candidate; //dt->methods[candidate];
  }
  errors("MultiMethodDispatchError",
      "Cannot find suitable method for argument types (%s)"); // TOOD(errormsg)

  return dt->len; //NULL;
}

mmethod *fn_dispatch(fn *f, list *args) {
  list *argst = ls_mk_init(0, ls_len(args), s_any, NULL, false, true, nil);
  for (size_t i = 0; i < ls_len(args); ++i) {
    ls_append(argst, typeof(ls_get(args, i)));
  }

  mmethod *general = NULL;
  for (size_t i = 0; i < ls_len(f->methods); ++i) {
    mmethod *m = (mmethod *) ls_get(f->methods, i).ptr;
    list *argt = m->arg_types;

    // All nils is the default method.
    if (ls_len(argt) > 0 && is_nil(ls_get(argt, 0))) {
      mmethod *gen_candidate = NULL;
      for (size_t j = 0; j < ls_len(argt); ++j) {
        cell argt_j = ls_get(argt, j);
        if (is_nil(argt_j)) {
          gen_candidate = m;
        } else if (cell_cmp(argt_j, ls_get(argst, j)) != 0) {
          gen_candidate = NULL;
          break;
        }
      }
      if (gen_candidate) { general = gen_candidate; }
    }

    if (ls_len(argst) != ls_len(argt)) { continue; }

    for (size_t j = 0; j < ls_len(argt); ++j) {
      if (sym_eq(ls_get(argt, j), ls_get(argst, j))) {
        return m;
      }
    }
  }
  return general;
}

cell fn_call(fn *f, env *e, list *args) {
  mmethod *m = fn_dispatch(f, args);
  if (!m) { verbose_lp(args); verbose_sp("\n"); }
  // TODO(errormsg)
  eassert(m, "MethodDispatchError",
      "No valid method of function %s found for arguments %s.");
  return method_execute(m, e, args);
}

static bool check_args(callable *cl, list *args) {
  if (cl->n_args >= 0) {
    return cl->n_args + ((cl->has_leftarg) ? 1 : 0) == ls_len(args);
  }
  return abs(cl->n_args) + ((cl->has_leftarg) ? 1 : 0) <= ls_len(args);
}

cell method_execute(mmethod *m, env *e, list *args) {
  callable *cl = (callable *) m;

  eassert(check_args(cl, args),
      "InvalidArgumentError", "Invalid argument count to callable");

  verbose_sp("Method Call: "); verbose_cp(cl->assigned_name);
  verbose_sp(" args: "); verbose_lp(args); verbose_sp(" => ");

  switch (m->type) {
    case clt_proton: {
      if (m->callable.eval_level != DEFAULT_EVAL_LVL) {
        e = env_for_eval_level(e->globals, m->callable.eval_level);
      }
      env *ne = env_new(e, e->globals);
      cell fname = m->callable.assigned_name;
      env_set(ne, s_self, from_method(m));
      m->callable.assigned_name = fname;
      assert(m->arg_names && ls_len(args) == ls_len(m->arg_names)
          && "Invalid source method.");
      for (size_t i = 0; i < ls_len(args); ++i) {
        cell arg_name = ls_get(m->arg_names, i),
             arg_val = ls_get(args, i);
        if (is_nil(arg_name)) { continue; }
        assert(celltype(arg_name) == t_sym && "Invalid argument name");
        env_set(ne, arg_name, arg_val);
      }
      return /*verbose_cpl*/(eval(ne, m->body));
    }
    case clt_ffi:
      return /*verbose_cpl*/(
          (m->fn)(e, from_list(args), m->closed_vars, m));
  }
}

extern bool cell_print_partial_ext;
static bool callable_named_print(callable *c, char **res, char *ex) {
  if (!cell_print_partial_ext && !is_nil(c->assigned_name)) {

    *res = esprintf(ex, "\u1D18");
    *res = cell_print(c->assigned_name, *res);
    return true;
  }
  return false;
}

char *mm_print(mmethod *m, char *ex) {
  char *res = NULL;
  if (callable_named_print(&m->callable, &res, ex)) { return res; }
  res = esprintf(ex, "<method ");
  if (is_nil(m->callable.assigned_name)) {
    res = esprintf(res, "anon");
  } else {
    res = cell_print(m->callable.assigned_name, res);
  }
  res = esprintf(res, " %d \uFE5B", m->callable.n_args);

  for (size_t i = 0; i < m->n_closed_vars; ++i) {
    res = cell_print(m->closed_vars[i], res);
    if (i < m->n_closed_vars - 1) { res = esprintf(res, ", "); }
  }
  return esprintf(res, " \uFE5C >");
}

char *fn_print(fn *f, char *ex) {
  char *res = NULL;
  if (callable_named_print(&f->callable, &res, ex)) { return res; }
  res = esprintf(ex, "<fn ");
  if (is_nil(f->callable.assigned_name)) {
    res = esprintf(res, "anon");
  } else {
    res = cell_print(f->callable.assigned_name, res);
  }

  for (size_t i = 0; i < ls_len(f->methods); ++i) {
    res = mm_print(to_method(ls_get(f->methods, i)), res);
  }
  return esprintf(res, ">");
}

bool is_callable(cell c) {
  cell_type ty = celltype(c);
  return ty == t_partial || ty == t_fn || ty == t_method;
}

callable *get_callable(cell c) {
  return (celltype(c) == t_partial) ? to_callable(c.p->fn) : to_callable(c);
}

void cl_set_accepts_curriedargs(callable *c, bool accepts) {
  c->accepts_curriedargs = accepts;
}

void cl_set_accepts_tupleargs(callable *c, bool accepts) {
  c->accepts_tupleargs = accepts;
}

bool cl_has_argprocessor(callable *c, cell name) {
  return dict_contains(c->arg_processors, name);
}

bool cl_has_argprocessors(callable *c) {
  return c->arg_processors != NULL;
}

bool cl_has_syntax(callable *c) {
  for (size_t i = 0; i < c->n_args; ++i) {
    if (is_set(c->arg_opts[i].kwd)) { return true; }
  }
  return false;
}

static env *apdict_to_env(env *parent, env *sym_resolve_env, dict *d) {
  env *ne = env_new(parent, parent->globals);
  if (d) {
    //verbose_sp("ARGPROC ENV: ");
    for (dict_it i = dict_iter(d); !dict_itend(i); dict_itnext(&i)) {
      list *ap_info = to_list(dict_itval(i));
      cell ap = ls_get(ap_info, 1),
           name = ls_get(ap_info, 0);
      if (celltype(ap) == t_sym) {
        ap = deassociate(ap);
        cell *c = env_lookup(sym_resolve_env, ap);
        // TODO(errmsg)
        eassert(c, "SyntaxError", "Undefined argument processor \"%s\".");
        ap = *c;
      }
      //verbose_cp(name); verbose_sp(": "); verbose_cp(ap); verbose_sp("\n");
      env_set(ne, name, ap);
    }
  }
  return ne;
}

env *cl_argprocessors_env(env *parent, env *sym_resolve_env, callable *c) {
  return apdict_to_env(parent, sym_resolve_env, c->arg_processors);
}

env *argopt_argproc_for_arg(env *parent, env *sym_resolve_env,
    arg_opt *ao, size_t n_kwd) {
  cell aps = ao->arg_proc;
  if (!is_set(aps)) { return env_new(parent, parent->globals); }

  assert(celltype(aps) == t_list);
  assert(n_kwd == 0 || celltype(ao->kwd) == t_list);

  list *apl = to_list(aps);
  if (is_dict(apl)) {
    assert(n_kwd == 0);
    return apdict_to_env(parent, sym_resolve_env, apl);
  }
  aps = ls_get(apl, n_kwd);
  assert(celltype(aps) == t_list && is_dict(to_list(aps)));
  return apdict_to_env(parent, sym_resolve_env, to_list(aps));
}

cell partial_mk(cell f) {
  assert(celltype(f) == t_fn || celltype(f) == t_method);
  partial *res = new(partial);
  res->fn = f;
  int nargs = partial_expected_args(res);
  res->applied_args =
      ls_mk_init(0, abs(nargs), s_any, NULL, false, nargs > 0, ct_grouped_args);
  return (cell) { .ty = t_partial, .p = res };
}

#define sgn(x) (((x) > 0) - ((x) < 0))

int partial_expected_args(partial *p) {
  callable *cl = to_callable(p->fn);
  int left_arg = ((cl->has_leftarg) ? 1 : 0);
  return cl->n_args + sgn(left_arg) * left_arg;
}

partial *partial_copy(partial *p) {
  partial *res = partial_mk(p->fn).p;
  ls_extend(res->applied_args, p->applied_args);
  return res;
}

list *mk_arg_ls(num n_args) {
  assert(n_args < 26);
  int p_args = abs(n_args);
  list *res = ls_mk_init(0, p_args, s_any, NULL, false, true, nil);
  for (int i = 0; i < p_args; ++i) {
    char l[2] = " ";
    l[0] = 'a' + (char) i;
    ls_append(res, sym(l));
  }
  if (n_args < 0) {
    cell last = ls_get(res, -1),
         repeating = from_list(ls_of_m(ct_appl, 2, last, s_plus));
    ls_set(res, -1, repeating);
  }
  return res;
}

#ifdef TESTING

// Test Suite init/fin need to be set before including test.h. Hacky :/
#define TEST_SUITE_INIT init();
#define TEST_SUITE_FIN fin();

#include "test.h"

#include "env.h"

static cell _f_add(env *e, cell args, cell *closed_vars, mmethod *self) {
  assert(celltype(args) == t_list);
  list *largs = to_list(args);
  assert(ls_len(largs) == 2);
  cell a = ls_get(largs, 0),
       b = ls_get(largs, 1);
  return from_num(to_num(a) + to_num(b));
}

static int test_fn_dispatch() {
  cell d_arg_types[] = { ct_num, ct_num };
  list *arg_types = ls_mk_init(2, 2, s_any, &d_arg_types, true, true, nil);
  fn *f = fn_mk_native(_f_add, false, arg_types, NULL, 0);
  list *call_args = ls_of(2, from_num(1), from_num(3));

  mmethod *m = fn_dispatch(f, call_args);
  test_assert_neq((void *) m, (void *) NULL);
  cell add_res = method_execute(m, NULL, call_args);
  test_assert(to_num(add_res), 4);
  return OK;
}

static type_id tid_num, tid_real, tid_int, tid_int8, tid_int16, tid_float,
               tid_float32, tid_float64,
               tid_collection, tid_container, tid_list, tid_vector,
               tid_linkedlist, tid_doublelinkedlist, tid_arrayvector,
               tid_fixedvector;

static void test_setup();

static void init() {
  gc_init();
  symtab_init();
  types_init(NULL);
  condsys_init(NULL);
  test_setup();
}

static void fin() {
  condsys_finalize();
  symtab_finalize();
  gc_finalize();
  types_finalize();
}

static void test_setup() {
  type_spec ts_abstract = (type_spec) { .kind = ti_abstract };
  tid_num = type_def(sym("num"), nil, &ts_abstract);
  tid_real = type_def(sym("real"), sym("num"), &ts_abstract);

  tid_int = type_def(sym("int"), sym("real"), &ts_abstract);
  type_spec ts_int8 = (type_spec) { .kind = ti_prim, .prim = { .bits = 8 } };
  tid_int8 = type_def(sym("int8"), sym("int"), &ts_int8);
  type_spec ts_int16 = (type_spec) { .kind = ti_prim, .prim = { .bits = 16 } };
  tid_int16 = type_def(sym("int16"), sym("int"), &ts_int16);

  tid_float = type_def(sym("float"), sym("real"), &ts_abstract);
  type_spec ts_float32 = (type_spec) { .kind = ti_prim, .prim = { .bits = 32 } };
  tid_float32 = type_def(sym("float32"), sym("float"), &ts_float32);
  type_spec ts_float64 = (type_spec) { .kind = ti_prim, .prim = { .bits = 64 } };
  tid_float64 = type_def(sym("float64"), sym("float"), &ts_float64);

  tid_collection = type_def(sym("collection"), nil, &ts_abstract);
  tid_container = type_def(sym("container"), sym("collection"), &ts_abstract);
  tid_list = type_def(sym("list"), sym("container"), &ts_abstract);
  tid_vector = type_def(sym("vector"), sym("container"), &ts_abstract);
  tid_linkedlist = type_def(sym("linkedlist"), sym("list"), &ts_abstract);
  tid_doublelinkedlist =
      type_def(sym("doublelinkedlist"), sym("list"), &ts_abstract);
  tid_arrayvector = type_def(sym("arrayvector"), sym("vector"), &ts_abstract);
  tid_fixedvector = type_def(sym("fixedvector"), sym("vector"), &ts_abstract);
}

extern type_id typecomps_to_typeid(type_id_component *tcs, size_t ntcs,
    type_info *ti);

static type_id id_for_entry(dispatch_index row, dispatch_index col) {
  return typecomps_to_typeid((
        type_id_component []) { row * 10 + col }, 1, NULL);
}

static int verify_table_row_sorted(dispatch_table *t, dispatch_index a) {
  for (size_t i = 1; i < t->len; ++i) {
    test_assert_lte(typeid_cmp(*dt_entry(t, i - 1, a), *dt_entry(t, i, a)), 0);
  }
  return OK;
}

static int verify_table(dispatch_table *dt) {
  for (size_t i = 0; i < dt->n_args; ++i) {
    sub_test(verify_table_row_sorted(dt, i));
  }
  return OK;
}

void dt_print_n(dispatch_table *t) {
  for (size_t i = 0; i < t->len; ++i) {
    printf("%2.4zu::", i);
    for (size_t j = 0; j < t->n_args; ++j) {
      type_id id = *dt_entry(t, i, j);
      char *name = cell_print(typeid_typeinfo(id)->name, NULL);
      printf("<%8.8s:%4d> ", name, *dt_method_index_entry(t, i, j));
      free(name);
    }
    printf("\n");
  }
}

#define args1(a, ta) ls_of(1, pair_mk(sym(#a), sym(#ta)))
#define args2(a, ta, b, tb) \
  ls_of(2, pair_mk(sym(#a), sym(#ta)), pair_mk(sym(#b), sym(#tb)))
#define alen(a) (sizeof(a) / sizeof(a[0]))
#define method(args) from_method(mm_mk_native(NULL, false, args, NULL, 0))
#define method_list(m) ls_mk_init(alen(m), alen(m), s_any, m, 0, 0, nil)

static int test_dispatch_table_mk() {
  cell methods_in_order[] = {
    method(args1(a, any)),
    method(args1(a, num)),
    method(args1(a, int)),
    method(args1(a, int8)),
    method(args1(a, int16)),
  };
  dispatch_table *dt_in_order =
      dispatch_table_mk(method_list(methods_in_order));
  sub_test(verify_table(dt_in_order));

  cell methods_rev_order[] = {
    method(args1(a, int16)),
    method(args1(a, int8)),
    method(args1(a, int)),
    method(args1(a, num)),
    method(args1(a, any)),
  };
  dispatch_table *dt_rev_order =
      dispatch_table_mk(method_list(methods_rev_order));
  sub_test(verify_table(dt_rev_order));

  cell methods_rnd_order[] = {
    method(args1(a, int16)),
    method(args1(a, num)),
    method(args1(a, any)),
    method(args1(a, int8)),
    method(args1(a, int)),
  };
  dispatch_table *dt_rnd_order =
      dispatch_table_mk(method_list(methods_rnd_order));
  sub_test(verify_table(dt_rnd_order));

  cell methods2[] = {
    method(args2(a, int8, b, int8)),
    method(args2(a, int16, b, int16)),
    method(args2(a, int, b, int)),
    method(args2(a, num, b, num)),
    method(args2(a, any, b, any)),
  };
  dispatch_table *dt2 = dispatch_table_mk(method_list(methods2));
  sub_test(verify_table(dt2));

  return OK;
}

static int test_dt_entry() {
#define DTE_ROWS 20
#define DTE_COLS 20
  dispatch_table *dt = dt_alloc(DTE_ROWS, DTE_COLS);
  for (size_t i = 0; i < DTE_ROWS; ++i) {
    for (size_t j = 0; j < DTE_COLS; ++j) {
      *dt_entry(dt, i, j) = id_for_entry(i, j);
    }
  }

  for (size_t i = 0; i < DTE_ROWS; ++i) {
    for (size_t j = 0; j < DTE_COLS; ++j) {
      test_assert(typeid_cmp(
            *dt_entry(dt, i, j), id_for_entry(i, j)), 0);
    }
  }

  return OK;
#undef DTE_ROWS
#undef DTE_COLS
}

extern int time(void *);

static int test_dispatch_sort_row() {
#define N 255
#define M 255
#define RAND_RUNS 10
  dispatch_table *t = dt_alloc(N, M);

  // In order.
  for (size_t i = 0; i < N; ++i) {
    for (size_t j = 0; j < M; ++j) {
      type_id id = id_for_entry(i, j);
      *dt_entry(t, i, j) = id;
    }
  }
  for (size_t i = 0; i < N; ++i) {
    dispatch_sort_row(t, i);
  }
  sub_test(verify_table(t));

  // Reverse.
  for (size_t i = 0; i < N; ++i) {
    for (size_t j = 0; j < M; ++j) {
      type_id id = id_for_entry(i, j);
      *dt_entry(t, i, j) = id;
    }
  }
  for (size_t i = 0; i < N; ++i) {
    dispatch_sort_row(t, i);
  }
  sub_test(verify_table(t));

  // Random.
  srand(time(NULL));
  for (size_t run = 0; run < RAND_RUNS; ++run) {
    for (size_t i = 0; i < N; ++i) {
      size_t val = rand() % 1000;

      for (size_t j = 0; j < M; ++j) {
        type_id id = id_for_entry(val, j);
        *dt_entry(t, i, j) = id;
      }
    }
    for (size_t i = 0; i < N; ++i) {
      dispatch_sort_row(t, i);
    }
    sub_test(verify_table(t));
  }
  return OK;
#undef RAND_RUNS
#undef M
#undef N
}

static int test_dispatch_arg() {

  return FAILED;
}

static int test_dispatch() {
  // - gen dispatch table
  // - try a bunch of arg types

#define args(...) ((type_id []) { __VA_ARGS__ })
#define m(mth) to_method(mth)
  cell m_add[] = {
    method(args2(a, any, b, any)),        //0
    method(args2(a, real, b, real)),      //1
    method(args2(a, real, b, any)),       //2 TODO: decide on ambiguity resln.
    method(args2(a, any, b, real)),       //3
    method(args2(a, int, b, int)),        //4
    method(args2(a, int8, b, int)),       //5
    method(args2(a, int8, b, int8)),      //6
    method(args2(a, int16, b, int8)),     //7
    method(args2(a, int16, b, int16)),    //8
    method(args2(a, float32, b, int8)),   //9
    method(args2(a, float32, b, float32)),//10
    method(args2(a, int, b, int8)),       //11
  };
  dispatch_table *t_add = dispatch_table_mk(method_list(m_add));

  test_assert(dispatch(t_add, args(tid_int8, tid_int8)), 6);
  test_assert(dispatch(t_add, args(tid_int, tid_int)), 4);
  test_assert(dispatch(t_add, args(tid_int16, tid_int8)), 7);
  test_assert(dispatch(t_add, args(tid_int16, tid_int16)), 8);
  test_assert(dispatch(t_add, args(tid_float32, tid_int8)), 9);
  test_assert(dispatch(t_add, args(tid_int, tid_int8)), 11);
  test_assert(dispatch(t_add, args(tid_float32, tid_float32)), 10);
  sub_test(test_assert_nosignal());

  cell m_amb[] = {
    method(args2(a, any, b, any)),        //0
    method(args2(a, int, b, int8)),       //1
    method(args2(a, int8, b, int)),       //2
    method(args2(a, any, b, any)),        //3
  };
  dispatch_table *t_amb = dispatch_table_mk(method_list(m_amb));
  dispatch(t_amb, args(tid_any, tid_any));
  sub_test(test_assert_signal("MultiMethodError"));
  dispatch(t_amb, args(tid_int8, tid_int8));
  sub_test(test_assert_signal("MultiMethodError"));
#undef m
#undef args

  return OK;
}

#undef method_list
#undef method
#undef alen
#undef args2
#undef args1

static test_info tests[] = {
  TEST(fn_dispatch),
  TEST(dispatch_table_mk),
  TEST(dt_entry),
  TEST(dispatch_sort_row),
  TEST(dispatch_arg),
  TEST(dispatch),
};

TEST_SUITE(tests, "fn.c")

#endif
