#pragma once

#include <stdbool.h>
#include <setjmp.h>

#include "cell.h"
#include "env.h"
#include "parser.h"

//void errorp(pos p, char *signal, char *msg);
void errors(const char *signal, const char *msg);
void errorsf(const char *signal, const char *fmsg, ...);
//void errorpf(pos p, char *signal, char *fmsg, ...);

void ssignal(cell signal, cell data);

void eassert(bool cond, const char *sig, const char *msg);
#define eassertr(r, cond, sig, msg) \
  do {                              \
    bool condn = (cond);            \
    if (!condn) {                   \
      errors(sig, msg);             \
      return r;                     \
    }                               \
  } while (0);

void ssignal(cell signal, cell data);

void push_signal_handler(cell signal, cell handler);
cell pop_signal_handler(cell signal);
cell get_signal_handler(cell signal);

void push_recovery(jmp_buf *b, cell name, cell desc);
void pop_recovery(cell signal);
cell list_recoveries();
cell invoke_recovery(cell recovery_name, cell args);
cell get_recovery_args();

void condsys_init(globals *g);
void condsys_finalize();

// Garbage collection.
struct gc_state;
void condsys_gc_mark_globals(struct gc_state *gs);

// Predefined errors.
void invalid_args_error(size_t nargs_exp, size_t nargs_got);

