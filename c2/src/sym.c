#include "sym.h"

#include <stdlib.h>
#include <string.h>

#include "assoc.h"
#include "dict.h"
#include "list.h"
#include "util.h"

cell ct_num;
cell ct_sym;
cell ct_list;
cell ct_str;
cell ct_fn;
cell ct_partial;
cell ct_pair;
cell ct_appl;
cell ct_appl_grouped;
cell ct_ptr;
cell ct_mem;
cell ct_assoc;
cell ct_nil;
cell ct_void;
cell ct_dict;
cell ct_code;
cell ct_dequeue;
cell ct_method;
cell ct_tuple;
cell ct_grouped_args;
cell ct_code_block;
cell ct_comment;
cell ct_unquote_splice;

cell s_self;

cell s_close_paren;
cell s_open_paren;
cell s_close_brace;
cell s_open_brace;
cell s_close_bracket;
cell s_open_bracket;
cell s_comma;
cell s_semicolon;
cell s_colon;
cell s_doublecolon;

cell s_qm; // question mark
cell s_asterisk;
cell s_plus;
cell s_dollar;
cell s_backquote;
cell s_pipe;

cell s_comment_start;
cell s_comment_end;
cell s_line_comment;
cell s_source;

cell s_BOF;
cell s_EOF;

static list *symtab;
#define SYMTAB_INIT_SZ 50

extern cell *dict_get_sym(list *d, cell k);

cell sym(const char *s) {
  size_t len = strlen(s) + 1;
  list ls;
  ls_init_data(&ls, len, s_char, (char *) s);
  cell q = from_list(&ls);
  cell *found = dict_get(symtab, q);
  if (!found) {
    list *ssym = ls_mk_init(len, len, s_char, s, true, true, nil);
    cell key = from_list(ssym);
    cell res = key;
    res.ty = t_sym;
    dict_set(symtab, key, res); 
    return res;
  }
  return *found;
}

const char *sym_to_cstr(cell s) {
  if (is_assoc(s)) { s = deassociate(s); }
  eassert(celltype(s) == t_sym && ls_is_sym(s.l) && s.l->entry_type == s_char,
      "TypeError", "Expected symbol.");
  return(char *) ls_at(s.l, 0); 
}

cell ty_to_sym(cell_type t) {
  switch (t) {
    case t_num:
     return ct_num;
    case t_sym:
     return ct_sym;
    case t_list:
     return ct_list;
    case t_mem:
     return ct_mem;
    case t_assoc:
     return ct_assoc;
    case t_fn:
     return ct_fn;
    case t_nil:
     return ct_nil;
    case t_void:
     return ct_void;
    case t_partial:
     return ct_partial;
    case t_ptr:
     return ct_ptr;
    case t_method:
     return ct_method;
  }
}

cell sym_cat(cell s1, cell s2) {
  const char *ss1 = sym_to_cstr(s1),
             *ss2 = sym_to_cstr(s2);
  size_t ll1 = ls_len(tc(s1, t_sym).l),
         ll2 = ls_len(tc(s2, t_sym).l);
  char res[ll1 + ll2 - 1];
  strncpy(res, ss1, ll1 - 1);
  strncpy(&res[ll1], ss2, ll2);
  return sym(res);
}

void symtab_init() {
  symtab = dict_mk();
  ct_num = sym("t_num");
  ct_sym = sym("t_sym");
  ct_list = sym("t_list");
  ct_str = sym("t_str");
  ct_sym = sym("t_sym");
  ct_pair = sym("t_pair");
  ct_appl = sym("t_appl");
  ct_appl_grouped = sym("t_appl_grouped");
  ct_mem = sym("t_mem");
  ct_fn = sym("t_fn");
  ct_method = sym("t_method");
  ct_ptr = sym("t_ptr");
  ct_assoc = sym("assoc");
  ct_nil = sym("t_nil");
  ct_void = sym("t_void");
  ct_dict = sym("t_dict");
  ct_code = sym("t_code");
  ct_dequeue = sym("t_dequeue");
  ct_tuple = sym("t_tuple");
  ct_grouped_args = sym("#grouped_args");
  ct_code_block = sym("#code_block");
  ct_comment = sym("#comment");
  ct_unquote_splice = sym("#unquote_splice");

  s_self = sym("self");

  s_close_paren = sym(")");
  s_open_paren = sym("(");
  s_close_brace = sym("}");
  s_open_brace = sym("{");
  s_close_bracket = sym("]");
  s_open_bracket = sym("[");
  s_comma = sym(",");
  s_colon = sym(":");
  s_doublecolon = sym("::");
  s_semicolon = sym(";");

  s_comment_start = sym("/*");
  s_comment_end = sym("*/");
  s_line_comment = sym("#");
  s_source = sym("\\source");

  s_EOF = sym("\\$");
  s_BOF = sym("\\^");

  s_qm = sym("?");
  s_asterisk = sym("*");
  s_plus = sym("+");
  s_dollar = sym("$");
  s_backquote = sym("`");
  s_pipe = sym("|");
}

void symtab_finalize() {
  symtab = NULL;
}

void symtab_gc_mark_globals(gc_state *gs) {
  if (symtab) {
    gc_push_gray(gs, from_list(symtab));
  }
}

// TESTS

#ifdef TESTING

// Test Suite init/fin need to be set before including test.h. Hacky :/
#define TEST_SUITE_INIT gc_init(); symtab = dict_mk();
#define TEST_SUITE_FIN symtab = NULL; gc_finalize(); 

#include "test.h"

static int test_sym() {
  list *syms = ls_mk();
  for (char c = '!'; c <= '}'; ++c) {
    char s[2] = { c, '\0' };
    ls_append(syms, sym(s));
  }

  for (char c = '!'; c <= '}'; ++c) {
    char s[2] = { c, '\0' };
    test_assert(1, sym_eq(ls_get(syms, c - '!'), sym(s)));
  }
  return OK;
}

static test_info tests[] = {
  TEST(sym),
};

TEST_SUITE(tests, "sym.c")

#endif
