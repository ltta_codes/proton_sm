#pragma once

#include "dict.h"
#include "list.h"
#include "mm.h"

typedef enum { evl_parser, evl_syntax, evl_macro, evl_runtime } eval_level;
extern int eval_levels[evl_runtime + 1];

typedef struct {
  // Root environments by evel_level.
  dict *envs;
} globals;

typedef struct env {
  list *vals;
  struct env *parent;
  int eval_level;
  bool sym_self_eval;
  bool is_runtime;
  bool is_root;
  globals *globals;
} env;

void eval_level_add(globals *glob, env *e, int eval_level);
env *env_for_eval_level(globals *glob, int eval_level);

cell *env_lookup(env *e, cell sym);
void env_set(env *e, cell symb, cell val);
env *env_new(env *parent, globals *glob);

bool is_runtime(env *e);

void env_gc_mark(gc_state *gs, env *e);

globals *env_init();
void env_finalize();

