#pragma once

#include "env.h"
#include "cell.h"

cell parse(const char *src);

void parser_init(globals *glob);
void parser_finalize(globals *glob);

