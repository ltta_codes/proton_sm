#pragma once

#include "env.h"

typedef enum { cl_src, cl_native } cl_type;
typedef cell (* native_fn)(struct env *e, cell arg);

struct closure {
  cl_type ty;

  union {
    cell src;
    native_fn native;
  };
};

closure *cl_mk(cl_type ty, cell src, native_fn fn);
