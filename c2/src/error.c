#include "error.h"

#include <assert.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

#include "cell.h"
#include "dict.h"
#include "eval.h"
#include "mm.h"
#include "sym.h"
#include "types.h"

static dict *sig_handlers;
static dict *recoveries;

static cell recovery_args;

typedef struct {
  jmp_buf *rec_point;
  cell name;
  cell desc;
} recovery_point;

static list *get_subtable(dict *d, cell subsym) {
  cell *res = dict_get(d, subsym);
  return to_list(*res);
}

static cell mk_appl(cell f, cell args) {
  list *app = ls_mk_init(0, 2, s_any, NULL, false, true, ct_appl);
  ls_append(app, f);
  ls_append(app, args);
  return from_list(app);
}

static cell escape_sym(cell s) {
  return mk_appl(sym("$"), s);
}

static globals *env_globals;

void try_signal_handlers(cell tab, cell signal, cell data) {
  cell *sig_tab = dict_get(sig_handlers, tab);
  if (!sig_tab || celltype(*sig_tab) != t_list) {
    return;
  }
  list *t = to_list(*sig_tab);
  for (size_t i = ls_len(t); i > 0; --i) {
    cell cl = ls_get(t, i - 1);
    assert(is_callable(cl));

    list *app_args = ls_mk_init(0, 2, s_any, NULL, false, true, ct_tuple);
    ls_append(app_args, escape_sym(signal));
    ls_append(app_args, data);

    eval_rt(env_globals, mk_appl(cl, from_list(app_args)));
  }
}

extern list *stack;
static void print_backtrace() {
  if (stack) {
    size_t frame_no = 1;
    printf("\n Backtrace:\n-------------------------------\n");
    printf(" %2d: main\n", 0);
    for (size_t i = 0; i < ls_len(stack); ++i) {
      cell val = ls_get(stack, i);
      if (is_callable(val)) {
        callable *cl = get_callable(val);
        char *s = cell_print(cl->assigned_name, NULL);
        printf(" %2zu: %s\n", frame_no++, s);
        free(s);
      }
    }
    printf("-------------------------------\n\n");
  }
}

void ssignal(cell signal, cell data) {
  print_backtrace();
  try_signal_handlers(signal, signal, data);
  try_signal_handlers(sym("AllSignals"), signal, data);
  printf(
      "\n>> FATAL ERROR: No registered signal or fallback handler for \"%s\".\n",
      sym_to_cstr(signal));
  exit(1);
}

/* void errorp(pos p, char *sig, char *msg) {
  printf("\n>> ERROR: %s %s line %zu:%zu\n", sig, msg, p.line, p.col);
  ssignal(sym(sig), from_cstr(msg));
} */

void eassert(bool cond, const char *sig, const char *msg) {
  if (!cond) {
    printf("ASSERTION FAILURE\n\n");
    errors(sig, msg);
  }
}

void errors(const char *sig, const char *msg) {
  printf("\n>> ERROR: %s\n", msg);
  ssignal(sym(sig), from_cstr(msg));
}

static char *verrorf(const char *signal, const char *fmt, va_list args) {
  char *buf;
  va_list aargs;
  va_copy(aargs, args);
  int bytes = vsnprintf(NULL, 0, fmt, args);
  va_end(args);
  buf = malloc(bytes + 2);

  bytes = vsnprintf(buf, bytes + 1, fmt, aargs);
  return buf;
}

void errorsf(const char *signal, const char *fmsg, ...) {
  va_list args;
  va_start(args, fmsg);
  char *buf = verrorf(signal, fmsg, args);
  va_end(args);

  errors(signal, buf);

  free(buf);
}

/* void errorpf(pos p, char *signal, char *fmsg, ...) {
  va_list args;
  va_start(args, fmsg);
  char *buf = verrorf(signal, fmsg, args);
  va_end(args);

  errorp(p, signal, buf);

  free(buf);
} */


#define NIL
#define check_sig(sig, ret)                     \
  if (celltype(sig) != t_sym) {                        \
    errors("TypeError", "Invalid signal type."); \
    return ret;                                 \
  }

#define check_handler(handler)                   \
  if (!is_callable(handler)) {                 \
    errors("TypeError", "Invalid handler type."); \
    return;                                      \
  }

static void push_to_subtable(list *t, cell subsym, cell val) {
  if (!dict_contains(t, subsym)) {
    dict_set(t, subsym, from_list(ls_mk()));
  }
  cell *st = dict_get(t, subsym);
  assert(st && celltype(*st) == t_list);
  ls_append(to_list(*st), val);
}

void push_signal_handler(cell signal, cell handler) {
  check_sig(signal, NIL);
  check_handler(handler);

  push_to_subtable(sig_handlers, signal, handler);
}

cell pop_signal_handler(cell signal) {
  check_sig(signal, nil);
  return ls_pop(get_subtable(sig_handlers, signal));
}

cell get_signal_handler(cell signal) {
  check_sig(signal, nil);
  list *st = get_subtable(sig_handlers, signal);
  size_t tl = st ? ls_len(st) : 0;
  if (tl == 0) {
    return nil;
  }
  return ls_get(st, tl - 1);
}

void push_recovery(jmp_buf *b, cell name, cell desc) {
  if (!((celltype(desc) == t_nil || is_str(desc)) && celltype(name) == t_sym)) {
    errors("InvalidArgumentError", "Invalid argument to push_recovery");
    return;
  }

  recovery_point *p = mm_alloc(sizeof(*p));
  *p = (recovery_point) {
    .rec_point = b,
    .name = name,
    .desc = desc,
  };
  cell cp = (cell) { .ty = t_void, .ptr = p };
  push_to_subtable(recoveries, name, cp);
}
 
void pop_recovery(cell signal) {
  check_sig(signal, NIL);
  ls_pop(get_subtable(recoveries, signal));
}

static inline recovery_point *as_recovery(cell c) {
  assert(celltype(c) == t_void);
  return (recovery_point *) c.ptr;
}

cell list_recoveries() {
  list *res = ls_mk();

  for (dict_it i = dict_iter(recoveries); !dict_itend(i); dict_itnext(&i)) {
    cell val = dict_itval(i);

    list *reco_pair = to_list(val);
    assert(ls_len(reco_pair) > 0);
    list *recos = to_list(ls_get(reco_pair, 1));
    if (ls_len(recos) == 0) { continue; }
    recovery_point *p = as_recovery(ls_get(recos, -1));
    ls_append(res, pair_mk(p->name, p->desc));
  }
  return from_list(res);
}

cell invoke_recovery(cell recovery_name, cell args) {
  cell *rp = dict_get(recoveries, recovery_name);
  list *treco = NULL;
  if (!rp || ls_len(treco = to_list(*rp)) == 0) {
    errors("InvalidArgumentError", "Cannot find named recovery");
  }
  recovery_point *p = as_recovery(ls_get(treco, - 1));
  
  list *aargs = ls_mk();
  ls_append(aargs, recovery_name);
  if (celltype(args) == t_list) {
    ls_extend(aargs, to_list(args));
  } else {
    ls_append(aargs, args);
  }
  recovery_args = from_list(aargs);
  
  longjmp(*p->rec_point, 1);
}

cell get_recovery_args() {
  return recovery_args;
}

void condsys_init(globals *glob) {
  env_globals = glob;
  sig_handlers = dict_mk();
  recoveries = dict_mk();
  recovery_args = nil;
}

void condsys_finalize() { 
  // Clear pointers and let GC free memory.
  sig_handlers = NULL;
  recoveries = NULL;
  recovery_args = nil;
}

void condsys_gc_mark_globals(struct gc_state *gc) {
  cell_gc_mark(gc, recovery_args); 
  if (sig_handlers) {
    cell_gc_mark(gc, from_list(sig_handlers));
  }

  if (recoveries) {
    for (size_t i = 0; i < ls_len(recoveries); ++i) {
      cell val = ls_get(recoveries, i);
      recovery_point *p = as_recovery(val);
      cell_gc_mark(gc, p->desc);
      cell_gc_mark(gc, p->name);
      gc_mark(gc, p, sizeof(recovery_point));
    }
  }
}

