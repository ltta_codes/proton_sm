#pragma once

#include <assert.h>
#include <limits.h>
#include <stdint.h>

#include "cell.h"
#include "sym.h"

typedef enum { s_char, s_num, s_pair, s_ptr, s_any } elem_type;
typedef enum { da_slice, da_coalloced, da_ptr, da_mut_buf } data_alloc_type;

typedef struct list {
  bool immutable: 1;
  bool shape_immutable: 1;
  bool has_meta: 1;
  bool homogenuous: 1;
  bool unboxed: 1;
  bool sorted: 1;
  elem_type entry_type: 3;       // Unboxed entry types for homogenous lists.
  data_alloc_type alloc_type: 2;

  size_t len; 
  void *data;
} list;

list *ls_mk();
list *ls_mk_init(size_t nelems, size_t nalloc, elem_type entry_type,
    const void *data, bool immutable, bool shape_immutable, cell meta);
void ls_init_data(list *l, size_t nelems, elem_type entry_type, void *data);
list *ls_of(size_t n, ...);
list *ls_of_mut(size_t n, ...);
list *ls_of_m(cell meta, size_t n, ...);
list *ls_of_m_mut(cell meta, size_t n, ...);
list *ls_of_nils(size_t n);

list *ls_setmeta(list *l, cell meta);
cell ls_getmeta(list *l);
bool ls_hasmeta(list *l);
bool meta_sym_eq(cell c, cell meta);

size_t ls_elem_sz(elem_type et);
cell_type elem_type_to_cell_type(elem_type et);

size_t ls_len(list *l);
cell ls_get(list *l, int index);
void *ls_at(list *l, int index);
list *ls_append(list *l, cell val);
void ls_set(list *l, int index, cell val);
void ls_insert(list *l, int index, cell val);
cell ls_pop(list *l);
void ls_droplast(list *l);
list *ls_copy(list *l);
list *ls_copy_ex(list *l, bool imm, bool shape_imm, cell meta);
void ls_extend(list *l, list *to_add);
void ls_clear(list *l);
void ls_compact(list *l);

char *ls_join(list *l, const char *sep);

bool ls_is_immutable(list *l);
bool ls_is_homogenuous(list *l);
bool ls_is_str(list *l);
bool ls_is_sym(list *l);

list *ls_set_immutable(list *l, bool imm);

size_t ls_alloc_len(list *l);

bool ls_is_sorted(list *l);
bool ls_is_sorted_ex(list *l, cell_cmp_fn cmp);
void ls_sorted_insert(list *l, cell val);
void ls_sorted_insert_ex(list *l, cell val, cell_cmp_fn cmp);
size_t ls_sorted_find(list *l, cell val, bool *found);
size_t ls_sorted_find_ex(list *l, cell val, bool *found, cell_cmp_fn cmp);

list *ls_slice(list *l, int start, int end);
void slice_reslice(list *l, int start_adj, int len_adj);
cell slice_popfront(list *l);

int ls_cmp(list *a, list *b);
void ls_printd(list *l);
char *ls_print(list *l, char *ex);

