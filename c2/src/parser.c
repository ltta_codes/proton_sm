#include "parser.h"

#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "assoc.h"
#include "error.h"
#include "list.h"
#include "sym.h"
#define VERBOSE
#include "util.h"

typedef enum {
  sep_none = 0,
  sep_both = 1,
  sep_repeat = 4,
} separator_info;

static const separator_info separators[256] = {
  ['{'] = sep_both,
  ['}'] = sep_both,
  ['('] = sep_both,
  [')'] = sep_both,
  ['['] = sep_both,
  [']'] = sep_both,
  [':'] = sep_both | sep_repeat,
  [';'] = sep_both | sep_repeat,
  [','] = sep_both,
  [' '] = sep_both,
  ['"'] = sep_both,
  ['\r'] = sep_both,
  ['\n'] = sep_both,
  ['\0'] = sep_both,
};

static char *str_token(const char **s, const char *start, size_t start_offs, size_t end_offs) {
  ptrdiff_t len = *s - start - start_offs - end_offs;
  char *sres = malloc(len + 1);
  strncpy(sres, start + start_offs, len);
  sres[len] = '\0';
  return sres;
}

static inline bool is_space(char c) {
  return (c == ' ' || c == '\t' || c == '\v' || c == '\f');
}

static inline cell ann_pos(cell node, size_t pos) {
  static cell s_pos;
  if (s_pos.ty != t_sym) { s_pos = sym("position"); }
  return associate(node, s_pos, from_num(pos));
}

static char *parse_str(const char **s) {
  const char *start = *s;
  do { (*s)++; } while (**s && **s != '"');
  if (**s != '"') { errors("SyntaxError", "Unterminated string"); }
  ++(*s);
  return str_token(s, start, 1, 1);
}

static cell next(const char **s, const size_t pos) {
  while (**s && is_space(**s)) { (*s)++; }
  if (**s == '\\' && (*(*s + 1) == '\n' || *(*s + 1) == '\r')) {
    *s += 2;
  }
  const char *start = *s;
  if (**s == '\0') { return ann_pos(s_EOF, pos); }

#if 1
  if (*start == '$' && *(start + 1) == '"') {
    ++(*s);
    char *syms = parse_str(s);
    cell res = sym(syms);
    free(syms);
    return ann_pos(res, pos);
  }
#endif

  while (**s && !(separators[(unsigned char) **s] & sep_both)) { (*s)++; }
  if (separators[(unsigned char) *start] & sep_both) {
    if (**s == '"') {
      char *sres = parse_str(s);
      cell res = from_cstr(sres);
      free(sres);
      return ann_pos(res, pos);
    }
    if (separators[(unsigned char) *start] & sep_repeat) { 
      while (*(*s + 1) && *(*s + 1) == *start) {
        ++(*s);
      }
    }
    char sep[*s - start + 2];
    strncpy(sep, start, *s - start + 1);
    sep[*s - start + 1] = '\0';
    ++(*s);
    return ann_pos(sym(sep), pos);
  }
  char *sres = str_token(s, start, 0, 0);

  if (('0' <= start[0] && '9' >= start[0]) || (start[0] == '-')) {
    num i = 0;
    if (str_to_num(sres, &i)) {
      free(sres);
      return ann_pos(from_num(i), pos);
    }
  }
  cell res = sym(sres);
  free(sres);
  return ann_pos(res, pos);
}

cell parse(const char *src) {
  const char *s = src;

  list *res = ls_setmeta(ls_mk(), ct_appl);
  ls_append(res, s_BOF);
  ls_append(res, s_open_brace);

  while (*s) {
    cell n = next(&s, s - src);
    ls_append(res, n);
  }

  if (*s == '\0') {
    ls_append(res, s_close_brace);
    ls_append(res, s_EOF);
  }

  return from_list(res);
}

#include "env.h"
#include "fn.h"

// TODO(opt): Right-associative list creation is rather inefficient.
//            Constantly creating rigt-most 2-tup lists right, then prepeding
//            one element by copying. Left-associative would be more efficient.
static cell _list_separator(env *e, cell args, cell closed_vars[],
    mmethod *self) {
  cell left = fst(args),
       right = snd(args);
  if (is_callable(right)
      && sym_eq(to_callable(right)->assigned_name, self->callable.assigned_name)) {
    return left;
  }
  if (is_void(left)) { return right; }
  if (is_void(right)) { return left; }
  if (meta_sym_eq(left, closed_vars[0])) {
    list *l = to_list(left);
    ls_append(l, right);
    return left;
  } else if (meta_sym_eq(right, closed_vars[0])) {
    list *l = ls_mk_init(0, ls_len(to_list(right)), s_any,
        NULL, false, false, closed_vars[0]);
    ls_append(l, left);
    ls_extend(l, to_list(right));
    return from_list(l);
  } else {
    list *res = ls_mk_init(0, 2, s_any, NULL, false, false, closed_vars[0]);
    ls_append(res, left);
    ls_append(res, right);
    return from_list(res);
  }
}

static cell _list_open(env *e, cell args, cell closed_vars[],
    mmethod *self) {
  if (is_void(args)
      || (celltype(args) == t_list && ls_len(to_list(args)) == 0)) {
    return from_list(ls_of_m(closed_vars[0], 0));
  }
  cell arg = fst(args);
  if (is_callable(arg)) {
    callable *cl = to_callable(arg);
    if (cl_has_argprocessor(&self->callable, cl->assigned_name)) {
      return from_list(ls_of_m(closed_vars[0], 0));
    }
  }
  if (celltype(arg) == t_list
      && sym_eq(ls_getmeta(to_list(arg)), closed_vars[1])) {
    return from_list(ls_setmeta(to_list(arg), closed_vars[0]));
  }
  list *res = ls_mk_init(0, 1, s_any, NULL, false, false, closed_vars[0]);
  ls_append(res, arg);
  return from_list(res);
}

#define PREC_SEP   -10
#define PREC_OPEN  -20
#define PREC_CLOSE 10


static cell _parser_val_fn(env *e, cell args, cell closed_vars[],
    mmethod *self) {
  return closed_vars[0];
}

static cell _parser_val(cell v, int prec) {
  mmethod *res = mm_mk_native(_parser_val_fn, false, mk_arg_ls(0),
      (cell []) { v }, 1);
  res->callable.prec = prec;
  return from_method(res);
}

static cell _parser_comment(env *e, cell args, cell closed_vars[], mmethod *self) {
  return from_list(ls_of_m(ct_comment, 1, fst(args)));
}

static cell named(mmethod *m, cell name, size_t prec) {
  m->callable.assigned_name = name;
  m->callable.has_leftarg = true;
  m->callable.prec = prec;
  return from_method(m);
}

static cell curried(mmethod *m) {
  m->callable.accepts_tupleargs = false;
  m->callable.accepts_curriedargs = true;
  return from_method(m);
}

static cell argproc_cl(native_fn f, size_t nargs, int prec,
  size_t n_closedvars, cell closed_vars[],
  list *args,
  size_t n_argprocs, ...) {
  va_list ap;
  va_start(ap, n_argprocs);
  mmethod *res = mm_mk_native(f, false, args,
      closed_vars, n_closedvars);
  callable *cl = &(res->callable);
  for (size_t i = 0; i < n_argprocs; ++i) {
    mmethod *m = va_arg(ap, mmethod *);
    cl_add_argprocessor(cl, m->callable.assigned_name, from_method(m));
  }
  va_end(ap);
  cl->prec = prec;
  cl->accepts_tupleargs = false;
  cl->accepts_curriedargs = true;
  return from_method(res);
}

static cell kwd_arg(cell arg) {
  assert(celltype(arg) == t_sym && "Expected symbol as kwd arg name");

  const char *s = sym_to_cstr(arg);
  size_t n = strlen(s);
  char rs[n + 2];
  rs[0] = '$';
  strncpy(&rs[1], s, n + 1);
  return sym(rs);
}

void parser_env(env *e) {
  e->sym_self_eval = true;

  env_set(e, s_BOF, _parser_val(s_BOF, INT_MAX));
  env_set(e, s_EOF, _parser_val(s_EOF, INT_MAX));

  cell s_tuple_builder = sym("__tuple_builder__");
  cell s_list_builder = sym("__list_builder__");
  cell s_code_builder = sym("__code_builder__");


  cell argname = sym("arg");
  cell ap_eatnl = named(mm_mk_native(_parser_val_fn, false, mk_arg_ls(0),
      (cell []) { _void }, 1), sym("\n"), 0);
  to_callable(ap_eatnl)->has_leftarg = false;

  cell ap_lists = named(mm_mk_native(_list_separator, true, mk_arg_ls(2),
      (cell []) { s_list_builder }, 1), s_comma, PREC_SEP);
  env_set(e, s_open_bracket, argproc_cl(_list_open, 1, PREC_OPEN,
      2, (cell []) { nil, s_list_builder },
      ls_of(2, argname, kwd_arg(s_close_bracket)),
      2, to_method(ap_lists), to_method(ap_eatnl)));

  cell ap_tups = named(mm_mk_native(_list_separator, true, mk_arg_ls(2),
      (cell []) { s_tuple_builder }, 1), s_comma, PREC_SEP);
  env_set(e, s_open_paren, argproc_cl(_list_open, 1, PREC_OPEN,
      2, (cell []) { ct_tuple, s_tuple_builder },
      ls_of(2, argname, kwd_arg(s_close_paren)),
      2, to_method(ap_tups), to_method(ap_eatnl)));

#ifdef parse_code_blocks
  cell ap_code = named(mm_mk_native(_list_separator, true, mk_arg_ls(2),
       (cell []) { s_code_builder }, 1), s_semicolon, PREC_SEP);
  cell ap_code_nl = named(mm_mk_native(_list_separator, true,
       ls_of(2, sym("l"), from_list(ls_of_m(ct_appl, 2, sym("r"), s_qm))), 
       (cell []) { s_code_builder }, 1), sym("\n"), PREC_SEP);
  env_set(e, s_open_brace, argproc_cl(_list_open, 1, PREC_OPEN,
      2, (cell []) { ct_code, s_code_builder },
      ls_of(2, argname, kwd_arg(s_close_brace)),
      2, to_method(ap_code), to_method(ap_code_nl)));
#endif

  env_set(e, s_line_comment, curried(mm_mk_native(_parser_comment,
      false,
          ls_of(2, sym("a"), sym("$\n")),
          //    from_list(ls_of_m(ct_appl, 2, sym("a"), s_qm)),
          //    from_list(ls_of_m(ct_appl, 2, sym("$\n"), s_qm))),
      NULL, 0)));

  env_set(e, sym("\\\n"), ap_eatnl);
  env_set(e, sym("\\\r"), ap_eatnl);
}

#include "eval.h"

void parser_init(globals *glob) {
  env *e = env_for_eval_level(glob, eval_levels[evl_parser]);
  parser_env(e);
}

void parser_finalize(globals *glob) {
  eval_level_add(glob, NULL, eval_levels[evl_parser]);
}

/* Tests to write:
 - parser_nl -> ignore multiple ";" => ";;", "define(a,1);a;;"
 - 1;;1 => 1
 - (()) => (())
 - ()() => ()
 */
