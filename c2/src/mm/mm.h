#pragma once

#include <stddef.h>

#include "cell.h"

#define GC_MIN_HEAP (1024 * 1024 * 10)
#define GC_COLLECTION_CHECKS 2096

#define new(ty) mm_alloc(sizeof(ty))

void *mm_alloc(size_t bytes);
void *mm_realloc(void *p, size_t bytes);
void mm_free(void *ptr);

char *mm_strdup(const char *s);

typedef struct gc_state gc_state;
extern gc_state *gc;

void gc_init();
void gc_finalize();

struct env;
bool gc_need_collection(gc_state *gs);
void gc_collect(gc_state *gs, struct env *curr_env);

typedef void (* gc_module_mark_fn)(gc_state *gs);
void gc_add_module(gc_state *gs, gc_module_mark_fn gc_mark_fn);

void gc_startcollection(gc_state *gs);
void gc_mark(gc_state *gs, const void *mem, size_t sz_bytes);
void gc_push_gray(gc_state *gs, cell c);
void gc_pin(gc_state *gs, const void *mem);
void gc_unpin(gc_state *gs, const void *mem);
void gc_sweep(gc_state *gs);

