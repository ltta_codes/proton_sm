#!/bin/bash
declare -i RESULT=0
for cmd in "$@"; do
  $cmd
  RESULT+=$?
done

RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color
printf "\n\n${YELLOW}[${RESULT}]${NC} Test Failures.\n" 
