#include "env.h"
 
#include "assoc.h"
#include "dict.h"
#include "fn.h"
#include "list.h"
#include "mm.h"
#include "sort.h"
#include "sym.h"
#include "types.h"

int eval_levels[evl_runtime + 1] = {
  [evl_parser]  = 0,
  [evl_syntax]  = 10,
  [evl_macro]  = 20,
  [evl_runtime]  = 100,
};

void env_gc_mark(gc_state *gs, env *e) {
  gc_mark(gs, e->globals, sizeof(globals));
  while (e) {
    for (size_t i = 0; i < ls_len(e->vals); ++i) {
      gc_push_gray(gs, ls_get(e->vals, i));
    }
    gc_mark(gs, e, sizeof(e));
    e = e->parent;
  }
}

cell *env_lookup(env *e, cell symb) {
  while (e) {
    cell *res = dict_get(e->vals, symb);
    if (res) { return res; }
    e = e->parent;
  }
  return NULL;
}

void env_set(env *e, cell symb, cell val) {
  if (is_assoc(symb)) { symb = deassociate(symb); }
  dict_set(e->vals, symb, val);
  if (is_callable(val)) {
    to_callable(val)->assigned_name = symb;
  }
}

static int num_cmp_pair_fst(cell ca, cell cb) {
  assert(is_pair(to_list(ca)) && is_pair(to_list(cb)));
  int a = to_num(ls_get(to_list(ca), 0)),
      b = to_num(ls_get(to_list(cb), 0));

  return (a == b) ? 0 : ((a > b) ? -1 : 1);
} 
void eval_level_add(globals *glob, env *e, int eval_level) {
  ls_sorted_insert_ex(
      glob->envs,
      pair_mk(from_num(eval_level), from_ptr(e)),
      num_cmp_pair_fst);
}

env *env_for_eval_level(globals *glob, int eval_level) {
  bool found = false;
  size_t idx = ls_sorted_find_ex(
      glob->envs, pair_mk(from_num(eval_level), nil), &found, num_cmp_pair_fst);
  if (!found) { return NULL; }
  return (env *) to_ptr(ls_get(to_list(ls_get(glob->envs, idx)), 1));
}

// Globally shared env across eval_levels with constants mainly.
env *shared_env;
globals *env_globals;

env *env_new(env *parent, globals *glob) {
  env *e = new(env);
  *e = (env) {
    .vals = dict_mk(),
    .parent = parent,
    .globals = glob,
    .eval_level = (parent) ? parent->eval_level : eval_levels[evl_runtime],
    .sym_self_eval = (parent) ? parent->sym_self_eval : false,
    .is_root = false,
  };
  return e;
}

static void gc_mark_env(gc_state *gs, env *e) {
  gc_mark(gs, e, sizeof(env));
  gc_push_gray(gs, from_list(e->vals));
}

static void gc_mark_mod_env(gc_state *gs) {
  if (!env_globals) { return; }
  gc_mark(gs, env_globals, sizeof(globals));
  for (size_t i = 0; i < ls_len(env_globals->envs); ++i) {
    list *p = to_list(ls_get(env_globals->envs, i));
    assert(is_pair(p) && celltype(ls_get(p, 0)) == t_num);
    gc_mark_env(gs, (env *) ls_get(p, 1).ptr);
  }
}

globals *env_init() {
  env_globals = new(globals);
  *env_globals = (globals) {
    .envs = ls_mk(),
  };

  shared_env = env_new(NULL, env_globals);
  env_set(shared_env, sym("evl_parser"), from_num(eval_levels[evl_parser]));
  env_set(shared_env, sym("evl_syntax"), from_num(eval_levels[evl_syntax]));
  env_set(shared_env, sym("evl_macro"), from_num(eval_levels[evl_macro]));
  env_set(shared_env, sym("evl_runtime"), from_num(eval_levels[evl_runtime]));
  env_set(shared_env, s_BOF, s_BOF); 
  env_set(shared_env, s_EOF, s_EOF); 

  gc_add_module(gc, gc_mark_mod_env);

  for (int lvl = evl_parser; lvl <= evl_runtime; ++lvl) {
    env *e = env_new(shared_env, env_globals);
    e->eval_level = eval_levels[lvl];
    e->is_root = true;
    if (lvl == evl_runtime) {
      e->is_runtime = true;
    } else {
      e->sym_self_eval = true;
    }
    eval_level_add(env_globals, e, eval_levels[lvl]);
  }

  return env_globals;
}

void env_finalize() {
  env_globals = NULL;
  shared_env = NULL;
}

