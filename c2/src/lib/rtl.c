#include "rtl.h"

#include <stdio.h>
#include <string.h>

#include "env.h"
#include "eval.h"
#include "error.h"
#include "fn.h"
#include "sort.h"
#include "types.h"
#include "util.h"

static void checklen(cell c, size_t l) {
  eassert(l == 1 || (celltype(c) == t_list && ls_len(to_list(c)) == l),
      "InvalidArgumentError", "Unexpected argument count");
}

#if 0
static cell eval_upto_macro(globals *g, cell c) {
  return eval_for_levels(g, c,
      eval_levels[evl_syntax], eval_levels[evl_macro]);
}
#endif

cell f_import(env *e, cell args, cell closed_vars[], mmethod *self) {
  cell fn = fst(args);
  const char *sfn = to_cstr(fn);
  return eval_file(e->globals, sfn);
}

cell f_print_env(env *e, cell args, cell closed_vars[], mmethod *self) {
  while (e) {
    char *s = ls_print(e->vals, NULL);
    printf("%s\n", s);
    free(s);
    e = e->parent;
  }
  fflush(0);
  return nil;
}

cell f_pair(env *e, cell args, cell closed_vars[], mmethod *self) {
  tc(args, t_list);
  checklen(args, 2);
  return pair_mk(fst(args), snd(args));
}

cell f_sort(env *e, cell args, cell closed_vars[], mmethod *self) {
  list *l = to_list(fst(args));
  sort(l, cell_cmp);
  return from_list(l);
}

#define MAX_LINE 4
static bool fgetl(char *dest, size_t n, FILE *in) {
  char c;
  size_t i = 0;
  while (i < n - 1 && (c = getc(in)) != EOF && c != '\n') { dest[i++] = c; }
  dest[i] = '\0';
  return i == n - 1;
}

cell f_readline(env *e, cell args, cell closed_vars[], mmethod *self) {
  char line[MAX_LINE];
  char *res = realloc(NULL, MAX_LINE);
  res[0] = '\0';
  size_t n_reads = 0;
  while (fgetl(line, MAX_LINE, stdin)) {
    if (n_reads++ > 0) {
      res = realloc(res, n_reads * (MAX_LINE - 1) + 1);
    }
    strncpy(res + (n_reads - 1) * (MAX_LINE - 1), line, MAX_LINE);
  }
  cell cres = from_cstr(res);
  free(res);
  return cres;
}

static void cprint(cell c) {
  if (is_str(c)) { printf("%s ", to_cstr(c)); return; }
  char *s = cell_print(c, NULL); printf("%s ", s); free(s);
}

cell f_print(env *e, cell args, cell closed_vars[], mmethod *self) {
  if (celltype(args) == t_list) {
    list *largs = to_list(args);
    for (size_t i = 0; i < ls_len(largs); ++i) {
      cprint(ls_get(largs, i));
    }
  } else {
    cprint(args);
  }
  printf("\n");
  return args;
}

#define lib_ffn(prefix, name, code) \
cell prefix##_##name(env *e, cell args, cell closed_vars[], mmethod *self) { code; }

// MATH

#include <math.h>

#define math_fn(name, cname) \
  lib_ffn(f, name, return from_num(cname(to_num(fst(args)))))

#define math_fn2(name, cname) \
  lib_ffn(f, name, return from_num(cname(to_num(fst(args)), to_num(snd(args)))))

#define math_op(name, csym) \
  lib_ffn(f, name, return from_num((to_num(fst(args)) csym to_num(snd(args)))))

#define lib_fn(name, code) lib_ffn(f, name, code)

math_op(add, +)
math_op(sub, -)
math_op(div, /)
math_op(mul, *)
math_op(mod, %)

math_fn(abs, abs)
math_fn(cos, cosf)
math_fn(sin, sinf)
math_fn(sqrt, sqrtf)
math_fn2(exp, powf)
math_fn(floor, floorf)
math_fn(ceiling, ceilf)
math_fn(log, logb)
math_fn2(max, fmax)
math_fn2(min, fmin)
lib_fn(even, return from_num(to_num(fst(args)) % 2 == 0))
lib_fn(odd, return from_num(to_num(fst(args)) % 2 == 1))
lib_fn(random, return from_num(rand() % to_num(fst(args))))

// LOGIC
math_op(or, |)
math_op(and, &)
math_fn(not, !)
lib_ffn(s, or, return
    from_num(to_num(eval(e, fst(args))) || to_num(eval(e, snd(args)))))
lib_ffn(s, and, return
    from_num(to_num(eval(e, fst(args))) && to_num(eval(e, snd(args)))))
lib_fn(truep, return from_num(to_num(fst(args)) != 0))
lib_fn(falsep, return from_num(to_num(fst(args)) == 0))

// COMPARISON
lib_fn(eq, return from_num(cell_cmp(fst(args), snd(args)) == 0))
lib_fn(lt, return from_num(cell_cmp(fst(args), snd(args)) > 0))
lib_fn(lte, return from_num(cell_cmp(fst(args), snd(args)) >= 0))
lib_fn(gt, return from_num(cell_cmp(fst(args), snd(args)) < 0))
lib_fn(gte, return from_num(cell_cmp(fst(args), snd(args)) <= 0))

// TYPING
lib_fn(isa, return from_num(isa(fst(args), snd(args))))
lib_fn(typeof, return typeof(fst(args)))

// FUNCTION/METHOD
// apply
// repeated

// FUNCTIONAL
// accumulate
// foldll == fold
// foldr == reduce
// unfold
// map
// filter

// LIST
lib_fn(append, ls_append(to_list(fst(args)), snd(args)); return fst(args))
lib_fn(get, return ls_get(to_list(fst(args)), to_num(snd(args))))
lib_fn(pop, return ls_pop(to_list(fst(args))))
lib_fn(clear, ls_clear(to_list(fst(args))); return fst(args))
lib_fn(len, return from_num(ls_len(to_list(fst(args)))))
lib_fn(slice,
    return from_list(ls_slice(to_list(fst(args)),
        to_num(snd(args)),
        to_num(nth(args, 2)))))
// TODO:
// delete
lib_fn(extend,
    ls_extend(to_list(fst(args)), to_list(snd(args))); return fst(args))
lib_fn(copy, return from_list(ls_copy(to_list(fst(args)))))
lib_fn(setmeta, ls_setmeta(to_list(fst(args)), snd(args)); return fst(args))
lib_fn(getmeta, return ls_getmeta(to_list(fst(args))))
lib_fn(is_immutable, return from_num(ls_is_immutable(to_list(fst(args)))))
lib_fn(is_homogenuous, return from_num(ls_is_homogenuous(to_list(fst(args)))))
lib_fn(is_str, return from_num(ls_is_str(to_list(fst(args)))))

// TYPES
// assoc
// getassoc
// typecheck
lib_fn(strtonum,
    num i = 0;
    if (!str_to_num(to_cstr(fst(args)), &i)) {
      errors("InvalidArgumentError", "String cannot be converted to num");
    }
    return from_num(i))

// CONTROL FLOW
// delimited continuations
// cond
// break?
// label
// goto

// SIGNAL/COND
lib_fn(error, signal(fst(args), snd(args)); return nil)
lib_fn(push_sig_handler,
    push_signal_handler(fst(args), eval(e, snd(args))); return nil)
lib_fn(pop_sig_handler, return pop_signal_handler(fst(args)))
lib_fn(get_sig_handler, return get_signal_handler(fst(args)))

// TODO: push recovery
lib_fn(push_recovery,
    assert(false); push_recovery(NULL, fst(args), snd(args)))
lib_fn(pop_recovery, pop_recovery(fst(args)); return nil)
lib_fn(list_recoveries, return list_recoveries())
lib_fn(invoke_recovery, return invoke_recovery(fst(args), snd(args)))

// IO
// open
// read
// write
// close

// BUILTIN
// fn
// quote
// syn-def
// sym
lib_ffn(s, quote, return fst(args))

static cell argproc_quote_expr_unquote(env *e, cell args, cell closed_vars[],
    mmethod *self) {
  env *ne = (env *) to_ptr(closed_vars[0]);
  cell arg = eval(ne, fst(args));
  return 
      (celltype(arg) == t_list && is_pair(to_list(arg))) ? snd(arg) : arg;
}

static cell argproc_quote_expr_splice(env *e, cell args, cell closed_vars[],
    mmethod *self) {
  env *ne = (env *) to_ptr(closed_vars[0]);
  cell farg = eval(ne, fst(args));
  list *arg = (celltype(farg) == t_list)
    ? to_list(farg)
    : ((is_set(farg)) ? ls_of_mut(1, farg) : ls_of_mut(0));
  return from_list(ls_setmeta(arg, ct_unquote_splice)); 
}

static cell unsplice(cell arg) {
  if (celltype(arg) == t_list) {
    list *larg = to_list(arg);
    bool immutable = ls_is_immutable(larg);
    ls_set_immutable(larg, false);
    int spliced = 0;
    bool has_spliced = false;
    for (size_t i = 0; i < ls_len(larg); ++i) {
      if (meta_sym_eq(ls_get(larg, i), ct_unquote_splice)) {
        spliced += ls_len(to_list(ls_get(larg, i))) - 1;
        has_spliced = true;
      }
    }
    list *res = ls_mk_init(0, ls_len(larg) + spliced, s_any, NULL,
        false, larg->shape_immutable, ls_getmeta(larg));
    for (size_t i = 0; i < ls_len(larg); ++i) {
      cell e = unsplice(ls_get(larg, i));
      if (meta_sym_eq(e, ct_unquote_splice)) {
        list *spl = to_list(e);
        for (size_t j = 0; j < ls_len(spl); ++j) {
          ls_append(res, ls_get(spl, j));
        }
      } else {
        if (celltype(e) == t_list && is_pair(to_list(e))) {
          ls_append(res, snd(e));
        } else {
          ls_append(res, e);
        }
      }
    }
    ls_set_immutable(res, immutable);
    return from_list(res);
  }
  return arg;
}

static cell m_quote_expr(env *e, cell args, cell closed_vars[], mmethod *self) {
  if (is_str(args)) { return sym(to_cstr(args)); }

  env *ne = env_new(NULL, e->globals);
  ne->sym_self_eval = true;
  ne->is_runtime = false;
  ne->is_root = true;
  ne->eval_level = e->eval_level;

  env_set(ne, sym("~"),
      from_method(
          mm_mk_native_n(argproc_quote_expr_splice, false, 1, 
              (cell []) { from_ptr(e) }, 1)));
  env_set(ne, sym("`"),
      from_method(
          mm_mk_native_n(argproc_quote_expr_unquote, false, 1,
              (cell []) { from_ptr(e) }, 1)));
  return unsplice(eval(ne, fst(args)));
}

static cell s_eval(env *e, cell args, cell closed_vars[], mmethod *self) {
  return eval_for_levels(e->globals, args, 0, e->eval_level);
}


// DEBUG
// trace
// untrace
// breakpoint
extern partial *to_partial(cell c);

static cell s_sourceof(env *e, cell args, cell closed_vars[], mmethod *self) {
  cell name = fst(args),
       val = eval(e, name);
  if (celltype(val) == t_partial) {
    val = to_partial(val)->fn;
  }
  char *sname = cell_print(name, NULL);
  printf("\nCallable \"%s\":\n", sname);
  free(sname);

  if (celltype(val) == t_fn) {
    list *ms = to_fn(val)->methods;
    for (size_t i = 0; i < ls_len(ms); ++i) {
      cell kv = ls_get(ms, i);
      cell types = ls_get(to_list(kv), 0),
           mm = ls_get(to_list(kv), 0);
      mmethod *m = to_method(mm);
      char *stypes = cell_print(types, NULL),
           *sfn_src = (m->type == clt_proton) ? cell_print(m->body, NULL) : NULL;
      printf("| %s => %s\n", stypes, (sfn_src) ? sfn_src : "<native fn>");
      free(stypes);
      if (sfn_src) { free(sfn_src); }
    }
  } else if (celltype(val) == t_method) {
    mmethod *m = to_method(val);
    char *smm =
        (m->type == clt_proton) ? cell_print(m->body, NULL) :  NULL;
    printf("| => %s\n", (smm) ? smm : "<native method>");
    if (smm) { free(smm); }
  }
  return nil;
}

static cell s_define(env *e, cell args, cell closed_vars[], mmethod *self) {
  tc(args, t_list);
  cell name = tc(fst(args), t_sym),
       exp = snd(args),
       val = eval(env_new(e, e->globals),  exp);
  env_set(e, name, val);
  return val;
}

static cell s_define_level(env *e, cell args, cell closed_vars[], mmethod *self) {
  tc(args, t_list);
  cell lvl = tc(fst(args), t_num),
       name = tc(snd(args), t_sym),
       exp = nth(args, 2);
  env *el = env_for_eval_level(e->globals, to_num(lvl));
  cell val = eval(env_new(el, e->globals),  exp);
  env_set(el, name, val);
  return val;
}

static bool name_eq(cell a, cell sym) {
  if (is_callable(a)) { return sym_eq(to_callable(a)->assigned_name, sym); }
  return sym_eq(a, sym);
}

static bool match_pair(list *largs, cell *k, cell *v) {
  if (!(largs && sym_eq(ls_getmeta(largs), ct_appl)
      && ls_len(largs) == 3 && name_eq(ls_get(largs, 1), s_colon))) {
    return false;
  }
  *k = ls_get(largs, 0);
  *v = ls_get(largs, 2);
  return true;
}

static bool match_pairs(list *largs, cell *i, cell *k, cell *v) {
  *k = *i = nil;
  switch (ls_len(largs)) {
    case 1:
      *k = tc(ls_get(largs, 0), t_sym);
      break;
    case 3:
      if (match_pair(largs, k, v)) { break; }
    case 5:
      if (match_pair(ls_slice(largs, 0, 2), i, k)
          && match_pair(ls_slice(largs, 2, -1), k, v)) { break; }
    default:
      errors("SyntaxError",
          "Invalid pair expression, expected [<index> : ] <key> : <val>");
      return false;
  }
  return true;
}

static cell s_foreach(env *e, cell args, cell closed_vars[], mmethod *self) {
  tc(args, t_list);
  env *ne = env_new(e, e->globals);
  cell res = nil;
  cell iter_name = fst(args),
       iterable = eval(e, snd(args)),
       code = nth(args, 2);
  tc(iterable, t_list);

  list *iter = to_list(iterable);
  cell ni = nil, nk = nil, nv = iter_name;
  bool name_pair = celltype(iter_name) == t_list
      && match_pairs(to_list(iter_name), &ni, &nk, &nv);
  if (!is_nil(nk)) { tc(nk, t_sym); }
  if (!is_nil(ni)) { tc(ni, t_sym); }
  tc(nv, t_sym);

  bool dict = is_dict(iter),
       has_index = !is_nil(ni);
  if (!name_pair) {
    for (size_t i = 0; i < ls_len(iter); ++i) {
      cell vv = ls_get(iter, i);
      env_set(ne, nv, vv);
      if (has_index) { env_set(ne, ni, from_num(i)); }
      res = eval(ne, code);
    }
  } else {
    for (size_t i = 0; i < ls_len(iter); ++i) {
      cell vv = ls_get(iter, i);
      if (has_index) { env_set(ne, ni, from_num(i)); }
      if (celltype(vv) == t_list && is_pair(to_list(vv)) && dict) {
        env_set(ne, nk, fst(vv));
        env_set(ne, nv, snd(vv));
      } else {
        env_set(ne, nk, from_num(i)); 
        env_set(ne, nv, vv);
      }
      res = eval(ne, code);
    }
  }
  return res;
}

static mmethod *mm_mk(cell fn_args, cell body) {
  mmethod *m = mm_mk_src(fn_args, body, NULL, 0);
  m->callable.eval_level = eval_levels[evl_runtime];
  cl_set_accepts_tupleargs(&m->callable, true);
  return m;
}

static cell gather_fnargs(cell args, int start, int end) {
  if (celltype(args) != t_list
      || !(meta_sym_eq(args, ct_tuple) || meta_sym_eq(args, ct_grouped_args))) {
    eassert(start == 0 && (end == -1 || end == 1),
        "SyntaxError", "Unexpected function argument format."); // TODO(errormsg)
    return from_list(ls_of_mut(1, args));
  }

  list *largs = ls_slice(to_list(args), start, end);
  if (ls_len(largs) == 1) {
    cell elem = ls_get(largs, 0);
    if (celltype(elem) == t_list 
        && (!ls_hasmeta(to_list(elem)) || meta_sym_eq(elem, ct_tuple))) {
      return elem;
    }
  }
  return from_list(largs);
}

static cell s_fn(env *e, cell args, cell closed_vars[], mmethod *self) {
  cell fn_args = gather_fnargs(args, 0, -2),
       body = nth(args, -1);
  return from_method(mm_mk(fn_args, body));
}

static cell s_op(env *e, cell args, cell closed_vars[], mmethod *self) {
  cell prec = tc(fst(args), t_num),
       fn_args = gather_fnargs(args, 1, -2),
       body = nth(args, -1);
  mmethod *m = mm_mk(fn_args, body);
  m->callable.prec = to_num(prec);
  m->callable.accepts_tupleargs = true;
  m->callable.accepts_curriedargs = true;
  m->callable.has_leftarg = true;
  return from_method(m);
}

static cell m_macro(env *e, cell args, cell closed_vars[], mmethod *self) {
  tc(args, t_list);
  cell fn_args = gather_fnargs(args, 1, -2),
       body = nth(args, -1),
       name = tc(fst(args), t_sym);
  int macro_lvl = eval_levels[evl_macro];
  body = eval_for_levels(e->globals, body, eval_levels[evl_syntax], macro_lvl);
  mmethod *m = mm_mk(fn_args, body);
  m->callable.eval_level = macro_lvl;
  m->callable.accepts_curriedargs = false;
  m->callable.accepts_tupleargs = true;
  env_set(env_for_eval_level(e->globals, macro_lvl), name, from_method(m)); 
  return nil;
}

static cell m_syntax_define(env *e, cell args, cell closed_vars[], mmethod *self) {
  tc(args, t_list);
  cell fn_args = gather_fnargs(args, 1, -2),
       name = tc(fst(args), t_sym),
       body = eval_for_levels(e->globals, nth(args, -1),
           eval_levels[evl_parser] + 1, eval_levels[evl_syntax] - 1);
  list *largs = to_list(fn_args); 
  
  // Create new env with quote/unquote only so as not to evaluate any other part
  // of arguments.
  env *ne = env_new(NULL, e->globals);
  cell *quote_unquote = env_lookup(e, s_dollar);
  if (quote_unquote) { env_set(ne, s_dollar, *quote_unquote); }
  ls_set(largs, -1, eval(e, ls_get(largs, -1)));

  mmethod *m = mm_mk(fn_args, body);
  int macro_lvl = eval_levels[evl_macro];
  m->callable.eval_level = macro_lvl;
  m->callable.eval_args = false;
  m->callable.accepts_curriedargs = true;
  m->callable.accepts_tupleargs = false;
  m->callable.assigned_name = name;

  env *ee = env_for_eval_level(e->globals, macro_lvl);
  cell *ex_f = env_lookup(ee, name);
  cell v = nil;
  if (ex_f) {
    switch (celltype(*ex_f)) {
      case t_partial:
        v = from_fn(syntax_add(ex_f->p->fn, from_method(m)));
        break;
      case t_fn:
      case t_method:
        v = from_fn(syntax_add(*ex_f, from_method(m)));
        break;
      default:
        v = from_fn(fn_syntax_mk(from_method(m)));
        break;
    }
  } else {
    v = from_fn(fn_syntax_mk(from_method(m))); 
  }
  env_set(ee, name, v);
  
  return nil;
}

static cell s_cond(env *e, cell args, cell closed_vars[], mmethod *self) {
  static char *syn_err = "SyntaxError",
              *syn_msg = "Invalid conditional syntax, expected: "
      "if(<cond>, <then code> {, <elif cond>, <elif code>}*, {<else code>}?)";
  eassert(ls_len(to_list(args)) >= 2, syn_err, syn_msg);

  cell cond = eval(env_new(e, e->globals), fst(args)),
       then = snd(args);
  if (to_num(cond) != 0) { return eval(env_new(e, e->globals), then); }

  if (ls_len(to_list(args)) == 3) {
    cell last = ls_get(to_list(args), 2);
    if (meta_sym_eq(last, ct_grouped_args)) {
      list *largs = ls_slice(to_list(last), 0, -1); 
      while (ls_len(largs) > 1) {
        eassert(ls_len(largs) >= 2, syn_err, syn_msg);
        cond = eval(env_new(e, e->globals), slice_popfront(largs));
        then = slice_popfront(largs);
        if (to_num(cond) != 0) { return eval(env_new(e, e->globals), then); }
      }
      assert(ls_len(largs) <= 1 && "If logic error");
      if (ls_len(largs) == 1) {
        return eval(env_new(e, e->globals), slice_popfront(largs));
      }
    } else {
      return eval(env_new(e, e->globals), last);
    }
  }

  return nil;
}

static cell rtl_fn(native_fn f, num nargs, num prec) {
  mmethod *m = mm_mk_native(f, false, mk_arg_ls(nargs), NULL, 0);
  m->callable.accepts_tupleargs = true;
  mm_setprec(m, prec);
  return (cell) { .ty = t_method, .mm = m };
}

static cell rtl_sfn(native_fn f, num nargs, num prec) {
  mmethod *m = mm_mk_native(f, false, mk_arg_ls(nargs), NULL, 0);
  mm_setprec(m, prec);
  callable *cl = &m->callable; 
  cl->accepts_tupleargs = true;
  cl->eval_args = false;
  return (cell) { .ty = t_method, .mm = m };
}

static cell rtl_op(native_fn f, num rargs, num prec) {
  mmethod *m = mm_mk_native(f, true, mk_arg_ls(rargs + 1), NULL, 0);
  mm_setprec(m, prec);
  return (cell) { .ty = t_method, .mm = m };
}

static cell no_eval_args(cell fn) {
  assert((celltype(fn) == t_fn || celltype(fn) == t_method)
      && "Expected callable fn or method");
  to_callable(fn)->eval_args = false;
  return fn;
}

static cell synonym(env *e, char *s) {
  cell *r = env_lookup(e, sym(s));
  assert(r && "Invalid synonym");
  return *r;
}

#if 0
static cell add_argprocessors(cell fn, size_t n, ...) { 
  va_list args;
  va_start(args, n);
  for (size_t i = 0; i < n; ++i) {
    cell name = va_arg(args, cell),
         argproc = va_arg(args, cell);
    cl_add_argprocessor(to_callable(fn), name, argproc);
  }
  return fn;
}
#endif

void rtl_env_rt_init(env *e) {
  // MATH
  env_set(e, sym("+"), rtl_op(f_add, 1, 30));
  env_set(e, sym("-"), rtl_op(f_sub, 1, 30));
  env_set(e, sym("*"), rtl_op(f_mul, 1, 40));
  env_set(e, sym("/"), rtl_op(f_div, 1, 40));
  env_set(e, sym("%"), rtl_op(f_mod, 1, 40));

  // MISC
  env_set(e, sym(":"), rtl_op(f_pair, 1, 40));
  env_set(e, sym("pair"), rtl_fn(f_pair, 1, 40));
  env_set(e, sym("print_env"), rtl_fn(f_print_env, 0, 0));

  // BUILTIN
  env_set(e, sym("eval"), rtl_sfn(s_eval, 1, DEFAULT_PREC));
  env_set(e, sym("define"), rtl_sfn(s_define, 2, DEFAULT_PREC));
  env_set(e, sym("define-@level"), rtl_sfn(s_define_level, 3, DEFAULT_PREC));
  env_set(e, sym("fn"), rtl_sfn(s_fn, 2, DEFAULT_PREC));
  env_set(e, sym("op"), rtl_sfn(s_op, 3, DEFAULT_PREC));
  env_set(e, sym("quote"), rtl_sfn(s_quote, 1, DEFAULT_PREC));

  // CONTROL FLOW
  env_set(e, sym("cond"), rtl_sfn(s_cond, -3, DEFAULT_PREC));
  env_set(e, sym("for-each"), rtl_sfn(s_foreach, 3, DEFAULT_PREC));

  env_set(e, sym("import"), rtl_fn(f_import, 1, DEFAULT_PREC));
  env_set(e, sym("source"), rtl_sfn(s_sourceof, 1, DEFAULT_PREC));

  // MATH
  env_set(e, sym("abs"), rtl_fn(f_abs, 1, DEFAULT_PREC));
  env_set(e, sym("cos"), rtl_fn(f_cos, 1, DEFAULT_PREC));
  env_set(e, sym("sin"), rtl_fn(f_sin, 1, DEFAULT_PREC));
  env_set(e, sym("sqrt"), rtl_fn(f_sqrt, 1, DEFAULT_PREC));
  env_set(e, sym("exp"), rtl_fn(f_exp, 1, DEFAULT_PREC));
  env_set(e, sym("floor"), rtl_fn(f_floor, 1, DEFAULT_PREC));
  env_set(e, sym("ceiling"), rtl_fn(f_ceiling, 1, DEFAULT_PREC));
  env_set(e, sym("log"), rtl_fn(f_log, 1, DEFAULT_PREC));
  env_set(e, sym("max"), rtl_fn(f_max, 1, DEFAULT_PREC));
  env_set(e, sym("min"), rtl_fn(f_min, 1, DEFAULT_PREC));
  env_set(e, sym("even"), rtl_fn(f_even, 1, DEFAULT_PREC));
  env_set(e, sym("odd"), rtl_fn(f_odd, 1, DEFAULT_PREC));
  env_set(e, sym("random"), rtl_fn(f_random, 1, DEFAULT_PREC));

  // LOGIC
  env_set(e, sym("&"), rtl_op(f_or, 1, 15));
  env_set(e, sym("|"), rtl_op(f_and, 1, 15));
  env_set(e, sym("not"), rtl_op(f_not, 1, 15));
  env_set(e, sym("&&"), no_eval_args(rtl_op(s_and, 1, 10)));
  env_set(e, sym("||"), no_eval_args(rtl_op(s_or, 1, 10)));
  env_set(e, sym("and"), synonym(e, "&&"));
  env_set(e, sym("or"), synonym(e, "||"));
  env_set(e, sym("true?"), rtl_fn(f_truep, 1, 0));
  env_set(e, sym("false?"), rtl_fn(f_falsep, 1, 0));

  // COMPARISON
  env_set(e, sym("=="), rtl_op(f_eq, 1, 20));
  env_set(e, sym("<"), rtl_op(f_lt, 1, 20));
  env_set(e, sym("<="), rtl_op(f_lte, 1, 20));
  env_set(e, sym(">"), rtl_op(f_gt, 1, 20));
  env_set(e, sym(">="), rtl_op(f_gte, 1, 20));

  // TYPING
  env_set(e, sym("isa"), rtl_fn(f_isa, 2, DEFAULT_PREC));
  env_set(e, sym("typeof"), rtl_fn(f_isa, 1, DEFAULT_PREC));
  
  // LIST
  env_set(e, sym("ls_append"), rtl_fn(f_append, 2, DEFAULT_PREC));
  env_set(e, sym("ls_get"), rtl_fn(f_get, 2, DEFAULT_PREC));
  env_set(e, sym("ls_pop"), rtl_fn(f_pop, 1, DEFAULT_PREC));
  env_set(e, sym("ls_clear"), rtl_fn(f_clear, 1, DEFAULT_PREC));
  env_set(e, sym("ls_len"), rtl_fn(f_len, 1, DEFAULT_PREC));
  env_set(e, sym("ls_slice"), rtl_fn(f_slice, 3, DEFAULT_PREC));
  env_set(e, sym("ls_extend"), rtl_fn(f_extend, 2, DEFAULT_PREC));
  env_set(e, sym("ls_copy"), rtl_fn(f_copy, 1, DEFAULT_PREC));
  env_set(e, sym("ls_setmeta"), rtl_fn(f_setmeta, 2, DEFAULT_PREC));
  env_set(e, sym("ls_getmeta"), rtl_fn(f_getmeta, 1, DEFAULT_PREC));
  env_set(e, sym("ls_is_homogenuous"),
      rtl_fn(f_is_homogenuous, 1, DEFAULT_PREC));
  env_set(e, sym("ls_is_immutable"), rtl_fn(f_is_immutable, 1, DEFAULT_PREC));
  env_set(e, sym("is_str"), rtl_fn(f_is_str, 1, DEFAULT_PREC));
  env_set(e, sym("sort"), rtl_fn(f_sort, 1, DEFAULT_PREC));

  // TYPES
  env_set(e, sym("str_to_num"), rtl_fn(f_strtonum, 1, DEFAULT_PREC));

  // SIGNAL/COND
  env_set(e, sym("error"), rtl_fn(f_error, 2, DEFAULT_PREC));
  env_set(e, sym("push_signal_handler"),
      rtl_fn(f_push_sig_handler, 2, DEFAULT_PREC));
  env_set(e, sym("pop_signal_handler"),
      rtl_fn(f_pop_sig_handler, 1, DEFAULT_PREC));
  env_set(e, sym("get_signal_handler"),
      rtl_fn(f_get_sig_handler, 1, DEFAULT_PREC));
  env_set(e, sym("push_recovery"),
      rtl_fn(f_push_recovery, 2, DEFAULT_PREC));
  env_set(e, sym("pop_recovery"),
      rtl_fn(f_pop_recovery, 2, DEFAULT_PREC));
  env_set(e, sym("list_recoveries"),
      rtl_fn(f_list_recoveries, 0, DEFAULT_PREC));
  env_set(e, sym("invoke_recovery"),
      rtl_fn(f_invoke_recovery, 2, DEFAULT_PREC));

  // IO
  env_set(e, sym("print"), rtl_fn(f_print, -1, DEFAULT_PREC));
  env_set(e, sym("readline"), rtl_fn(f_readline, 0, DEFAULT_PREC));

  // VALUES
  env_set(e, sym("nil"), nil); 
}

void rtl_env_macro_init(env *e) {
  // BUILTIN
  env_set(e, sym("define-@level"), rtl_sfn(s_define_level, 3, DEFAULT_PREC));
  env_set(e, sym("$"),
      rtl_fn(m_quote_expr, 1, DEFAULT_PREC));
  env_set(e, sym("eval-macro-level"), rtl_sfn(s_eval, 1, DEFAULT_PREC));
#if 0
  env_set(e, sym("$"),
      add_argprocessors(
          rtl_fn(m_quote_expr, 1, DEFAULT_PREC),
          2,

          sym("~"),
          rtl_fn(argproc_quote_expr_splice, 1, DEFAULT_PREC),

          sym("`"),
          rtl_fn(argproc_quote_expr_unquote, 1, DEFAULT_PREC)));
#endif

  env_set(e, sym("macro-define"), rtl_sfn(m_macro, 3, DEFAULT_PREC)); 

  env_set(e, s_EOF, _void);
  env_set(e, s_BOF, _void);
}

void rtl_env_syntax_init(env *e) {
  env_set(e, sym("define-@level"), rtl_sfn(s_define_level, 3, DEFAULT_PREC));
  env_set(e, sym("eval-syntax-level"), rtl_sfn(s_eval, 1, DEFAULT_PREC));
  env_set(e, sym(":"), rtl_op(f_pair, 1, 40));
  env_set(e, sym("$"),
      rtl_fn(m_quote_expr, 1, DEFAULT_PREC));
  env_set(e, sym("syntax-define"), rtl_sfn(m_syntax_define, 3, DEFAULT_PREC));
}

void rtl_init(globals *glob) {
  env *syntax_env = env_for_eval_level(glob, eval_levels[evl_syntax]);
  rtl_env_syntax_init(syntax_env);
  syntax_env->sym_self_eval = true;

  rtl_env_macro_init(env_for_eval_level(glob, eval_levels[evl_macro]));

  rtl_env_rt_init(env_for_eval_level(glob, eval_levels[evl_runtime]));
}

void rtl_finalize() {
}

