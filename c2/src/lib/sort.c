#include "sort.h"

#include <string.h>

static inline void swap(list *l, size_t i, size_t j, size_t elemsz) {
  char t[elemsz];
  memcpy(t, ls_at(l, i), elemsz);
  memcpy(ls_at(l, i), ls_at(l, j), elemsz);
  memcpy(ls_at(l, j), t, elemsz);
}

static size_t partition(list *l, size_t low, size_t high,
    int (* cmp)(cell a, cell b), size_t elemsz) {
  size_t i = low, j = high + 1;
  cell v = ls_get(l, low);
  while (true) { 
    while (cmp(ls_get(l, ++i), v) > 0 && i < high) {}
    while (cmp(v, ls_get(l, --j)) > 0 && j > low) {}
    if (i >= j) { break; }
    swap(l, i, j, elemsz);
  }

  swap(l, low, j, elemsz);
  return j;
}

static void quicksort(list *l, size_t low, size_t high,
    int (* cmp)(cell a, cell b), size_t elemsz) {
  if (low >= high) { return; }
  size_t pivot = partition(l, low, high, cmp, elemsz);
  if (pivot > 0) { quicksort(l, low, pivot - 1, cmp, elemsz); }
  if (pivot < high) { quicksort(l, pivot + 1, high, cmp, elemsz); }
}

void sort(list *l, int (* cmp)(cell a, cell b)) {
  quicksort(l, 0, ls_len(l) - 1, cmp, ls_elem_sz(l->entry_type));
}

// TESTS

#ifdef TESTING

// Test Suite init/fin need to be set before including test.h. Hacky :/
#define TEST_SUITE_INIT gc_init();
#define TEST_SUITE_FIN gc_finalize(); 

#include "test.h"

#include <time.h>

static bool is_sorted(list *l) {
  for (size_t i = 0; i < ls_len(l) - 1; ++i) {
    if (cell_cmp(ls_get(l, i), ls_get(l, i + 1)) < 0) { return false; }
  }
  return true;
}

static int test_sort_str() {
  list *l = ls_mk();
  for (char c = '}'; c >= '!'; ++c) {
    char s[2] = { c, '\0' };
    ls_append(l, from_cstr(s));
  }

  sort(l, cell_cmp);

  test_assert(is_sorted(l), true);

  return OK;
}

static int test_sort_num() {
  srand(time(NULL));
  for (size_t n = 100; n < 100000; n *= 2) {
    list *l = ls_mk();
    for (size_t i = 0; i < n; ++i) {
      ls_append(l, from_num(rand()));
    }
    sort(l, cell_cmp);

    test_assert(is_sorted(l), true);
    for (size_t i = 0; i < ls_len(l) - 1; ++i) {
      test_assert(to_num(ls_get(l, i)) <= to_num(ls_get(l, i + 1)), 1);
    }
  }

  return OK;
}

static test_info tests[] = {
  TEST(sort_str),
  TEST(sort_num),
};

TEST_SUITE(tests, "sort.c")

#endif
