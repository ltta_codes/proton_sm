#pragma once

#include "cell.h"
#include "list.h"

void sort(list *l, int (* cmp)(cell a, cell b));
