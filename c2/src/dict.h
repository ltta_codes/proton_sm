#pragma once

#include "list.h"

typedef list dict;

typedef int (* dict_cmp_fn)(cell a, cell b);

dict *dict_mk();

dict *dict_set(dict *d, cell k, cell v);
cell *dict_get(dict *d, cell k);

bool dict_contains(dict *d, cell k);

bool is_dict(list *l);

typedef struct {
  size_t i;
  list *d;
} dict_it;

dict_it dict_iter(dict *d);
bool dict_itend(dict_it i);
void dict_itnext(dict_it *i);
cell dict_itval(dict_it i);
char *dict_print(dict *d, char *ex);
