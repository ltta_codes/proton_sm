#pragma once

#include <limits.h>

#include "cell.h"
#include "dict.h"
#include "env.h"
#include "util.h"

#define DEFAULT_PREC 0

typedef struct fn fn;
typedef struct mmethod mmethod;

typedef cell (* native_fn)(env *e, cell args, cell closed_vars[],
    mmethod *self);

typedef enum { clt_proton, clt_ffi } closure_type;

#define ARG_MAX USHRT_MAX

typedef struct {
  cell kwd;
  cell arg_proc;
  bool kwd_only: 1;
  bool optional: 1;
  bool repeating: 1;
} arg_opt;

typedef struct callable {
  num prec;
  bool has_leftarg;
  bool accepts_tupleargs;
  bool accepts_curriedargs;
  bool eval_args;

  int eval_level;

  num n_args;
  arg_opt *arg_opts;

  dict *arg_processors;

  cell assigned_name;
} callable;

struct mmethod {
  callable callable;

  closure_type type;
  union {
    cell body;
    native_fn fn;
  };

  list *arg_names;
  list *arg_types;

  list *closed_var_names;
  size_t n_closed_vars;
  cell closed_vars[];
};

struct fn {
  callable callable;
  list *methods;
  bool syntactic_dispatch;
};

struct partial {
  cell fn;
  list *applied_args;
};

fn *fn_mk(size_t n_methods);
fn *fn_mk_native(native_fn f, bool left_arg, list *args,
    cell closed_vars[], size_t n_closed_vars);
fn *fn_mk_src(cell src, num nargs, bool left_arg, list *arg_names, list *arg_types);
void fn_method_add(fn *f, mmethod *m);
fn *fn_setprec(fn *f, num prec);
fn *fn_setname(fn *f, cell name);
callable *to_callable(cell c);

fn *syntax_add(cell ex, cell cl);
fn *fn_syntax_mk(cell cl);

mmethod *mm_mk_native_n(native_fn f, bool left_arg, int nargs,
    cell closed_vars[], size_t n_closed_vars);
mmethod *mm_mk_native(native_fn f, bool left_arg, list *args,
    cell closed_vars[], size_t n_closed_vars);
mmethod *mm_mk_src(cell args, cell body, cell closed_vars[],
    size_t n_closed_vars);
mmethod *mm_setprec(mmethod *m, num prec);
char *mm_print(mmethod *m, char *ex);

cell fn_call(fn *f, env *e, list *args);

mmethod *fn_dispatch(fn *f, list *args);
cell method_execute(mmethod *m, env *e, list *args);
char *fn_print(fn *f, char *ex);

bool is_callable(cell c);
callable *get_callable(cell c);
void cl_set_accepts_tupleargs(callable *c, bool accepts);
void cl_set_accepts_curriedargs(callable *c, bool accepts);
void cl_add_argprocessor(callable *c, cell name, cell processor);
env *cl_argprocessors_env(env *parent, env *sym_resolve_env, callable *c);
env *argopt_argproc_for_arg(env *parent, env *sym_resolve_env,
    arg_opt *ao, size_t n_kwd);
bool cl_has_argprocessors(callable *c);
bool cl_has_argprocessor(callable *c, cell name);
bool cl_has_syntax(callable *c);

cell partial_mk(cell f);
int partial_expected_args(partial *p);
partial *partial_copy(partial *p);

list *mk_arg_ls(num n_args);

#define arg(args, idx) \
  (ls_len(to_list((args))) > (idx) ? ls_get(to_list((args)), (idx)) : nil)
