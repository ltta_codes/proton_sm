#include "hash.h"

#include "assoc.h"
#include "list.h"
#include "sym.h"


#define BITS 64
//#define BITS 32

#if (BITS == 32)
# define XXH_NO_LONG_LONG
#endif

#define XXH_PRIVATE_API
#include "xxhash.h"


#define HASH_COMBINE_MAGIC 0x9E3779B97F4A7C15
//#define HASH_COMBINE_MAGIC 0x9ddfea08eb382d69ULL
//#define HASH_COMBINE_MAGIC 0x9e3779b9

#define CONCAT(A, B) CONCAT_(A, B)
#define CONCAT_(A, B) A##B

#define XXH(x, l) CONCAT(XXH, BITS)((x), (l), HASH_COMBINE_MAGIC)
#define XXH_(name) CONCAT(CONCAT(XXH, BITS), _#name)

// boost::hash_combine
static inline hash_t hash_combinei(hash_t h1, hash_t h2) {
  h1 ^= h2 + HASH_COMBINE_MAGIC + (h1 << 6) + (h1 >> 2);
  return h1;
}

static inline hash_t hashcti(cell_type ct) {
  return XXH(&ct, sizeof(cell_type));
}

static inline hash_t hashti(cell_type ct, void *p, hash_t l) {
  return hash_combinei(hashcti(ct), XXH(p, l));
}

static inline hash_t hash_listi(list *l) {
  hash_t h = hash(ls_getmeta(l));
  for (size_t i = 0; i < ls_len(l); ++i) {
    h = hash_combinei(h, hash(ls_get(l, i)));
  }
  return hash_combinei(hashcti(t_list), h);
}

hash_t hash(cell c) {
  cell_type ct = celltype(c);
  switch (ct) {
    case t_num:
      return hashti(ct, &c.n, sizeof(num));
    case t_void:
    case t_ptr:
    case t_sym:
      return hashti(ct, &c.ptr, sizeof(void *));
    case t_list:
      return hash_listi(c.l);
    case t_mem:
      assert(false && "TODO");
      return 0;
    case t_assoc:
      return hash(deassociate(c));
    case t_fn:
      assert(false && "TODO");
      return 0;
    case t_method:
      assert(false && "TODO");
      return 0;
    case t_partial:
      assert(false && "TODO");
      return 0;
    case t_nil: {
      unsigned char i = 0;
      return hashti(ct, &i, sizeof(unsigned char));
    }
  }
}

hash_t hash_list(list *l) {
  return hash_listi(l);
}

hash_t hash_combine(hash_t h1, hash_t h2) {
  return hash_combinei(h1, h2);
}


// TESTS

#ifdef TESTING

// Test Suite init/fin need to be set before including test.h. Hacky :/
#define TEST_SUITE_INIT gc_init();
#define TEST_SUITE_FIN gc_finalize(); 

#include "test.h"

#ifdef DEBUG
# define N 1000
#else
# define N 10000
#endif

static int test_num_hash() {
  for (size_t i = 0; i < N; ++i) {
    for (size_t j = 0; j < N; ++j) {
      if (i != j) { test_assert_neq(hash(from_num(i)), hash(from_num(j))); }
      else { test_assert(hash(from_num(i)), hash(from_num(j))); }
    }
  }
  return OK;
}

static int test_str_hash() {
  char *test_strs[] = { "ab", "abc", "hello", "test_str", "$@#$!@#!@" };
  size_t n_strs = sizeof(test_strs) / sizeof(test_strs[0]);
  for (size_t i = 0; i < n_strs; ++i) {
    for (size_t j = 0; j < n_strs; ++j) {
      if (i != j) {
        test_assert_neq(
            hash(from_cstr(test_strs[i])),
            hash(from_cstr(test_strs[j])));
      }
      else {
        test_assert(
            hash(from_cstr(test_strs[i])),
            hash(from_cstr(test_strs[j])));
      }
    }
  }
  test_assert(
      hash(from_cstr(test_strs[0])),
      hash(from_cstr(test_strs[0])));
  return OK;
}

static int test_list_hash() {
  list *lists[4];
  lists[0] = ls_of(3, nil, from_num(1), from_cstr("abc"));
  lists[1] = ls_of(1, from_num(1));
  lists[2] = ls_of(4, nil, nil, from_num(1), from_cstr("abc"));
  lists[3] = ls_of(4, nil, nil, nil, nil); 
  size_t n_lists = sizeof(lists) / sizeof(lists[0]);
  for (size_t i = 0; i < n_lists; ++i) {
    for (size_t j = 0; j < n_lists; ++j) {
      if (i != j) {
        test_assert_neq(
            hash_list(lists[i]),
            hash_list(lists[j]));
      }
      else {
        test_assert(
            hash_list(lists[i]),
            hash_list(lists[j]));
      }
    }
  }
  test_assert(
      hash_list(lists[0]),
      hash_list(lists[0]));
  return OK;
}

static test_info tests[] = {
  TEST(num_hash),
  TEST(str_hash),
  TEST(list_hash),
};

TEST_SUITE(tests, "hash.c")

#endif
