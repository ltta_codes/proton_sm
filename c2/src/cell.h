#pragma once

#include <stdbool.h>
#include <stdlib.h>

typedef struct cell cell;
typedef struct list list;
typedef struct fn fn;
typedef struct mmethod mmethod;
typedef struct partial partial;
typedef struct assoc assoc;
typedef int num;

typedef enum {
  t_nil = 0, // Nil should be zero, default value.
  t_num, t_sym, t_list, t_mem, t_assoc, t_fn, t_method, t_partial, 
  t_void,
  t_ptr /* only used internally */,
  t_any,
} cell_type;

struct cell {
  cell_type ty;
  union {
    list *l;
    num n;
    void *m;
    assoc *a;
    fn *f;
    mmethod *mm;
    partial *p;
    void *ptr;
  };
};

cell_type celltype(cell c);
cell typeof(cell c);
bool isa(cell c, cell type);
bool is_assoc(cell c);

cell from_cstr(const char *s);
cell from_num(num n);
cell from_list(list *l);
cell from_ptr(void *p);
cell from_fn(fn *f);
cell from_method(mmethod *m);
cell from_assoc(assoc *a);

num to_num(cell c);
const char *to_cstr(cell c);
list *to_list(cell c);
void *to_ptr(cell c);
mmethod *to_method(cell c);
fn *to_fn(cell c);
partial *to_partial(cell c);

char *cell_print(cell c, char *ex);
void cell_printd(cell c);

// Sign(return) indicates "side" of larger value.
// Returns > 0 if a less than b, 0 if a == b, < 0 if b less than a.
typedef int (* cell_cmp_fn)(cell a, cell b);
int cell_cmp(cell a, cell b);

bool is_nil(cell c);
bool is_void(cell c);
bool is_set(cell c);
bool is_str(cell c);
bool is_tuple(cell c);

extern cell nil;
extern cell _void;

struct gc_state;
void cell_gc_mark(struct gc_state *gs, cell c);
void cell_gc_mark_globals(struct gc_state *gs);

#ifndef __MEM_SAVE__
const char *celltype_to_str(cell_type t);
#endif
