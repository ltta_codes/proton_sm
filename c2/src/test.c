#include "test.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

unsigned int n_asserts = 0;

// Signals.

static char *last_signal = NULL;

void condsys_init(void *_unused) {
}

void condsys_finalize() {
  clear_signal();
}

void errors(const char *signal, const char *msg) {
  clear_signal();
  last_signal = strdup(signal);
}

void errorsf(const char *signal, const char *msg, const char *fmt, ...) {
  errors(signal, msg);
}

void eassert(bool cond, const char *sig, const char *msg) {
  if (!cond) { errors(sig, msg); }
}

int test_assert_signal(const char *name) {
  test_assert_neq((void *) last_signal, (void *) NULL);
  test_assert(strcmp(name, last_signal), 0);
  clear_signal();
  return S_OK;
}

int test_assert_nosignal() {
  test_assert((void *) last_signal, (void *) NULL);
  return S_OK;
}

void clear_signal() {
  if (last_signal) { free(last_signal); last_signal = NULL; }
}


// Possible to set breakpoints on test failure return.
int test_fail(int arg) {
  return arg;
}

// Set breakpoint on point of failure.
void failing() { }

