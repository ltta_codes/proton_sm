#pragma once

#include "env.h"
#include "fn.h"

#define PREC_TUPLE 200

cell eval(env *e, cell ast);
cell eval_rt(globals *g, cell ast);
cell eval_macro(globals *g, cell ast);
cell eval_for_levels(globals *glob, cell ast, int start, int end);

cell closure_native_mk(native_fn f, list *arg_names, list *arg_types,
    num prec, const char *assigned_name, bool left_arg);

cell eval_file(globals *glob, const char *filename);
cell eval_src(globals *glob, char *src, char *display_filename);

