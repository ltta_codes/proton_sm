#pragma once

#include <stdio.h>

#include "cell.h"

typedef struct test_suite test_suite;
typedef int (*test_fn)();

typedef struct {
  test_fn test;
  const char *name;
} test_info;

// Possible to set breakpoints on test failure return.
int test_fail(int arg);
// Set breakpoint on point of failure.
void failing();

// Signals.
int test_assert_signal(const char *name);
int test_assert_nosignal();
void clear_signal();

#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_YELLOW  "\x1b[33m"
#define COLOR_BLUE    "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN    "\x1b[36m"
#define COLOR_RESET   "\x1b[0m"

#define OK (test_assert_nosignal())
#define S_OK (-1)
#define FAILED (test_fail(__LINE__))

#define sub_test(t) \
  do { int r = (t); if (r != S_OK) { return r; } } while (0);

extern unsigned int n_asserts;

#if __STDC__==1 && __STDC_VERSION__ >= 201112L
# define VAL_FMT(v) _Generic((v), num: "%d", size_t: "%zu",      \
      char *: "%s", const char *: "%s", bool: "%d", void*: "%p", \
      default: "%x")
# define VAL_VAL(v) _Generic((v), num: (v), size_t: (v),             \
      char *: (v), const char *: (v), bool: (v), void *: (v),\
      default: (unsigned int) (v)) 
      
#else
__STDC_VERSION__
# define VAL_FMT(v) "%s"
# define VAL_VAL(v) ""
#endif

#define TEST(name) (test_info) { &test_##name, #name }

#define test_assert_cmp(val, expected, cmp)                                     \
    ++n_asserts;                                                                \
    if (!((val) cmp (expected))) {                                              \
      printf("          !! FAILED assert: line %d :: %s, "                      \
          "expected \"", __LINE__, #val);                                       \
      printf(VAL_FMT(expected), VAL_VAL(expected));                             \
      printf("\" to \"" #cmp "\" \"");                                          \
      printf(VAL_FMT(val), VAL_VAL(val));                                       \
      printf("\".\n" );                                                         \
      failing();                                                                \
      return FAILED;                                                            \
    }

#define test_assert(val, expected) test_assert_cmp(val, expected, ==)
#define test_assert_neq(val, expected) test_assert_cmp(val, expected, !=)
#define test_assert_lt(val, expected) test_assert_cmp(val, expected, <)
#define test_assert_lte(val, expected) test_assert_cmp(val, expected, <=)
#define test_assert_gt(val, expected) test_assert_cmp(val, expected, >)
#define test_assert_gte(val, expected) test_assert_cmp(val, expected, >=)

#ifndef TEST_SUITE_INIT
# define TEST_SUITE_INIT
#endif

#ifndef TEST_SUITE_FIN
# define TEST_SUITE_FIN
#endif

#define TEST_SUITE(test_infos, suite_name)                                    \
int main(int argc, const char *argv[]) {                                      \
  do { TEST_SUITE_INIT } while(0);                                            \
  printf("TESTING \"%s\"...\n", suite_name);                                  \
  size_t n = 0, succ = 0, fail = 0;                                           \
  for (size_t i = 0; i < sizeof((test_infos)) / sizeof(test_info); ++i) {     \
    test_fn testfn = (test_infos)[i].test;                                    \
    n++;                                                                      \
    const char *test_name = (test_infos)[i].name;                             \
    if (testfn() != S_OK) {                                                     \
      fail++;                                                                 \
      printf("  " COLOR_RED "[FAILED]" COLOR_RESET ": %s\n", test_name);      \
    } else {                                                                  \
      succ++;                                                                 \
      printf("  " COLOR_GREEN "[OK]" COLOR_RESET ":     %s\n", test_name);    \
    }                                                                         \
  }                                                                           \
  if (!fail) {                                                                \
    printf(">> %zu Tests || All tests passed! [%zu assertions]\n",            \
        n, n_asserts);                                                        \
  } else {                                                                    \
    printf(">> %zu Tests || %zu FAILED tests, %zu passed.\n", n, fail, succ); \
  }                                                                           \
  do { TEST_SUITE_FIN } while(0);                                             \
  return fail;                                                                   \
}

