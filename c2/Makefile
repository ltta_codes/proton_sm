#-----Macros---------------------------------
DEBUG ?= 1
ASAN ?= $(DEBUG)
VERBOSE ?= 0

SANITIZE ?=

TLIB = 

ifeq ($(ASAN), 1)
SANITIZE=-fsanitize=address -fno-omit-frame-pointer -O0 -g3
#SANITIZE=-fsanitize=leak -fno-omit-frame-pointer -O0
#SANITIZE=-fsanitize=memory -fno-omit-frame-pointer -O0
endif

WARNINGS=-pedantic -Wall -Wno-gnu -Wno-variadic-macros
#WARNINGS=

# set up compiler and options
ifeq ($(DEBUG), 1)
CFLAGS += -O0 -g3 $(INCLUDES) -Wall -Winline -std=c11 $(SANITIZE) $(WARNINGS) -DDEBUG
#CFLAGS = -O0 -g $(INCLUDES)
else
CFLAGS += -O3 $(INCLUDES) -std=c11
endif

ifeq ($(VERBOSE), 1)
CFLAGS += -DVERBOSE
endif

# GCC kludge
CFLAGS += -D_GNU_SOURCE
CXXFLAGS = -O0 $(INCLUDES) -Wall -Winline -stdlib=libstdc++\
    $(SANITIZE) $(WANRINGS)

LDFLAGS = -lm -g3 $(SANITIZE) 
#LDFLAGS = -lm -g-full

# XCode toolchain.
#CC              := $(shell xcodebuild -find clang)
#CXX             := $(shell xcodebuild -find clang++)

# Toolchain in search path.
CC              := clang
CXX             := clang++
#CC              := gcc
#CXX             := g++

# Brew llvm/toolchain.
#CC              := /usr/local/opt/llvm/bin/clang 
#CFLAGS          += -I/usr/local/opt/llvm/include
#LDFLAGS         += -L/usr/local/opt/llvm/lib
#CXX             := /usr/local/opt/llvm/bin/clang++ 
#CXXFLAGS        += -I/usr/local/opt/llvm/include


LD              := $(CC) $(LDFLAGS)
LDXX            := $(CXX) $(LDFLAGS) $(SANITIZE) --stdlib=libstdc++

MODULES     := . mm lib
BUILD_ROOT  := build
SRC_ROOT    := src
APP_DIR     := .
SRC_DIR     := $(addprefix $(SRC_ROOT)/,$(MODULES))
APP_MODULES :=
RTL_FILES   := rtl.0.proton rtl.proton

TESTS            := ./list ./types ./eval ./sym lib/sort ./assoc ./dict ./hash ./fn ./util
TEST_TARGETS     := $(addsuffix _test,$(TESTS))
TEST_TARGETS_RUN := $(addsuffix _test_run,$(TESTS))

BUILD_DIR := $(addprefix $(BUILD_ROOT)/,$(MODULES) $(TEST_MODULES) $(APP_MODULES))

BASE_INCLUDES  = -I./$(SRC_ROOT)
EXTRA_INCLUDES = -I./contrib/linenoise -I./contrib/xxHash
EXTRA_SRCS     = ./contrib/linenoise/linenoise.c

ALL_SRC  := $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.c)) $(EXTRA_SRCS)
SRC	     := $(filter-out $(SRC_ROOT)/./test.c,$(ALL_SRC))
GEN_SRC  := $(subst $(SRC_ROOT)/$(APP_DIR)/repl_main.c,,$(SRC))
OBJ	     := $(patsubst $(SRC_ROOT)/%.c,$(BUILD_ROOT)/%.o,$(GEN_SRC))
TEST_OBJ := $(filter-out $(BUILD_ROOT)/./error.o $(BUILD_ROOT)/lib/rtl.o,$(OBJ))
INCLUDES := $(addprefix -I,$(SRC_DIR)) $(BASE_INCLUDES) $(EXTRA_INCLUDES)
RTL_SRC  := $(addprefix $(SRC_ROOT)/lib/,$(RTL_FILES))
RTL_OBJ  := $(addprefix $(BUILD_ROOT)/,$(RTL_FILES))

APP_SRC_DIR := $(addprefix $(SRC_ROOT)/,$(APP_MODULES))
APP_SRC	    := $(foreach sdir,$(APP_SRC_DIR),$(wildcard $(sdir)/*.c))
APP_OBJ	    := $(patsubst $(SRC_ROOT)/%.c,$(BUILD_ROOT)/%.o,$(APP_SRC))

vpath %.c $(SRC_DIR)

define make-goal
$(BUILD_ROOT)/$1/%.o: $(SRC_ROOT)/$1/%.c
	@echo $$< $$@
	$(CC) $(CFLAGS) $(INCLUDES) -c $$< -o $$@

$(BUILD_ROOT)/$1/%.o: $(SRC_ROOT)/$1/%.cpp
	@echo $$< $$@
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c $$< -o $$@
endef

define make-test
$1_test: $(BUILD_ROOT)/test/$1_test
	@echo Created $1_test

$1_test_run: $1_test
	@echo ./$(BUILD_ROOT)/test/$1_test >> ./$(BUILD_ROOT)/test/test_stages.txt

$(BUILD_ROOT)/test/$1_test.o: $(SRC_ROOT)/$1.c $(TEST_OBJ)
	$(CC) $(CFLAGS) $(INCLUDES) -DTESTING -c $$< -o $$@

$(BUILD_ROOT)/test/$1_test: $(BUILD_ROOT)/test/$1_test.o $(filter-out $(BUILD_ROOT)/$1.o,$(TEST_OBJ)) test.o
	@echo test_objs: $$^
	@echo build_root: $(BUILD_ROOT)/$1.o
	@echo filter:  $(filter-out $(BUILD_ROOT)/$1.o,$(OBJ))
	@echo inp: $$^ 
	@echo outp: $$@
	$(LD) $$^ -o $$@ 
endef

.PHONY: all checkdirs clean alltests

all: checkdirs proton

checkdirs: $(BUILD_DIR) $(BUILD_ROOT)/test $(BUILD_ROOT)/test/lib

check:
	@echo gen_src: $(GEN_SRC)
	@echo src: $(SRC)
	@echo obj: $(OBJ)
	@echo test-obj: $(BUILD_ROOT)/error.o $(TEST_OBJ)
	@echo vpath: $(VPATH)
	@echo src_dir: $(SRC_DIR)
	@echo main: $(SRC_ROOT)/repl_main.c
	@echo san: $(SANITIZE)
	@echo debug: $(DEBUG)
	@echo cflags: $(CFLAGS)
	@echo ldflags: $(LDFLAGS)
	@echo build dir: $(BUILD_DIR)
	@echo tests: $(TESTS)
	@echo test_targets: $(TEST_TARGETS)
	@echo test_targets_run: $(TEST_TARGETS_RUN)
	@echo test  $(SRC_DIR)/test.c
	@echo RTL: $(RTL_OBJ) $(RTL_SRC)

$(BUILD_DIR) $(BUILD_ROOT)/test $(BUILD_ROOT)/test/lib:
	@mkdir -p $@
	@echo $@

CLEAN_DIRS := $(foreach dir,$(BUILD_DIR),$(subst .,,$(basename $(dir)/)))
clean:
	@find . -name "*.o" -exec rm {} \;
	@echo $(BUILD_DIR)
	@-rm -rf $(CLEAN_DIRS)

$(foreach bdir,$(MODULES) $(APP_MODULES) $(TEST_MODULES) ,$(eval $(call make-goal,$(bdir))))

$(BUILD_ROOT)/%.proton: $(SRC_ROOT)/lib/%.proton
	@echo "Copying RTL File:" $^
	cp $^ $@

rtl_objects: checkdirs $(RTL_OBJ)

$(BUILD_ROOT)/proton: $(OBJ) $(SRC_ROOT)/repl_main.o
	@echo Linking...
	@echo src $^ target $@
	$(LD) $(LDFLAGS) $^ -o $@
	@echo Done.

proton: checkdirs $(BUILD_ROOT)/proton rtl_objects

# Tests.

tests: checkdirs $(TEST_TARGETS)

cleantests:
	@-rm -f ./$(BUILD_ROOT)/test/test_stages.txt
	@-rm -f ./$(BUILD_ROOT)/test/test_runner.sh

runalltests:
	@cp $(SRC_ROOT)/test_runner.sh $(BUILD_ROOT)/test/test_runner.sh
	@<$(BUILD_ROOT)/test/test_stages.txt xargs bash $(BUILD_ROOT)/test/test_runner.sh

runtests: cleantests checkdirs $(TEST_TARGETS_RUN) runalltests

$(BUILD_ROOT)/test/%.o: %.c
	@echo $< $@
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

$(foreach test,$(TESTS),$(eval $(call make-test,$(test))))

$(warning $(RTL_OBJ) $(RTL_SRC) $(RTL_FILES))
