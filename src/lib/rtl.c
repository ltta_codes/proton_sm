#include "rtl.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "cell.h"
#include "error.h"
#include "mm.h"
#include "sym.h"
#include "util.h"

list *stack;

void check_nargs(cell args, long expected) {
  size_t l = len(args);
  if (expected < 0) {
    if (l < -expected) {
      errorf("InvalidArgsError", "Expected at least %lu arguments, got %zu.",
          -expected, l);
    }
  } else if (l != expected) {
    errorf("InvalidArgsError", "Expected exactly %lu arguments, got %zu.",
        expected, l);
  }
}

size_t len(cell c) {
  switch (typeof(c)) {
    case t_list:
      return to_list(c)->len;
    case t_slice:
      return slice_len(to_slice(c));
    case t_nil:
      return 0;
    default:
      return 1;
  }
}

slice *as_slice(cell c) {
  switch (typeof(c)) {
    case t_list:
      return ls_slice(to_list(c), 0, -1);
    case t_slice:
      return to_slice(c);
    default:
      return ls_slice(ls_of(nil, c, undef), 0, 1);
  }
}

cell at(cell c, long i) {
  switch (typeof(c)) {
    case t_list:
      return ls_get(to_list(c), i);
    case t_slice:
      return slice_at(to_slice(c), i);
    default:
      error("TypeError", "Expected list, dict or slice.");
      return undef;
  }
}

list *split(slice *s, slice *separators, bool keep_seps) {
  list *res = ls_mk();
  size_t start = 0;
  for (size_t i = 0; i < slice_len(s); ++i) {
    for (size_t j = 0; j < slice_len(separators); ++j) {
      if (cell_cmp(slice_at(s, i), slice_at(separators, j)) == 0) {
        ls_append(res, cell_slice(
              slice_slice(s, start, i)));
        start = i  + ((keep_seps) ? 0 : 1);
      }
    }
  }
  if (start < slice_len(s)) {
    ls_append(res, cell_slice(slice_slice(s, start, -1)));
  }

  return res;
}

static const char *ty_str(cell_type ty) {
  switch (ty) {
    case t_nil:
      return "nil";
    case t_undef:
      return "undef";
    case t_num:
      return "num";
    case t_sym:
      return "sym";
    case t_str:
      return "str";
    case t_list:
      return "list";
    case t_pair:
      return "pair";
    case t_mem:
      return "mem";
    case t_closure:
      return "closure";
    case t_slice:
      return "slice";
    case t_meta:
      return "meta";
    case t_fn:
      return "function";
  }
}
void typecheck(cell c, cell_type ty) {
  if (typeof(c) != ty) {
    errorf("TypeError", "Expected type \"%s\", got \"%s\".",
        ty_str(ty), ty_str(typeof(c)));
  }
}

static cell cl_mk(env *e, int rargs, size_t largs,
    eval_level lvl, long prec, bool eval_args, bool tup_args) {
  closure *c = new(closure);
  callable *cl = new(callable);
  *cl = (callable) {
    .code_type = ct_src,
    .body = nil,
    .arg_names = NULL,
    .arg_types = &empty_slice,
    .nargs = rargs,
    .largs = largs,
  };
  *c = (closure) {
    .code = cl,
    .fn = NULL,
  };

  return cell_closure(c);
}

static cell mmethod_register(env *e, const char *name, closure *c,
    long prec, bool eval_args, bool tup_args) {
  cell sname = cell_sym(name);
  cell fn = env_lookupn(e, sname);
  if (is_undef(fn)) {
    callable *cl = c->code;
    fn = mmethod_mk(c, cl->nargs, cl->largs, prec, eval_args, tup_args, sname);
    env_set(e, sname, fn);
  } else {
    function *ff = to_fn(fn);
    callable *cl = c->code;
    if (ff->nargs != cl->nargs || ff->largs != cl->largs) {
      error("InvalidMethodError", "Number of left/right arguments of generic "
          "function multi-methods do not match.");
    }
    mmethod_append(ff, c);
  }
  c->fn = to_fn(fn);
  return fn;
}

cell builtin_mk(env *e, const char* name, int rargs, size_t largs,
    extern_fn ext_fn, eval_level lvl, long prec, bool eval_args,
    bool tup_args) {
  closure *c = to_closure(
      cl_mk(e, rargs, largs, lvl, prec, eval_args, tup_args));
  c->code->ext_fn = ext_fn;
  c->code->code_type = ct_native;

  return mmethod_register(e, name, c, prec, eval_args, tup_args);
}

#define builtin_op(name, code, tl, tr, tres)  \
cell name(env *e, cell args) {                \
  check_nargs(args, 3);                       \
  tl left = to_##tl(at(args, 0));             \
  tr right = to_##tr(at(args, 2));            \
  tres res;                                   \
  code;                                       \
  return cell_##tres(res);                    \
}
#define to_any(a) (a)
#define any cell
#define cell_cell(a) (a)

builtin_op(f_mul, res = left * right, num, num, num)
builtin_op(f_add, res = left + right, num, num, num)
builtin_op(f_div, res = left / right, num, num, num)
builtin_op(f_sub, res = left - right, num, num, num)
builtin_op(f_mod, res = left % right, num, num, num)
builtin_op(f_exp, res = pow(left, right), num, num, num)
builtin_op(f_lt, res = cell_cmp(left, right) < 0, any, any, num)
builtin_op(f_lte, res = cell_cmp(left, right) <= 0, any, any, num)
builtin_op(f_gt, res = cell_cmp(left, right) > 0, any, any, num)
builtin_op(f_gte, res = cell_cmp(left, right) >= 0, any, any, num)
builtin_op(f_eq, res = cell_cmp(left, right) == 0, any, any, num)
builtin_op(f_neq, res = cell_cmp(left, right) != 0, any, any, num)

cell f_print(env *e, cell args) {
  size_t llen = len(args) - 1;
  char *p = NULL;
  for (size_t i = 0; i < llen; ++i) {
    p = cell_print(at(args, i + 1), p);
    if (i < llen - 1) {
      p = esprintf(p, " ");
    }
  }
  printf("%s\n", p);
  free(p);
  return nil;
}

// assign(name, value)
cell s_assign(env *e, cell args) {
  check_nargs(args, 3);
  cell name = at(args, 1),
       val = eval_newenv(e, false, at(args, 2));
  typecheck(name, t_sym);
  if (typeof(val) == t_closure) {
    to_closure(val)->fn->assigned_name = name;
    mmethod_register(e, name.sym->s, to_closure(val),
        DEFAULT_PRECEDENCE, true, true); 
  }
  env_set(e, name, val);
  return val;
}

// cond(condition_1, expr_1, [condition_2, expr_2, ..., else_expr])
cell s_cond(env *e, cell args) {
  check_nargs(args, -3);
  slice *sargs = slice_slice(as_slice(args), 1, -1);
  while (slice_len(sargs) > 0) {
    if (slice_len(sargs) == 1) {
      return eval(e, false, slice_at(sargs, 0));
    } else {
      cell cond = eval_newenv(e, false, slice_popf(sargs)),
           code = slice_popf(sargs);
      if (typeof(cond) == t_num && to_num(cond) != 0) {
        return eval(e, false, code);
      }
    }
  }
  return nil;
}

static void if_process_cond_expr(slice *cond_expr, list *cond_args) {
  if (slice_len(cond_expr) < 2) {
    error("SyntaxError", "Expected condition and then-expression.");
    return;
  }
  cell cond = (slice_len(cond_expr) == 2)
    ? slice_at(cond_expr, 0)
    : cell_list(
        ls_setmeta(ls_fromslice(slice_slice(cond_expr, 0, -2)), sym_t_appl));
  cell exp = slice_at(cond_expr, -1);
  if (typeof(exp) == t_list) {
    ls_setmeta(to_list(exp), sym_t_code);
  }
  ls_append(cond_args, cond);
  ls_append(cond_args, exp);
}

cell m_if(env *e, cell args) {
  check_nargs(args, -3);
  list *res = ls_of(sym_t_appl, sym_cond, undef),
       *largs = ls_of(sym_t_tuple, undef);
  ls_append(res, cell_list(largs));
  list *seps = ls_of(nil, sym_else, sym_elif, undef);
  slice *sargs = ls_slice(
      split(slice_slice(as_slice(args), 1, -1), ls_slice(seps, 0, -1), true),
      0, -1);

  while (slice_len(sargs) > 0) {
    if (ls_len(largs) == 0) {
      if_process_cond_expr(as_slice(slice_popf(sargs)), largs);
    } else {
      cell branch_args = slice_popf(sargs);
      if (len(branch_args) < 2) {
        error("SyntaxError", "Invalif if syntax.");
      }
      slice *sbranch = as_slice(branch_args);
      cell branch_ty = slice_at(sbranch, 0);
      sbranch = slice_slice(sbranch, 1, -1);
      if (cell_cmp(branch_ty, sym_elif) == 0) {
        if_process_cond_expr(sbranch, largs);
      } else if (cell_cmp(branch_ty, sym_else) == 0) {
        if (slice_len(sargs) > 1) {
          error("SyntaxError",
              "Expected one expression or block to follow \"else\".");
        }
        cell exp = slice_popf(sbranch);
        if (typeof(exp) == t_list && is_nil(to_list(exp)->meta)) {
          ls_setmeta(to_list(exp), sym_t_code);
        }
        ls_append(largs, exp);
      } else {
        error("SyntaxError", "Expected \"elif\" or \"else\".");
        return undef;
      }
    }
  }
  assert(slice_len(sargs) == 0);

  return cell_list(res);
}

static cell make_assignment(cell name, cell val) {
  return cell_list(ls_of(sym_t_appl,
      cell_sym("assign"),
      cell_list(ls_of(sym_t_tuple, name, val, undef)),
      undef));
}

// let <name> `=` <val>
cell m_let(env *e, cell args) {
  check_nargs(args, -4);
  cell name = at(args, 1),
       assn = at(args, 2),
       val = (len(args) == 4)
           ? at(args, 3)
           : cell_list(ls_setmeta(
               ls_fromslice(slice_slice(as_slice(args), 3, -1)), sym_t_appl));
  typecheck(name, t_sym);
  if (typeof(assn) != t_sym || assn.sym != sym_assign.sym) {
    error("SyntaxError", "Let syntax expected: let <name> = <value>.");
  }
  return make_assignment(name, macro_expand(e, val));
}

static cell check_arg_name(cell arg_name) {
  if (typeof(arg_name) != t_sym) {
    error("SyntaxError", "Invalid argument name");
  }
  return arg_name;
}

static cell check_arg_type(cell arg_type) {
  if (typeof(arg_type) != t_sym) {
    error("TypeError", "Invalid argument type");
  }
  return arg_type;
}

static list *extract_arg_types(slice *args) {
  list *arg_types = ls_mk();
  size_t args_skipped = 0;
  for (size_t i = 0; i < slice_len(args); ++i) {
    cell arg = slice_at(args, i);
    if (typeof(arg) == t_pair) {
      cell arg_type = check_arg_type(to_pair(arg)->snd);
      for (size_t j = 0; j < args_skipped + 1; ++j) {
        ls_append(arg_types, arg_type);
      }
      args_skipped = 0;
    } else if (typeof(arg) == t_sym) {
      ++args_skipped;
    }
  }
  for (size_t i = 0; i < args_skipped; ++i) {
    ls_append(arg_types, sym_t_any);
  }
  return arg_types;
}

static list *extract_arg_names(slice *args) {
  list *arg_names = ls_mk();
  for (size_t i = 0; i < slice_len(args); ++i) {
    cell arg = slice_at(args, i);
    if (typeof(arg) == t_pair) {
      ls_append(arg_names, check_arg_name(to_pair(arg)->fst));
    } else {
      ls_append(arg_names, check_arg_name(arg));
    }
  }
  return arg_names;
}

// fn (args: list) [body: list]
cell m_fn(env *e, cell args) {
  check_nargs(args, 3);
  slice *arg_names = as_slice(at(args, 1));
  cell body = at(args, 2);
  
  closure *res = to_closure(cl_mk(e, slice_len(arg_names), 0, evl_runtime,
      DEFAULT_PRECEDENCE, true, true));
  res->code->body = body;
      
  return cell_closure(res);
}

cell m_defn(env *e, cell args) {
  check_nargs(args, 4);
  cell name = at(args, 1);
  typecheck(name, t_sym);
  cell cl = m_fn(e, cell_slice(slice_slice(as_slice(args), 1, 3)));
  return make_assignment(name, cl);
}

static void add_op(env *e, const char *name, extern_fn fn, int prec) {
  builtin_mk(e, name, 1, 1, fn, evl_runtime, prec, true, true);
}

static void add_fn(env *e, const char *name, extern_fn fn, size_t rargs) {
  builtin_mk(e, name, 0, rargs, fn, evl_runtime, DEFAULT_PRECEDENCE,
      true, true);
}

static void add_keyword(env *e, const char *name, extern_fn fn,
    size_t rargs) {
  builtin_mk(e, name, 0, rargs, fn, evl_macro,
      DEFAULT_PRECEDENCE, true, false);
}

static void add_macro(env *e, const char *name, extern_fn fn,
    size_t rargs) {
  builtin_mk(e, name, 0, rargs, fn, evl_macro,
      DEFAULT_PRECEDENCE, true, true);
}

static void add_special(env *e, const char *name, extern_fn fn,
    size_t rargs) {
  builtin_mk(e, name, 0, rargs, fn, evl_runtime,
      DEFAULT_PRECEDENCE, false, true);
}

void stdlib_init(env *e) {
  stack = ls_mk();

  add_op(e, "<", f_lt, 10);
  add_op(e, "<=", f_lte, 10);
  add_op(e, ">", f_gt, 10);
  add_op(e, ">=", f_gte, 10);
  add_op(e, "==", f_eq, 10);
  add_op(e, "!=", f_neq, 10);

  add_op(e, "+", f_add, 30);
  add_op(e, "-", f_sub, 30);
  add_op(e, "*", f_mul, 40);
  add_op(e, "/", f_div, 40);
  add_op(e, "%", f_mod, 40);
  add_op(e, "^", f_exp, 50);

  add_fn(e, "print", f_print, -1);

  add_keyword(e, "let", m_let, -3);
  add_keyword(e, "if", m_if, -3);
  add_keyword(e, "fn", m_fn, 2);

  add_special(e, "assign", s_assign, 2);
  add_special(e, "cond", s_cond, -3);
}

void stdlib_deinit(env *e) {
  stack = NULL;
}

