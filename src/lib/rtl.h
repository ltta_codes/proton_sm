#pragma once

#include "eval.h"

#define DEFAULT_PRECEDENCE 1000
void check_nargs(cell args, long expected);

size_t len(cell c);
cell at(cell c, long i);
slice *as_slice(cell c);
list *split(slice *s, slice *separators, bool keep_seps);
void typecheck(cell c, cell_type ty);

cell builtin_mk(env *e, const char* name, int rargs, size_t largs,
    extern_fn fn, eval_level lvl, long prec, bool eval_args, bool tup_args);

cell m_fn(env *e, cell args);
cell m_op(env *e, cell args);
cell m_opn(env *e, cell args);

cell s_cond(env *e, cell args);

cell f_mul(env *e, cell args);
cell f_div(env *e, cell args);
cell f_sub(env *e, cell args);
cell f_add(env *e, cell args);
cell f_mod(env *e, cell args);

cell f_eq(env *e, cell args);
cell f_neq(env *e, cell args);

cell f_append(env *e, cell args);
cell f_pop(env *e, cell args);
cell f_len(env *e, cell args);
cell f_at(env *e, cell args);

cell f_print(env *e, cell args);

cell s_assign(env *e, cell args);

cell m_let(env *e, cell args);
cell m_fn(env *e, cell args);

void stdlib_init(env *e);
void stdlib_deinit(env *e);
