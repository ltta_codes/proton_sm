#pragma once

#include "cell.h"

typedef struct {
  size_t line, col;
} pos;

char *read_all(const char *filename);
cell parse(const char *src, const char *fn);
pos getpos(cell c);
