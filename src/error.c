#include "error.h"

#include <assert.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

#include "cell.h"
#include "eval.h"
#include "mm.h"
#include "sym.h"

static list *sig_handlers;
static list *recoveries;

static cell recovery_args;

typedef struct {
  jmp_buf *rec_point;
  cell name;
  cell desc;
} recovery_point;

static list *get_subtable(list *t, cell subsym) {
  assert(tb_contains(t, subsym));
  return to_list(tb_get(t, subsym));
}

static cell mk_appl(cell f, cell args) {
  list *app = ls_mk();
  ls_setmeta(app, sym_t_appl);
  ls_append(app, f);
  ls_append(app, args);
  return cell_list(app);
}

static cell escape_sym(cell sym) {
  return mk_appl(cell_sym("$"), sym);
}

extern env *global_env;
void try_signal_handlers(cell tab, cell signal, cell data) {
  if (!tb_contains(sig_handlers, tab)) {
    return;
  }
  list *t = get_subtable(sig_handlers, tab);
  for (size_t i = ls_len(t); i > 0; --i) {
    cell cl = ls_get(t, i - 1);
    assert(typeof(cl) == t_closure && "");

    list *app_args = ls_mk();
    ls_setmeta(app_args, sym_t_tuple);
    ls_append(app_args, escape_sym(signal));
    ls_append(app_args, data);

    eval(global_env, false, mk_appl(cl, cell_list(app_args)));
  }
}

extern list *stack;
static void print_backtrace() {
  if (stack) {
    size_t frame_no = 1;
    printf("\n Backtrace:\n-------------------------------\n");
    printf(" %2d: main\n", 0);
    for (ls_iter i = ls_iterate(stack); !ls_iter_end(i); i = ls_iter_next(i)) {
      if (typeof(i.val) == t_closure) {
        closure *c = (closure *) i.val.ptr;
        assert(typeof(c->fn->assigned_name) == t_sym);
        printf(" %2zu: %s\n", frame_no++, c->fn->assigned_name.sym->s);
      }
    }
    printf("-------------------------------\n\n");
  }
}

void signal(cell signal, cell data) {
  print_backtrace();
  try_signal_handlers(signal, signal, data);
  try_signal_handlers(cell_sym("AllSignals"), signal, data);
  printf(
      "\n>> FATAL ERROR: No registered signal or fallback handler for \"%s\".\n",
      signal.sym->s);
  exit(1);
}

void errorp(pos p, char *sig, char *msg) {
  printf("\n>> ERROR: %s %s line %zu:%zu\n", sig, msg, p.line, p.col);
  signal(cell_sym(sig), cell_str(msg));
}

void error(char *sig, char *msg) {
  printf("\n>> ERROR: %s\n", msg);
  signal(cell_sym(sig), cell_str(msg));
}

static char *verrorf(char *signal, char *fmt, va_list args) {
  char *buf;
  va_list aargs;
  va_copy(aargs, args);
  int bytes = vsnprintf(NULL, 0, fmt, args);
  va_end(args);
  buf = malloc(bytes + 2);

  bytes = vsnprintf(buf, bytes + 1, fmt, aargs);
  return buf;
}

void errorf(char *signal, char *fmsg, ...) {
  va_list args;
  va_start(args, fmsg);
  char *buf = verrorf(signal, fmsg, args);
  va_end(args);

  error(signal, buf);

  free(buf);
}

void errorpf(pos p, char *signal, char *fmsg, ...) {
  va_list args;
  va_start(args, fmsg);
  char *buf = verrorf(signal, fmsg, args);
  va_end(args);

  errorp(p, signal, buf);

  free(buf);
}


#define NIL
#define check_sig(sig, ret)                     \
  if (typeof(sig) != t_sym) {                        \
    error("TypeError", "Invalid signal type."); \
    return ret;                                 \
  }

#define check_handler(handler)                   \
  if (typeof(handler) != t_closure) {                 \
    error("TypeError", "Invalid handler type."); \
    return;                                      \
  }

static void push_to_subtable(list *t, cell subsym, cell val) {
  if (!tb_contains(t, subsym)) {
    tb_set(t, subsym, cell_list(ls_mk()));
  }
  cell st = tb_get(t, subsym);
  assert(typeof(st) == t_list);
  ls_append(to_list(st), val);
}

void push_signal_handler(cell signal, cell handler) {
  check_sig(signal, NIL);
  check_handler(handler);

  push_to_subtable(sig_handlers, signal, handler);
}

cell pop_signal_handler(cell signal) {
  check_sig(signal, nil);
  return ls_pop(get_subtable(sig_handlers, signal));
}

cell get_signal_handler(cell signal) {
  check_sig(signal, nil);
  list *st = get_subtable(sig_handlers, signal);
  size_t tl = st ? ls_len(st) : 0;
  if (tl == 0) {
    return nil;
  }
  return ls_get(st, tl - 1);
}

void push_recovery(jmp_buf *b, cell name, cell desc) {
  if (!((typeof(desc) == t_nil || typeof(desc) == t_str) && typeof(name) == t_sym)) { 
    error("InvalidArgumentError", "Invalid argument to push_recovery");
    return;
  }

  recovery_point *p = mm_alloc(sizeof(*p));
  *p = (recovery_point) {
    .rec_point = b,
    .name = name,
    .desc = desc,
  };
  cell cp = (cell) { .type = t_undef, .ptr = p };
  push_to_subtable(recoveries, name, cp);
}
 
void pop_recovery(cell signal) {
  check_sig(signal, NIL);
  ls_pop(get_subtable(recoveries, signal));
}

static inline recovery_point *as_recovery(cell c) {
  assert(typeof(c) == t_undef);
  return (recovery_point *) c.ptr;
}

cell list_recoveries() {
  list *res = ls_mk();
  for (ls_iter i = ls_iterate(recoveries);
      !ls_iter_end(i);
      i = ls_iter_next(i)) {
    list *recos = to_list(i.val);
    if (ls_len(recos) == 0) { continue; }
    recovery_point *p = as_recovery(ls_get(recos, -1));
    list *rp = ls_mk();
    ls_setmeta(rp, sym_t_tuple);
    tb_set(rp, sym_name, p->name);
    tb_set(rp, sym_description, p->desc);
    ls_append(res, cell_list(rp));
  }
  return cell_list(res);
}

cell invoke_recovery(cell recovery_name, cell args) {
  cell rp = tb_get(recoveries, recovery_name);
  list *treco = to_list(rp);
  if (ls_len(treco) == 0) {
    error("InvalidArgumentError", "Cannot find named recovery");
  }
  recovery_point *p = as_recovery(ls_get(treco, - 1));
  
  list *aargs = ls_mk();
  ls_append(aargs, recovery_name);
  if (typeof(args) == t_list) {
    ls_extend(aargs, to_list(args));
  } else {
    ls_append(aargs, args);
  }
  recovery_args = cell_list(aargs);
  
  longjmp(*p->rec_point, 1);
}

cell get_recovery_args() {
  return recovery_args;
}

void condsys_init() {
  sig_handlers = ls_mk();
  recoveries = ls_mk();
  recovery_args = nil;
}

void condsys_finalize() { 
  // Clear pointers and let GC free memory.
  sig_handlers = NULL;
  recoveries = NULL;
  recovery_args = nil;
}

void condsys_gc_mark_globals(struct gc_state *gc) {
  cell_gc_mark(gc, recovery_args); 
  if (sig_handlers) {
    cell_gc_mark(gc, cell_list(sig_handlers));
  }

  if (recoveries) {
    for (ls_iter i = ls_iterate(recoveries);
        !ls_iter_end(i);
        i = ls_iter_next(i)) {
      recovery_point *p = as_recovery(i.val);
      cell_gc_mark(gc, p->desc);
      cell_gc_mark(gc, p->name);
      gc_mark(gc, p, sizeof(recovery_point));
    }
  }
}

