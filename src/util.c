#include "util.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
#define POSIX
#endif

#ifdef POSIX
  #include <libgen.h>
  #include <unistd.h>
#else
  #error TODO: Implement non-POSIX dirname.
#endif

char *get_cwd(const char *main_file) {
#ifdef POSIX
  return getcwd(NULL, 0);
#else
# error TODO: Implement non-POSIX dirname.
#endif
}

void set_cwd(const char *path) {
#ifdef POSIX
  chdir(path);
#else
# error TODO: Implement non-POSIX dirname.
#endif
}

void set_cwd_fn(const char *main_file) {
#ifdef POSIX
  char *fn = strdup(main_file);
  const char *path = dirname(fn);
  chdir(path);
  free(fn);
#else
# error TODO: Implement non-POSIX dirname.
#endif
}

char *vesprintf(char *s, const char *fmt, va_list args) {
  va_list argsc;
  va_copy(argsc, args);
  int len = vsnprintf(NULL, 0, fmt, argsc);
  va_end(argsc);

  size_t slen = s == NULL ? 0 : strlen(s);
  s = realloc(s, slen + len + 1);
  vsnprintf(&s[slen], len + 1, fmt, args);
  return s;
}

char *esprintf(char *s, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  char *res = vesprintf(s, fmt, args);
  va_end(args);
  return res;
}

bool strtolong(char *s, long *res) {
  char *e = NULL;
  *res = strtol(s, &e, 10);
  if (e && *e == '\0') {
    return true;
  }
  return false;
}

// GDB/LLDB debugging short symbols.

#include "cell.h"
void _cp(cell c) {
  char *s = cell_print(c, NULL);
  printf("%s\n", s);
  fflush(0);
  free(s);
}

void _lp(list *t) {
  char *s = ls_printd(t, NULL);
  printf("%s\n", s);
  fflush(0);
  free(s);
}

char *slice_printd(slice *t, char *ex);
void _sp(slice *s) {
  char *o = slice_printd(s, NULL);
  printf("%s\n", o);
  fflush(0);
  free(o);
}
