#pragma once

#include "cell.h"

typedef struct app_stack {
  struct app_stack *parent; // Parent in current application.
  struct app_stack *up;     // Up in previous application (recur. parent).
  cell info;                // Additional attached information.
  list *stack;
} app_stack;

typedef struct state {
  app_stack *app_stack;
  eval_level eval_lvl;
} state;

typedef struct env {
  list *e;
  struct env *parent;

  state * const state;
} env;

env *env_mk(env *parent, state *const state);
cell env_lookup(env *e, cell k);
cell env_lookupn(env *e, cell k); // Does not raise error when not found.
cell env_get(env *e, cell k);
void env_set(env *e, cell k, cell v);
cell env_at(env *e, size_t i);
size_t env_len(env *e);
env *env_parent(env *e);
bool env_is_evallvl(env *e, eval_level evll);
env *env_set_evallvl(env *e, eval_level evll);

list *app_stackl(env *e);

app_stack *app_stack_get(env *e);
void app_stack_pushstack(env *e);
void app_stack_popstack(env *e);
bool app_stack_hasparent(app_stack *a);
app_stack *app_stack_parent(app_stack *a);
app_stack *app_stack_new(env *e);
void app_stack_end(env *e);
