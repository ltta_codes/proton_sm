#pragma once

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct {
  const char *s;
  size_t len;
} sym;

typedef struct cell cell;

typedef sym str;
typedef struct list list;
typedef struct pair pair;
typedef struct meta meta;
typedef struct closure closure;
typedef struct function function;
typedef struct callable callable;
typedef struct slice slice;

typedef enum {
  t_nil, t_undef, t_num, t_sym, t_str, t_list, t_pair, t_mem,
  t_closure, t_fn, t_slice, t_meta,
} cell_type;

typedef int num;

struct cell {
  cell_type type;
  union {
    list *list;
    sym *sym;
    str *str;
    num num;
    pair *pair;
    meta *meta;
    void *ptr;
    slice *slice;
    closure *cl;
    function *fn;
  };
};

struct list {
  cell meta;
  size_t len, cap;
  cell *data;
};

struct slice {
  size_t start, len;
  cell *data;
};

struct pair {
  cell fst, snd;
};

struct meta {
  cell meta, content;
};

struct env;
typedef cell (* extern_fn)(struct env *e, cell args);

#define MIN_PRECEDENCE LONG_MIN
typedef enum { evl_parser = 0, evl_macro = 1, evl_runtime = 5, } eval_level;
struct callable {
  enum { ct_native, ct_src } code_type;
  union {
    extern_fn ext_fn;
    cell body;
  };
  slice *arg_names;
  slice *arg_types;
  int nargs;
  size_t largs;
};

struct function {
  list *closures; // dict of closures.

  eval_level eval_lvl;
  long precedence;
  bool eval_rargs;
  bool tup_args;

  int nargs;
  size_t largs;

  cell assigned_name;
};

struct closure {
  callable *code;
  function *fn;

  list *closed_vars;
};

cell_type typeof(cell c);

extern cell nil;
extern cell undef;

list *ls_mk();
list *ls_of(cell meta, ...);
list *ls_copy(list *l);
list *ls_append(list *t, cell c);
cell ls_get(list *t, long i);
cell ls_pop(list *t);
list *ls_fromslice(slice *s);
cell ls_getmeta(list *l);
list *ls_setmeta(list *l, cell meta);
size_t ls_len(list *l);
list *ls_extend(list *l, list *l2);

extern list empty_list;
extern slice empty_slice;

cell *tb_find(list *l, cell k);
bool tb_contains(list *l, cell k);
cell tb_get(list *l, cell k);
void tb_set(list *l, cell k, cell v);

cell cell_sym(const char *s);
cell cell_str(const char *s);
cell cell_strna(const char *s);
cell cell_num(num num);
cell cell_list(list *l);
cell cell_pair(cell fst, cell snd);
cell cell_closure(closure *cl);
cell cell_slice(slice *s);
cell cell_fn(function *f);

bool is_meta(cell c);
cell meta_get(cell c);
cell meta_strip(cell c);
cell meta_set(cell c, cell meta);

long cell_cmp(cell a, cell b);
long ls_cmp(list *a, list *b);

num to_num(cell c);
list *to_list(cell c);
pair *to_pair(cell c);
sym *to_sym(cell c);
str *to_str(cell c);
closure *to_closure(cell c);
slice *to_slice(cell c);
function *to_fn(cell c);

bool sym_eq(cell c1, cell c2);

bool is_nil(cell c);
bool is_undef(cell c);

typedef struct {
  enum { it_key, it_val, it_pair, it_pair_undef, it_tabpair } iter_ty;
  size_t i;
  list *l;
  cell val;
} ls_iter;

ls_iter ls_iterate(list *t);
bool ls_iter_end(ls_iter it);
bool ls_iter_hasnext(ls_iter it);
ls_iter ls_iter_next(ls_iter iter);
char *ls_printd(list *t, char *ex);
slice *ls_slice(list *t, long start, long len);

cell slice_at(slice *s, long i);
slice *slice_slice(slice *s, long start, long end);
list *slice_list(slice *s);
cell slice_popf(slice *s);
size_t slice_len(slice *s);

cell mmethod_mk(closure *f1, int nargs, size_t largs, long precedence,
    bool eval_rargs, bool tup_args, cell assigned_name);
cell mmethod_append(function *f, closure *cl);
closure *mmethod_lookup(function *f, slice *args);

struct gc_state;
void cell_gc_mark(struct gc_state *gs, cell c);
void cell_gc_mark_globals(struct gc_state *gs);

char *cell_print(cell c, char *ex);
