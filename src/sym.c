#include "sym.h"


cell sym_t_appl;
cell sym_t_tuple;
cell sym_t_code;
cell sym_t_any;
cell sym_name;
cell sym_description;
cell sym_app_stack;
cell sym_assign;

cell sym_brk_round_open;
cell sym_brk_round_close;
cell sym_brk_curly_open;
cell sym_brk_curly_close;
cell sym_brk_square_open;
cell sym_brk_square_close;

cell sym_semicolon;
cell sym_comma;

cell sym_if;
cell sym_elif;
cell sym_else;
cell sym_cond;

list *sym_tab;

void symtab_init() {
#define sym(n) sym_##n = cell_sym(#n)
  sym_tab = ls_mk();  

  sym(t_appl);
  sym(t_tuple);
  sym(t_code);
  sym(t_any);
  sym(name);
  sym(description);
  sym_app_stack = cell_sym("$app_stack");
  sym_assign = cell_sym("=");

  sym_brk_round_open = cell_sym("(");
  sym_brk_round_close = cell_sym(")");
  sym_brk_curly_open = cell_sym("{");
  sym_brk_curly_close = cell_sym("}");
  sym_brk_square_open = cell_sym("[");
  sym_brk_square_close = cell_sym("]");
  sym_semicolon = cell_sym(";");
  sym_comma = cell_sym(",");

  sym(if);
  sym(else);
  sym(elif);
  sym(cond);

#undef sym
}

void symtab_finalize() {
  sym_tab = NULL;
}

