#include "env.h"

#include <assert.h>

#include "error.h"
#include "mm.h"
#include "sym.h"

env *env_mk(env *parent, state * const state) {
  env *res = new(env);
  *res = (env) {
    .parent = parent,
    .e = ls_mk(),
    .state = state,
  };
  return res;
}

cell env_lookupn(env *e, cell k) {
  k = meta_strip(k);
  while (e) {
    cell *res = tb_find(e->e, k);
    if (res && !is_undef(*res)) {
      return *res;
    }
    e = e->parent;
  }
  return undef;
}

cell env_lookup(env *e, cell k) {
  cell res = env_lookupn(e, k);
  if (is_undef(res)) {
    char *rep = cell_print(k, NULL);
    errorf("EnvLookupError", "Symbol not found in env \"%s\".", rep);
    free(rep);
  }
  return res;
}

cell env_get(env *e, cell k) {
  cell *res = tb_find(e->e, k);
  if (!res || is_undef(*res)) {
    char *rep = cell_print(k, NULL);
    errorf("EnvLookupError", "Symbol not found in env \"%s\".", rep);
    free(rep);
  }
  return *res;
}

void env_set(env *e, cell k, cell v) {
  ls_append(e->e, cell_pair(k, v));
}

cell env_at(env *e, size_t i) {
  return ls_get(e->e, i);
}

size_t env_len(env *e) {
  return ls_len(e->e);
}

env *env_parent(env *e) {
  return e->parent;
}

bool env_is_evallvl(env *e, eval_level evll) {
  return e->state->eval_lvl;
}

env *env_set_evallvl(env *e, eval_level evll) {
  e->state->eval_lvl = evll;
  return e;
}

list *app_stackl(env *e) {
  assert(e->state);
  return e->state->app_stack->stack;
}

app_stack *app_stack_get(env *e) {
  assert(e->state);
  return e->state->app_stack;
}

void app_stack_pushstack(env *e) {
  app_stack *o_stack = app_stack_get(e),
            *napp_stack = new(app_stack);
  *napp_stack = (app_stack) {
    .parent = o_stack,
    .up = o_stack->up,
    .info = nil,
    .stack = ls_setmeta(ls_mk(), sym_t_appl),
  };
  e->state->app_stack = napp_stack;
}

void app_stack_popstack(env *e) {
  app_stack *stk = app_stack_get(e);
  e->state->app_stack = stk->parent;
}

bool app_stack_hasparent(app_stack *s) {
  return s && s->parent;
}

app_stack *app_stack_parent(app_stack *s) {
  return (s) ? s->parent : NULL;
}

app_stack *app_stack_new(env *e) {
  app_stack *o_stack = app_stack_get(e),
            *napp_stack = new(app_stack);
  *napp_stack = (app_stack) {
    .parent = NULL,
    .up = o_stack,
    .stack = ls_setmeta(ls_mk(), sym_t_appl),
  };
  e->state->app_stack = napp_stack;
  return napp_stack;
}

void app_stack_end(env *e) {
  assert(e->state && e->state->app_stack);
  e->state->app_stack = e->state->app_stack->up;
}
