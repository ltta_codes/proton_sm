#include "cell.h"

#include <assert.h>
#include <stdarg.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "error.h"
#include "mm.h"
#include "sym.h"
#include "util.h"

list empty_list = (list) {
  .meta = (cell) { .type = t_nil, },
  .len = 0,
  .cap = 0,
  .data = NULL,
};
slice empty_slice = (slice) {
  .data = NULL,
  .start = 0,
  .len = 0,
};

cell nil = (cell) { .type = t_nil };
cell undef = (cell) { .type = t_undef };

static inline cell meta_stripi(cell c) {
  while (c.type == t_meta) {
    c = c.meta->content;
  }
  return c;
}

cell_type typeof(cell c) {
  c = meta_stripi(c);
  return c.type;
}

list *ls_mk() {
  list *res = new(list);
  *res = (list) { .len = 0, .cap = 0, .data = NULL };
  return res;
}

list *ls_of(cell meta, ...) {
  va_list args;
  va_start(args, meta);
  list *res = ls_mk();
  res->meta = meta;
  cell arg = va_arg(args, cell);
  while (!is_undef(arg)) {
    ls_append(res, arg);
    arg = va_arg(args, cell);
  }
  va_end(args);
  return res;
}

static void ls_reserve(list *l, size_t cap) {
  if (cap > l->cap) {
    if (l->cap == 0) {
      l->cap = 4;
      cell *rdata = mm_alloc(l->cap * sizeof(cell));
      memcpy(rdata, l->data, l->len * sizeof(cell));
      l->data = rdata;
    } else {
      l->cap = (l->cap + 1) * 2;
      l->data = mm_realloc(l->data, l->cap * sizeof(cell));
    }
  }
}

list *ls_copy(list *l) {
  return ls_setmeta(ls_fromslice(ls_slice(l, 0, -1)), l->meta);
}

list *ls_append(list *t, cell c) {
  ls_reserve(t, t->len + 1);
  t->data[t->len++] = c;
  return t;
}

list *ls_extend(list *l, list *l2) {
  ls_reserve(l, l->len + l2->len);
  memcpy(&(l->data[l->len]), l2->data, sizeof(cell) * l2->len);
  return l;
}

cell ls_get(list *t, long i) {
  if (i < 0) {
    i = t->len + i;
  }
  assert(i < t->len && "Index out of bounds");
  return t->data[i];
}

cell ls_pop(list *t) {
  if (t->len == 0) {
    error("IndexOutOfBoundsError", "Cannot pop element from empty list");
    return nil;
  }
  cell res = t->data[--t->len];
  if (t->len < t->cap / 2) {
    t->cap = t->len + 1;
    t->data = mm_realloc(t->data, t->cap * sizeof(cell));
  }
  return res;
}

slice *ls_slice(list *l, long start, long len) {
  slice s = (slice) { .data = l->data, .start = 0, .len = l->len };
  return slice_slice(&s, start, len);
}

slice *slice_slice(slice *s, long start, long len) {
  if (start < 0) { start = s->len + start + 1; }
  if (len < 0) { len = s->len + len + 1 - start; }
  assert(start >= 0 && start >= 0);
  assert(len <= s->len - start && len >= 0);
  slice *res = new(slice);
  *res = (slice) { .data = s->data, .start = s->start + start, .len = len };
  return res;
}

list *ls_fromslice(slice *s) {
  list *res = new(list);
  size_t llen = s->len;
  *res = (list) {
    .len = llen, 
    .cap = llen,
    .data = mm_alloc(llen * sizeof(cell)),
  };
  memcpy(res->data, &s->data[s->start], llen * sizeof(cell));
  return res;
}

cell ls_getmeta(list *l) { return l->meta; }
list *ls_setmeta(list *l, cell meta) { l->meta = meta; return l; }
size_t ls_len(list *l) { return l->len; }

cell slice_at(slice *s, long i) {
  if (i < 0) { i = s->len + i; }
  assert(i < s->len && i >= 0);
  return s->data[s->start + i];
}

cell slice_popf(slice *s) {
  assert(s->len > 0);
  s->len--;
  return s->data[s->start++];
}

size_t slice_len(slice *s) {
  return s->len;
}

cell mmethod_mk(closure *f1, int nargs, size_t largs, long precedence,
    bool eval_rargs, bool tup_args, cell assigned_name) {
  function *fn = new(function);
  *fn = (function) {
    .closures = ls_mk(),
    .precedence = precedence,
    .eval_rargs = eval_rargs,
    .tup_args = tup_args,
    .assigned_name = assigned_name,
    .nargs = nargs,
    .largs = largs,
  };
  mmethod_append(fn, f1);
  return cell_fn(fn);
}

cell mmethod_append(function *f, closure *cl) {
  ls_append(f->closures,
      cell_pair(cell_slice(cl->code->arg_types), cell_closure(cl)));
  return cell_fn(f);
}

closure *mmethod_lookup(function *f, slice *args) {
  cell *res = tb_find(f->closures, cell_slice(args));
  if (!res) { return NULL; }
  return to_closure(*res);
}

extern list *sym_tab;
static cell *tb_findi(list *l, cell k, bool str_sym_cmp);

cell cell_sym(const char *s) {
  sym sq = (sym) { .s = s, .len = strlen(s) };
  cell sc = (cell) { .type = t_sym, .sym = &sq };
  cell *res = tb_findi(sym_tab, sc, true);
  if (!res || typeof(*res) == t_undef) {
    cell sm = cell_str(s);
    sm.type = t_sym;
    ls_append(sym_tab, cell_pair(sm, sm));
    return sm;
  }
  return *res;
}

cell cell_str(const char *s) {
  return cell_strna(mm_strdup(s));
}

cell cell_strna(const char *s) {
  str *st = new(str);
  *st = (str) { .s = s, .len = strlen(s) };
  return (cell) { .type = t_str, .str = st };
}

cell cell_num(int num) {
  return (cell) { .type = t_num, .num = num };
}

cell cell_list(list *l) {
  return (cell) { .type = t_list, .ptr = l };
}

cell cell_pair(cell fst, cell snd) {
  pair *p = new(pair);
  *p = (pair) { .fst = fst, .snd = snd };
  return (cell) { .type = t_pair, .ptr = p };
}

cell cell_closure(closure *cl) {
  return (cell) { .type = t_closure, .ptr = cl };
}

cell cell_slice(slice *s) {
  return (cell) { .type = t_slice, .slice = s };
}

cell cell_fn(function *f) {
  return (cell) { .type = t_fn, .fn = f };
}

static cell *tb_findi(list *l, cell k, bool str_sym_cmp) {
  for (size_t i = 0; i < l->len; ++i) {
    assert(typeof(l->data[i]) == t_pair && "Expected list of pairs");
    cell v = l->data[i];
    if (typeof(v) != t_pair) {
      error("TypeError", "Invalid type, expected list of pairs");
      return NULL;
    }
    pair *p = to_pair(v); 
    if (str_sym_cmp && (typeof(k) == t_sym && typeof(p->fst) == t_sym)) {
      if (strcmp(p->fst.sym->s, k.sym->s) == 0) {
        return &p->snd;
      } else {
        continue;
      }
    }
    if (cell_cmp(k, p->fst) == 0) {
      return &p->snd;
    }
  }
  return NULL;
}

cell *tb_find(list *l, cell k) {
  return tb_findi(l, k, false);
}

bool tb_contains(list *t, cell k) {
  return tb_find(t, k) != NULL;
}

cell tb_get(list *t, cell k) {
  cell *res = tb_find(t, k);
  if (!res) { error("InvalidKeyError", "Key not in table."); }
  return *res;
}

void tb_set(list *l, cell k, cell v) {
  cell *prev = tb_find(l, k);
  if (prev) {
    *prev = v;
  } else {
    ls_append(l, cell_pair(k, v));
  }
}

static long slice_cmp(slice *a, slice *b) {
  if (a->len != b->len) { return a->len - b->len; }
  for (size_t i = 0; i < a->len; ++i) {
    long c = cell_cmp(a->data[i], b->data[i]);
    if (c != 0) { return c; }
  }
  return 0;
}

long ls_cmp(list *a, list *b) {
  slice sa = (slice) { .start = 0, .len = a->len, .data = a->data  },
        sb = (slice) { .start = 0, .len = b->len, .data = b->data  };
  return slice_cmp(&sa, &sb);
}

long cell_cmp(cell a, cell b) {
  a = meta_stripi(a);
  b = meta_stripi(b);

  if (typeof(a) != typeof(b)) {
    return typeof(a) - typeof(b);
  }
  switch (typeof(a)) {
    case t_nil:
    case t_undef:
      return 0;
    case t_str:
      if (a.str->len != b.str->len) { return a.str->len - b.str->len; }
      return strcmp(a.str->s, b.str->s);
    case t_sym:
      return (ptrdiff_t) a.sym - (ptrdiff_t) b.sym;
    case t_slice:
      return slice_cmp(a.slice, b.slice);
    case t_list:
      return ls_cmp((list *) a.ptr, (list *) b.ptr);
    case t_pair: {
      pair *p1 = (pair *) a.ptr,
           *p2 = (pair *) b.ptr;
      if (cell_cmp(p1->fst, p2->fst) == 0 && cell_cmp(p1->snd, p2->snd) == 0) {
        return 0;
      }
      return -1;
    }
    case t_num:
      return to_num(a) - to_num(b);
    case t_meta:
      return cell_cmp(a.meta->meta, b.meta->meta);
    case t_closure:
      return (ptrdiff_t) a.cl - (ptrdiff_t) b.cl;
    case t_fn:
      return (ptrdiff_t) a.fn - (ptrdiff_t) b.fn;
    case t_mem:
      assert(false && "To implement");
      return -1;
  }
}

static void check_type(cell c, cell_type t) {
  if (typeof(c) != t) {
    error("TypeError", "Invalid type");
  }
}

int to_num(cell c) {
  c = meta_stripi(c);
  check_type(c, t_num);
  return c.num;
}

list *to_list(cell c) {
  c = meta_stripi(c);
  check_type(c, t_list);
  return c.list;
}

pair *to_pair(cell c) {
  c = meta_stripi(c);
  check_type(c, t_pair);
  return c.pair;
}
sym *to_sym(cell c) {
  c = meta_stripi(c);
  check_type(c, t_sym);
  return c.sym;
}

str *to_str(cell c) {
  c = meta_stripi(c);
  check_type(c, t_str);
  return c.str;
}

closure *to_closure(cell c) {
  c = meta_stripi(c);
  check_type(c, t_closure);
  return (closure *) c.ptr;
}

slice *to_slice(cell c) {
  c = meta_stripi(c);
  check_type(c, t_slice);
  return c.slice;
}

function *to_fn(cell c) {
  c = meta_strip(c);
  check_type(c, t_fn);
  return c.fn;
}

cell meta_set(cell c, cell vmeta) {
  if (c.type != t_meta) {
    meta *res = new(meta);
    res->content = c;
    res->meta = vmeta;
    return (cell) { .type = t_meta, .meta = res };
  } else {
    c.meta->meta = vmeta;
  }
  return c;
}

bool is_meta(cell c) { return c.type == t_meta; }
cell meta_get(cell c) { assert(c.type == t_meta); return c.meta->meta; }
cell meta_strip(cell c) { return meta_stripi(c); }

bool is_nil(cell c) { return c.type == t_nil; }
bool is_undef(cell c) { return c.type == t_undef; }

ls_iter ls_iterate(list *l) {
  ls_iter it = (ls_iter) { .l = l, .i = 0, };
  it = ls_iter_next(it);
  return it;
}

ls_iter ls_iter_next(ls_iter iter) {
  if (iter.l && (iter.i < iter.l->len)) {
    iter.val = iter.l->data[iter.i++];
  } else {
    iter.val = undef; 
  }
  return iter;
}

bool ls_iter_end(ls_iter it) {
  return !it.l || is_undef(it.val);
}

bool ls_iter_hasnext(ls_iter it) {
  return it.l && it.i < ls_len(it.l);
}

static char *ls_cellprn(cell c, char *ex) {
  if (typeof(c) == t_pair) {
    ex = cell_print(c.pair->fst, ex);
    ex = esprintf(ex, ": ");
    ex = cell_print(c.pair->snd, ex);
    return ex;
  }
  return cell_print(c, ex);
}

static char *slice_printi(slice *t, char *ex, const char *sep, const char *open,
    const char *close) {
  ex = esprintf(ex, open);
  for (size_t i = 0; i < t->len; ++i) {
    ex = ls_cellprn(t->data[t->start + i], ex);
    if (i < t->len - 1) {
      ex = esprintf(ex, sep);
    }
  }

  return esprintf(ex, close);
}

char *slice_printd(slice *t, char *ex) {
  return slice_printi(t, ex, ", ", "[|", "|]");
}

static char *ls_printi(list *t, char *ex, const char *sep, const char *open,
    const char *close) {
  slice s = (slice) { .start = 0, .len = t->len, .data = t->data };
  return slice_printi(&s, ex, sep, open, close);
}

char *ls_print(list *t, char *ex) {
  if (t->meta.sym == sym_t_appl.sym) {
    return ls_printi(t, ex, " ", "", "");
  } else if (t->meta.sym == sym_t_code.sym) {
    return ls_printi(t, ex, "; ", "{", "}");
  } else if (t->meta.sym == sym_t_tuple.sym) {
    return ls_printi(t, ex, ", ", "(", ")");
  } else {
    return ls_printi(t, ex, ", ", "[", "]");
  }
}

char *ls_printd(list *t, char *ex) {
  if (t->meta.sym == sym_t_appl.sym) {
    return ls_printi(t, ex, " ", "@[", "]@");
  } else if (t->meta.sym == sym_t_code.sym) {
    return ls_printi(t, ex, "; ", "{", "}");
  } else if (t->meta.sym == sym_t_tuple.sym) {
    return ls_printi(t, ex, ", ", "(", ")");
  } else {
    return ls_printi(t, ex, ", ", "[", "]");
  }
}

const char *cell_printt(cell c) {
  char *repr = cell_print(c, NULL);
  char *res = mm_strdup(repr);
  free(repr);
  return res;
}

static char *cell_print_closure(cell c, char *ex, bool print_body) {
  assert(typeof(c) == t_closure);
  closure *cl = c.cl;
  ex = esprintf(ex, "<closure ");
  if (!is_nil(cl->fn->assigned_name)) {
    ex = cell_print(cl->fn->assigned_name, ex);
    ex = esprintf(ex, "(");
  } else {
    ex = esprintf(ex, "%p (", cl);
  }
  if (cl->code->arg_names) { 
    for (size_t i = 0; i < slice_len(cl->code->arg_names); ++i) {
      ex = cell_print(slice_at(cl->code->arg_names, i), ex);
      ex = esprintf(ex, ", ");
    }
  } else {
    ex = esprintf(ex, "%lu/%zu", cl->fn->largs, cl->fn->nargs);
  }
  ex = esprintf(ex, ")");
  if (print_body && cl->code->code_type == ct_src) {
    ex = esprintf(ex, " = ");
    ex = cell_print(cl->code->body, ex);
  }
  return esprintf(ex, ">");
}

static char *cell_print_fn(cell c, char *ex) {
  function *f = to_fn(c);
  ex = esprintf(ex, "<fn {");
  for (size_t i = 0; i < ls_len(f->closures); ++i) {
    pair *mm_entry = to_pair(ls_get(f->closures, i));
    cell types = mm_entry->fst,
         cl = mm_entry->snd;
    ex = esprintf(ex, "$");
    ex = cell_print(types, ex);
    ex = esprintf(ex, "::");
    ex = cell_print_closure(cl, ex, false);
    if (i < ls_len(f->closures) - 1) {
      ex = esprintf(ex, ", ");
    }
  }
  return esprintf(ex, "}>");
}

char *cell_print(cell c, char *ex) {
  c = meta_strip(c);
  switch (typeof(c)) {
    case t_nil:
      return esprintf(ex, "nil");
    case t_undef:
      return esprintf(ex, "<undef>");
    case t_num:
      return esprintf(ex, "%d", c.num);
    case t_str:
      return esprintf(ex, "\"%s\"", c.str->s);
    case t_slice:
      return slice_printd(to_slice(c), ex);
    case t_list:
      return ls_printd(to_list(c), ex);
    case t_sym:
      return esprintf(ex, "%s", c.sym->s);
    case t_closure:
      return cell_print_closure(c, ex, true);
    case t_fn:
      return cell_print_fn(c, ex);
    case t_pair:
      ex = esprintf(ex, "<pair ");
      ex = cell_print(c.pair->fst, ex);
      ex = esprintf(ex, ", ");
      ex = cell_print(c.pair->snd, ex);
      ex = esprintf(ex, ">");
      return ex;
    case t_mem:
      ex = esprintf(ex, "<mem %x>", c.ptr);
      return ex;
    case t_meta:
      ex = esprintf(ex, "<meta ");
      ex = cell_print(c.meta->meta, ex);
      ex = esprintf(ex, ":");
      ex = cell_print(c.meta->content, ex);
      ex = esprintf(ex, ">");
      return ex;
  }
  return ex;
}

// Garbage collection.

static void mark_code(gc_state *gs, callable *c) {
  gc_mark(gs, c, sizeof(callable));

  if (c->arg_names) {
    gc_push_gray(gs, cell_slice(c->arg_types));
    gc_push_gray(gs, cell_slice(c->arg_names));
  }
  switch (c->code_type) {
    case ct_native:
      break;
    case ct_src:
      gc_push_gray(gs, c->body);
      break;
  }
}

static void mark_function(gc_state *gs, function *f) {
  gc_mark(gs, f, sizeof(function));

  for (size_t i = 0; i < ls_len(f->closures); ++i) {
    closure *c = to_closure(ls_get(f->closures, i));
    gc_push_gray(gs, cell_closure(c));
  }
  cell_gc_mark(gs, f->assigned_name);
}

static void mark_closure(gc_state *gs, closure *c) {
  mark_code(gs, c->code);
  mark_function(gs, c->fn);
}

void partial_gc_mark(struct gc_state *gs, cell cp) {
  assert(false && "TODO");
}

void closure_gc_mark(struct gc_state *gs, cell cc) {
  closure *c = to_closure(cc);
  gc_mark(gs, c, sizeof(closure));
  mark_closure(gs, c);
}

void cell_gc_mark_globals(struct gc_state *gs) {
  if (sym_tab) {
    cell_gc_mark(gs, cell_list(sym_tab));
  }
}

void cell_gc_mark(gc_state *gs, cell c) {
  switch (typeof(c)) {
    case t_nil:
    case t_undef:
    case t_num:
      break;
    case t_str:
      gc_mark(gs, c.str->s, c.str->len);
      gc_mark(gs, c.str, sizeof(str));
      break;
    case t_sym:
      gc_mark(gs, c.sym->s, c.sym->len);
      gc_mark(gs, c.sym, sizeof(sym));
      break;
    case t_list: {
      list *t = to_list(c);
      if (t) {
        gc_mark(gs, t, sizeof(list));
        gc_mark(gs, t->data, t->cap * sizeof(cell));
        for (size_t i = 0; i < t->len; ++i) {
          gc_push_gray(gs, t->data[i]);
        }
      }
      break;
    }
    case t_slice: {
      slice *s = to_slice(c);
      if (s) {
        gc_mark(gs, s, sizeof(slice));
        // TODO: this under-counts the actual memory-use by a bit.
        gc_mark(gs, s->data, s->len * sizeof(cell));
        for (size_t i = s->start; i < s->len; ++i) {
          gc_push_gray(gs, s->data[i]);
        }
      }
      break;
    }
    case t_pair:
      gc_mark(gs, c.pair, sizeof(pair));
      gc_push_gray(gs, c.pair->fst);
      gc_push_gray(gs, c.pair->snd);
      break;
    case t_meta:
      gc_mark(gs, c.meta, sizeof(meta));
      gc_push_gray(gs, c.meta->meta);
      gc_push_gray(gs, c.meta->content);
      break;
    case t_closure:
      closure_gc_mark(gs, c);
      break;
    case t_mem:
      assert(false && "TODO");
      break;
    case t_fn:
      mark_function(gs, to_fn(c));
      break;
  }
}

