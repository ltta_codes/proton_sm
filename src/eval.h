#pragma once

#include "cell.h"
#include "env.h"
#include "mm.h"

cell macro_expand(env *e, cell src);

cell eval(env *e, bool sym_self_eval, cell src);
cell eval_newenv(env *e, bool sym_self_eval, cell src);
cell eval_in_env(env *e, bool sym_self_eval, const char *filename);
cell eval_src_in_env(env *e, bool sym_self_eval, char *src,
    char *display_filename);

void eval_gc_mark_globals(gc_state *gs);
