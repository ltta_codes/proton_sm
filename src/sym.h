#pragma once

#include "cell.h"


extern cell sym_t_appl;
extern cell sym_t_tuple;
extern cell sym_t_code;
extern cell sym_t_any;
extern cell sym_name;
extern cell sym_description;
extern cell sym_app_stack;

extern cell sym_assign;

extern cell sym_brk_round_open;
extern cell sym_brk_round_close;
extern cell sym_brk_curly_open;
extern cell sym_brk_curly_close;
extern cell sym_brk_square_open;
extern cell sym_brk_square_close;
extern cell sym_semicolon;
extern cell sym_comma;

extern cell sym_if;
extern cell sym_elif;
extern cell sym_else;
extern cell sym_cond;

void symtab_init();
void symtab_finalize();
