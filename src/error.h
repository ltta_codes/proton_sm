#pragma once

#include <stdbool.h>
#include <setjmp.h>

#include "cell.h"
#include "parser.h"

void errorp(pos p, char *signal, char *msg);
void error(char *signal, char *msg);
void errorf(char *signal, char *fmsg, ...);
void errorpf(pos p, char *signal, char *fmsg, ...);

void signal(cell signal, cell data);

void push_signal_handler(cell signal, cell handler);
cell pop_signal_handler(cell signal);
cell get_signal_handler(cell signal);

void push_recovery(jmp_buf *b, cell name, cell desc);
void pop_recovery(cell signal);
cell list_recoveries();
cell invoke_recovery(cell recovery_name, cell args);
cell get_recovery_args();

void condsys_init();
void condsys_finalize();

// Garbage collection.
struct gc_state;
void condsys_gc_mark_globals(struct gc_state *gs);

// Predefined errors.
void invalid_args_error(size_t nargs_exp, size_t nargs_got);

