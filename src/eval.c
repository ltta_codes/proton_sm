#include "eval.h"

#include <assert.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>

#include "error.h"
#include "mm.h"
#include "parser.h"
#include "sym.h"
#include "util.h"

extern list *stack;

static long eval_apps(env *e, bool sym_self_eval, slice *app, 
    long n, long prec);

static inline size_t closure_nargs(closure *cl, size_t max_args) {
  size_t nargs = cl->code->largs > 0 ? cl->code->largs + 1 : 0;
  nargs += (cl->code->nargs < 0)
    ? max_args - cl->code->largs
    : cl->code->nargs;
  return nargs;
}

static void arg_error(pos p, size_t exp, size_t got, bool is_left) {
  errorpf(p, "InvaligArgumentError",
      "Insufficient %s args, excted %zu, got %zu.",
      (is_left) ? "left" : "right", exp, got);
}

static bool is_application(list *l) {
  return typeof(l->meta) == t_sym && l->meta.sym == sym_t_appl.sym;
}

static cell eval_list(env *e, bool sym_self_eval, list *l);

static slice *closure_get_args(env *e, bool sym_self_eval, closure *cl,
    slice *app, cell app_node) {
  assert(e->state);
  if (ls_len(app_stackl(e)) < cl->code->largs) {
    app_stack *stk = app_stack_get(e),
              *top_stk = stk;
    while (stk->parent) {
      stk = stk->parent;
      while (ls_len(top_stk->stack) < cl->code->largs
          && ls_len(stk->stack) > 0) {
        ls_append(top_stk->stack, ls_pop(stk->stack));
      }
    }
    if (ls_len(top_stk->stack) < cl->code->largs) {
      errorp(getpos(app_node), "ApplicationError",
          "Insufficient number of left args.");
      return 0;
    }
  }
  ls_append(app_stackl(e), cell_closure(cl));
  
  size_t rargs = 0; 
  if (cl->code->nargs != 0) {
    if (slice_len(app) == 0) {
      arg_error(getpos(app_node), cl->code->nargs, slice_len(app), false);
    }
    cell next = slice_at(app, 0);
    if (typeof(next) == t_list && cl->fn->tup_args
        && !is_application(to_list(next))) {
      cell lsargs = (cl->fn->eval_rargs)
          ? eval_list(e, sym_self_eval, to_list(next))
          : next;
      if (typeof(lsargs) == t_list) {
        list *l = to_list(lsargs);
        for (size_t i = 0; i < l->len; ++i) {
          ls_append(app_stackl(e), ls_get(l, i));
        }
        rargs = ls_len(l);
      } else {
        ls_append(app_stackl(e), lsargs);
        rargs = 1;
      }
      ++app->start;
      --app->len;
    } else {
      rargs = (cl->code->nargs < 0) ? slice_len(app) : cl->code->nargs;
      if (cl->fn->eval_rargs) {
        rargs = eval_apps(e, sym_self_eval, app, cl->code->nargs,
            cl->fn->precedence);
      } else {
        for (size_t i = 0; i < rargs; ++i) {
          cell arg = slice_at(app, i);
          ls_append(app_stackl(e), arg);
        }
        app->start += rargs;
        app->len -= rargs;
      }
    }
  }

  size_t actual_nargs = rargs + cl->code->largs + 1;
  if (app_stackl(e)->len < actual_nargs) {
    errorf("InvalidArgumentError", "Insufficient arguments provided, "
        "expected %zu, got %zu.", actual_nargs, app_stackl(e)->len);
  }
  return ls_slice(app_stackl(e), app_stackl(e)->len - actual_nargs,
      actual_nargs);
}

static cell closure_exec(env *e, bool sym_self_eval, closure *cl,
    slice *args, cell app_node) {
  ls_append(stack, cl->fn->assigned_name);

  size_t nargs = closure_nargs(cl, args->len);
  cell res = nil;

  switch (cl->code->code_type) {
    case ct_src: {
      env *ne = env_mk(e, e->state);

      assert(cl->code->arg_names->len == nargs
          || (cl->code->nargs < 0 && -nargs == cl->code->arg_names->len - 1));

      if (args->len != nargs + 1
          || (cl->code->nargs < 0 && args->len < nargs)) {
        errorp(getpos(app_node), "InvalidArgumentError",
            "Insuffienct arguments provided.");
        goto ret;
      }

      for (size_t i = 0; i < nargs; ++i) {
        env_set(ne, slice_at(cl->code->arg_names, i), slice_at(args, i + 1));
      }
      if (cl->code->nargs < 0) {
        env_set(ne, slice_at(cl->code->arg_names, -1),
            cell_slice(slice_slice(args, nargs, args->len)));
      }
      res = eval(ne, sym_self_eval, cl->code->body);
      break;
    }
    case ct_native:
      res = (* cl->code->ext_fn)(e, cell_slice(args));
      break;
  }
ret:
  ls_pop(stack);
  return res;
}

static void closure_exec_app(env *e, bool sym_self_eval, closure *cl,
    slice *app, cell app_node) {
  slice *largs = closure_get_args(e, sym_self_eval, cl, app, app_node);
  app_stackl(e)->len -= largs->len;

  cell res = closure_exec(e, sym_self_eval, cl, largs, app_node);

  if (!is_undef(res)) {
    ls_append(app_stackl(e), res);
  }
}

static bool is_callable(cell c) {
  return typeof(c) == t_closure || typeof(c) == t_fn;
}

static long eval_apps(env *e, bool sym_self_eval, slice *app, 
    long n, long prec) {
  assert(e->state);
  long app_n = n >= 0 ? n : app->len,
       args_n = ls_len(app_stackl(e));

  while (ls_len(app_stackl(e)) - args_n < app_n && slice_len(app) > 0) {
    cell next_val = slice_popf(app),
         next = eval(e, sym_self_eval, next_val);
    if (!is_callable(next)
        || to_closure(next)->fn->eval_lvl > e->state->eval_lvl) {
      ls_append(app_stackl(e), next);
    } else {
      closure *cl = NULL;
      if (typeof(next) == t_fn) {
        cl = fn_get_closure();
      } else if (typeof(next) == t_closure) {
        cl = to_closure(next);
      } else { assert(false && "Unhandled callable type."); }

      if (cl->fn->precedence < prec) { break; }
      
      closure_exec_app(e, sym_self_eval, cl, app, next);
    }
  }

  // Check if the next value is possibly an "operator" that would
  // left-associate with last element on app stack and thus evaluate.
  while (true) {
    cell next_val = (slice_len(app) > 0) ? slice_at(app, 0) : nil,
         next = eval(e, sym_self_eval, next_val);
    if (is_callable(next)) {
      closure *cl = to_closure(next);
      if (cl->fn->precedence > prec && cl->code->largs > 0
          && cl->fn->eval_lvl <= e->state->eval_lvl) {
        slice_popf(app);
        closure_exec_app(e, sym_self_eval, cl, app, next);
        continue;
      }
    }
    break;
  }

  return ls_len(app_stackl(e)) - args_n;
}

static cell eval_application(env *e, bool sym_self_eval, slice *app) {
  assert(e->state);

  app_stack_new(e);
  while (slice_len(app) > 0) {
    eval_apps(e, sym_self_eval, app, -1, MIN_PRECEDENCE);
  }

  app_stack *stack = app_stack_get(e);
  while (stack->parent) {
    list *parent = stack->parent->stack;
    if (ls_len(stack->stack) > 0) {
      ls_append(parent, cell_list(stack->stack));
    }
    stack = stack->parent;
  }

  e->state->app_stack = stack;
  cell res = nil;
  list *l_stack = stack->stack;
  switch (l_stack->len) {
    case 0: res = nil; break;
    case 1: res = l_stack->data[0]; break;
    default: res = cell_list(ls_copy(l_stack)); break;
  }
  app_stack_end(e);
  return res;
}

static cell eval_slice(env *e, bool sym_self_eval, slice *s) {
  list *res = ls_mk();
  res->meta = nil;
  for (size_t i = s->start; i < s->len; ++i) {
    ls_append(res, eval(e, sym_self_eval, s->data[i]));
  }
  return cell_list(res);
}

static cell eval_list(env *e, bool sym_self_eval, list *l) {
 if (typeof(l->meta) == t_sym) {
    if (l->meta.sym == sym_t_appl.sym) {
      return eval_application(e, sym_self_eval, ls_slice(l, 0, -1));
    } else if (l->meta.sym == sym_t_code.sym
        && e->state->eval_lvl == evl_runtime) {
      cell res = nil;
      for (size_t i = 0; i < l->len; ++i) {
        res = eval(e, sym_self_eval, l->data[i]);
      }
      return res;
    }
  }

  list *res = ls_mk();
  res->meta = l->meta;
  for (size_t i = 0; i < l->len; ++i) {
    ls_append(res, eval(e, sym_self_eval, l->data[i]));
  }
  return cell_list(res);
}

cell eval(env *e, bool sym_self_eval, cell src) {
  switch (typeof(src)) {
    case t_nil:
    case t_num:
    case t_str:
    case t_closure:
    case t_fn:
      return src;
    case t_sym: 
      if (sym_self_eval) {
        cell res = env_lookupn(e, src);
        return (is_undef(res) ? src : res);
      } else {
        return env_lookup(e, src);
      }
    case t_pair:
      return cell_pair(
          eval(e, sym_self_eval, to_pair(src)->fst),
          eval(e, sym_self_eval, to_pair(src)->snd));
    case t_slice:
      return eval_slice(e, sym_self_eval, to_slice(src));
    case t_list:
      return eval_list(e, sym_self_eval, to_list(src));
    case t_meta:
      return eval(e, sym_self_eval, src.meta->content);
    case t_mem:
      assert(false && "TODO");
      break;
    case t_undef:
      errorp(getpos(src), "EvalError", "Cannot evaluate undefined.");
  }
  return undef;
}

cell macro_expand(env *e, cell src) {
  e->state->eval_lvl = evl_macro;
  return eval(e, true, src);
}

cell eval_newenv(env *e, bool sym_self_eval, cell src) {
  env *ne = env_mk(e, e->state);
  env_set_evallvl(ne, evl_runtime);
  return eval(ne, sym_self_eval, src);
}

cell eval_in_env(env *e, bool sym_self_eval, const char *filename) {
  char *cwd = get_cwd();

  // Read file, then set cwd so that relative imports work.
  char *src = read_all(filename);
  set_cwd_fn(filename);

  cell ast = parse(src, filename);
  ast = macro_expand(e, ast);
  free(src);
  char *asts = cell_print(ast, NULL);
  printf("AST:\n\n%s\n\n", asts);
  free(asts);

  env_set_evallvl(e, evl_runtime);
  cell res = eval(e, sym_self_eval, ast);

  // Reset cwd.
  set_cwd(cwd);
  free(cwd);

  return res;
}

cell eval_src_in_env(env *e, bool sym_self_eval, char *src,
    char *display_filename) {
  cell ast = parse(src, display_filename);

  // Macro expand.
  ast = macro_expand(e, ast);
  char *asts = cell_print(ast, NULL);
  printf("AST:\n\n%s\n\n", asts);
  free(asts);

  env_set_evallvl(e, evl_runtime);
  return eval(e, sym_self_eval, ast);
}

void eval_gc_mark_globals(gc_state *gs) {
  gc_push_gray(gs, cell_list(stack));
}
