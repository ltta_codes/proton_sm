#pragma once

#include <stdarg.h>
#include <stdbool.h>

char *get_cwd();
void set_cwd_fn(const char *from_filename);
void set_cwd(const char *path);
bool strtolong(char *s, long *res);

char *vesprintf(char *s, const char *fmt, va_list args);
char *esprintf(char *s, const char *fmt, ...);
