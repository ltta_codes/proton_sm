#include "mm.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cell.h"
#include "error.h"
#include "eval.h"

typedef enum { mc_white, mc_black } gc_mark_color;
struct gc_state {
  void **ptrs;
  size_t allocs;
  size_t heap_sz;
  size_t deallocs;
  size_t n;
  size_t cap;
  gc_mark_color curr_white;

  struct {
    cell *cells;
    size_t n;
    size_t cap;
  } grays;

  size_t gc_growth_percent;
  size_t gc_next_collection;
};

typedef struct {
  gc_mark_color mark_bit: 1;
  size_t ptr_idx;
} gc_obj;

gc_state *gc;

static inline gc_mark_color curr_black(gc_state *gs) {
  return gs->curr_white == mc_white ? mc_black : mc_white;
}

static inline bool is_black(gc_state *gs, gc_mark_color c) {
  return c != gs->curr_white;
}

static size_t gc_put_ptr(gc_state *gs, void *ptr) {
  if (gs->n + 1 >= gs->cap) {
    gs->cap = gs->cap * 1.2 + 2;
    gs->ptrs = realloc(gs->ptrs, gs->cap * sizeof(void *));
    if (!gs->ptrs) {
      error("MemoryError", "Cannot reallocate GC table.");
    }
  }
  gs->ptrs[gs->n++] = ptr;
  return gs->n - 1;
}

void *mm_realloc(void *p, size_t bytes) {
  if (bytes == 0) { mm_free(p); return NULL; }

  if (p) { p = (char *) p - sizeof(gc_obj); }
  gc_obj *obj = realloc(p, bytes + sizeof(gc_obj));
  obj->mark_bit = gc->curr_white;
  if (p) {
    gc->ptrs[obj->ptr_idx] = obj;
  } else {
    obj->ptr_idx = gc_put_ptr(gc, obj);
  }
  ++gc->allocs;
  gc->heap_sz += bytes;
  return ((char *) obj) + sizeof(gc_obj);
}

void *mm_alloc(size_t bytes) {
  void *res = mm_realloc(NULL, bytes);
  memset(res, 0, bytes);
  return res;
}

void mm_free(void *ptr) {
  if (ptr) {
    gc_obj *obj = (gc_obj *) (((char *) ptr) - sizeof(gc_obj));
    gc->ptrs[obj->ptr_idx] = NULL;
    free(obj);
  }
}

char *mm_strdup(const char *s) {
  size_t len = strlen(s);
  char *sts = mm_alloc(len + 1);
  strncpy(sts, s, len + 1);
  return sts;
}

void gc_init() {
  gc = malloc(sizeof(gc_state));
  *gc = (gc_state) {
    .ptrs = NULL,
    .n = 0,
    .cap = 0,
    .curr_white = mc_white,

    .grays = {
      .cells = NULL,
      .n = 0,
      .cap = 0,
    },
  };
}

void gc_finalize() {
  for (size_t i = 0; i < gc->n; ++i) {
    free(gc->ptrs[i]);
  }
  free(gc->ptrs);
  free(gc->grays.cells);
  free(gc);
}

bool gc_need_collection(gc_state *gs) {
  return gs->heap_sz > GC_MIN_HEAP &&
      (gs->heap_sz > gs->gc_next_collection || gs->deallocs > 2 * gs->allocs);
}

void gc_startcollection(gc_state *gs) {
  gs->curr_white = curr_black(gs);
  gs->gc_next_collection =
      gs->heap_sz + (gs->heap_sz * gs->gc_growth_percent) / 100;
  gs->heap_sz = 0;
}

void gc_push_gray(gc_state *gs, cell c) {
  if (gs->grays.n + 1 >= gs->grays.cap) {
    gs->grays.cap = gs->grays.cap * 3 / 2 + 2;
    gs->grays.cells = realloc(gs->grays.cells, gs->grays.cap * sizeof(cell));
  }
  gs->grays.cells[gs->grays.n++] = c;
}

void gc_mark(gc_state *gs, const void *mem, size_t sz_bytes) {
  if (mem) {
    gc_obj *obj = (gc_obj *) (((char *) mem) - sizeof(gc_obj));
    obj->mark_bit = curr_black(gs);
    gs->heap_sz += sz_bytes;
  }
}

void gc_sweep(gc_state *gs) {
  for (size_t i = 0; i < gs->n; ++i) {
    gc_obj *o = gs->ptrs[i];
    if (!o || !is_black(gs, o->mark_bit)) {
      if (o) { free(o); }
      // Inline compact the free list by pulling a last pointer forward.
      // OPT: maybe compacting in one go front-back/back-front is more
      // cache-friendly and faster?
      gc_obj *next = NULL;
      gs->ptrs[i] = NULL;
      while (!next && gs->n > 1) { next = gs->ptrs[gs->n-- - 1]; }
      gs->ptrs[i] = next;
      if (next) {
        ((gc_obj *) next)->ptr_idx = i; 
        --i;
      }
      ++gs->deallocs;
    }
  }
}

static inline size_t maxz(size_t a, size_t b) { return a < b ? b : a; }
static bool is_ptr_cell(cell c) {
  switch (typeof(c)) {
    case t_list:
    case t_slice:
    case t_sym:
    case t_str:
    case t_closure:
    case t_pair:
    case t_meta:
    case t_mem:
      return true;
    default:
      return false;
  }
}

void gc_collect(gc_state *gs, env *curr_env) {
  gc_startcollection(gs);

  cell_gc_mark_globals(gs);
  condsys_gc_mark_globals(gs);
  eval_gc_mark_globals(gs);

  env *e = curr_env;
  if (e && e->state) {
    app_stack *stk = app_stack_get(e);
    while (stk) {
      gc_mark(gs, stk, sizeof(app_stack));
      gc_push_gray(gs, cell_list(stk->stack));
      if (!is_ptr_cell(stk->info)) {
        gc_push_gray(gs, stk->info);
      }
      stk = stk->parent;
    }
  }
  while (e) {
    for (size_t i = 0; i < env_len(e); ++i) {
      gc_push_gray(gs, env_at(e, i));
    }
  }

  while (gs->grays.n > 0) {
    cell c = gs->grays.cells[--gs->grays.n];
    cell_gc_mark(gs, c);
  }

  gc_sweep(gs);

  assert(gs->deallocs <= gs->allocs);
  gs->allocs -= gs->deallocs;
  size_t gray_expected = maxz(gs->allocs / 3, 20);
  if (gray_expected < gs->grays.n) {
    gs->grays.cells = realloc(gs->grays.cells, gray_expected);
  }
}

