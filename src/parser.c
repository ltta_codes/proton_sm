#include "parser.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cell.h"
#include "env.h"
#include "eval.h"
#include "error.h"
#include "mm.h"
#include "sym.h"
#include "util.h"
#include "lib/rtl.h"

typedef struct {
  char *s;
  size_t line, col;
} token;

typedef struct {
  const char *src;
  const char *pos;

  size_t line, col;
  const char *fn;

  token tok;
} parser;

char delimiters[256] = {
  ['('] = true,
  [')'] = true,
  ['['] = true,
  [']'] = true,
  ['{'] = true,
  ['}'] = true,
  [','] = true,
  [';'] = true,
  [':'] = true,
  [' '] = true,
  ['\t'] = true,
  ['\r'] = true,
  ['\n'] = true,
  ['\0'] = true,
};

static void nextc(parser *p) {
  if (*p->pos == '\r' || *p->pos == '\n') {
    p->col = 0;
    ++p->line;
  } else {
    ++p->col;
  }
  ++p->pos;
}

pos getpos(cell c) {
  if (is_meta(c)) {
    cell meta = meta_get(c);
    if (typeof(meta) == t_num) {
      union { unsigned int u; int i; } su_int;
      su_int.i = to_num(meta);
      return (pos) {
        .line = (su_int.u >> 16) & ((1 << 16) - 1),
        .col = su_int.u & ((1 << 16) - 1),
      };
    }
  }
  return (pos) { .line = 0, .col = 0 };
}

static cell setpos(cell c, pos p) {
  union { unsigned int u; int i; } su_int;
  su_int.u = ((p.line << 16) & (((1 << 16) - 1) << 16))
           | (p.col & ((1 << 16) - 1));
  return meta_set(c, cell_num(su_int.i));
}

static cell tok_next(parser *p) {
  while (*p->pos == ' ' || *p->pos == '\t') { nextc(p); }
  const char *s = p->pos;

  pos position = (pos) { .line = p->line, .col = p->col };

  if (*p->pos == '\r' || *p->pos == '\n') {
    while (*p->pos && (*p->pos == '\r' || *p->pos == '\n')) { nextc(p); }
    return setpos(cell_sym(";"), position);
  }

  if (*p->pos == '"') {
    nextc(p);
    while (*p->pos && *p->pos != '"') { nextc(p); }
    if (*p->pos != '"') {
      position = (pos) { .line = p->line, .col = p->col };
      errorp(position, "ParseError", "Non-terminated string.");
    }
    char *ts = strndup(s + 1, p->pos - s - 1);
    ++p->pos;
    cell res = cell_str(ts);
    free(ts);
    return setpos(res, position);
  }

  if (*p->pos >= '0' && *p->pos <= '9') {
    while (*p->pos >= '0' && *p->pos <= '9') { nextc(p); }
    char *ns = strndup(s, p->pos - s);
    long lres = 0;
    bool is_num = strtolong(ns, &lres);
    free(ns);
    if (is_num) { return setpos(cell_num(lres), position); }
  }

  while (*p->pos && !delimiters[(unsigned char) *p->pos]) { nextc(p); }
  if (s == p->pos) { ++p->pos; }
  char *tok_s = strndup(s, p->pos - s);
  cell res = cell_sym(tok_s);
  free(tok_s);
  return setpos(res, position);
}

static cell parse_code(parser *p) {
  list *res = ls_mk();
  ls_setmeta(res, sym_t_appl);
  while (*p->pos) {
    cell t = tok_next(p);
    ls_append(res, t); 
  }
  return cell_list(res);
}


static env *parser_env();
extern env *global_env;

static cell parse_source(parser *p) {
  cell read_src = parse_code(p);
  env *saved_global_env = global_env;
  global_env = parser_env();
  cell parsed_src = eval(global_env, true, read_src);
  global_env = saved_global_env;

  return parsed_src;
}

cell parse(const char *src, const char *fn) {
  parser *p = malloc(sizeof(parser));
  *p = (parser) {
    .tok = (token) { .s = NULL },
    .line = 0,
    .col = 0,
    .pos = src,
    .src = src,
    .fn = fn,
  };
  cell res = parse_source(p);
  free(p);
  return res;
}

char *read_all(const char *filename) {
  char *res;
  long len;
  FILE *f = fopen(filename, "r");
  if (f) {
    fseek(f, 0, SEEK_END);
    len = ftell(f);
    rewind(f);
    res = malloc((len + 1) * (sizeof(char)));
    fread(res, sizeof(char), len, f);
    fclose(f);
    res[len] = '\0';
    return res;
  } else {
    error("IOError", "Cannot open file");
    return NULL;
  }
}

static cell parser_semicolon(env *e, cell args) {
  check_nargs(args, 1);
  app_stack *app_stk = app_stack_get(e);
  list *stk_appl = ls_setmeta(ls_copy(app_stk->stack), sym_t_appl);
  app_stk->stack->len = 0;

  app_stack *parent = app_stack_parent(app_stk);
  if (parent && parent->stack->meta.sym == sym_t_code.sym) {
    ls_append(parent->stack, cell_list(stk_appl));
  } else {
    ls_append(app_stk->stack, cell_list(stk_appl));
    ls_setmeta(app_stk->stack, sym_t_code);
    app_stack_pushstack(e);
    app_stack_get(e)->info = sym_semicolon;
  }
  return undef;
}

static cell parser_comma(env *e, cell args) {
  check_nargs(args, 1);
  app_stack *app_stk = app_stack_get(e);
  cell elem = nil;
  if (ls_len(app_stk->stack) > 1) {
    elem = cell_list(ls_setmeta(ls_copy(app_stk->stack), sym_t_appl));
  } else if (ls_len(app_stk->stack) == 1) {
    elem = ls_get(app_stk->stack, 0);
  }
  app_stk->stack->len = 0;

  app_stack *parent = app_stack_parent(app_stk);
  if (parent && is_nil(parent->stack->meta)) {
    ls_append(parent->stack, elem); 
  } else {
    ls_append(app_stk->stack, elem); 
    ls_setmeta(app_stk->stack, nil);
    app_stack_pushstack(e);
    app_stack_get(e)->info = sym_comma;
  }
  return undef;
}

static cell parser_colon(env *e, cell args) {
  check_nargs(args, -3);
  cell key = at(args, 0),
       val = (len(args) == 3)
           ? at(args, 2)
           : cell_list(ls_fromslice(slice_slice(as_slice(args), 2, -1)));
  return cell_pair(key, val);
}

static cell parser_bracket_open(env *e, cell marker_sym) {
  app_stack_pushstack(e);
  app_stack_get(e)->info = marker_sym;
  return undef;
}

static cell parser_bracket_close(env *e, cell marker_sym, const char *ch) {
  list *l = app_stackl(e);
  cell info = app_stack_get(e)->info;
  if (info.sym == sym_semicolon.sym || info.sym == sym_comma.sym) {
    if (ls_len(app_stackl(e)) > 0) {
      assert(app_stack_get(e)->parent && app_stack_get(e)->parent->stack);
      ls_append(app_stack_get(e)->parent->stack, cell_list(app_stackl(e)));
    }
    app_stack_popstack(e);
    l = app_stackl(e);
  }
  if (!l || app_stack_get(e)->info.sym != marker_sym.sym) {
    errorf("SyntaxError", "Unmatched closing bracket \"%s\".", ch);
  }
  app_stack_popstack(e);
  list *pl = app_stackl(e);
  ls_append(pl, cell_list(l));
  return undef;
}

static cell parser_bracket_round_open(env *e, cell args) {
  return parser_bracket_open(e, sym_brk_round_open);
}
static cell parser_bracket_square_open(env *e, cell args) {
  return parser_bracket_open(e, sym_brk_square_open);
}
static cell parser_bracket_curly_open(env *e, cell args) {
  return parser_bracket_open(e, sym_brk_curly_open);
}

static cell parser_bracket_round_close(env *e, cell args) {
  return parser_bracket_close(e, sym_brk_round_open, ")");
}
static cell parser_bracket_square_close(env *e, cell args) {
  return parser_bracket_close(e, sym_brk_square_open, ")");
}
static cell parser_bracket_curly_close(env *e, cell args) {
  return parser_bracket_close(e, sym_brk_curly_open, ")");
}

static cell parser_builtin_mk(env *e, const char *name, extern_fn fn,
    long prec) {
  return builtin_mk(e, name, 0, 0, fn, evl_parser, prec, false, false);
}

static env *parser_env() {
  state *st = new(state);
  app_stack *stk = new(app_stack);
  *stk = (app_stack) { .parent = NULL, .stack = ls_mk() };
  *st = (state) { .app_stack = stk };
  env *e = env_mk(NULL, st);
  parser_builtin_mk(e, ";", parser_semicolon, -30);
  parser_builtin_mk(e, ",", parser_comma, -20);
  builtin_mk(e, ":", 1, -1, parser_colon, evl_parser, -10, false, false);

  parser_builtin_mk(e, "(", parser_bracket_round_open, -30);
  parser_builtin_mk(e, ")", parser_bracket_round_close, -30);
  parser_builtin_mk(e, "[", parser_bracket_square_open, -30);
  parser_builtin_mk(e, "]", parser_bracket_square_close, -30);
  parser_builtin_mk(e, "{", parser_bracket_curly_open, -30);
  parser_builtin_mk(e, "}", parser_bracket_curly_close, -30);
  return e;
}

