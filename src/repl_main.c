#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cell.h"
#include "error.h"
#include "eval.h"
#include "linenoise.h"
#include "mm.h"
#include "parser.h"
#include "rtl.h"
#include "sym.h"
#include "util.h"

env *global_env;

#define HISTORY_FILE "~/.proto_history"

static char *line = NULL;

bool load_rtl(size_t argc, char **argv) {
  for (size_t i = 2; i < argc; ++i) {
    if (strcmp(argv[i], "--nortl") == 0) {
      return false;
    }
  }
  return true;
}

int main(int argc, char **argv) {
  gc_init();
  state *st = new(state);
  *st = (state) { .app_stack = NULL };
  global_env = env_mk(NULL, st);
  symtab_init();
  condsys_init();

  char *cwd = get_cwd();

  // Exit interpreter recovery.
  jmp_buf recovery_exit;
  if (setjmp(recovery_exit) == 0) {
    push_recovery(&recovery_exit, cell_sym("Exit"),
        cell_str("Exit the interpreter."));

    stdlib_init(global_env);

    if (load_rtl(argc, argv)) {
      eval_in_env(global_env, false, "build/rtl.proton");
    }

    // Reload main source recovery.
    jmp_buf recovery_restart;
    if (setjmp(recovery_restart) == 0) {
      push_recovery(&recovery_restart, cell_sym("ReloadSource"),
          cell_str("Reload main source."));
    } else {
      set_cwd(cwd);
      printf("Reloading Source Code \"%s\"...", argv[1]);
    }

    if (argc >= 2) {
      cell res = eval_in_env(global_env, false, argv[1]);
      char *sres = cell_print(res, NULL);
      printf("\nRES: %s\n", sres);
      free(sres);
    } else {

      linenoiseHistoryLoad(HISTORY_FILE);

      jmp_buf recovery_ignore_error;
      if (setjmp(recovery_ignore_error) == 0) {
        push_recovery(&recovery_ignore_error, cell_sym("Ignore"),
            cell_str("Ignore last input"));
      } else {
        if (line) { free(line); }
      }

      while((line = linenoise("p+ > ")) != NULL) {
        if (line[0] != '\0' && line[0] != '/') {
            cell res = eval_src_in_env(global_env, false, line, "<repl>");
            char *sres = cell_print(res, NULL);
            printf(":: %s\n", sres);
            free(sres);

            linenoiseHistoryAdd(line); /* Add to the history. */
            linenoiseHistorySave(HISTORY_FILE); /* Save the history on disk. */
        } 
        free(line);
        line = NULL;
      }
      return 0;
    }

  } else {
    if (line) { free(line); }
  }

  if (cwd) { free(cwd); }

  condsys_finalize();
  gc_collect(gc, NULL);
  symtab_finalize();
  gc_collect(gc, NULL);
  gc_finalize();

  return 0;
}
